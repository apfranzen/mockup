/** @jsx jsx */
import React from 'react';
import { TextInput } from '../../components/common/formInputs/TextInput';
import { buildName, buildValue } from './dependencyHelpers';
import { ValDepMode, LocalList, LocaleList } from '../_validations';
import { FieldArray, getIn, ErrorMessage, Field } from 'formik';
import { SelectionInput } from '../../components/common/formInputs/SelectionInput';
import { TextArea } from '../../components/common/formInputs/TextArea';
import {
  Icon,
  initializeIcons,
  Text,
  Separator,
  Button,
  IconButton,
  CompoundButton,
} from 'office-ui-fabric-react/';
import { css, jsx } from '@emotion/core';
import Checkbox from '../../components/common/formInputs/Checkbox';
export const ConfigApplyMethod = props => {
  const { mode, values, errors } = props;
  return (
    <div>
      <span>
        <Checkbox
          name="validState"
          label="If checked values matching the (pattern, values, range) above satisfy the condition."
        />

        <br />
        <Checkbox
          name="nullIsValid"
          label="If checked blank values are included in the list of values that satisfy the condition."
        />

        <br />
        {props.valDepMode === ValDepMode.validation ? (
          <Checkbox name="strong" label="Cannot be overridden" />
        ) : null}

        {props.valDepMode === ValDepMode.validation && (
          <FieldArray
            name="customFailureMessages"
            render={arrayHelpers => (
              <div>
                <div
                  style={{
                    marginLeft: '10px',
                    display: 'grid',
                    gridGap: '6px',
                    gridTemplateColumns: '16px 40% 40% 16px',
                    padding: 6,
                    border: '1px solid red',
                  }}
                >
                  <Text
                    style={{ gridColumn: 2, justifySelf: 'center' }}
                    variant="medium"
                  >
                    Message*:
                  </Text>
                  <Text
                    style={{ gridColumn: 3, justifySelf: 'center' }}
                    variant="medium"
                  >
                    Locale*:
                  </Text>

                  {values.customFailureMessages &&
                    values.customFailureMessages.map((option, index) => (
                      <>
                        <p style={{ gridColumn: 1, alignSelf: 'center' }}>
                          {index + 1}
                        </p>
                        <Field
                          css={props => css`
                            padding: 0.25rem;
                            grid-column: 2;
                            height: 1.5rem;
                            border: 1px solid #ccc;
                            border-color: ${getIn(
                              props.errors,
                              `customFailureMessages.[${index}].message`
                            )
                              ? 'red'
                              : ''};
                            &:focus {
                              border-color: #007eff;
                              box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
                                0 0 0 3px rgba(0, 126, 255, 0.1);
                              outline: none;
                            }
                          `}
                          name={`customFailureMessages[${index}].message`}
                          // label="Message"
                          error={getIn(
                            errors,
                            `customFailureMessages.[${index}].message`
                          )}
                        />
                        <Field
                          css={props => css`
                            padding: 0.25rem;
                            grid-column: 3;
                            height: 1.5rem;
                            border: 1px solid #ccc;
                            border-color: ${getIn(
                              errors,
                              `customFailureMessages.[${index}].locale`
                            )
                              ? 'red'
                              : ''};
                            &:focus {
                              border-color: #007eff;
                              box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
                                0 0 0 3px rgba(0, 126, 255, 0.1);
                              outline: none;
                            }
                          `}
                          component={SelectionInput}
                          name={`customFailureMessages[${index}].locale`}
                          // label="Locale"
                          error={getIn(
                            errors,
                            `customFailureMessages.[${index}].locale`
                          )}
                          options={Object.keys(LocaleList).map(key => ({
                            label: LocaleList[key],
                            value: key,
                          }))}
                        />

                        <Icon
                          style={{ gridColumn: 4, alignSelf: 'center' }}
                          iconName={'Remove'}
                          onClick={() => {
                            arrayHelpers.remove(index);
                          }}
                          title="Remove"
                        />
                        <ErrorMessage
                          render={msg => (
                            <Text
                              style={{ gridColumn: 2, color: 'red' }}
                              variant="small"
                            >
                              {msg}
                            </Text>
                          )}
                          name={`customFailureMessages[${index}].label`}
                        />
                        <ErrorMessage
                          render={msg => (
                            <Text
                              style={{ gridColumn: 3, color: 'red' }}
                              variant="small"
                            >
                              {msg}
                            </Text>
                          )}
                          name={`customFailureMessages[${index}].value`}
                        />
                      </>
                    ))}

                  <Button
                    style={{
                      gridColumn: '1 / 5',
                      width: '100%',
                    }}
                    text="Add"
                    iconProps={{ iconName: 'Add' }}
                    onClick={() => arrayHelpers.push({ label: '', value: '' })}
                  />
                </div>

                {/* <i
                    style={{ color: 'green' }}
                    className="far fa-plus-circle fa"
                    onClick={() => arrayHelpers.push({ value: '', label: '' })}
                    title="Remove"
                  /> */}
              </div>
            )}
          />
        )}
        {/* <Field
          name="failureMessage.message"
          label="Message"
          // options={Object.keys(LocaleList).map(key => ({
          component={TextArea}
          //   label: LocaleList[key],
          //   value: key,
          // }))}
        />
        <Field
          name="failureMessage.locale"
          label="Locale"
          component={SelectionInput}
          options={Object.keys(LocaleList).map(key => ({
            label: LocaleList[key],
            value: key,
          }))}
        />*/}
      </span>
    </div>
  );
};

export default ConfigApplyMethod;

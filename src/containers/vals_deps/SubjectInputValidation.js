import React, { useContext } from 'react';
import ConfigApplyMethod from './ConfigApplyMethod';
import DisplayHeading from '../../components/common/DisplayHeading';
import { AsyncSelect } from '../../lib/hooks';
import { HrefContext } from '../../components/LayoutRoot';

export const SubjectInputValidation = props => {
  const { studyId } = useContext(HrefContext);

  const { setMetaData } = props;
  return (
    <div>
      <DisplayHeading legend="Subject Input Validation Script" />
      {props.TargetId}
      <AsyncSelect
        trigger={true}
        url={`/studies/${studyId}/scripts/`}
        name="value"
        value={props.values.value}
        label="Select a Script"
        handleChange={(e, data) => {
          props.formikProps.handleChange(e);
          setMetaData({ ...data.filter(item => item.id === e.target.value) });
        }}
        resolver={function(data) {
          return data.map(script => ({
            value: script.study.id,
            label: script.study.title,
          }));
        }}
      />

      <div>
        <div>
          <div>
            <ConfigApplyMethod
              {...props}
              handleStateSet={props.handleStateSet}
              handleSubmit={props.handleSubmit}
              handleAdd={props.handleAdd}
              allowSubmit={props.allowSubmit}
              loadExistingValidator={props.loadExistingValidator}
              handleUpdate={props.handleUpdate}
              validState={props.validState}
              strong={props.strong}
              nullIsValid={props.nullIsValid}
              failureMode={props.failureMode}
              mode={props.mode}
            />
          </div>
        </div>
      </div>
      <div id="validators">
        <ul />
      </div>
      <div>
        <label>
          <span>*</span>
          Indicates a required field
        </label>
      </div>
    </div>
  );
};

export default SubjectInputValidation;

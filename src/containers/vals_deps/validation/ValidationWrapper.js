import React from 'react';
import { connect } from 'react-redux';
import {
  updateDependencyCondition,
  addCondition,
  deleteCondition,
} from '../../../redux-modules/actions';
import _ from 'lodash';
import ConditionDisplay from '../ConditionDisplay/ConditionDisplay';
import { ConditionConfig } from '../ConditionConfig';
import { helpers } from '../../../lib/helpers';
import { ValDepMode } from '../../_validations';
import { Text } from 'office-ui-fabric-react';

class ValidationWrapper extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isAdding: null,
      currentInput: null,
      activeConditionId: null,
    };
    this.addConditionHandler = this.addConditionHandler.bind(this);
    this.selectCondition = this.selectCondition.bind(this);
  }

  selectCondition(id, event) {
    console.log({ id });

    event.stopPropagation();
    if (id !== '0') {
      // dissallow selecting root element(id: 0)
      this.setState({
        activeConditionId: this.state.activeConditionId !== id ? id : null,
      });
    }
  }

  addConditionHandler(id) {
    console.log({ id });

    this.setState(prevState => ({
      isAdding: prevState.isAdding === id ? false : id,
      activeConditionId: id,
    }));
  }

  render() {
    const form = this.props.form;

    const currentSelect = () => {
      if (this.state.activeConditionId) {
        const arrCondition = this.state.activeConditionId.split('.');
        return arrCondition.length === 1 && arrCondition[0] === '0'
          ? { type: 'CompositeCondition', conditions: [], operator: 'AND' }
          : this.props.model.validators[arrCondition[1]];
      } else {
        return null;
      }
    };

    const targetId = this.props.activeEntityUUID;

    const priorIds = helpers.priorIds(form, targetId);
    return (
      <div>
        <Text
          variant="xLarge"
          // style={{ color: 'rgb(0, 120, 212)' }}
        >
          This value is valid if:
        </Text>
        <div
          style={{
            margin: '20px',
            padding: '4px',
            minHeight: '60px',
            width: '80%',
          }}
        >
          <ConditionDisplay
            selectCondition={this.selectCondition}
            activeConditionId={this.state.activeConditionId}
            conditions={this.props.model.validators}
            validator
            addConditionHandler={this.addConditionHandler}
            isAdding={this.state.isAdding}
          />
        </div>
        {this.state.activeConditionId ? (
          <ConditionConfig
            currentSelect={currentSelect()}
            form={form}
            isAdding={this.state.isAdding}
            priorIds={priorIds}
            activeEntityUUID={this.props.activeEntityUUID}
            updateDependencyCondition={this.props.updateDependencyCondition}
            addConditionHandler={this.addConditionHandler}
            addCondition={this.props.addCondition}
            deleteCondition={this.props.deleteCondition}
            targetId={this.state.targetId}
            activeConditionId={this.state.activeConditionId}
            selectCondition={this.selectCondition}
            validator
            valDepMode={ValDepMode.validation}
          />
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const model = _.get(state.form, state.app.activeEntityUUID);

  return {
    model,
    form: state.form,
    autoId: state.form.autoId,
    activeEntityUUID: state.app.activeEntityUUID,
    previousSiblings: [],
  };
};

export default connect(
  mapStateToProps,
  { updateDependencyCondition, addCondition, deleteCondition }
  // { replace }
)(ValidationWrapper);

import React, { useState } from 'react';
import { Formik, Form } from 'formik';
import { address } from '../../lib/address';
import { ConditionConfigRenderMode } from '../_validations';
import { utility } from '../../lib/utility';
import { AddCondition } from './dependency/AddCondition';
import { ValDepMode } from '../_validations';
import * as Yup from 'yup';
import { DisplayState } from '../../components/PropertyPanel/EntityProperty/formikHelpers';
import { TextInput } from '../../components/common/formInputs/TextInput';
import { Text } from 'office-ui-fabric-react';

export const Sources = {
  local: 'local',
  remote: 'remote',
};

const handleDisable = (values, id, touched) => {
  const touchedResult = Object.keys(utility.flattenObject(touched)).includes(
    id
  );

  const value = values[id];
  return touchedResult && value !== '';
};

const sanitizeSuccessors = (
  e,
  values,
  setFieldValue,
  setFieldTouched,
  touched
) => {
  return;
  // if (!values) return;

  // const {
  //   target: { id },
  // } = e;

  // const index = formSeries[values.source].indexOf(id.split('.')[1]);
  // const idsFollowing = formSeries[values.source].slice(
  //   index + 1,
  //   formSeries[values.source].length
  // );
  // const flattenedTouched = utility.flattenObject(touched);
  // const touchedInputs = Object.keys(flattenedTouched).filter(
  //   input => flattenedTouched[input]
  // );
  // console.log({ idsFollowing, touchedInputs });

  // const followingTouched = idsFollowing.filter(input =>
  //   touchedInputs.includes(input)
  // );
  // console.log({ followingTouched });

  // // if there are values for these fields, reset them
  // followingTouched.forEach(input => {
  //   console.log({ input });
  //   setFieldTouched(`draft.${input}`, false);
  //   setFieldValue(`draft.${input}`, '');
  // });
};

/**
 *
 * @param {string} source local or remote
 * @param {string} identifyingKey ex. 4.1
 * @param {Object} form entire form
 * @param {*} inputs
 */

export const ConditionConfig = props => {
  const { priorIds } = props;
  const [metaData, setMetaData] = useState({});
  const schema = Yup.object().shape({
    value: Yup.string().min(2, 'Too Short!'),
    testMe: Yup.string().min(2, 'too short'),
  });
  console.log('ConditionConfig');
  return (
    <div>
      {// props.valDepMode === ValDepMode.validation ||
      !props.isAdding ? (
        <Formik
          // validateOnBlur
          enableReinitialize={true}
          initialValues={{
            ...props.currentSelect,
          }}
          validationSchema={schema}
          onSubmit={(values, { setSubmitting }) => {
            props.updateDependencyCondition(
              props.activeEntityUUID,
              props.activeConditionId,
              values,
              props.valDepMode
            );
          }}
        >
          {formikProps => {
            const { values, dirty, isSubmitting, handleSubmit } = formikProps;

            const deleteHandler = e => {
              e.preventDefault();
              props.selectCondition(null, e);

              props.deleteCondition(
                props.activeEntityUUID,
                props.activeConditionId,
                props.valDepMode
              );
            };

            return (
              <Form onSubmit={handleSubmit}>
                <Text variant="medium">Update Condition</Text>
                {/* <DisplayState {...formikProps.touched} /> */}
                {/* <DisplayState {...metaData} />
                <DisplayState {..._.omit(formikProps.values, 'conditions')} /> */}
                {/* <DisplayState {...options.inputsInForm} /> */}
                {React.createElement(address.mapExpression(values), {
                  values,
                  form: props.form,
                  currentSelect: props.currentSelect,
                  formikProps: formikProps,
                  activeConditionId: props.activeConditionId,
                  activeEntityUUID: props.activeEntityUUID,
                  priorIds,
                  selectCondition: props.selectCondition,
                  mode: ConditionConfigRenderMode.edit,
                  validator: props.validator,
                  errors: formikProps.errors,
                  metaData,
                  valDepMode: props.valDepMode,
                })}
                <button style={{ color: 'red' }} onClick={deleteHandler}>
                  Delete This Condition
                </button>

                <br />

                <button type="submit" disabled={isSubmitting || !dirty}>
                  Submit
                </button>
              </Form>
            );
          }}
        </Formik>
      ) : null}
      <hr />

      {props.isAdding && (
        // <p>Add condition</p>
        <AddCondition
          isAdding={props.isAdding}
          activeEntityUUID={props.activeEntityUUID}
          activeConditionId={props.activeConditionId}
          handleDisable={handleDisable}
          sanitizeSuccessors={sanitizeSuccessors}
          priorIds={priorIds}
          metaData={metaData}
          setMetaData={setMetaData}
          form={props.form}
          addCondition={props.addCondition}
          addConditionHandler={props.addConditionHandler}
          valDepMode={props.valDepMode}
        />
      )}
    </div>
  );
};

import React, { useState, useContext } from 'react';
import { AsyncSelect, useDataApi } from '../../lib/hooks';
import { EntityTypes } from '../../model/Types';
import { EligibleDependents } from '../_validations';
import { DisplayState } from '../../components/PropertyPanel/EntityProperty/formikHelpers';
import { Field } from 'formik';
import { SelectionInput } from '../../components/common/formInputs/SelectionInput';
import { TextInput } from '../../components/common/formInputs/TextInput';
import { apiUrl } from '../../redux-modules/actions';
import { HrefContext } from '../../components/LayoutRoot';

export const RemoteConfig = props => {
  const { studyId } = useContext(HrefContext);

  const {
    formikProps: { values, touched, handleChange, errors, setFieldValue },
    handleDisable,
    setMetaData,
  } = props;
  const [eventDefs, setEventDefs] = useState([]);

  return (
    <>
      <DisplayState {...values} />
      <AsyncSelect
        trigger={values.remote}
        url={`${apiUrl}/studies/${studyId}/forms`}
        name="_formsInStudy"
        id="_formsInStudy"
        label="Select Form"
        disabled={handleDisable(values, '_formsInStudy', touched)}
        onChange={e => {
          /** This is somewhat of a hacky way to handle this, but this was the most declarative way to solve this problem at the time. Notes:
           * targetId: this is a temporary value, that is never serialized. Simply used as a string interpolation to pupulate the other values below
           */
          setFieldValue('_formsInStudy', e.target.value.split(','));
          setFieldValue('formId', e.target.value.split(',')[0]);
          setFieldValue('form-name', e.target.value.split(',')[1]);
        }}
        resolver={function (data) {
          console.log('data2: ', data);

          return data.map(item => ({
            value: `${item.id},${item.name}`,
            label: item.name,
          }));
        }}
      />
      <AsyncSelect
        trigger={values.formId}
        url={`${apiUrl}/studies/${studyId}/crf/${values.formId}/versions`}
        name="_versionId"
        label="Form Version"
        handleChange={handleChange}
        disabled={handleDisable(values, '_versionId', touched)}
        onChange={e => {
          const test =
            /** This is somewhat of a hacky way to handle this, but this was the most declarative way to solve this problem at the time. Notes:
             * targetId: this is a temporary value, that is never serialized. Simply used as a string interpolation to pupulate the other values below
             */
            setFieldValue('_versionId', e.target.value);
          setFieldValue('versionId', e.target.value.split(',')[1]);
          setFieldValue('version', e.target.value.split(',')[0]); // versionDescription from CDART response
        }}
        resolver={function (data) {
          return data.map(item => ({
            value: `${item.versionDescription},${item.id}`,
            label: item.majorVersion,
          }));
        }}
      />

      <AsyncSelect
        url={`${apiUrl}/studies/${studyId}/inputs/${values.versionId}`}
        trigger={values.versionId}
        name="targetId"
        label="Inputs"
        disabled={handleDisable(values, 'targetId', touched)}
        onChange={e => {
          /** This is somewhat of a hacky way to handle this, but this was the most declarative way to solve this problem at the time. Notes:
           * targetId: this is a temporary value, that is never serialized. Simply used as a string interpolation to pupulate the other values below
           */
          setFieldValue('targetId', e.target.value);
          setFieldValue('internalId', e.target.value.split(',')[0]);
          setFieldValue('entityType', e.target.value.split(',')[1]);
          setFieldValue(
            'inputType',
            e.target.value.split(',')[2].toLowerCase()
          );
        }}
        resolver={function (data) {
          const filteredResponse = data.filter(input =>
            EligibleDependents.includes(
              EntityTypes[
              input.entityType
                .split('.')
                .pop()
                .replace('CDS', '')
              ]
            )
          );

          return filteredResponse.map(response => {
            return {
              label: `${response.externalId} -  ${response.entityType
                .split('.')
                .pop()
                .replace('CDS', '')}`,
              value: `${response.internalId},${response.entityType},${response.inputType}`,
            };
          });
        }}
      />

      <AsyncSelect
        url={`${apiUrl}/studies/${studyId}/eventDefs`}
        trigger={values.targetId}
        name="eventDef"
        label="Event Definition"
        resolver={function (data) {
          return data ? data.map(eventDef => eventDef) : [];
        }}
      />

      <Field
        name="occurrence"
        options={[
          { label: 'First', value: '-1' },
          { label: 'Other', value: '-99' },
        ]}
        label="Occurrence"
        component={SelectionInput}
      />

      <Field
        name="occurrenceNumber"
        type="number"
        label="Occurrence #"
        component={TextInput}
        disabled={!values.occurrence || values.occurrence === '-1'}
      />
    </>
  );
};

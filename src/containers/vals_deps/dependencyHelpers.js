import RangeValidator from '../../model/validation/model.RangeValidator';
import SubjectInputValidator from '../../model/validation/model.SubjectInputValidator';
import PatternValidator from '../../model/validation/model.PatternValidator';
import NoOpValidator from '../../model/validation/model.NoOpValidator';
import EnumerationValidator from '../../model/validation/model.EnumerationValidator';
import DependencyExpression from '../../model/validation/model.DependencyExpression';

export const generateCondition = properties => {
  const types = {
    RangeValidator: new RangeValidator(properties),
    SubjectInputValidator: new SubjectInputValidator(properties),
    PatternValidator: new PatternValidator(properties),
    NoOpValidator: new NoOpValidator(properties),
    EnumerationValidator: new EnumerationValidator(properties),
    DependencyExpression: new DependencyExpression(properties),
  };
  console.log(properties, properties.hasOwnProperty('class'));

  return properties.class
    ? types[properties.class]
    : types['DependencyExpression'];
};

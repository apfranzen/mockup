import React from 'react';

import AppliedValidator from '../ConditionDisplay/AppliedValidator';
import DependencyExpression from '../ConditionDisplay/DependencyExpression';

export const conditions = {
  // DefaultValidator: AppliedValidator,
  EnumerationValidator: AppliedValidator,
  PatternValidator: AppliedValidator,
  SubjectInputValidator: AppliedValidator,
  NoOpValidator: AppliedValidator,
  RangeValidator: AppliedValidator,
  DependencyExpression: DependencyExpression,
};

export const Condition = props => {
  console.log(props.model.type, props.model.type in conditions);
  const Component = props.model.class
    ? conditions[props.model.class]
    : conditions['DependencyExpression'];
  return <Component {...props} />;
};

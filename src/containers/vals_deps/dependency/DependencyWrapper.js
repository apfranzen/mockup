import React from 'react';
import { ValDepMode } from '../../_validations';
import { connect } from 'react-redux';
import {
  updateDependencyCondition,
  addCondition,
  deleteCondition,
} from '../../../redux-modules/actions';
import _ from 'lodash';
import ConditionDisplay from '../ConditionDisplay/ConditionDisplay';
import { ConditionConfig } from '../ConditionConfig';
import { helpers } from '../../../lib/helpers';
import { Text } from 'office-ui-fabric-react';

class DependencyWrapper extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      currentInput: null,
      activeConditionId: null,
      isAdding: null,
      // activeConditionId: '0.6.1',
    };

    this.selectCondition = this.selectCondition.bind(this);
    this.addConditionHandler = this.addConditionHandler.bind(this);
  }

  addConditionHandler(id) {
    this.setState(prevState => ({
      isAdding: prevState.isAdding === id ? false : id,
      activeConditionId: id,
    }));
  }

  selectCondition(id, event) {
    event.stopPropagation();
    console.log({ id, event });
    // don't allow root activeCondition to be set, because the operator cannot be changed

    if (id !== '0') {
      // dissallow selecting root element(id: 0)
      this.setState({
        activeConditionId: this.state.activeConditionId !== id ? id : null,
        isAdding: null,
      });
    }
  }
  render() {
    const form = this.props.form;

    const currentSelect = () => {
      if (!this.state.activeConditionId) {
        return null;
      } else if (String(this.state.activeConditionId) === '0') {
        return { type: 'CompositeCondition' };
      } else {
        return helpers.traverseDependencies(this.props.model.dependencies, [
          ...this.state.activeConditionId
            .split('.')
            .slice(1, this.state.activeConditionId.split('.').length),
        ]);
      }
    };

    return (
      <div>
        <Text
          variant="xLarge"
          // style={{ color: 'rgb(0, 120, 212)' }}
        >
          Enable this input if the following is true:
        </Text>
        <div>
          <ConditionDisplay
            addConditionHandler={this.addConditionHandler}
            isAdding={this.state.isAdding}
            selectCondition={this.selectCondition}
            activeConditionId={this.state.activeConditionId}
            conditions={
              this.props.model.dependencies.conditions.map(
                condition =>
                  console.log(condition) || {
                    ...condition,
                    ...(condition.remote
                      ? {}
                      : {
                          externalIdentifier: this.props.entityInternalMapToExternalId.filter(
                            entity => condition.internalId === entity.internalId
                          )[0].externalIdentifier,
                        }),
                  }
              ) || []
            }
          />
        </div>
        {this.state.activeConditionId || this.state.isAdding ? (
          <ConditionConfig
            isAdding={this.state.isAdding}
            currentSelect={currentSelect()}
            form={form}
            // priorIds={[]}
            priorIds={helpers.priorIds(
              this.props.form,
              this.props.activeEntityUUID
            )}
            activeEntityUUID={this.props.activeEntityUUID}
            updateDependencyCondition={this.props.updateDependencyCondition}
            addCondition={this.props.addCondition}
            addConditionHandler={this.addConditionHandler}
            deleteCondition={this.props.deleteCondition}
            targetId={this.state.targetId}
            activeConditionId={this.state.activeConditionId}
            valDepMode={ValDepMode.dependency}
            selectCondition={this.selectCondition}
            // key={this.state.activeConditionId}
          />
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  const model = _.get(state.form, state.app.activeEntityUUID);
  const entityInternalMapToExternalId = Object.values(state.form).map(
    entity => ({
      internalId: entity.internalId,
      externalIdentifier: entity.externalIdentifier,
    })
  );

  return {
    model,
    form: state.form,
    autoId: state.form.autoId,
    activeEntityUUID: state.app.activeEntityUUID,
    previousSiblings: [],
    entityInternalMapToExternalId,
  };
};

export default connect(
  mapStateToProps,
  { updateDependencyCondition, addCondition, deleteCondition }
  // { replace }
)(DependencyWrapper);

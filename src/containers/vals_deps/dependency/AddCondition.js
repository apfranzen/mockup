import React from 'react';
import { Formik, Form, Field } from 'formik';

import { SelectionInput } from '../../../components/common/formInputs/SelectionInput';
import { address } from '../../../lib/address';
import { DisplayState } from '../../../components/PropertyPanel/EntityProperty/formikHelpers';
import * as Yup from 'yup';

import { ValDepMode, ConditionConfigRenderMode } from '../../_validations';
import DisplayHeading from '../../../components/common/DisplayHeading';
import { Icon } from 'office-ui-fabric-react';
const defaultFormState = (
  currentSelect,
  valDepMode,
  activeEntityUUID,
  activeEntity
) => {
  console.log({ valDepMode });

  const config = {
    remote: valDepMode === ValDepMode.validation ? false : '',
    type: valDepMode === ValDepMode.validation ? 'AppliedValidator' : '',
    formsInStudy: '',
    version: '',
    targetId: valDepMode === ValDepMode.validation ? activeEntityUUID : '',
    targetIdType: valDepMode === ValDepMode.validation ? activeEntity.type : '',
    class: '',
    value: '',
    validState: true,
    nullIsValid: true,
    strong: false,
    occurrence: '',
    occurrenceNumber: '',
  };
  console.log({ ...currentSelect, ...config });
  return { ...currentSelect, ...config };
};

export const AddCondition = props => {
  const {
    handleDisable,
    sanitizeSuccessors,
    priorIds,
    metaData,
    setMetaData,
  } = props;
  const schema = Yup.object().shape({
    // value: Yup.string()
    //   .min(2, 'Too Short!')
    //   .required('Required'),
    testMe: Yup.string().min(2, 'too short'),
  });
  return (
    <>
      <Formik
        // validateOnBlur
        validationSchema={schema}
        enableReinitialize={true}
        initialValues={defaultFormState(
          props.currentSelect,
          props.valDepMode,
          props.activeEntityUUID,
          props.form[props.activeEntityUUID]
        )}
        onSubmit={(values, { resetForm }) => {
          const { activeConditionId, activeEntityUUID } = props;

          const valDepMode = props.valDepMode;
          console.log({ values });

          props.addCondition(
            activeEntityUUID,
            activeConditionId,
            values,
            valDepMode
          );
          props.addConditionHandler(null);
          resetForm(
            defaultFormState(
              props.currentSelect,
              props.valDepMode,
              props.activeEntityUUID,
              props.form[props.activeEntityUUID]
            )
          );
        }}
      >
        {formikProps => {
          const {
            values,
            touched,
            dirty,
            isSubmitting,
            resetForm,
          } = formikProps;

          return (
            <Form>
              {/* <DisplayState {...{ errors: formikProps.errors }} />
              <DisplayState {...{ values: formikProps.values }} /> */}
              <DisplayHeading legend="Add Condition To Dependency Expression" />
              {/* <p>{JSON.parse(props.isAdding)}</p> */}
              {/* {console.log(props.valDepMode)} */}
              {props.valDepMode !== ValDepMode.validation && (
                <Field
                  name="type"
                  label="Condition Type"
                  disabled={handleDisable(values, 'type', touched)}
                  options={[
                    {
                      value: 'AppliedValidator',
                      label: 'Validator',
                    },
                    {
                      value: 'CompositeCondition',
                      label: 'Composite Condition',
                    },
                  ]}
                  component={SelectionInput}
                />
              )}

              {// render the correct component based on type
              values.type &&
                React.createElement(address.whichCondition(values.type), {
                  values,
                  form: props.form,
                  errors: formikProps.errors,
                  currentSelect: props.currentSelect,
                  formikProps: formikProps,
                  activeEntityUUID: props.activeEntityUUID,
                  priorIds,
                  mode: ConditionConfigRenderMode.add,
                  sanitizeSuccessors: sanitizeSuccessors,
                  handleDisable: handleDisable,
                  metaData,
                  setMetaData,
                  valDepMode: props.valDepMode,
                })}

              <button type="submit" disabled={isSubmitting || !dirty}>
                Add
              </button>
              <button
                onClick={e => {
                  e.preventDefault();
                  resetForm(
                    defaultFormState(
                      props.currentSelect,
                      props.valDepMode,
                      props.activeEntityUUID,
                      props.form[props.activeEntityUUID]
                    )
                  );
                }}
              >
                Reset
              </button>
            </Form>
          );
        }}
      </Formik>
    </>
  );
};

import React from 'react';
import ConfigApplyMethod from './ConfigApplyMethod';
import DisplayHeading from '../../components/common/DisplayHeading';
import { TextInput } from '../../components/common/formInputs/TextInput';
import { buildName, buildValue } from './dependencyHelpers';
import { getIn, Field } from 'formik';
export const EnumerationValidation = props => {
  const { mode, values, errors } = props;
  return (
    <div>
      <DisplayHeading legend="Enumeration" />
      {props.TargetId}

      <Field label="Enumeration Value" name="value" component={TextInput} />

      <div>
        <div id="edu_unc_tcrdms_model_form_validation_validators_PatternValidator">
          {/* begin AppliedValidator*/}
          <div>
            <ConfigApplyMethod
              {...props}
              handleStateSet={props.handleStateSet}
              handleSubmit={props.handleSubmit}
              handleAdd={props.handleAdd}
              allowSubmit={props.allowSubmit}
              loadExistingValidator={props.loadExistingValidator}
              handleUpdate={props.handleUpdate}
              validState={props.validState}
              strong={props.strong}
              nullIsValid={props.nullIsValid}
              failureMode={props.failureMode}
            />
          </div>
          {/* end AppliedValidator*/}
        </div>
      </div>
      <div id="validators">
        <ul />
      </div>

      <div>
        <label>
          <span>*</span>
          Indicates a required field
        </label>
      </div>
    </div>
  );
};

export default EnumerationValidation;

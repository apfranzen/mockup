import React from 'react';
import ConfigApplyMethod from './ConfigApplyMethod';
import DisplayHeading from '../../components/common/DisplayHeading';
import { TextInput } from '../../components/common/formInputs/TextInput';
import { buildName, buildValue } from './dependencyHelpers';
import { Field } from 'formik';
export const PatternValidation = props => {
  console.log(props.formikProps);

  const { mode, values } = props;
  // return <DisplayHeading legend="Pattern Validation" />;
  return (
    <div>
      <div>
        <DisplayHeading legend="Pattern Validation" />
        {props.TargetId}
        <Field name="value" label="Validation Pattern" component={TextInput} />
        {/* <TextInput
          name={buildName('value', mode)}
          // value={buildValue('value', mode, values)}
          error={
            props.formikProps.errors.value && props.formikProps.touched.value
          }

        /> */}

        <div>
          <div>
            <ConfigApplyMethod
              {...props}
              handleStateSet={props.handleStateSet}
              handleSubmit={props.handleSubmit}
              handleAdd={props.handleAdd}
              allowSubmit={props.allowSubmit}
              loadExistingValidator={props.loadExistingValidator}
              handleUpdate={props.handleUpdate}
              validState={props.validState}
              strong={props.strong}
              nullIsValid={props.nullIsValid}
              failureMode={props.failureMode}
              valDepMode={props.valDepMode}
            />
          </div>
        </div>
      </div>
      <div id="validators">
        <ul />
      </div>
      <div>
        <label>
          <span>*</span>
          Indicates a required field
        </label>
      </div>
    </div>
  );
};

export default PatternValidation;

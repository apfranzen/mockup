import React from 'react';
import { address } from '../../lib/address';
import { SelectionInput } from '../../components/common/formInputs/SelectionInput';
import { ConditionConfigRenderMode } from '../_validations';
import DisplayHeading from '../../components/common/DisplayHeading';
import { valTypeByDataType } from '../_validations';
import { RemoteConfig } from './RemoteConfig';
import { ValDepMode } from '../_validations';
import { Field } from 'formik';
import { translateBoolean } from '../../model/validation/model.PatternValidator';
import { translate } from '../../lib/translate';
const getValidationClassOptions = (remote, targetId, form, values) => {
  if (!targetId) {
    return [];
  }

  if (!remote) {
    if (!form[targetId].dataType) {
      return [];
    } else if (form[targetId].dataType) {
      return valTypeByDataType[form[targetId].dataType].validations.map(
        validation => ({
          label: translate.validationTypeDescriptor(validation),
          value: validation,
        })
      );
    }
  } else if (remote) {
    // if (!values.inputType) {
    //   return [];
    // }
    console.log(values);

    const inputType = values.inputType;

    const result = valTypeByDataType[inputType].validations;

    return result;
  }
};

export const AppliedValidatorConfig = props => {
  const {
    values,
    formikProps,
    sanitizeSuccessors,
    handleDisable,
    priorIds,
    metaData,
    setMetaData,
  } = props;
  const { touched, setFieldValue } = formikProps;

  return (
    <div>
      <br />
      <div>
        <DisplayHeading legend="Validator" />

        {formikProps.values.type === 'AppliedValidator' &&
          props.valDepMode !== ValDepMode.validation && (
            <Field
              name="remote"
              label="Source"
              disabled={handleDisable(props.values, 'remote', touched)}
              onChange={e => {
                setFieldValue('remote', translateBoolean(e.target.value));
              }}
              options={[
                { value: 'false', label: 'This Form' },
                { value: 'true', label: 'Remote Form' },
              ]}
              component={SelectionInput}
            />
          )}
        {props.valDepMode !== ValDepMode.validation && !values.remote ? (
          <Field
            name="targetId"
            onChange={e => {
              setFieldValue('targetId', e.target.value);
              setFieldValue('internalId', e.target.value);
            }}
            options={
              !values.remote
                ? priorIds.map((id, index) => ({
                    label: `  ${props.form[id].externalIdentifier} -
                   ${props.form[id].type}`,
                    value: id,
                  }))
                : []
            }
            component={SelectionInput}
          />
        ) : null}

        {values.remote && (
          <RemoteConfig
            form={props.form}
            activeEntityUUID={props.activeEntityUUID}
            formikProps={formikProps}
            errors={formikProps.errors}
            handleDisable={handleDisable}
            sanitizeSuccessors={sanitizeSuccessors}
            metaData={metaData}
            setMetaData={setMetaData}
            currentSelect={props.currentSelect}
          />
        )}
        <Field
          name="class"
          label="Validation Type"
          disabled={handleDisable(props.values, 'class', touched)}
          options={getValidationClassOptions(
            values.remote,
            values.targetId,
            props.form,
            values
          )}
          component={SelectionInput}
        />
        {/* below only used for local AppliedValidators */}
        {(values.class || values.type === 'CompositeCondition') &&
          React.createElement(address.mapExpression(values), {
            values,
            form: props.form,
            currentSelect: props.currentSelect,
            formikProps: formikProps,
            errors: formikProps.errors,
            activeEntityUUID: props.activeEntityUUID,
            priorIds,
            mode: ConditionConfigRenderMode.add,
            metaData,
            setMetaData,
            valDepMode: props.valDepMode,
          })}
      </div>
    </div>
  );
};

export default AppliedValidatorConfig;

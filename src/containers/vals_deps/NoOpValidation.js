import React from 'react';
import DisplayHeading from '../../components/common/DisplayHeading';
import { SelectionInput } from '../../components/common/formInputs/SelectionInput';
import { noOpValues } from '../_validations';
import { Field } from 'formik';

export const NoOpValidation = props => {
  const { mode, values } = props;
  return (
    <div>
      <DisplayHeading legend="Empty Field Validation" />
      {props.TargetId}

      <Field
        name="nullIsValid"
        label="Validation Pattern"
        options={[
          { label: noOpValues.true, value: true },
          { label: noOpValues.false, value: false },
        ]}
        component={SelectionInput}
      />

      <div id="validators">
        <ul />
      </div>
      <div>
        <label>
          <span>*</span>
          Indicates a required field
        </label>
      </div>
    </div>
  );
};

export default NoOpValidation;

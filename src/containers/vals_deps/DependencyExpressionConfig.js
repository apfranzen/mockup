import React from 'react';
import { SelectionInput } from '../../components/common/formInputs/SelectionInput';
import {
  operators,
  ConditionConfigRenderMode,
  ValDepMode,
} from '../_validations';
import { buildName, buildValue } from './dependencyHelpers';
import DisplayHeading from '../../components/common/DisplayHeading';
import { Field } from 'formik';

export const DependencyExpressionConfig = props => {
  const { activeConditionId, mode, values } = props;
  return (
    <div>
      <br />
      <div>
        <DisplayHeading
          legend={`${
            mode === ConditionConfigRenderMode.edit
              ? 'Edit Dependency Expression'
              : 'Add Dependency Expression'
          }`}
        />
        <Field
          name="operator"
          type="selection"
          label="Operator"
          disabled={
            // props.activeConditionId === '0' &&
            // props.valDepMode !== ValDepMode.validation
            activeConditionId === '0'
          }
          options={
            // access operators and map over, showing user what they expect and the value according to CDART
            Object.keys(operators).map((operator, i) => ({
              label: Object.values(operators).sort()[i],
              value: Object.keys(operators).sort()[i],
            }))
          }
          component={SelectionInput}
        />
      </div>
    </div>
  );
};

export default DependencyExpressionConfig;

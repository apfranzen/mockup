import React, { Component } from 'react';
import { ValidatorTemplate } from '../AppliedValidatorTemplates';
import './dependencies.css';

class AppliedValidator extends Component {
  constructor(props) {
    super(props);
    this.state = {
      visible: false,
      collapsed: true,
      hover: false,
    };
    this.addHandler = this.addHandler.bind(this);
    this.hoverHandler = this.hoverHandler.bind(this);
    this.leaveHandler = this.leaveHandler.bind(this);
  }

  addHandler(event) {
    this.setState({
      visible: false,
    });
  }
  hoverHandler(event) {
    console.log('clicked');
    event.stopPropagation();
    this.setState({
      hover: true,
    });
  }
  leaveHandler(event) {
    console.log('clicked');
    event.stopPropagation();
    this.setState({
      hover: false,
    });
  }
  render() {
    const styleRangeValidator = {
      width: '100%',
      padding: '6px',
      borderRadius: '3px',
      fontSize: '1.2rem',
    };

    return (
      <li
        // onMouseOver={this.hoverHandler}
        // onMouseLeave={this.leaveHandler}
        style={{
          ...(!this.props.model.remote ? { marginLeft: '12px' } : {}),
          ...styleRangeValidator,
          ...(this.props.active ? { backgroundColor: 'lightBlue' } : {}),
          ...(this.state.hover ? { border: '1px solid green' } : {}),
        }}
        onClick={event => this.props.selectCondition(this.props.id, event)}
      >
        <ValidatorTemplate {...this.props} model={this.props.model} />
      </li>
    );
  }
}

export default AppliedValidator;

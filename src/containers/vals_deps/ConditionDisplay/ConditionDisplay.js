import React, { Component } from 'react';
import { Condition } from '../dependency/helpers';
import './dependencies.css';
import { operators } from '../../_validations';
import { Text } from 'office-ui-fabric-react';

class ConditionDisplay extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expand: true,
      selected: null,
    };
    this.handleExpand = this.handleExpand.bind(this);
  }
  handleExpand(event) {
    this.setState({
      expand: !this.state.expand,
    });
  }

  render() {
    const defaultCondition = [
      {
        type: 'DependencyExpression',
        operator: 'AND',
        conditions: this.props.conditions || [],
      },
    ];

    return (
      <div style={{ background: 'rgb(244, 244, 244' }}>
        {defaultCondition.map(
          (condition, index) =>
            console.log(
              this.props.activeConditionId,
              String(index),
              String(this.props.activeConditionId)
            ) || (
              <Condition
                addConditionHandler={this.props.addConditionHandler}
                isAdding={this.props.isAdding}
                active={String(index) === String(this.props.activeConditionId)}
                model={condition}
                selected={this.props.activeConditionId}
                selectCondition={this.props.selectCondition}
                id={`${index}`}
                style={{ marginLeft: '20px' }}
                validator={this.props.validator}
              />
            )
        )}
      </div>
    );
  }
}

export default ConditionDisplay;

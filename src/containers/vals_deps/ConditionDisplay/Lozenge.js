import React from 'react';

export const Lozenge = props => (
  <div
    style={{
      backgroundColor: props.backgroundColor || 'white',
      borderRadius: '3px',
      padding: '0px 3px 0px 3px',
      border: `1px solid ${props.color}`,
      margin: '0px 4px 0px 4px',
      display: 'inline',
      ...props.style,
    }}
    onClick={props.onClick}
  >
    {props.children}
  </div>
);

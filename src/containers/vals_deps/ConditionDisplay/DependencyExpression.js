import React, { Component } from 'react';
import { Condition } from '../dependency/helpers';
import { Lozenge } from './Lozenge';
import { connect } from 'react-redux';
import _ from 'lodash';
import './dependencies.css';
import { Icon, PrimaryButton } from 'office-ui-fabric-react';
import { operators } from '../../_validations';

class DependencyExpression extends Component {
  constructor(props) {
    super(props);
    this.state = {
      expand: true,
      hover: false,
      isAdding: false,
    };
    this.handleExpand = this.handleExpand.bind(this);
    this.hoverHandler = this.hoverHandler.bind(this);
    this.leaveHandler = this.leaveHandler.bind(this);
  }

  handleExpand(event) {
    event.stopPropagation();

    this.setState({
      expand: !this.state.expand,
    });
  }

  hoverHandler(event) {
    event.stopPropagation();

    console.log('clicked');

    this.setState({
      hover: true,
    });
  }
  leaveHandler(event) {
    console.log('clicked');
    event.stopPropagation();
    this.setState({
      hover: false,
    });
  }

  render() {
    const { isAdding } = this.props;
    console.log({ isAdding });

    const LozengeExpander = props => (
      <span
        onClick={props.onClick}
        style={{
          backgroundColor: 'white',
          borderRadius: '3px',
          border: `1px solid ${props.color}`,
          cursor: 'pointer',
          textAlign: 'center',
          // width: '60px',
          // height: '30px',
          ...props.style,
        }}
        title="Select Condition"
      >
        {props.text} {props.children}
      </span>
    );
    const styleCompositeCondition = {
      minHeight: '120px',
      marginTop: '6px',
      marginLeft: '4px',
      marginBottom: '20px',
    };

    const collapserStyle = {
      position: 'absolute',
      left: '-23px',
      top: '50%',
      // height: '22px',
      transform: 'translateY(-50%) rotate(270deg)',
      textAlign: 'center',

      padding: '6px',
    };

    const modifiedId = this.props.id.replace(/\./g, '');
    const alternateId = modifiedId.length % 2 === 0;
    return (
      <div
        style={{
          ...styleCompositeCondition,
          borderLeft: '1px dashed lightgreen',
          background:
            this.state.expand && alternateId ? 'white' : 'rgb(244, 244, 244)',
          position: 'relative',
          ...(this.state.expand
            ? { border: null }
            : {
                minHeight: null,
              }),
          ...(this.props.active && !this.props.isAdding
            ? { backgroundColor: 'rgb(255, 255, 153)' }
            : {}),
          ...(this.state.hover ? { border: '1px solid green' } : {}),
        }}
        // onMouseOver={this.hoverHandler}
        // onMouseLeave={this.leaveHandler}
        onClick={event => {
          event.stopPropagation();
          this.props.selectCondition(this.props.id, event);
        }}
      >
        {this.state.expand && (
          <LozengeExpander
            // text={`here`}

            color="lightGreen"
            style={{ ...collapserStyle, zIndex: '100' }}
          >
            <Icon
              iconName="MapLayers"
              style={{ color: 'rgb(16, 110, 190)' }}
              title="Composite Condition"
            />

            <span> {operators[this.props.model.operator]}</span>
            <Icon
              iconName="CollapseContentSingle"
              style={{ fontSize: '.8rem', marginLeft: '10px' }}
              // style={{ background: 'lightGreen', fontSize: '2rem' }}
              onClick={this.handleExpand}
              title="Collapse"
            />
          </LozengeExpander>
        )}

        <ul
          style={{
            // paddingLeft: '0px',
            ...(!this.state.expand
              ? { border: null, paddingLeft: '0px' }
              : {
                  borderLeft: '1px dashed lightGreen',
                  paddingLeft: '50px',
                }),
          }}
        >
          <li>
            {!this.state.expand ? (
              <span style={{ cursor: 'pointer' }}>
                <Lozenge
                  style={{ zIndex: '100' }}
                  color="lightGreen"
                  onClick={event =>
                    this.props.selectCondition(this.props.id, event)
                  }
                >
                  {`${operators[this.props.model.operator]} `}
                  <Icon
                    iconName="CircleAdditionSolid"
                    style={{ color: 'lightGreen' }}
                    onClick={this.handleExpand}
                    title="Collapse"
                  />
                  {/* <i
                    style={{ color: 'lightgreen' }}
                    className="far fa-plus-circle fa"
                    onClick={this.handleExpand}
                    title="Expand"
                  /> */}
                </Lozenge>
                :{' '}
                <span style={{ fontStyle: 'italic' }}>
                  ...
                  {this.props.model.conditions &&
                    this.props.model.conditions.length}
                  condition(s)
                </span>
              </span>
            ) : null}
          </li>

          {this.state.expand &&
            this.props.model.conditions.map((condition, index) => (
              <Condition
                isAdding={this.props.isAdding}
                addConditionHandler={this.props.addConditionHandler}
                active={`${this.props.id}.${index}` === this.props.selected}
                selected={this.props.selected}
                selectCondition={this.props.selectCondition}
                key={`${this.props.id}.${index}`}
                id={`${this.props.id}.${index}`}
                model={condition}
                validator={this.props.validator}
              />
            ))}
          {this.state.expand && (
            <li style={{ margin: '6px' }}>
              <PrimaryButton
                disabled={isAdding === this.props.id}
                onClick={e => {
                  e.stopPropagation();
                  this.props.addConditionHandler(this.props.id, e);
                }}
                text="Add Condition"
                iconProps={{ iconName: 'Add' }}
                style={
                  {
                    // background: 'rgb(0, 120, 212)',
                    // border: '1px solid black',
                  }
                }
              />
            </li>
          )}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = state =>
  console.log(_.get(state.form, state.app.activeEntityUUID).dependencies) || {
    dependencies: _.get(state.form, state.app.activeEntityUUID).dependencies
      .conditions,
  };

DependencyExpression = connect(mapStateToProps)(DependencyExpression);

export default DependencyExpression;

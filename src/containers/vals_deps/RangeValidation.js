import React from 'react';
import { buildName, buildValue } from './dependencyHelpers';
import DisplayHeading from '../../components/common/DisplayHeading';
import ConfigApplyMethod from './ConfigApplyMethod';
import { TextInput } from '../../components/common/formInputs/TextInput';
import { Field } from 'formik';
import Checkbox from '../../components/common/formInputs/Checkbox';
export const RangeValidation = props => {
  const { mode, values } = props;

  return (
    <div>
      <DisplayHeading legend="Range Validation" />
      {props.TargetId}
      <form>
        <div>
          <div>
            <p />
            <table>
              <tbody>
                <tr>
                  <td>
                    <Field label="Min" name="min" component={TextInput} />
                  </td>
                  <td>
                    <Checkbox name="minInclusive" label="Min Inclusive" />
                  </td>
                </tr>
                <tr>
                  <td>
                    <Field label="Max" name="max" component={TextInput} />
                  </td>
                  <td>
                    <Checkbox name="maxInclusive" label="Max Inclusive" />
                  </td>
                </tr>
              </tbody>
            </table>
            <ConfigApplyMethod
              {...props}
              handleStateSet={props.handleStateSet}
              handleSubmit={props.handleSubmit}
              handleAdd={props.handleAdd}
              allowSubmit={props.allowSubmit}
              loadExistingValidator={props.loadExistingValidator}
              handleUpdate={props.handleUpdate}
              validState={props.validState}
              strong={props.strong}
              nullIsValid={props.nullIsValid}
              failureMode={props.failureMode}
            />
          </div>
        </div>
      </form>
    </div>
  );
};

export default RangeValidation;

/** @jsx jsx */
import React, { useEffect, useState, useContext } from 'react';
import { Lozenge } from './ConditionDisplay/Lozenge';
import { noOpValues } from '../_validations';
import { useDataApi } from '../../lib/hooks';
import { Icon } from 'office-ui-fabric-react/';
import { css, jsx } from '@emotion/core';
import { HrefContext } from '../../components/LayoutRoot';
export const RangeTemplate = props => (
  <p style={{ display: 'inline-block' }}>
    {!props.validator
      ? [
          <Lozenge backgroundColor="#f50">{props.displayId}</Lozenge>,
          `is between`,
        ]
      : `Between`}
    <Lozenge color="#f50">{props.model.min}</Lozenge>
    <Lozenge color="#f50">{props.model.max}</Lozenge>
  </p>
);

export const EnumerationTemplate = props => (
  <p style={{ display: 'inline-block' }}>
    {!props.validator
      ? [<Lozenge backgroundColor="#f50">{props.displayId}</Lozenge>, '=']
      : null}
    <Lozenge color="#f50">{props.model.value}</Lozenge>
  </p>
);

export const PatternTemplate = props => {
  return (
    <p
      style={{
        display: 'flex',
        alignItems: 'center',
      }}
    >
      {props.model.remote && (
        <Icon
          iconName={'Remote'}
          css={theme => css`
            color: ${theme.colors.blue};
            /* font-size: 1.5rem; */
            /* margin-right: 6px; */
          `}
          title="Remote Condition"
        />
      )}
      {!props.validator
        ? [
            <Lozenge backgroundColor="#f50">{props.displayId}</Lozenge>,
            'matches:',
          ]
        : null}
      <Lozenge color="#f50">{props.model.value}</Lozenge>
    </p>
  );
};

export const SubjectInputTemplate = props => (
  <p style={{ display: 'inline-block' }}>
    {!props.validator
      ? [
          <Lozenge backgroundColor="#f50">{props.displayId}</Lozenge>,
          'validates against script:',
        ]
      : null}

    <Lozenge color="#f50">{props.model.value}</Lozenge>
  </p>
);

export const NoOpTemplate = props => {
  console.log({ props });

  return (
    <p style={{ display: 'inline-block' }}>
      {!props.validator
        ? [<Lozenge backgroundColor="#f50">{props.displayId}</Lozenge>, 'is']
        : null}
      <Lozenge color="#f50">{noOpValues[props.model.nullIsValid]}</Lozenge>
    </p>
  );
};

export const Default = <p>No Dependencies On This Input</p>;

export const template = {
  RangeValidator: RangeTemplate,
  EnumerationValidator: EnumerationTemplate,
  PatternValidator: PatternTemplate,
  SubjectInputValidator: SubjectInputTemplate,
  NoOpValidator: NoOpTemplate,
  default: Default,

  // 'Default - no template',
};

export const ValidatorTemplate = props => {
  const {
    model: { remote, internalId, externalIdentifier, versionId },
  } = props;
  const { studyId } = useContext(HrefContext);

  const url = `/studies/${studyId}/inputs/${versionId}`;

  const { data, isLoading, isError, errorMessage } = useDataApi(true, url);
  const getDisplayId = () => {
    if (!remote) {
      return externalIdentifier || 'fix me';
    } else {
      return isLoading || !data.length
        ? '...loading'
        : data.filter(input => input.internalId === internalId)[0].externalId;
    }
  };

  const Component =
    template[props.model.class in template ? props.model.class : 'default'];
  return <Component {...props} displayId={getDisplayId()} />;
};

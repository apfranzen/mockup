import React, { useEffect, useState, createContext } from 'react';
import { connect } from 'react-redux';
import FormRoot from '../components/FormEntities/FormEntity/FormRoot';
import {
  createEntity,
  deleteTab,
  initForm,
  loadFormState,
  reorderTabs,
  selectTab,
  loadFiles,
} from '../redux-modules/actions';

/** @jsx jsx */
import { jsx } from '@emotion/core';

function FormContainer(props) {
  const { existingName } = props;

  useEffect(() => {
    // props.loadFormState(window.location);
    // props.loadFiles();
  }, []);

  useEffect(() => {
    document.title = `Form Designer - ${props.existingName || 'New Form'}`;
  }, [existingName]);

  return (
    <FormRoot
      activeTab={props.activeTab}
      setGridWidth={props.setGridWidth}
      initForm={props.initForm}
      numRows={props.numRows}
      initForm2={props.initForm2}
    />
  );
}

const mapStateToProps = state => ({
  id: 1,
  // formId: state.app.formId || null,
  existingName: state.app.cdartStatus.formName || '',
  activeEntityUUID: state.app.activeEntityUUID,
  activeTab: state.form[state.app.activeTab],
  tabs: state.form['0'].children.map(topLevelId => {
    return {
      uuid: state.form[topLevelId].uuid,
      legend: state.form[topLevelId].legend,
    };
  }),
  initForm2: state.app.initForm
});

const _FormContainer = connect(
  mapStateToProps,
  {
    initForm,
    loadFormState,
    selectTab,
    reorderTabs,
    createEntity,
    deleteTab,
    loadFiles,
  }
)(FormContainer);

export default _FormContainer;

import { EntityTypes } from '../model/Types';

/** Mode to detirmine which context an condition is being used in */
export const ValDepMode = {
  validation: 'validation',
  dependency: 'dependency',
};

/** Mode to show correct Title in DependencyExpressionConfig */
export const ConditionConfigRenderMode = {
  edit: 'edit',
  add: 'add',
};

/**Only these types can be used as dependents */
export const EligibleDependents = [
  EntityTypes.TextInput,
  EntityTypes.Checkbox,
  EntityTypes.SelectionInput,
  EntityTypes.ASTextInput,
  EntityTypes.CDSTextInput,
];

export const valTypeByDataType = {
  string: {
    type: 'string',
    length: 2,
    validations: [
      'PatternValidator',
      'NoOpValidator',
      'EnumerationValidator',
      'SubjectInputValidator',
    ],
  },
  date: {
    type: 'date',
    fixed: false,
    full: false,
    partialExpression: null,
    timeZone: null,
    validations: ['NoOpValidator', 'EnumerationValidator', 'RangeValidator'],
  },
  integer: {
    type: 'integer',
    length: 2,
    validations: [
      'PatternValidator',
      'NoOpValidator',
      'EnumerationValidator',
      'RangeValidator',
    ],
  },
  float: {
    type: 'float',
    length: 2,
    validations: [
      'PatternValidator',
      'NoOpValidator',
      'EnumerationValidator',
      'RangeValidator',
    ],
  },
};

export const noOpValues = {
  true: 'Empty',
  false: 'Not Empty',
};

export const operators = {
  OR: 'ANY', // equal to OR logical operator
  AND: 'ALL', // equal to AND logical operator
  XOR: 'ONE', // exactly one
};

export const Conditions = {
  PatternValidator: 'PatternValidator',
  NoOpValidator: 'NoOpValidator',
  EnumerationValidator: 'EnumerationValidator',
  RangeValidator: 'RangeValidator',
  SubjectInputValidator: 'SubjectInputValidator',
};

export const inputDefinedValidator = {
  // [EntityTypes.Echo]: [
  //   Conditions.PatternValidator,
  //   Conditions.NoOpValidator,
  //   Conditions.EnumerationValidator,
  //   Conditions.RangeValidator,
  // ],
  // [EntityTypes.CDSTextInput]: [
  //   Conditions.PatternValidator,
  //   Conditions.NoOpValidator,
  //   Conditions.EnumerationValidator,
  //   Conditions.SubjectInputValidator,
  // ],
  // [EntityTypes.ASTextInput]: null,
  // [EntityTypes.TextArea]: null,
  // [EntityTypes.Checkbox]: null,
  // [EntityTypes.SelectionInput]: null,
  // [EntityTypes.TextBlock]: null,
  // [EntityTypes.ImageBlock]: null,
  [EntityTypes.TextInput]: [
    Conditions.PatternValidator,
    Conditions.NoOpValidator,
    Conditions.EnumerationValidator,
    Conditions.SubjectInputValidator,
  ],
  [EntityTypes.EchoInput]: [
    Conditions.PatternValidator,
    Conditions.NoOpValidator,
    Conditions.EnumerationValidator,
    Conditions.RangeValidator,
  ],
  [EntityTypes.CDSTextInput]: [
    Conditions.PatternValidator,
    Conditions.NoOpValidator,
    Conditions.EnumerationValidator,
    Conditions.SubjectInputValidator,
  ],
};

export const DataTypeByInput = {
  TextInput: ['string', 'date', 'integer', 'float'],
  CDSTextInput: ['string', 'date', 'integer', 'float'],
  FormSection: ['string', 'date', 'integer', 'float'],
  TextArea: false,
  Checkbox: false,
  SelectionInput: ['string', 'integer', 'float'],
  TextBlock: ['string', 'date', 'integer', 'float'],
  ImageBlock: ['string', 'date', 'integer', 'float'],
  ASTextInput: false,
  EchoInput: ['string', 'date', 'integer', 'float'],
  Form: ['string', 'date', 'integer', 'float'],
};

export const validatorsByInput = {
  Any: [
    'PatternValidator',
    'NoOpValidator',
    'EnumerationValidator',
    'SubjectInputValidator',
  ],
};

export const userDefined = {
  Pattern: {
    formDependency: '',
    inputId: '',
    eventDefinition: '',
    occureance: '',
    occuranceNum: 0,
    validState: false,
    nullIsValid: false,
    strong: false,
    validationPattern: 'samplePattern',
    customFailureMessaage: 'sampleFailureMessage',
    language: '',
    overRideable: false,
    eventDef: 'sampleDef',
  },
  NoOp: {
    emptyField: false,
    anyValue: false,
    customFailureMessaage: '',
    language: '',
    country: '',
    overRideable: false,
    eventDef: 'sampleDef',
  },
  Enumeration: {
    formDependency: '',
    inputId: '',
    eventDefinition: '',
    occureance: '',
    occuranceNum: 0,
    validationPattern: '',
    overRideable: false,
    eventDef: 'sampleDef',
  },
  SubjectInputValidation: { script: '' },
  Range: {
    type: 'SubjectInputValidation',
    customFailureMessage: 'customFailureMessage',
    validState: 'validState',
    strong: 'strong',
    nullIsValid: 'nullIsValid',
    inputIndex: 'inputIndex',
    externalId: 'externalId',
    maxInclusive: 'maxInclusive',
    minInclusive: 'minInclusive',
    min: 'min',
    max: 'max',
  },
};

export const validations = {
  String: {
    type: 'String',
    length: 2,
  },
  Date: {
    type: 'Date',
    fixed: false,
    full: false,
    partialExpression: null,
    timeZone: null,
  },
  Float: {
    type: 'Float',
    length: 2,
  },
  Integer: {
    type: 'Integer',
    length: 2,
  },
  Pattern: {
    value: 'pattern content',
  },
  EmptyField: {
    value: 'EmptyField content',
  },
  Enumeration: {
    value: 'Enumeration content',
  },
  SubjectInputValidation: {
    value: 'SubjectInputValidation content',
  },
};
export const locals2 = ['Albanian', 'Arabic'];

export const LocalList = [
  {
    label: '',
    value: '',
  },
  {
    label: 'Albanian',
    value: 'sq',
  },
  {
    label: 'Arabic',
    value: 'ar',
  },
  {
    label: 'Belarusian',
    value: 'be',
  },
  {
    label: 'Bulgarian',
    value: 'bg',
  },
  {
    label: 'Catalan',
    value: 'ca',
  },
  {
    label: 'Chinese',
    value: 'zh',
  },
  {
    label: 'Croatian',
    value: 'hr',
  },
  {
    label: 'Czech',
    value: 'cs',
  },
  {
    label: 'Danish',
    value: 'da',
  },
  {
    label: 'Dutch',
    value: 'nl',
  },
  {
    label: 'English',
    value: 'en',
  },
  {
    label: 'Estonian',
    value: 'et',
  },
  {
    label: 'Finnish',
    value: 'fi',
  },
  {
    label: 'French',
    value: 'fr',
  },
  {
    label: 'German',
    value: 'de',
  },
  {
    label: 'Greek',
    value: 'el',
  },
  {
    label: 'Hebrew',
    value: 'iw',
  },
  {
    label: 'Hindi',
    value: 'hi',
  },
  {
    label: 'Hungarian',
    value: 'hu',
  },
  {
    label: 'Icelandic',
    value: 'is',
  },
  {
    label: 'Indonesian',
    value: 'in',
  },
  {
    label: 'Irish',
    value: 'ga',
  },
  {
    label: 'Italian',
    value: 'it',
  },
  {
    label: 'Japanese',
    value: 'ja',
  },
  {
    label: 'Korean',
    value: 'ko',
  },
  {
    label: 'Latvian',
    value: 'lv',
  },
  {
    label: 'Lithuanian',
    value: 'lt',
  },
  {
    label: 'Macedonian',
    value: 'mk',
  },
  {
    label: 'Malay',
    value: 'ms',
  },
  {
    label: 'Maltese',
    value: 'mt',
  },
  {
    label: 'Norwegian',
    value: 'no',
  },
  {
    label: 'Polish',
    value: 'pl',
  },
  {
    label: 'Portuguese',
    value: 'pt',
  },
  {
    label: 'Romanian',
    value: 'ro',
  },
  {
    label: 'Russian',
    value: 'ru',
  },
  {
    label: 'Serbian',
    value: 'sr',
  },
  {
    label: 'Slovak',
    value: 'sk',
  },
  {
    label: 'Slovenian',
    value: 'sl',
  },
  {
    label: 'Spanish',
    value: 'es',
  },
  {
    label: 'Swedish',
    value: 'sv',
  },
  {
    label: 'Thai',
    value: 'th',
  },
  {
    label: 'Turkish',
    value: 'tr',
  },
  {
    label: 'Ukrainian',
    value: 'uk',
  },
  {
    label: 'Vietnamese',
    value: 'vi',
  },
];
// 'CDART.CrfModel.LocaleList'
export const LocaleList = {
  '': '',

  // sq: 'Albanian',

  sq_AL: 'Albanian (Albania)',

  // ar: 'Arabic',

  ar_DZ: 'Arabic (Algeria)',

  ar_BH: 'Arabic (Bahrain)',

  ar_EG: 'Arabic (Egypt)',

  ar_IQ: 'Arabic (Iraq)',

  ar_JO: 'Arabic (Jordan)',

  ar_KW: 'Arabic (Kuwait)',

  ar_LB: 'Arabic (Lebanon)',

  ar_LY: 'Arabic (Libya)',

  ar_MA: 'Arabic (Morocco)',

  ar_OM: 'Arabic (Oman)',

  ar_QA: 'Arabic (Qatar)',

  ar_SA: 'Arabic (Saudi Arabia)',

  ar_SD: 'Arabic (Sudan)',

  ar_SY: 'Arabic (Syria)',

  ar_TN: 'Arabic (Tunisia)',

  ar_AE: 'Arabic (United Arab Emirates)',

  ar_YE: 'Arabic (Yemen)',

  // be: 'Belarusian',

  be_BY: 'Belarusian (Belarus)',

  // bg: 'Bulgarian',

  bg_BG: 'Bulgarian (Bulgaria)',

  // ca: 'Catalan',

  ca_ES: 'Catalan (Spain)',

  // zh: 'Chinese',

  zh_CN: 'Chinese (China)',

  zh_HK: 'Chinese (Hong Kong)',

  zh_SG: 'Chinese (Singapore)',

  zh_TW: 'Chinese (Taiwan)',

  // hr: 'Croatian',

  hr_HR: 'Croatian (Croatia)',

  // cs: 'Czech',

  cs_CZ: 'Czech (Czech Republic)',

  // da: 'Danish',

  da_DK: 'Danish (Denmark)',

  // nl: 'Dutch',

  nl_BE: 'Dutch (Belgium)',

  nl_NL: 'Dutch (Netherlands)',

  // en: 'English',

  en_AU: 'English (Australia)',

  en_CA: 'English (Canada)',

  en_IN: 'English (India)',

  en_IE: 'English (Ireland)',

  en_MT: 'English (Malta)',

  en_NZ: 'English (New Zealand)',

  en_PH: 'English (Philippines)',

  en_SG: 'English (Singapore)',

  en_ZA: 'English (South Africa)',

  en_GB: 'English (United Kingdom)',

  en_US: 'English (United States)',

  // et: 'Estonian',

  et_EE: 'Estonian (Estonia)',

  // fi: 'Finnish',

  fi_FI: 'Finnish (Finland)',

  // fr: 'French',

  fr_BE: 'French (Belgium)',

  fr_CA: 'French (Canada)',

  fr_FR: 'French (France)',

  fr_LU: 'French (Luxembourg)',

  fr_CH: 'French (Switzerland)',

  // de: 'German',

  de_AT: 'German (Austria)',

  de_DE: 'German (Germany)',

  de_GR: 'German (Greece)',

  de_LU: 'German (Luxembourg)',

  de_CH: 'German (Switzerland)',

  // el: 'Greek',

  el_CY: 'Greek (Cyprus)',

  el_GR: 'Greek (Greece)',

  // iw: 'Hebrew',

  iw_IL: 'Hebrew (Israel)',

  // hi: 'Hindi',

  hi_IN: 'Hindi (India)',

  // hu: 'Hungarian',

  hu_HU: 'Hungarian (Hungary)',

  // is: 'Icelandic',

  is_IS: 'Icelandic (Iceland)',

  // in: 'Indonesian',

  in_ID: 'Indonesian (Indonesia)',

  // ga: 'Irish',

  ga_IE: 'Irish (Ireland)',

  // it: 'Italian',

  it_IT: 'Italian (Italy)',

  it_CH: 'Italian (Switzerland)',

  // ja: 'Japanese',

  ja_JP: 'Japanese (Japan)',

  'ja_JP_JP_#u-ca-japanese': 'Japanese (Japan,JP)',

  // ko: 'Korean',

  ko_KR: 'Korean (South Korea)',

  // lv: 'Latvian',

  lv_LV: 'Latvian (Latvia)',

  // lt: 'Lithuanian',

  lt_LT: 'Lithuanian (Lithuania)',

  // mk: 'Macedonian',

  mk_MK: 'Macedonian (Macedonia)',

  // ms: 'Malay',

  ms_MY: 'Malay (Malaysia)',

  // mt: 'Maltese',

  mt_MT: 'Maltese (Malta)',

  // no: 'Norwegian',

  no_NO: 'Norwegian (Norway)',

  no_NO_NY: 'Norwegian (Norway,Nynorsk)',

  // pl: 'Polish',

  pl_PL: 'Polish (Poland)',

  // pt: 'Portuguese',

  pt_BR: 'Portuguese (Brazil)',

  pt_PT: 'Portuguese (Portugal)',

  // ro: 'Romanian',

  ro_RO: 'Romanian (Romania)',

  // ru: 'Russian',

  ru_RU: 'Russian (Russia)',

  // sr: 'Serbian',

  sr_BA: 'Serbian (Bosnia and Herzegovina)',

  'sr__#Latn': 'Serbian (Latin)',

  'sr_BA_#Latn': 'Serbian (Latin,Bosnia and Herzegovina)',

  'sr_ME_#Latn': 'Serbian (Latin,Montenegro)',

  'sr_RS_#Latn': 'Serbian (Latin,Serbia)',

  sr_ME: 'Serbian (Montenegro)',

  sr_CS: 'Serbian (Serbia and Montenegro)',

  sr_RS: 'Serbian (Serbia)',

  // sk: 'Slovak',

  sk_SK: 'Slovak (Slovakia)',

  // sl: 'Slovenian',

  sl_SI: 'Slovenian (Slovenia)',

  // es: 'Spanish',

  es_AR: 'Spanish (Argentina)',

  es_BO: 'Spanish (Bolivia)',

  es_CL: 'Spanish (Chile)',

  es_CO: 'Spanish (Colombia)',

  es_CR: 'Spanish (Costa Rica)',

  es_CU: 'Spanish (Cuba)',

  es_DO: 'Spanish (Dominican Republic)',

  es_EC: 'Spanish (Ecuador)',

  es_SV: 'Spanish (El Salvador)',

  es_GT: 'Spanish (Guatemala)',

  es_HN: 'Spanish (Honduras)',

  es_MX: 'Spanish (Mexico)',

  es_NI: 'Spanish (Nicaragua)',

  es_PA: 'Spanish (Panama)',

  es_PY: 'Spanish (Paraguay)',

  es_PE: 'Spanish (Peru)',

  es_PR: 'Spanish (Puerto Rico)',

  es_ES: 'Spanish (Spain)',

  es_US: 'Spanish (United States)',

  es_UY: 'Spanish (Uruguay)',

  es_VE: 'Spanish (Venezuela)',

  // sv: 'Swedish',

  sv_SE: 'Swedish (Sweden)',

  // th: 'Thai',

  th_TH: 'Thai (Thailand)',

  'th_TH_TH_#u-nu-thai': 'Thai (Thailand,TH)',

  // tr: 'Turkish',

  tr_TR: 'Turkish (Turkey)',

  // uk: 'Ukrainian',

  uk_UA: 'Ukrainian (Ukraine)',

  // vi: 'Vietnamese',

  vi_VN: 'Vietnamese (Vietnam)',
};

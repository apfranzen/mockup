import React, { useState, createContext, useRef } from 'react';
import { connect } from 'react-redux';
import FormEntity from '../components/FormEntities/FormEntity';
import { EntityTypes } from '../model/Types';
import {
  createEntity,
  dragEnd,
  dragStart,
  drop,
  moveEntity,
  selectEntity,
  updateProperty,
} from '../redux-modules/actions';

export const accepts = {
  [EntityTypes.Form]: Object.keys(EntityTypes).filter(
    entity => entity !== EntityTypes.Form
  ),
};
export const ResizeContext = createContext(false);
function FormEntityContainer(props) {
  const [isResizing, setIsResizing] = useState('');

  function dropHandler(event) {
    event.stopPropagation();
    props.drop(props.model.uuid, props.sectionUUID, {
      screenX: event.screenX,
    });
  }

  function dragStartHandler(event) {
    const {
      model: { uuid },
      dragStart,
      sectionUUID,
    } = props;
    event.stopPropagation();
    dragStart(uuid, sectionUUID, {
      screenX: event.screenX,
    });
  }

  return (
    <ResizeContext.Provider value={{ isResizing, setIsResizing }}>
      <FormEntity
        createEntity={props.createEntity}
        moveEntity={props.moveEntity}
        accepts={accepts[EntityTypes.Form]}
        model={props.model}
        active={props.active}
        uuid={props.model.uuid}
        isResizing={props.isResizing}
        isDragging={props.isDragging}
        lastInRow={props.lastInRow}
        parentWidth={props.parentWidth}
        sectionUUID={props.sectionUUID}
        root={props.root}
        autoIds={props.autoIds}
        childrenEntities={props.childrenEntities}
        numRows={props.numRows}
        initForm={props.initForm}
        selectEntity={props.selectEntity}
        dragStartHandler={dragStartHandler}
        dragPreview={props.dragPreview}
        // dragEndHandler={dragEndHandler}
        dropHandler={dropHandler}
        updateProperty={props.updateProperty}
      />
    </ResizeContext.Provider>
  );
}

/* ownProps are props passed into FormEntityContainer that can be used to assist
 in looking up data in mapStateToProps
 The goal is to pass children model to FormSection, render necessary Rows and then render the appropriate FormEntityContainers there, so that one can move an entity when offset doesn't account for dropping on Row

 Cannot calculate the Row any other way because the height of the entities is variable based on content
*/

const mapStateToProps = (state, ownProps) => ({
  model: state.form[ownProps.id],
  sectionUUID: ownProps.sectionUUID,
  id: ownProps.id,
  active: state.app.activeEntityUUID === ownProps.id,
  isResizing: state.app.isResizing,
  autoIds: state.form[0].autoIds,
  ...(state.form[ownProps.id].type === EntityTypes.FormSection
    ? {
      childrenEntities: state.form[ownProps.id].children.map(childId => ({
        key: childId,
        id: childId,
        row: state.form[childId].row,
        type: state.form[childId].type,
      })),
      numRows: state.form[ownProps.id].children
        .map(childId => state.form[childId].row)
        .reduce(function (previousLargestNumber, currentLargestNumber) {
          return currentLargestNumber > previousLargestNumber
            ? currentLargestNumber
            : previousLargestNumber;
        }, 0),
    }
    : {}),
  ...(state.app.initForm ? { initForm: state.app.initForm } : {}),

  parentWidth: state.form[ownProps.sectionUUID].width,
  root: state.app.activeTab === ownProps.sectionUUID, //
});
const mapDispatchToProps = {

  updateProperty,
  createEntity,
  moveEntity,
  selectEntity,
  dragStart,
  dragEnd,
  drop,

};
const ConnectedFormEntityContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(FormEntityContainer);

export default ConnectedFormEntityContainer;

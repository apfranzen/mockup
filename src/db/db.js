import Dexie from 'dexie';

const db = new Dexie('FormDesignerDB');
db.version(1).stores({ files: '++id' });

export default db;

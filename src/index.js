import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import LayoutRoot from './components/LayoutRoot';
import './index.css';
import * as serviceWorker from './serviceWorker';
import configureStore from './redux-modules/configureStore';

const store = configureStore();

let renderApp = () => {
  ReactDOM.render(
    <Provider store={store}>
      <LayoutRoot />
    </Provider>,
    document.getElementById('root')
  );
};

if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept('./components/LayoutRoot', renderApp);
}
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
renderApp();

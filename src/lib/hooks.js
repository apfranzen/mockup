import React, { Fragment, useEffect, useReducer } from 'react';
import axios from 'axios';
import { SelectionInput } from '../components/common/formInputs/SelectionInput';
import { Field } from 'formik';
const username = process.env.REACT_APP_USERNAME;
const password = process.env.REACT_APP_PASSWORD;
export function dataFetchReducer(state, action) {
  switch (action.type) {
    case 'FETCH_INIT':
      return { ...state, isLoading: true, isError: false };
    case 'FETCH_SUCCESS':
      return {
        ...state,
        isLoading: false,
        isError: false,
        data: action.payload,
      };
    case 'FETCH_FAILURE':
      return {
        ...state,
        errorMessage: action.errorMessage,
        isLoading: false,
        isError: true,
        data: [],
      };
    default:
      throw new Error();
  }
}

export const useDataApi = (trigger, initialUrl, initialData = []) => {
  // const [url, setUrl] = useState(initialUrl);

  const [state, dispatch] = useReducer(dataFetchReducer, {
    isLoading: false,
    isError: false,
    errorMessage: null,
    data: initialData,
  });

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => {
      dispatch({ type: 'FETCH_INIT' });

      try {
        console.log({ initialUrl });

        const result = await axios(initialUrl, {
          auth: {

          },
          withCredentials: true,
        });

        if (!didCancel) {
          dispatch({ type: 'FETCH_SUCCESS', payload: result.data });
        }
      } catch (error) {
        if (!didCancel) {
          dispatch({ type: 'FETCH_FAILURE', errorMessage: error.message });
        }
      }
    };
    if (trigger) {
      fetchData();
      console.log(trigger, initialUrl);
    }

    return () => {
      didCancel = true;
    };
  }, [trigger]);

  return { ...state };
};
export const usePostDataApi = (trigger, initialUrl, initialData = []) => {
  // const [url, setUrl] = useState(initialUrl);

  const [state, dispatch] = useReducer(dataFetchReducer, {
    isLoading: false,
    isError: false,
    errorMessage: null,
    data: initialData,
  });

  useEffect(() => {
    let didCancel = false;

    const fetchData = async () => {
      dispatch({ type: 'FETCH_INIT' });

      try {
        console.log({ initialUrl });

        const result = await axios.post(initialUrl, {
          auth: {

          },
          withCredentials: true,
          data: {
            glossary: {
              title: 'example glossary',
              GlossDiv: {
                title: 'S',
                GlossList: {
                  GlossEntry: {
                    ID: 'SGML',
                    SortAs: 'SGML',
                    GlossTerm: 'Standard Generalized Markup Language',
                    Acronym: 'SGML',
                    Abbrev: 'ISO 8879:1986',
                    GlossDef: {
                      para:
                        'A meta-markup language, used to create markup languages such as DocBook.',
                      GlossSeeAlso: ['GML', 'XML'],
                    },
                    GlossSee: 'markup',
                  },
                },
              },
            },
          },
        });

        if (!didCancel) {
          dispatch({ type: 'FETCH_SUCCESS', payload: result.data });
        }
      } catch (error) {
        if (!didCancel) {
          dispatch({ type: 'FETCH_FAILURE', errorMessage: error.message });
        }
      }
    };
    if (trigger) {
      fetchData();
      console.log(trigger, initialUrl);
    }

    return () => {
      didCancel = true;
    };
  }, [trigger]);

  return { ...state };
};

export function AsyncSelect(props) {
  const {
    trigger,
    url,
    name,
    id,
    label,
    onChange,
    selector,
    value,
    resolver,
    disabled,
    required,
  } = props;
  const { data, isLoading, isError, errorMessage } = useDataApi(trigger, url);

  return (
    <Fragment>
      {isError && <div>{`Something went wrong ... ${errorMessage}`}</div>}

      <Field
        {...props}
        name={name}
        label={label}
        options={isLoading ? ['...loading'] : resolver(data)}
        component={SelectionInput}
      />
    </Fragment>
  );
}

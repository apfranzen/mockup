import { isFormInput } from '../model/Types';

export const utility = {
  /**
   * Detirmines if an entity is the last in row for
   * @param {number} sectionWidth
   * @param {Object} model Model of entity in quetion
   * @param {Array} children Parent section's array of children
   * @returns {boolean}
   */
  lastInRow: (sectionWidth, model, children) => {
    const indexOfEntity = children.map(child => child.id).indexOf(model.uuid);

    var runningTotal = 0;
    for (var i = 0; i <= indexOfEntity; ++i) {
      runningTotal += utility.total(model);
    }
    return runningTotal % sectionWidth === 0 ? true : false;
  },
  /**
   *
   * @param {FormEntity} entity
   * @param {FormSection} section
   * @param {number[]} path
   * @returns {FormEntity}
   */

  add: (path, entity, section) => {
    let e = entity;
    if (path.length > 1) {
      e = utility.add(path.slice(1), entity, section.children()[path[0]]);
    }
    let newChildren = section.children().slice(0);
    newChildren.splice(path[0], path.length > 1 ? 1 : 0, e);
    return section.setChildren(newChildren);
  },
  /**
   * @param {FormSection} section
   * @param {number[]} path
   * @returns {FormEntity}
   */
  remove: (path, section) => {
    // if (path.length === 1 && section.children()[path[0]] === undefined) {
    //   throw new Error("path OOB");
    // }
    let newChildren = section.children().slice(0);
    if (path.length > 1) {
      newChildren[path[0]] = utility.remove(
        path.slice(1),
        section.children()[path[0]]
      );
    } else {
      newChildren.splice(path[0], 1);
    }
    return section.setChildren(newChildren);
  },
  mutate: (address, properties) => {
    const entity = address.byPath(address);
    utility.remove(address);
    utility.add(
      address.rehydrate(Object.assign({}, entity.properties(), properties)),
      address
    );
  },
  findAll: (e, testFn) => {
    let result = testFn.call({}, e) ? [e] : [];
    if (!e.children) {
      return result;
    }
    return result.concat(
      e
        .children()
        .map(e => utility.flatten(utility.findAll(e, testFn)))
        .reduce((a, b) => [...a, ...b], [])
    );
  },
  /**
   * flatten the given array or array-like such that any members
   * which are themselves array-likes will be inserted into the
   * resulting array at the position of their parent's occurrence.
   *
   * Ex: [1, [2, 3], 4, [5, [6]]] => [1, 2, 3, 4, 5, 6]
   *
   * @param {Array} arr array to flatter, not undefined
   * @returns {Array} utility.flattened array
   */
  flatten: arr =>
    arr.reduce(
      (a, b) => a.concat(Array.isArray(b) ? utility.flatten(b) : b),
      []
    ),

  /**
   * Returns the total width of an entity including prePromptWidth(optional), width, postPromptWidth(optional)
   * @param {entity} Object Model
   * @returns {number}
   */
  total: entity => {
    if ('prePromptWidth' in entity) {
      const resultingSum =
        entity.prePromptWidth + entity.width + entity.postPromptWidth;

      return resultingSum;
    } else {
      return entity.width;
    }
  },

  arraysEqual: (a, b) => {
    if (a === b) return true;
    if (a == null || b == null) return false;
    if (a.length !== b.length) return false;
    for (var i = 0; i < a.length; ++i) {
      if (a[i] !== b[i]) return false;
    }
    return true;
  },

  // import {defaultPropsFE} from './constants/defaultPropsFE';
  // import Form from './components/FormEntities/Form';
  // import FormSectionComponent from './components/FormEntities/FormSection';
  // import TextInputComponent from './components/FormEntities/TextInput';
  // import TextAreaComponent from './components/FormEntities/TextArea';
  // import CheckboxComponent from './components/FormEntities/Checkbox';
  // import { Form } from './data/Form';
  // import { FormSection } from './data/FormSection';
  // import TextInput from "../data/TextInput";
  // import TextArea from "../data/TextArea";
  // import { Checkbox } from './data/Checkbox';

  createArrRange: (start, end) =>
    new Array(end - start + 1).fill(undefined).map((_, i) => i + start),

  round: (value, decimals) =>
    Number(Math.round(value + 'e' + decimals) + 'e-' + decimals),

  /**
   * orders form by tabOrder and returns only inputs (no formEntities)
   *  {1: {id: 1, width: 24, prepend: 0}} => [{id: 1, width: 24, prepend: 0}]
   * @param {object} form
   * @returns Array
   */
  formInputsByTabOrder: form => {
    const entityTypes = {
      TextInput: 'TextInput',
      TextArea: 'TextArea',
      Checkbox: 'Checkbox',
      SelectionInput: 'SelectionInput',
      // TextBlock: 'TextBlock',
      // ImageBlock: 'ImageBlock',
      ASTextInput: 'ASTextInput',
      CDSTextInput: 'CDSTextInput',
      EchoInput: 'EchoInput',
    };

    const findIds = parent => {
      return parent.reduce((accumulator, currentId) => {
        // console.log(form);

        if (form[currentId].children) {
          const orderedByTabOrder = form[currentId].children.sort(
            (ob1, ob2) => {
              if (form[ob1].tabOrder > form[ob2].tabOrder) {
                return 1;
              } else if (form[ob1].tabOrder < form[ob2].tabOrder) {
                return -1;
              } else {
                // terminal case
                return 0;
              }
            }
          );

          return accumulator.concat(findIds(orderedByTabOrder));
        } else {
          return accumulator.concat(currentId);
        }
      }, []);
    };

    return findIds(form[0].children)
      .map(id => form[id])
      .filter(isFormInput);
  },

  arrayToObject: (array, keyField) =>
    array.reduce((obj, item) => {
      obj[item[keyField]] = item;
      return obj;
    }, {}),

  flattenObject: obj => {
    const flattened = {};

    Object.keys(obj).forEach(key => {
      if (typeof obj[key] === 'object') {
        Object.assign(flattened, utility.flattenObject(obj[key]));
      } else {
        flattened[key] = obj[key];
      }
    });

    return flattened;
  },
};

import React from 'react';
import { EntityTypes } from '../model/Types';
import FormSection from '../components/FormEntities/FormEntity/FormSection';
import Checkbox from '../components/FormEntities/FormEntity/Checkbox';
import TextArea from '../components/FormEntities/FormEntity/TextArea';
import TextInput2 from '../components/FormEntities/FormEntity/TextInput';
import SelectionInput from '../components/FormEntities/FormEntity/SelectionInput';
import TextBlock from '../components/FormEntities/FormEntity/TextBlock';
import ImageBlock from '../components/FormEntities/FormEntity/ImageBlock';
import PatternValidation from '../containers/vals_deps/PatternValidation';
import EnumerationValidation from '../containers/vals_deps/EnumerationValidation';
import SubjectInputValidation from '../containers/vals_deps/SubjectInputValidation';
import RangeValidation from '../containers/vals_deps/RangeValidation';
import NoOpValidation from '../containers/vals_deps/NoOpValidation';
import DependencyExpressionConfig from '../containers/vals_deps/DependencyExpressionConfig';
import AppliedValidatorConfig from '../containers/vals_deps/AppliedValidatorConfig';

export const FormEntityUI = props => {
  const { modelInstance } = props;

  const FragmentsEnum = {
    [EntityTypes.FormSection]: FormSection,
    [EntityTypes.TextArea]: TextArea,
    [EntityTypes.Checkbox]: Checkbox,
    [EntityTypes.TextInput]: TextInput2,
    // [EntityTypes.SelectionInput]: SelectionInput,
    // [EntityTypes.TextBlock]: TextBlock,
    // [EntityTypes.ImageBlock]: ImageBlock,
    // [EntityTypes.ASTextInput]: TextInput2,
    // [EntityTypes.EchoInput]: TextInput2,
    // [EntityTypes.CDSTextInput]: TextInput2,
    // [EntityTypes.RandomizationInput]: TextInput2,
  };
  // if (!FragmentsEnum[modelInstance])
  //   throw Error(`getFormEntityUI OOB: ${modelInstance}`);
  const Component = FragmentsEnum[props.model.type];
  console.log('Object.keys(props): ', props.model.type, props.model.uuid);
  return <Component {...props} />;
};

export const address = {
  whichCondition: modelInstance => {
    console.log({ modelInstance });

    if (modelInstance === 'AppliedValidator') {
      return AppliedValidatorConfig;
    } else if (modelInstance === 'CompositeCondition') {
      return DependencyExpressionConfig;
    }
  },
  mapExpression: modelInstance => {
    console.log({ modelInstance });

    if (modelInstance.class === 'PatternValidator') {
      return PatternValidation;
    } else if (modelInstance.class === 'EnumerationValidator') {
      return EnumerationValidation;
    } else if (modelInstance.class === 'NoOpValidator') {
      return NoOpValidation;
    } else if (modelInstance.class === 'RangeValidator') {
      return RangeValidation;
    } else if (modelInstance.class === 'SubjectInputValidator') {
      return SubjectInputValidation;
    } else if (modelInstance.type === 'DependencyExpression') {
      return DependencyExpressionConfig;
    } else if (modelInstance === undefined) {
      return <p style={{ color: 'red' }}>unknown type</p>;
    }
  },
};

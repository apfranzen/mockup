import { EntityTypes } from '../model/Types';

export const translate = {
  entityTypeToDescriptor: entityType => {
    const readable = {
      [EntityTypes.CDSTextInput]: 'CDS Text Input',
      [EntityTypes.TextInput]: 'Input Text',
      [EntityTypes.TextArea]: 'Input Text Area',
      [EntityTypes.Checkbox]: 'Checkbox',
      [EntityTypes.SelectionInput]: 'Selection Input',
      [EntityTypes.TextBlock]: 'Text Block',
      [EntityTypes.ImageBlock]: 'Image Block',
      [EntityTypes.ASTextInput]: 'Auto-suggest',
      [EntityTypes.FormSection]: 'Form Section',
      [EntityTypes.Form]: 'Form',
      [EntityTypes.EchoInput]: 'Echo Input',
      [EntityTypes.RandomizationInput]: 'Randomization Input',
    };
    return readable[entityType];
  },

  validationTypeDescriptor: type => {
    const readable = {
      PatternValidator: 'Pattern',
      NoOpValidator: 'Empty Field',
      EnumerationValidator: 'Enumeration',
      RangeValidator: 'Range',
      SubjectInputValidator: 'Subject Input',
    };
    console.log(readable[type]);
    return readable[type];
  },
};

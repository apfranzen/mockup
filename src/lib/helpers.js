import { address } from './address';
import { utility } from './utility';
import * as R from 'ramda';
import { EligibleDependents } from '../containers/_validations';

export const helpers = {
  traverseDependencies: (depObj, path) => {
    if (path.length < 1) {
      return depObj;
    } else {
      const popped = path.shift();
      console.log(depObj);
      return helpers.traverseDependencies(depObj.conditions[popped], path);
    }
  },
  findIndexWithAttr: (array, attr, value) => {
    for (var i = 0; i < array.length; i += 1) {
      if (array[i][attr] === value) {
        return i;
      }
    }
    return -1;
  },

  sortByPath: arrEntitiesWithPath => {
    return arrEntitiesWithPath.sort((a, b) => {
      const aVersions = a.path.split('.');
      const bVersions = b.path.split('.');
      const length = Math.max(aVersions.length, bVersions.length);
      for (let i = 0; i < length; i++) {
        if (!aVersions[i]) {
          return -1;
        }
        if (!bVersions[i]) {
          return 1;
        }
        if (aVersions[i] !== bVersions[i]) {
          return aVersions[i] - bVersions[i];
        }
      }
      return 0;
    });
  },

  entitiesByDomOrder: form => {
    // 1. decorate entire form
    const decoratedForm = Object.values(helpers.decoratePath(form));

    // 2. order that form by path
    return helpers.sortByPath(decoratedForm);
  },

  /**
   * should also filter out any non form inputs as well
   */
  priorIds: (form, targetId) => {
    // 1. decorate entire form
    const decoratedForm = Object.values(helpers.decoratePath(form)).map(
      formEntity => ({
        ...formEntity,
        path: formEntity.path,
      })
    );

    // 2. order that form
    const sortedForm = helpers.sortByPath(decoratedForm);

    // console.log(
    //   decoratedForm.filter((formEntity, index) =>
    //     formEntity.uuid === targetId ? index : null
    //   )
    // );

    const indexOfTarget = helpers.findIndexWithAttr(
      sortedForm,
      'uuid',
      targetId
    );
    // 3. some slice function.

    const result = sortedForm
      .slice(0, indexOfTarget)
      .filter(item => EligibleDependents.includes(item.type));
    // 4. return array to render from
    // const result2 = result.map(item => ({
    //   value: item.uuid,
    //   label: item.externalId,
    // }));

    return result.map(item => item.uuid);
  },

  decoratePath: (form, location = 0, pathPrefix = '0') => {
    // console.log({ pathPrefix, location });
    // return {};
    return {
      ...form,
      ...(form[location].hasOwnProperty('children')
        ? R.sort(
            (a, b) => a.row - b.row || a.column - b.column,
            R.map(id => form[id], form[location].children)
          ).reduce(
            (form, ent, index) =>
              helpers.decoratePath(form, ent.uuid, pathPrefix + '.' + index),
            form
          )
        : {}),
      ...{ [location]: R.assoc('path', pathPrefix, form[location]) },
    };
  },

  /**
   *
   * @param {Array} arrKeys array of keys
   * @param {Object} originalObject
   * @param {Object} output
   */
  returnOnly: (arrKeys, originalObject, output = {}) => {
    arrKeys.forEach(id => (output[id] = originalObject[id]));
    return output;
  },

  /**
   * @param {Number} oldIndex
   * @param {Number} newIndex
   * @param {Arrau} originalArray
   */
  reorderArray: (oldIndex, newIndex, originalArray) => {
    const movedItem = originalArray.filter((item, index) => index === oldIndex);
    const remainingItems = originalArray.filter(
      (item, index) => index !== oldIndex
    );

    const reorderedItems = [
      ...remainingItems.slice(0, newIndex),
      movedItem[0],
      ...remainingItems.slice(newIndex),
    ];

    return reorderedItems;
  },

  /**
   * given the provided subEntity, calc which number to begin event targets at
   * @param {Object} model - Model of the current entity
   * @param {string} currentEntity} - Current subEntity to calc what number to begin event target for
   * @returns {Number} num
   */
  calcStart: (model, currentEntity) => {
    if (currentEntity === 'prepend') {
      return 0;
    } else if (currentEntity === 'append') {
      return (
        model.prepend +
        (model.prePromptWidth || 0) +
        model.width +
        (model.postPromptWidth || 0)
      );
    } else if (currentEntity === 'prePrompt') {
      return model.prepend;
    } else if (currentEntity === 'FormInput') {
      return model.prepend + (model.prePromptWidth || 0);
    } else if (currentEntity === 'postPrompt') {
      return model.prepend + (model.prePromptWidth || 0) + model.width;
    }
  },
  /**
   * given the provided subEntity, calc which number to begin event targets at
   * @param {Object} model - Model of the current entity
   * @param {string} currentEntity} - Current subEntity to calc what number to begin event target for
   * @returns {Number} num
   */
  calcResizerColumn: (model, currentEntity) => {
    if (currentEntity === 'prepend') {
      return 0;
    } else if (currentEntity === 'append') {
      return (
        model.prepend +
        model.prePromptWidth +
        model.width +
        model.postPromptWidth
      );
    } else if (currentEntity === 'prePrompt') {
      return model.prepend + model.prePromptWidth;
    } else if (currentEntity === 'width') {
      return model.prepend + model.prePromptWidth + model.width;
    } else if (currentEntity === 'postPrompt') {
      return (
        model.prepend +
        model.prePromptWidth +
        model.width +
        model.postPromptWidth
      );
    }
  },
  widthAccessor: className => {
    switch (className) {
      case 'prepend':
        return 'prepend';
      case 'append':
        return 'append';
      case 'prePrompt':
        return 'prePromptWidth';
      case 'postPrompt':
        return 'postPromptWidth';
      default:
        return 'width';
    }
  },
  marginCalc: props => {
    const _margin = [0, 0, 0, 0];
    _margin[1] = props.model.append() > 0 ? 4 : 0;
    _margin[3] = props.model.prepend() > 0 ? 4 : 0;
    return _margin
      .map(el => `${el}px`)
      .toString()
      .replace(/,/g, ' ');
  },
  restoreDonorSiblingAddress: (arr, props) => {
    let draggedEntity = address.byPath(props.form, arr);

    // get donor's parent
    const donorParent = address.byPath(
      props.form,
      arr.slice(0, arr.length - 1)
    );
    const entitySelf = address.byPath(props.form, arr);
    // console.log(total(0, 8, 0))
    // console.log(typeof(entitySelf.prepend()), typeof(entitySelf.width()), typeof(entitySelf.append()))
    // console.log(total(entitySelf.prepend(), entitySelf.width(), entitySelf.append()),
    //   total(donorParent.prepend() + donorParent.width() + donorParent.append()))

    if (
      donorParent.children().length === 1 ||
      utility.total(
        entitySelf.prepend(),
        entitySelf.width(),
        entitySelf.append()
      ) ===
        utility.total(
          donorParent.prepend(),
          donorParent.width(),
          donorParent.append()
        )
    ) {
      console.log('entity being removed from formSection is the last child');
      return false;
    } else {
      console.log('donor formSection is not an empty nester');
      const toLeft = arr => {
        const _toLeft = [...arr];
        if (_toLeft[arr.length - 1] < 1) {
          return false;
        } else {
          _toLeft[arr.length - 1] = _toLeft[arr.length - 1] - 1;
          return {
            address: _toLeft,
            entity: address.byPath(props.form, _toLeft),
          };
        }
      };
      const toRight = arr => {
        const _toRight = [...arr];
        _toRight[arr.length - 1] = _toRight[arr.length - 1] + 1;
        return { address: arr, entity: address.byPath(props.form, _toRight) };
      };

      if (toLeft(arr)) {
        console.log(
          'previous entity exists, adding to append: ',
          toLeft(arr).address
        );
        return {
          address: toLeft(arr).address,
          properties: {
            append:
              toLeft(arr).entity.append() +
              draggedEntity.prepend() +
              draggedEntity.width() +
              draggedEntity.append(),
          },
        };
      } else {
        console.log(
          'no previous entity exists, adding to prepend, ',
          address.byPath(props.form, toRight(arr).address),
          {
            address: toRight(arr).address,
            properties: {
              prepend:
                toRight(arr).entity.prepend() +
                draggedEntity.prepend() +
                draggedEntity.width() +
                draggedEntity.append(),
            },
          }
        );
        return {
          address: toRight(arr).address,
          properties: {
            prepend:
              toRight(arr).entity.prepend() +
              draggedEntity.prepend() +
              draggedEntity.width() +
              draggedEntity.append(),
          },
        };
      }
    }
  },
};

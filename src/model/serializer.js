import * as R from 'ramda';
import { getEntitiesInRow } from '../redux-modules/formModelHelpers';
import { EntityTypes, isFormInput } from './Types';
import {
  traverseDependencyTree,
  getPrepend,
  getAppend,
  getParentId,
  nestProperties,
} from './serializer-helpers';
import { tabNameReconcile } from '../components/PropertyPanel/autoIdHelpers';
import db from '../db/db';
import { utility } from '../lib/utility';
import Dexie from 'dexie';

/**
 * Serializes redux state for CDART consumption by sending over the wire, or to export to allow the use  to save the JSON
 * @param {object} state
 */
export function serialize(form, formAttachedFiles) {
  return {
    'json-version': '2.0', // increment version from previous Form Designer
    content: {
      ...transformFormAtEntity(form, formAttachedFiles),
      metadata: {
        majorVersion: 1,
        minorVersion: 1,
        topLevelSections: null,
      },
      crf: {
        type: 'CRF',
        id: '',
        name: '',
        title: '',
        study: { type: 'Study', id: '', title: '', shortCode: '' },
      },
    },
  };
}

export const renameKeys = R.curry((keysMap, obj) =>
  R.reduce(
    (acc, key) => R.assoc(keysMap[key] || key, obj[key], acc),
    {},
    R.keys(obj)
  )
);

export const handleFormAttachedFiles = (entity, formAttachedFiles) => {
  if (entity.type === 'Form') {
    console.log({ formAttachedFiles });
    return {
      ...entity,
      formAttachedFiles,
    };
  } else {
    return entity;
  }
};

/**
 * handle transformations that require entire form context
 * @param {obj} form
 * @returns {obj} form
 */
export function transformFormAtEntity(form, formAttachedFiles) {
  // handle transformations for each entity
  console.log(form);

  const transformedFormArr = Object.values(form).map(entity =>
    R.pipe(
      entity => getLayout(entity, form),
      handlePrompts,
      entity => handleFormAttachedFiles(entity, formAttachedFiles),
      transformValidators,
      transformDepencies,
      handleDataType,
      entity => orderWithinFormSection(entity, form)
    )(entity)
  );
  console.log(transformedFormArr);
  // lastly perform form-wide scoped transformations
  return R.pipe(
    transformedFormArr =>
      tabNameReconcile(R.indexBy(R.prop('uuid'), transformedFormArr), true),
    transformedFormObj => nestRecursively(Object.values(transformedFormObj))
  )(transformedFormArr);
}

function transformValidators(entityObj) {
  if (entityObj.hasOwnProperty('validators') && entityObj.validators.length) {
    console.log(entityObj);

    console.log({
      ...entityObj,
      validators: entityObj.validators.map(validator => ({
        ...validator,
        class: 'edu.unc.tcrdms.model.form.validation.validators.'.concat(
          validator.class
        ),
        ...nestProperties(validator),
        ...(validator.hasOwnProperty('customFailureMessages') &&
          validator.customFailureMessages.length
          ? {
            customFailureMessages: validator.customFailureMessages.map(
              message => ({ [message.locale]: message.message })
            ),
          }
          : {}),
      })),
    });
    return {
      ...entityObj,
      validators: entityObj.validators.map(validator => ({
        ...validator,
        class: 'edu.unc.tcrdms.model.form.validation.validators.'.concat(
          validator.class
        ),
        // properties: [
        //   {
        //     name: 'pattern',
        //     value: 'pattern',
        //   },
        // ],
        ...nestProperties(validator),
        // 'custom-failure-messages': [
        //   {
        //     be_BY: 'this is the failure message',
        //   },
        // ],
        ...(validator.hasOwnProperty('customFailureMessages') &&
          validator.customFailureMessages.length
          ? {
            ['custom-failure-messages']: validator.customFailureMessages.map(
              message => ({ [message.locale]: message.message })
            ),
          }
          : {}),
      })),
    };
  } else return entityObj;
}

/**
 * transform dependencies to match CDART format
 */
function transformDepencies(entityObj) {
  console.log(
    entityObj.hasOwnProperty('dependencies') &&
    !entityObj.dependencies.conditions.length
  );
  if (
    entityObj.hasOwnProperty('dependencies') &&
    !entityObj.dependencies.conditions.length
  ) {
    console.log('no deps');

    return R.omit(['dependencies'], entityObj);
  }
  return R.evolve(
    {
      dependencies: deps => [traverseDependencyTree(deps)],
    },
    entityObj
  );
}

/**
 *  {id: 1, width: 24, prepend: 0} => {id: 1, layout: {width: 24, prepend: 0}}
 * @param {string} insertKey
 * @param {array} arrKeysToRemove
 * @param {object} entity object to update immutably
 */
const foldIntoKey = (insertKey, arrKeysToRemove, entity) => ({
  ...R.omit(arrKeysToRemove, entity),
  [insertKey]: R.pick(arrKeysToRemove, entity),
});

/**
 * Given an entity, return the layout key with values consistent with what CDART expects, i.e.
 * {
 *  column: x,
 *  row: y
 * }
 * return:
 * {
 *  layout: {
 *    prepend: x,
 *    append: y,
 *    width: z
 *  }
 * }
 * @param {object} entity
 * @param {object} form
 */
function getLayout(entity, form) {
  if (entity.type === 'Form') return entity;
  const id = entity.uuid;
  console.log(entity.type, 'topLevel' in entity && entity.topLevel);
  if ('topLevel' in entity && entity.topLevel) {
    return foldIntoKey('layout', ['prepend', 'width', 'append'], entity);
  }

  if (entity.type === EntityTypes.Form) {
    return entity;
  }
  console.log('here');

  //  1. get parent id
  const parentId = getParentId(form, id);

  // 2. get all entities in row
  const arrEntitiesInRow = getEntitiesInRow(form, parentId, entity.row);

  const parentEntity = form[parentId];

  return {
    ...R.omit(['row', 'column', 'width'], entity),
    layout: {
      prepend: getPrepend(entity, parentEntity, arrEntitiesInRow),
      width: entity.width,
      append: getAppend(entity, parentEntity, arrEntitiesInRow),
    },
    // existing Form Designer used property inputWidth instead of width, only for form inputs
    // ...(isFormInput(entity) ? { inputWidth: entity.width } : {}),
  };
}

const handlePrompts = entityObj =>
  isFormInput(entityObj)
    ? {
      ...R.omit(
        ['prePrompt', 'prePromptWidth', 'postPrompt', 'postPromptWidth'],
        entityObj
      ),
      prompts: {
        pre: {
          prompt: entityObj.prePrompt,
          promptPreWidth: entityObj.prePromptWidth,
        },
        post: {
          prompt: entityObj.postPrompt,
          promptPostWidth: entityObj.postPromptWidth,
        },
      },
    }
    : entityObj;

/**
 * handle arbitrary nested format for inputType
 */
function handleDataType(entityObj) {
  // remove all properties associated with inputType that need to be nested
  const nested = ['TextInput', 'SelectionInput', 'CDSTextInput'].includes(
    entityObj.type
  )
    ? {
      inputType: {
        ...(entityObj.hasOwnProperty('dataType')
          ? { dataType: entityObj.dataType }
          : {}),
        ...(entityObj.hasOwnProperty('precision') &&
          entityObj.precision !== ''
          ? { precision: entityObj.precision.join('') }
          : {}),
        ...(entityObj.hasOwnProperty('fixed')
          ? { fixed: entityObj.fixed }
          : {}),
        ...(entityObj.hasOwnProperty('timezone')
          ? { timezone: entityObj.timezone }
          : {}),
      },
    }
    : {};
  // timeZone and precision is oddly not nested within inputType in CDART format, so nest them
  return { ...R.omit(['precision', 'timezone'], entityObj), ...nested };
}

/**
 * takes a Form data structure in an array and nests children for CDART consumption
 * @param {array} objectsById
 * @returns {object} Form
 */
const nestRecursively = (formArr, rootIndex = 0) => {
  console.log({ formArr });
  const root = formArr[rootIndex];

  if ('children' in root) {
    return {
      ...R.omit(['children', 'uuid'], root),
      entities: root.children.map(id =>
        nestRecursively(formArr, formArr.map(e => e.uuid).indexOf(id))
      ),
    };
  } else {
    return R.omit(['uuid'], root);
  }
};

const orderWithinFormSection = (entity, objectsById) => {
  if (!('children' in entity)) {
    return entity;
  } else if ('children' in entity) {
    return {
      ...R.dissoc('children', entity),
      children: entity.children.sort(
        // sort the children by row and then by column
        (a, b) =>
          objectsById[a].row - objectsById[b].row ||
          objectsById[a].column - objectsById[b].column
      ),
    };
  }
};


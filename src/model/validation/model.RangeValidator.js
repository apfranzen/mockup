import AppliedValidator from './model.AppliedValidator';

/** Class represents a Validator */
export default class RangeValidator extends AppliedValidator {
  /**
   *
   * @param {string} properties.type
   * @param {boolean} properties.validState
   * @param {boolean} properties.strong
   * @param {boolean} properties.nullIsValid
   * @param {string} properties.id
   * @param {boolean} properties.maxInclusive
   * @param {boolean} properties.minInclusive
   * @param {number} properties.min
   * @param {string} properties.id
   * @param {Object} properties.properties
   */
  constructor(properties) {
    super(properties);
    this.validState = properties.validState || false;
    this.strong = properties.strong || false;
    this.nullIsValid = properties.nullIsValid || false;
    this.min = properties.min || 0;
    this.max = properties.max || 0;
    this.minInclusive = properties.minInclusive || false;
    this.maxInclusive = properties.maxInclusive || false;
    this.properties = properties.properties;
  }

  validState() {
    return this.validState;
  }

  strong() {
    return this.strong;
  }

  nullIsValid() {
    return this.nullIsValid;
  }

  id() {
    return this.id;
  }

  maxInclusive() {
    return this.maxInclusive;
  }

  minInclusive() {
    return this.minInclusive;
  }

  min() {
    return this.min;
  }

  max() {
    return this.max;
  }

  getProperties() {
    return this.properties;
  }

  properties() {
    return {
      type: this.type(),
      validState: this.validState(),
      strong: this.strong(),
      nullIsValid: this.nullIsValid(),
      id: this.id(),
      min: this.min(),
      max: this.max(),
      minInclusive: this.minInclusive(),
      maxInclusive: this.maxInclusive(),
      properties: this.getProperties(),
    };
  }
}

import AppliedValidator from './model.AppliedValidator';

/** Class represents a Validator */
export default class SubjectInputValidator extends AppliedValidator {
  /**
   *
   * @param {string} properties.type
   * @param {boolean} properties.validState
   * @param {boolean} properties.strong
   * @param {boolean} properties.nullIsValid
   * @param {string} properties.id
   * @param {string} properties.name
   * @param {Object} properties.properties
   */
  constructor(properties) {
    super(properties);
    this.type = 'SubjectInputValidator';
    this.validState = properties.validState;
    this.strong = properties.strong;
    this.nullIsValid = properties.nullIsValid;
    this.id = properties.id;
    this.name = properties.name;
    this.value = properties.value;
    this.properties = properties.properties;
  }

  name() {
    return this.name;
  }

  getProperties() {
    return this.properties;
  }

  type() {
    return this.type;
  }
  value() {
    return this.value;
  }

  properties() {
    return {
      type: this.type(),
      validState: this.validState(),
      strong: this.strong(),
      nullIsValid: this.nullIsValid(),
      id: this.id(),
      name: this.name(),
      value: this.value(),
      properties: this.getProperties(),
    };
  }
}

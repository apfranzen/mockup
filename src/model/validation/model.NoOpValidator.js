import AppliedValidator from './model.AppliedValidator';

/** Class represents a Validator */
export default class NoOpValidator extends AppliedValidator {
  /**
   *
   * @param {string} properties.type
   * @param {boolean} properties.validState
   * @param {boolean} properties.strong
   * @param {boolean} properties.nullIsValid
   * @param {string} properties.id
  //  * @param {string} properties.name
   */
  constructor(properties) {
    super(properties);
    this.type = 'NoOpValidator';
    this.value = properties.value;
  }

  name() {
    return this.name;
  }

  value() {
    return this.value;
  }
  properties() {
    return {
      type: this.type(),
      validState: this.validState(),
      strong: this.strong(),
      nullIsValid: this.nullIsValid(),
      id: this.id(),
      name: this.name(),
      value: this.value(),
    };
  }
}

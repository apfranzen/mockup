/** Class represents a Validator */
export default class Validator {
    /**
     *
     * @param {string} properties.type
     */
    constructor(properties) {
        // console.log(properties);
        this.type = properties.type;
    }
    // Getter
    type() {
        return this.type;
    }

    /**
     *
     * Returns public properties of a Validator.
     * @return {object}
     * @memberof Validator
     */
    properties() {
        return {
            type: this.type(),
        };
    }
}

import Validator from './model.Validator';
import { operators } from '../../containers/_validations';

/** Class represents a Validator */
// class DependencyExpression extends Validator {
export default class DependencyExpression {
  /**
   *
   * @param {string} properties.type
   * @param {string} properties.operator
   * @param {array} properties.conditions
   */
  constructor(properties = {}) {
    this.type = 'DependencyExpression';
    this.operator = properties.operator || operators.ALL;
    this.conditions = properties.conditions || [];
  }

  operator() {
    return this.operator;
  }

  conditions() {
    return this.conditions;
  }

  properties() {
    return {
      type: this.type(),
      operator: this.operator(),
      conditions: this.conditions(),
    };
  }
}

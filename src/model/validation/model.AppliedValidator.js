import Validator from './model.Validator';

/** Class represents a Validator */
export default class AppliedValidator extends Validator {
  /**
   *
   * @param {string} properties.type
   * @param {boolean} properties.validState
   * @param {boolean} properties.strong
   * @param {boolean} properties.nullIsValid
   * @param {string} properties.id
   * @param {Array} properties.customFailureMessages
   */
  constructor(properties) {
    super(properties);

    this.type = 'AppliedValidator';
    this.validState = properties.validState;
    this.strong = properties.strong;
    this.nullIsValid = properties.nullIsValid;

    /** dependency & remote properties */
    this.remote = properties.remote;
    this['form-name'] = properties['form-name'];
    this.version = properties.version;
    this.versionId = properties.versionId;
    this.entityType = properties.entityType;
    this.inputType = properties.inputType;
    this.internalId = properties.internalId;
    this.occurrence = properties.occurrence;
    this.occurrenceNumber = properties.occurrenceNumber;
    this.eventDef = properties.eventDef;
    this.class = properties.class;
    this.customFailureMessages =
      properties.hasOwnProperty(this.customFailureMessages) &&
      properties.customFailureMessages.length
        ? properties.customFailureMessages.filter(
            message => message.locale && message.message
          )
        : [];
  }

  customFailureMessages() {
    return this.customFailureMessages;
  }

  validState() {
    return this.validState;
  }

  strong() {
    return this.strong;
  }

  nullIsValid() {
    return this.nullIsValid;
  }

  id() {
    return this.id;
  }

  remote() {
    return this.remote;
  }
  formName() {
    return this.formName;
  }
  entityType() {
    return this.entityType;
  }
  inputType() {
    return this.inputType;
  }
  internalId() {
    return this.internalId;
  }
  occurrence() {
    return this.occurrence;
  }
  occurrenceNumber() {
    return this.occurrenceNumber;
  }
  eventDef() {
    return this.eventDef;
  }
  class() {
    return this.class;
  }

  properties() {
    return {
      uuid: this.uuid(),
      type: this.type(),
      customFailureMessages: this.customFailureMessages(),
      validState: this.validState(),
      strong: this.strong(),
      nullIsValid: this.nullIsValid(),
      id: this.id(),
    };
  }
}

export enum EntityTypes {
  FormSection = 'FormSection',
  TextInput = 'TextInput',
  SelectionInput = 'SelectionInput',
  TextArea = 'TextArea',
  Checkbox = 'Checkbox',
  CDSTextInput = 'CDSTextInput',
  TextBlock = 'TextBlock',
  ImageBlock = 'ImageBlock',
  ASTextInput = 'ASTextInput',
  EchoInput = 'EchoInput',
  RandomizationInput = 'RandomizationInput',
  Form = 'Form',
}

export enum FormInputTypes {
  TextInput = 'TextInput',
  TextArea = 'TextArea',
  Checkbox = 'Checkbox',
  SelectionInput = 'SelectionInput',
  ASTextInput = 'ASTextInput',
  CDSTextInput = 'CDSTextInput',
  EchoInput = 'EchoInput',
  RandomizationInput = 'RandomizationInput',
}

/**
 * helper function to filter formInputs
 * @param {object} entity
 */
export const isFormInput = entity =>
  Object.keys(FormInputTypes).includes(entity.type);

export const PromptModes = {
  prePrompt: 'prePrompt',
  postPrompt: 'postPrompt',
};

export const RenderModes = {
  radio: 'radio',
  menu: 'menu',
};

export const InputTypes = {
  string: 'string',
  integer: 'integer',
  float: 'float',
  number: 'number',
  boolean: 'boolean',
  null: 'null',
};

export const ResizeTypes = {
  width: 'width', // width of input
  postPromptWidth: 'postPromptWidth', // (optional - only applicable to FormInputs)
  prePromptWidth: 'prePromptWidth', // (optional - only applicable to FormInputs)
};

export const DataType = {
  string: 'string',
  date: 'date',
  integer: 'integer',
  float: 'float',
};

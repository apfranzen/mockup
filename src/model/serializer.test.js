import { serialize } from './serializer';
import { formSeed } from '../redux-modules/reducer';
/**
 - testing -
  - describe the behavior
  - serialize
    - 0 entities
    - 1 entities
      - passes all of the attributes of
        - pass arbitrary key value pairs - conveys attributes from entity
    - (line 20) assure children property is removed
    - parent child/parent relationships
      - can nest arbitrarily deep

 */

const baseObject = {
  'json-version': '2.0',
  content: expect.any(Object),
};

describe('the serialize method', () => {
  xit('returns an Object', () => {
    expect(serialize({ form: { '0': {} } })).toBeInstanceOf(Object);
  });

  xit('returns JSON version 1.0 & content', () => {
    expect(serialize({ form: { '0': {} } })).toMatchObject(baseObject);
  });

  xit('conveys all attributes from an entity', () => {
    const attributes = { someProperty: '', secondProperty: '' };
    const appState = { form: { '0': attributes } };

    expect(serialize(appState).content).toMatchObject(attributes);
  });

  xit('returns 1 entity when passed 1 entity', () => {
    const sampleAttributes = { app: {}, form: { '0': { children: [] } } };
    expect(serialize(sampleAttributes).content.entities.length).toEqual(0);
  });

  xit('returns 2 entities when passed 2 entities', () => {
    const sampleAttributes = {
      app: {},
      form: { '0': { children: ['1'] }, '1': {} },
    };
    expect(serialize(sampleAttributes).content.entities.length).toEqual(1);
  });

  it('can return an arbitrarily nested object', () => {
    const sampleAttributes = {
      app: {},
      form: {
        '0': {
          foo: 'a',
          children: ['1'],
        },
        '1': {
          foo: 'b',
          children: ['2'],
        },
        '2': {
          foo: 'c',
          children: ['3'],
        },
        '3': { foo: 'd' },
      },
    };

    console.log(serialize(sampleAttributes));
    let result = serialize(sampleAttributes).content;
    expect(result.foo).toEqual('a');
    result = result.entities[0];
    expect(result.foo).toEqual('b');
    result = result.entities[0];
    expect(result.foo).toEqual('c');
    result = result.entities[0];
    expect(result.foo).toEqual('d');
  });
});

import * as R from 'ramda';
import { utility } from '../lib/utility';
import { helpers } from '../lib/helpers';
/**
 *
 * @param {object} form
 * @param {numer} id
 */
export function getParentId(form, id) {
  const parentIds = Object.keys(form).filter(
    parent => 'children' in form[parent]
  );
  return parentIds.filter(parent => form[parent].children.includes(id))[0];
}

/**
 *
 * @param {object} entity
 * @param {object} parentEntity
 * @param {array} arrEntitiesInRow array of all entities in row of entity
 * @returns {number}
 */
export function getPrepend(entity, parentEntity, arrEntitiesInRow) {
  if (arrEntitiesInRow.map(e => e.uuid).indexOf(entity.uuid) === 0) {
    return entity.column - parentEntity.column;
  } else {
    const entityIdToLeft =
      arrEntitiesInRow.map(e => e.uuid).indexOf(entity.uuid) - 1;
    return (
      entity.column -
      (arrEntitiesInRow[entityIdToLeft].column +
        utility.total(arrEntitiesInRow[entityIdToLeft]))
    );
  }
}

/**
 *
 * @param {object} entity
 * @param {object} parentEntity
 * @param {array} arrEntitiesInRow array of all entities in row of entity
 * @returns {number}
 */
export function getAppend(entity, parentEntity, arrEntitiesInRow) {
  if (
    arrEntitiesInRow.map(e => e.uuid).indexOf(entity.uuid) ===
    arrEntitiesInRow.length - 1
  ) {
    // if last in row
    return (
      utility.total(parentEntity) - (entity.column + utility.total(entity)) + 1
    );
  } else {
    // any other possibility
    const entityIdToRight =
      arrEntitiesInRow.map(e => e.uuid).indexOf(entity.uuid) + 1;

    return (
      arrEntitiesInRow[entityIdToRight].column -
      (entity.column + utility.total(entity))
    );
  }
}

/** match existing form designer serialization data structure */
export function nestProperties(dependencyObj) {
  if (dependencyObj.class === 'RangeValidator') {
    return {
      properties: [
        {
          name: 'min',
          value: dependencyObj.min,
        },
        {
          name: 'max',
          value: dependencyObj.max,
        },
        {
          name: 'min-inclusive',
          value: dependencyObj.minInclusive,
        },
        {
          name: 'max-inclusive',
          value: dependencyObj.maxInclusive,
        },
      ],
    };
  } else if (dependencyObj.class === 'PatternValidator') {
    return {
      properties: [
        {
          name: 'pattern',
          value: dependencyObj.value,
        },
      ],
    };
  } else if (dependencyObj.class === 'EnumerationValidator') {
    return {
      properties: [
        {
          name: dependencyObj.value,
          value: dependencyObj.value,
        },
      ],
    };
  }
}

export function traverseDependencyTree(dependenciesObj) {
  if (dependenciesObj.type === 'AppliedValidator') {
    const dependencies = {
      ...dependenciesObj,
      class: 'edu.unc.tcrdms.model.form.validation.validators.'.concat(
        dependenciesObj.class
      ),
      ...([
        'RangeValidator',
        'PatternValidator',
        'EnumerationValidator',
      ].includes(dependenciesObj.class)
        ? nestProperties(dependenciesObj)
        : {}),
    };

    return R.omit(['formName'], dependencies);
  }
  // transform nested conditions of DependencyExpression
  return {
    ...dependenciesObj,
    conditions: dependenciesObj.conditions.map(condition =>
      traverseDependencyTree(condition)
    ),
  };
}

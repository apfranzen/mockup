import * as R from 'ramda';
import { hasChildren } from './deserializer.utils';
import { isFormInput } from './Types';
import { utility } from '../lib/utility';
import { renameKeys } from './serializer';
import db from '../db/db.js';
import { addFiles } from '../redux-modules/actions';
// import { dispatch } from 'redux';

const uuidv4 = require('uuid/v4');

/**
 * const orig = {
  prePrompt: 3,
  width: 8,
  postPrompt: 5
}

const result = {
  column: 3,
  width: 8,
}


go through and add a row to each entity
accummulate
 */
export function test() {
  return null;
}
/*
  deserialize a JSON string
  must support the following cases:
    json-version: 2.0
      minimal transformation required. Essentially loading straight into redux state
    json-version: 1.0
      reverse transformations performed in serializer must be performed here, taking serialized json and mapping into the correct format for Form Designer 2.0
*/

export function deserialize(formString) {
  return JSON.parse(formString);
}

/**
 *
 * @param {array} items
 */
// export function flatten(items) {
//   // return R.reduce(
//   //   getChildren,
//   //   [],
//   //   items.entities instanceof Array ? items.entities : [items.entities]
//   // );

//   return items.entities instanceof Array
//     ? items.entities
//     : [items.entities].reduce(
//         (acc, item, index) => getChildren(acc, item, index),
//         []
//       );

//   function getChildren(acc, item, index) {
//     if (item.entities && item.entities.length > 0) {
//       return item.entities.reduce((acc, item) => {
//         console.log({ acc });
//         return getChildren(acc, item);
//       }, acc.concat(item));
//       // R.reduce(getChildren, acc.concat(item), item.entities);
//     } else {
//       return acc.concat(item);
//     }
//   }
// }

export const flatten = obj => {
  console.log(obj);
  const array = Array.isArray(obj) ? obj : [obj];

  return array.reduce((acc, value, index) => {
    console.log(value.entities);

    const mapthese = Array.isArray(value.entities)
      ? value.entities
      : [value.entities];
   // if internalId exists, use provided internalId - else generate new
    var uuids = value.entities ? mapthese.map(item => item.hasOwnProperty('internalId') ? item.internalId : uuidv4()) : [];
    // var uuids = value.entities ? mapthese.map(item => item.hasOwnProperty('internalId') ? item.internalId : uuidv4()) : [];
    // add current entity to accumulator
    if (value.type)
      acc.push({ ...value, ...(value.entities ? { entities: uuids } : {}) });
    if (value.entities) {
      // call flatten on each child
      acc = acc.concat(
        flatten(
          mapthese.map((item, index) => ({ ...item, uuid: uuids[index] }))
        )
      );
    }
    return acc;
  }, []);
};

// export const flatten = obj => {
//   return R.pipe(
//     flattenAndAssignId,
//     giveFormId
//   )(obj);
// };
export const giveFormId = entity => {
  if (entity.type === 'Form') {
    return { ...entity, uuid: 0 };
  } else {
    return entity;
  }
};

export function getById(entity) {
  if (hasChildren(entity)) {
    return {
      ...entity,
      entities: entity.entities.map(entity => entity.uuid),
    };
  } else {
    return entity;
  }
}

const getParentEntity = (id, formArr) => {
  return formArr.filter(
    entity => entity.hasOwnProperty('children') && entity.children.includes(id)
  )[0];
};

const getEntity = (id, formArr) => {
  return formArr.filter(item => (item.uuid === id ? item : null))[0];
};

const getTotal = entity => {
  const { prepend, append, width, prePromptWidth, postPromptWidth } = entity;
  console.log(entity.type, entity.text, prepend + append + width);
  if (entity.type === 'CDSTextInput') {
    console.log(entity);
  }
  if (entity.type === 'Form') return 24;
  if (entity.hasOwnProperty('prePromptWidth')) {
    return prepend + append + width + prePromptWidth + postPromptWidth;
  } else {
    console.log(entity.type);
    return prepend + append + width;
  }
};

export const getLayout = form => {
  let gridCount = 1;
  let rowCount = 1;
  return form.map(targetEntity => {
    if (targetEntity.type === 'Form') return targetEntity;
    console.log(getTotal(getParentEntity(targetEntity.uuid, form)));
    let parentWidth = getTotal(getParentEntity(targetEntity.uuid, form)) +1;
    const getRow = (entityId, form, index, parentEntity) => {
      if (index === 0) {
        console.log(parentWidth, getTotal(getEntity(entityId, form)) + 1)
        gridCount = getTotal(getEntity(entityId, form)) + 1;
        rowCount = 1;
        return {
          ...getEntity(entityId, form),
          row: 1,
          column: getEntity(entityId, form).prepend +1,
        };
      } else {
        var proposedGridCount = gridCount + getTotal(getEntity(entityId, form)) ;
        console.log(getEntity(entityId, form).uuid, {entity: getEntity(entityId, form), getTotal: getTotal(getEntity(entityId, form)), proposedGridCount, parentWidth, 'same row': proposedGridCount <= parentWidth, rowCount})

        // if (proposedGridCount <= parentWidth) {
        if (proposedGridCount < parentWidth) {
          const result = {
            ...getEntity(entityId, form),
            row: rowCount,
            column: gridCount + getEntity(entityId, form).prepend + 1,
          };
          gridCount = proposedGridCount;

          return result;
        } else {
          gridCount = getTotal(getEntity(entityId, form));
         rowCount++

          return {
            ...getEntity(entityId, form),
            row: rowCount,
            column: getEntity(entityId, form).prepend + 1,
          };
        }
      }
    };
    const parentEntity = getParentEntity(targetEntity.uuid, form);

    const result = parentEntity.children
      .map((entityId, index) => getRow(entityId, form, index, parentEntity))
      .filter(entity => entity.uuid === targetEntity.uuid)[0];
    return result;
  });
};

export function finalization(formArr) {
  return utility.arrayToObject(formArr, 'uuid');
}

export function convertDateTextInputToTextInput(formEntity) {
  if (formEntity.type === 'DateTextInput') {
    return {
      ...formEntity,
      type: 'TextInput',
    };
  } else {
    return formEntity;
  }
}

export function convertRandomizationInputToCDSTextInput(formEntity) {
  if (formEntity.type === 'RandomizationInput') {
    return {
      ...formEntity,
      type: 'CDSTextInput',
    };
  } else {
    return formEntity;
  }
}

export function handleFormAttachedFiles(formEntity) {
  if (formEntity.type === 'Form') {
    console.log('handleFormAttachedFile');

const {formAttachedFiles} = formEntity

    const objectToArray = (object) =>
      Object.keys(object).map(uuid => ({ ...object[uuid], uuid, fileContent: object[uuid].fileContent }))
    db.table('files').clear().then(
      objectToArray(formAttachedFiles).forEach(file => db.table('files')
        .add(file))
    )
      // dispatch(addFiles({...fileToAdd}))
    const addToDb = (fileToAdd) => {


      db.table('files')
        .add(fileToAdd)
        // .then(id => {
        //   // console.log(id);
        //   return id
        //   // dispatch(loadFiles());
        // })

    }
console.log(formAttachedFiles);




    return  R.omit(['formAttachedFiles'], formEntity)
  } else return formEntity
}

export function handleValidators(entityObj) {


  if (entityObj.hasOwnProperty('validators') && entityObj.validators.length){
    const result = {
      ...entityObj,
      validators: entityObj.validators.map(validator => ({
        ...validator,
        class: validator.class.split('.validators.')[1],

        ...(validator.hasOwnProperty('custom-failure-messages') &&
          validator['custom-failure-messages'].length
          ? {
            customFailureMessages: validator['custom-failure-messages'].map(
              message => ({ local: Object.keys(message)[0], message: Object.values(message)[0] })
            ),
          }
          : {}),
      })),
    }
    console.log(result);

return result
  } else {
    return entityObj
  }
}
export function handleDependencies(entityObj) {

  if (entityObj.type === 'TextInput' && entityObj.hasOwnProperty('dependencies')) {
    console.log(entityObj.dependencies[0])
  // if (entityObj.hasOwnProperty('dependencies') ) {
    /*
    [{
        "type": "DependencyExpression",
        "operator": "OR",
        "conditions": [{
          "input-index": "FORM8219914",
          "remote": false,
          "properties": [{
            "name": "pattern",
            "value": "ghj"
          }],
          "type": "AppliedValidator",
          "class": "edu.unc.tcrdms.model.form.validation.validators.PatternValidator",
          "validState": true,
          "strong": false,
          "nullIsValid": true
        }]
      }]
    */

    // conditions: [{ … }]
    // operator: "OR"
    // type: "DependencyExpression"
    return {
      ...entityObj, dependencies: {conditions: entityObj.dependencies[0].conditions.map(cond => ({...cond, class: cond.class.split('.validators.')[1], value: cond.properties[0].value}))}
    }
    return entityObj
  } else if (entityObj.type === 'TextInput' && !entityObj.hasOwnProperty('dependencies')) {return ({...entityObj, dependencies: {conditions: []}})} else return entityObj
}

/*
export function identifyFormTabs(entity) {
  if (entity.type !=='FormSection') {
    return entity
  }
}
*/
export function identifyFormTabs(formArr) {
 const FormChildren = formArr[0].children
return formArr.map(entity => FormChildren.includes(entity.uuid) ? ({...entity, topLevel: true}): entity)

}
export function transformEntities(cdartObject) {
  console.log(cdartObject);

  const contentToEntities = renameKeys({ content: 'entities' }, cdartObject);
  const flattened = flatten(contentToEntities);
  console.log(flattened);
  const entityLevelTransformations = flattened.map(entity =>
    R.pipe(

      convertDateTextInputToTextInput,
      convertRandomizationInputToCDSTextInput,
      transformLayoutProps,
      giveFormId,
      handleValidators,
      handleDependencies,
      handleFormAttachedFiles,
      entity => renameKeys({ entities: 'children' }, entity)
    )(entity)
  );

  return R.pipe(
    getLayout,
    identifyFormTabs,
    finalization
  )(entityLevelTransformations);
}

export function transformLayoutProps(formEntity) {
  if (formEntity.type === 'Form') {
    return formEntity;
  } else if (formEntity.type === 'FormSection') {
    return {
      ...formEntity,
      width: 24,
      prepend: 0,
      append: 0,
    };
  } else if (isFormInput(formEntity)) {
    const {
      prompts: {
        pre: { promptPreWidth },
        post: { promptPostWidth },
      },
      layout: { prepend, append, width },
      inputWidth,
    } = formEntity;
    return {
      ...R.omit(['prompts', 'layout'], formEntity),
      width,
      prePromptWidth: promptPreWidth,
      postPromptWidth: promptPostWidth,
      prePrompt: formEntity.prompts.pre.prompt,
      postPrompt: formEntity.prompts.post.prompt,
      prepend,
      append,
    };
  } else {
    return {
      ...R.omit(['prompts', 'layout'], formEntity),
      width: formEntity.layout.width,
      prepend: formEntity.layout.prepend,
      append: formEntity.layout.append,
    };
  }
}

export const cdartSampleInput = {
  'json-version': '1.0',
  entities: {
    type: 'Form',
    allowEventAttachedFiles: false,
    autoIds: {
      prefix: 'FORM',
      enabled: false,
      separator: null,
    },
    existingName: 'aa',
    existingId: null,
    entities: [
      {
        type: 'FormSection',
        id: 1,
        legend: 'Tab',
        layout: {
          width: 24,
        },
        // topLevel: true,
        entities: [
          {
            type: 'FormSection',
            id: 2,
            legend: 'Tab',
            layout: {
              width: 24,
            },
            entities: [
              {
                doubleEntry: false,
                maxLength: 80,
                defaultText: '',
                autoNumberRule: 'N+',
                inputWidth: 4,
                type: 'TextInput',
                id: 3,
                index: 'FORM23896131',
                tabOrder: 1,
                sasCodeLabel: '',
                autoTab: false,
                name: '',
                cloneId: 0,
                inputType: {
                  dataType: 'string',
                },
                prompts: {
                  pre: {
                    prompt: '',
                    promptPreWidth: 2,
                  },
                  post: {
                    prompt: '',
                    promptPostWidth: 2,
                  },
                },
                externalIdentifier: 'FORM75976131',
                layout: {
                  width: 19,
                  prepend: 0,
                  append: 0,
                },
              },
              {
                doubleEntry: false,
                maxLength: 80,
                defaultText: '',
                autoNumberRule: 'N+',
                inputWidth: 4,
                type: 'TextInput',
                id: 3,
                tabOrder: 1,
                sasCodeLabel: '',
                autoTab: false,
                name: '',
                cloneId: 0,
                inputType: {
                  dataType: 'string',
                },
                prompts: {
                  pre: {
                    prompt: '',
                    promptPreWidth: 0,
                  },
                  post: {
                    prompt: '',
                    promptPostWidth: 0,
                  },
                },
                layout: {
                  width: 10,
                  prepend: 0,
                  append: 0,
                },
              },
              {
                type: 'DateTextInput',
                layout: {
                  width: 7,
                  prepend: 1,
                  append: 0,
                },
                name: '',
                index: 'BST0a',
                sasCodeLabel: 'Shipment date',
                cloneId: 0,
                tabOrder: 1,
                inputWidth: 3,
                autoNumberRule: 'N,L+',
                timeZoneChoice: false,
                inputType: {
                  dataType: 'date',
                  fixed: false,
                  precision: 'ymd',
                  timezone: 'Unknown/Unknown',
                },
                prompts: {
                  pre: {
                    prompt: 'Shipment Date',
                    promptPreWidth: 4,
                  },
                  post: {
                    prompt: '',
                    promptPostWidth: 0,
                  },
                },
              },
            ],
          },
        ],
      },
    ],
    metadata: {
      majorVersion: 1,
      minorVersion: 1,
      topLevelSections: null,
    },
    formAttachedFiles: {},
    QbyQ: {},
  },
};

export const cdartSampleInput2 = {
  "json-version": "1.0",
  "content": {
    "type": "Form",
    "allowEventAttachedFiles": false,
    "autoIds": {
      "enabled": true,
      "separator": ".",
      "prefix": "BST"
    },
    "formAttachedFiles": {
    },
    "QbyQ": {
    },
    "metadata": {
      "majorVersion": 1,
      "description": "1.0",
      "topLevelSections": "tabs"
    },
    "entities": [{
      "type": "FormSection",
      "layout": {
        "width": 24,
        "prepend": 0,
        "append": 0
      },
      "legend": "BST 0a-",
      "entities": [{
        "type": "TextBlock",
        "layout": {
          "width": 7,
          "prepend": 17,
          "append": 0
        },
        "text": "Click here to open the QxQ for this form"
      }, {
        "type": "TextBlock",
        "layout": {
          "width": 6,
          "prepend": 1,
          "append": 17
        },
        "text": "**ADMINISTRATIVE INFORMATION**"
      }, {
        "type": "DateTextInput",
        "layout": {
          "width": 7,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST0a",
        "sasCodeLabel": "Shipment date",
        "cloneId": 0,
        "tabOrder": 1,
        "inputWidth": 3,
        "autoNumberRule": "N,L+",
        "timeZoneChoice": false,
        "inputType": {
          "dataType": "date",
          "fixed": false,
          "precision": "ymd",
          "timezone": "Unknown/Unknown"
        },
        "prompts": {
          "pre": {
            "prompt": "Shipment Date",
            "promptPreWidth": 4
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        },
        "validators": [{
          "type": "AppliedValidator",
          "class": "edu.unc.tcrdms.model.form.validation.validators.DateRangeValidator",
          "properties": [{
            "name": "max-inclusive",
            "value": "true"
          }, {
            "name": "min",
            "value": "20170301T======-Unknown/Unknown"
          }, {
            "name": "min-inclusive",
            "value": "true"
          }, {
            "name": "max",
            "value": "$TODAY"
          }],
          "validState": true,
          "strong": false,
          "nullIsValid": true
        }]
      }, {
        "type": "TextInput",
        "layout": {
          "width": 5,
          "prepend": 1,
          "append": 10
        },
        "name": "",
        "index": "BST0b",
        "sasCodeLabel": "Staff ID",
        "cloneId": 0,
        "tabOrder": 2,
        "inputWidth": 2,
        "autoNumberRule": "N,L+",
        "maxLength": 3,
        "contents": "",
        "doubleEntry": false,
        "autoTab": false,
        "inputType": {
          "dataType": "string"
        },
        "prompts": {
          "pre": {
            "prompt": "Staff Initials",
            "promptPreWidth": 3
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        },
        "validators": [{
          "type": "AppliedValidator",
          "class": "edu.unc.tcrdms.model.form.validation.validators.PatternValidator",
          "properties": [{
            "name": "pattern",
            "value": "^([A-Z]{2,3})$"
          }],
          "custom-failure-messages": [{
            "en_US": "Enter Staff Initials using (2- 3) capital letters."
          }],
          "validState": true,
          "strong": false,
          "nullIsValid": true
        }]
      }, {
        "type": "TextBlock",
        "layout": {
          "width": 22,
          "prepend": 1,
          "append": 1
        },
        "text": "***Instructions:*** Use this form to mark medication bottles as received or damaged.  This information is used to determine bottles available for assignment to subject.  This form is grouped by pharmacy shipment (EVENT number).  To find the correct EVENT number, browse to the latest EVENT for new shipments, or use the Bottle Inventory Report to find bottles from previous shipments.\n**Note: Uncheck column b for bottles NOT received.  Mark column c for damaged bottles.**"
      }, {
        "type": "TextBlock",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "text": "**a. Bottle ID**"
      }, {
        "type": "TextBlock",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "text": "**b. Received**"
      }, {
        "type": "TextBlock",
        "layout": {
          "width": 3,
          "prepend": 0,
          "append": 13
        },
        "text": "**c. Damaged**"
      }, {
        "type": "TextInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST1a",
        "sasCodeLabel": "Bottle 1 bottle ID",
        "cloneId": 0,
        "tabOrder": 3,
        "inputWidth": 3,
        "autoNumberRule": "N+,L+",
        "maxLength": 8,
        "contents": "",
        "doubleEntry": false,
        "autoTab": false,
        "inputType": {
          "dataType": "string"
        },
        "prompts": {
          "pre": {
            "prompt": "1a.",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "Checkbox",
        "layout": {
          "width": 1,
          "prepend": 1,
          "append": 2
        },
        "name": "",
        "index": "BST1b",
        "sasCodeLabel": "Bottle 1 received",
        "cloneId": 0,
        "tabOrder": 4,
        "inputWidth": 1,
        "autoNumberRule": "N,L+",
        "defaultState": true,
        "inputType": {
          "dataType": "boolean"
        },
        "prompts": {
          "pre": {
            "prompt": "1b.",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "Checkbox",
        "layout": {
          "width": 1,
          "prepend": 0,
          "append": 15
        },
        "name": "",
        "index": "BST1c",
        "sasCodeLabel": "Bottle 1 damaged",
        "cloneId": 0,
        "tabOrder": 5,
        "inputWidth": 1,
        "autoNumberRule": "N,L+",
        "defaultState": false,
        "inputType": {
          "dataType": "boolean"
        },
        "prompts": {
          "pre": {
            "prompt": "1c.",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "TextInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST2a",
        "sasCodeLabel": "Bottle 2 bottle ID",
        "cloneId": 0,
        "tabOrder": 6,
        "inputWidth": 3,
        "autoNumberRule": "N+,L+",
        "maxLength": 8,
        "contents": "",
        "doubleEntry": false,
        "autoTab": false,
        "inputType": {
          "dataType": "string"
        },
        "prompts": {
          "pre": {
            "prompt": "2a.",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "Checkbox",
        "layout": {
          "width": 1,
          "prepend": 1,
          "append": 2
        },
        "name": "",
        "index": "BST2b",
        "sasCodeLabel": "Bottle 2 received",
        "cloneId": 0,
        "tabOrder": 7,
        "inputWidth": 1,
        "autoNumberRule": "N,L+",
        "defaultState": true,
        "inputType": {
          "dataType": "boolean"
        },
        "prompts": {
          "pre": {
            "prompt": "2b.",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "Checkbox",
        "layout": {
          "width": 1,
          "prepend": 0,
          "append": 15
        },
        "name": "",
        "index": "BST2c",
        "sasCodeLabel": "Bottle 2 damaged",
        "cloneId": 0,
        "tabOrder": 8,
        "inputWidth": 1,
        "autoNumberRule": "N,L+",
        "defaultState": false,
        "inputType": {
          "dataType": "boolean"
        },
        "prompts": {
          "pre": {
            "prompt": "2c.",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "TextInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST3a",
        "sasCodeLabel": "Bottle 3 bottle ID",
        "cloneId": 0,
        "tabOrder": 9,
        "inputWidth": 3,
        "autoNumberRule": "N+,L+",
        "maxLength": 8,
        "contents": "",
        "doubleEntry": false,
        "autoTab": false,
        "inputType": {
          "dataType": "string"
        },
        "prompts": {
          "pre": {
            "prompt": "3a.",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "Checkbox",
        "layout": {
          "width": 1,
          "prepend": 1,
          "append": 2
        },
        "name": "",
        "index": "BST3b",
        "sasCodeLabel": "Bottle 3 received",
        "cloneId": 0,
        "tabOrder": 10,
        "inputWidth": 1,
        "autoNumberRule": "N,L+",
        "defaultState": true,
        "inputType": {
          "dataType": "boolean"
        },
        "prompts": {
          "pre": {
            "prompt": "3b.",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "Checkbox",
        "layout": {
          "width": 1,
          "prepend": 0,
          "append": 15
        },
        "name": "",
        "index": "BST3c",
        "sasCodeLabel": "Bottle 3 damaged",
        "cloneId": 0,
        "tabOrder": 11,
        "inputWidth": 1,
        "autoNumberRule": "N,L+",
        "defaultState": false,
        "inputType": {
          "dataType": "boolean"
        },
        "prompts": {
          "pre": {
            "prompt": "3c.",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "TextBlock",
        "layout": {
          "width": 22,
          "prepend": 1,
          "append": 1
        },
        "text": "***Instructions:*** Use this form to mark medication bottles as received or damaged.  This information is used to determine bottles available for assignment to subject.  This form is grouped by pharmacy shipment (EVENT number).  To find the correct EVENT number, browse to the latest EVENT for new shipments, or use the Bottle Inventory Report to find bottles from previous shipments.\n**Note: Uncheck column b for bottles NOT received.  Mark column c for damaged bottles.**"
      }, {
        "type": "TextBlock",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "text": "**a. Bottle ID**"
      }, {
        "type": "TextBlock",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "text": "**b. Order Date**"
      }, {
        "type": "TextBlock",
        "layout": {
          "width": 3,
          "prepend": 0,
          "append": 2
        },
        "text": "**c. Exp Date**"
      }, {
        "type": "TextBlock",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 4
        },
        "text": "**a. Lot number**"
      }, {
        "type": "EchoInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST4a",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 12,
        "inputWidth": 3,
        "autoNumberRule": "N+,L+",
        "contents": "",
        "doubleEntry": false,
        "autoTab": false,
        "source": "BST0a",
        "editable": false,
        "allowManualEvaluation": true,
        "inputType": {
          "dataType": "date",
          "fixed": false,
          "precision": "ymd",
          "timezone": "Unknown/Unknown"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "DateTextInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST4b",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 13,
        "inputWidth": 3,
        "autoNumberRule": "N,L+",
        "timeZoneChoice": false,
        "inputType": {
          "dataType": "date",
          "fixed": false,
          "precision": "ymd",
          "timezone": "Unknown/Unknown"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "DateTextInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST4c",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 14,
        "inputWidth": 3,
        "autoNumberRule": "N,L+",
        "timeZoneChoice": false,
        "inputType": {
          "dataType": "date",
          "fixed": false,
          "precision": "ymd",
          "timezone": "Unknown/Unknown"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "TextInput",
        "layout": {
          "width": 4,
          "prepend": 1,
          "append": 7
        },
        "name": "",
        "index": "BST4d",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 15,
        "inputWidth": 4,
        "autoNumberRule": "N,L+",
        "maxLength": 12,
        "contents": "",
        "doubleEntry": false,
        "autoTab": false,
        "inputType": {
          "dataType": "string"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "EchoInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST5a",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 16,
        "inputWidth": 3,
        "autoNumberRule": "N+,L+",
        "contents": "",
        "doubleEntry": false,
        "autoTab": false,
        "source": "BST2a",
        "editable": false,
        "allowManualEvaluation": true,
        "inputType": {
          "dataType": "string"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "DateTextInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST5b",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 17,
        "inputWidth": 3,
        "autoNumberRule": "N,L+",
        "timeZoneChoice": false,
        "inputType": {
          "dataType": "date",
          "fixed": false,
          "precision": "ymd",
          "timezone": "Unknown/Unknown"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "DateTextInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST5c",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 18,
        "inputWidth": 3,
        "autoNumberRule": "N,L+",
        "timeZoneChoice": false,
        "inputType": {
          "dataType": "date",
          "fixed": false,
          "precision": "ymd",
          "timezone": "Unknown/Unknown"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "TextInput",
        "layout": {
          "width": 4,
          "prepend": 1,
          "append": 7
        },
        "name": "",
        "index": "BST5d",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 19,
        "inputWidth": 4,
        "autoNumberRule": "N,L+",
        "maxLength": 12,
        "contents": "",
        "doubleEntry": false,
        "autoTab": false,
        "inputType": {
          "dataType": "string"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "EchoInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST6a",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 20,
        "inputWidth": 3,
        "autoNumberRule": "N+,L+",
        "contents": "",
        "doubleEntry": false,
        "autoTab": false,
        "source": "BST3a",
        "editable": false,
        "allowManualEvaluation": true,
        "inputType": {
          "dataType": "string"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "DateTextInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST6b",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 21,
        "inputWidth": 3,
        "autoNumberRule": "N,L+",
        "timeZoneChoice": false,
        "inputType": {
          "dataType": "date",
          "fixed": false,
          "precision": "ymd",
          "timezone": "Unknown/Unknown"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "DateTextInput",
        "layout": {
          "width": 3,
          "prepend": 1,
          "append": 0
        },
        "name": "",
        "index": "BST6c",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 22,
        "inputWidth": 3,
        "autoNumberRule": "N,L+",
        "timeZoneChoice": false,
        "inputType": {
          "dataType": "date",
          "fixed": false,
          "precision": "ymd",
          "timezone": "Unknown/Unknown"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }, {
        "type": "TextInput",
        "layout": {
          "width": 4,
          "prepend": 1,
          "append": 7
        },
        "name": "",
        "index": "BST6d",
        "sasCodeLabel": "",
        "cloneId": 0,
        "tabOrder": 23,
        "inputWidth": 4,
        "autoNumberRule": "N,L+",
        "maxLength": 12,
        "contents": "",
        "doubleEntry": false,
        "autoTab": false,
        "inputType": {
          "dataType": "string"
        },
        "prompts": {
          "pre": {
            "prompt": "",
            "promptPreWidth": 0
          },
          "post": {
            "prompt": "",
            "promptPostWidth": 0
          }
        }
      }]
    }],
    "crf": {
      "type": "CRF",
      "id": 311,
      "name": "BST",
      "title": "BST",
      "study": {
        "type": "Study",
        "id": 13,
        "title": "JIM",
        "shortCode": "J"
      }
    }
  }
}

export const cdartSample3 = {
  'json-version': '1.0',
  content: {
    type: 'Form',
    allowEventAttachedFiles: false,
    autoIds: {
      enabled: false,
      separator: null,
      prefix: 'FORM',
    },
    formAttachedFiles: {},
    QbyQ: {},
    metadata: {
      majorVersion: 1,
      description: '1.0',
      topLevelSections: 'tabs',
    },
    entities: [
      {
        type: 'FormSection',
        layout: {
          width: 24,
          prepend: 0,
          append: 0,
        },
        legend: 'Tab',
        entities: [
          {
            type: 'TextInput',
            layout: {
              width: 5,
              prepend: 2,
              append: 17,
            },
            name: '',
            externalIdentifier: 'FORM5052121',
            index: 'FORM5052121',
            sasCodeLabel: '',
            cloneId: 0,
            tabOrder: 1,
            inputWidth: 3,
            autoNumberRule: 'N+',
            maxLength: 80,
            contents: '',
            doubleEntry: false,
            autoTab: false,
            inputType: {
              dataType: 'string',
            },
            prompts: {
              pre: {
                prompt: '',
                promptPreWidth: 2,
              },
              post: {
                prompt: '',
                promptPostWidth: 0,
              },
            },
            defaultText: '',
          },
          {
            source: 'FORM5052121',
            editable: false,
            allowManualEvaluation: true,
            autoNumberRule: 'N+',
            inputWidth: 3,
            type: 'EchoInput',
            index: 'FORM8739114',
            tabOrder: 3,
            sasCodeLabel: '',
            autoTab: false,
            name: '',
            cloneId: 0,
            inputType: {
              dataType: 'string',
            },
            prompts: {
              pre: {
                prompt: '',
                promptPreWidth: 2,
              },
              post: {
                prompt: '',
                promptPostWidth: 2,
              },
            },
            externalIdentifier: 'FORM60374814',
            layout: {
              width: 7,
              prepend: 1,
              append: 16,
            },
          },
          {
            defaultText: '',
            cdsName: 'ACHIEVEANT11',
            editable: false,
            allowManualEvaluation: false,
            evaluationPolicy: 'RENDER_FIRST',
            autoNumberRule: 'N+',
            inputWidth: 3,
            type: 'CDSTextInput',
            index: 'FORM59864016',
            tabOrder: 4,
            sasCodeLabel: '',
            autoTab: false,
            name: '',
            cloneId: 0,
            inputType: {
              dataType: 'string',
            },
            prompts: {
              pre: {
                prompt: '',
                promptPreWidth: 3,
              },
              post: {
                prompt: '',
                promptPostWidth: 3,
              },
            },
            externalIdentifier: 'FORM54941716',
            layout: {
              width: 9,
              prepend: 2,
              append: 13,
            },
          },
          {
            sizeRows: 1,
            autoNumberRule: 'N+',
            inputWidth: 3,
            type: 'TextArea',
            index: 'FORM14766517',
            tabOrder: 5,
            sasCodeLabel: '',
            autoTab: false,
            name: '',
            cloneId: 0,
            inputType: {
              dataType: 'string',
            },
            prompts: {
              pre: {
                prompt: '',
                promptPreWidth: 3,
              },
              post: {
                prompt: '',
                promptPostWidth: 3,
              },
            },
            externalIdentifier: 'FORM13775617',
            layout: {
              width: 9,
              prepend: 2,
              append: 13,
            },
          },
          {
            defaultState: false,
            autoNumberRule: 'N+',
            inputWidth: 3,
            type: 'Checkbox',
            index: 'FORM92205718',
            tabOrder: 7,
            sasCodeLabel: '',
            autoTab: false,
            name: '',
            cloneId: 0,
            inputType: {
              dataType: 'boolean',
            },
            prompts: {
              pre: {
                prompt: '',
                promptPreWidth: 3,
              },
              post: {
                prompt: '',
                promptPostWidth: 2,
              },
            },
            externalIdentifier: 'FORM20942318',
            layout: {
              width: 8,
              prepend: 2,
              append: 14,
            },
          },
          {
            options: [
              {
                label: '',
                value: '',
              },
              {
                label: 'label1',
                value: 'value1',
              },
              {
                label: 'label2',
                value: 'value2',
              },
            ],
            mode: 'menu',
            autoNumberRule: 'N+',
            inputWidth: 3,
            type: 'SelectionInput',
            index: 'FORM78291020',
            tabOrder: 8,
            sasCodeLabel: '',
            autoTab: false,
            name: '',
            cloneId: 0,
            inputType: {
              dataType: 'string',
            },
            prompts: {
              pre: {
                prompt: '',
                promptPreWidth: 3,
              },
              post: {
                prompt: '',
                promptPostWidth: 3,
              },
            },
            externalIdentifier: 'FORM37329820',
            layout: {
              width: 9,
              prepend: 2,
              append: 13,
            },
          },
          {
            type: 'TextBlock',
            text: 'This is text block content',
            background: 'NONE',
            layout: {
              width: 24,
              prepend: 0,
              append: 0,
            },
          },
          {
            maxLength: 80,
            defaultText: '',
            autoNumberRule: 'N+',
            inputWidth: 7,
            type: 'ASTextInput',
            index: 'FORM114321',
            tabOrder: 10,
            sasCodeLabel: '',
            autoTab: false,
            name: '',
            cloneId: 0,
            inputType: {
              dataType: 'blob',
            },
            prompts: {
              pre: {
                prompt: '',
                promptPreWidth: 4,
              },
              post: {
                prompt: '',
                promptPostWidth: 4,
              },
            },
            externalIdentifier: 'FORM23245721',
            meaning: 'dictionary meaning',
            dictionaryName: 'Medispan',
            layout: {
              width: 15,
              prepend: 2,
              append: 7,
            },
          },
          {
            maxLength: 80,
            defaultText: '',
            autoNumberRule: 'N+',
            inputWidth: 5,
            type: 'RandomizationInput',
            index: 'FORM81404123',
            tabOrder: 11,
            sasCodeLabel: '',
            autoTab: false,
            name: '',
            cloneId: 0,
            inputType: {
              dataType: 'string',
            },
            prompts: {
              pre: {
                prompt: '',
                promptPreWidth: 4,
              },
              post: {
                prompt: '',
                promptPostWidth: 3,
              },
            },
            externalIdentifier: 'FORM68116423',
            randomShim: 'ACT1ONPILOT',
            layout: {
              width: 12,
              prepend: 3,
              append: 9,
            },
          },
        ],
      },
    ],
    crf: {
      type: 'CRF',
      id: 342,
      name: 'single',
      title: 'single title',
      study: {
        type: 'Study',
        id: 13,
        title: 'JIM',
        shortCode: 'J',
      },
    },
  },
};

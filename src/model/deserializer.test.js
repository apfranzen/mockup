import {
  deserialize,
  flatten,
  transformEntities,
  cdartSampleInput,
  getLayout,
  getById,
} from './deserializer';
// import { formSeed } from '../redux-modules/reducer';
/**
 - testing -
  - describe the behavior
  - deserialize
    - 0 entities
    - 1 entities
      - passes all of the attributes of
        - pass arbitrary key value pairs - conveys attributes from entity
    - (line 20) assure children property is removed
    - parent child/parent relationships
      - can nest arbitrarily deep

 */

const baseObject = {
  'json-version': '1.0',
  content: expect.any(Object),
};

// describe('the getById method', () => {
//   it('assigns entity id in children array', () => {
//     expect(typeof getById(flatten(cdartSampleInput))[0].entities[0]).toEqual(
//       'string'
//     );
//     // expect(flatten(cdartSampleInput)[0].type).toEqual('Form');
//   });
// });

describe('the flatten method', () => {
  it('flattens a tree structure', () => {
    expect(flatten(cdartSampleInput)).toHaveLength(5);
    expect(flatten(cdartSampleInput)[0].type).toEqual('Form');
    expect(typeof flatten(cdartSampleInput)[1].uuid).toEqual('string');
  });
});

describe('the transform entities method', () => {
  it('transforms each entity', () => {
    // Form Designer expects Form to always have uuid of 0
    expect(transformEntities(flatten(cdartSampleInput))[0].uuid).toEqual(0);
    expect(typeof transformEntities(flatten(cdartSampleInput))[1].uuid).toBe(
      'string'
    );
  });
});

describe('the deserialize method', () => {
  it('deserializes a string', () => {
    expect(deserialize(JSON.stringify({ form: { '0': {} } }))).toBeInstanceOf(
      Object
    );
  });

  xit('returns JSON version 1.0 & content', () => {
    expect(deserialize({ form: { '0': {} } })).toMatchObject(baseObject);
  });

  xit('conveys all attributes from an entity', () => {
    const attributes = { someProperty: '', secondProperty: '' };
    const appState = { form: { '0': attributes } };

    expect(deserialize(appState).content).toMatchObject(attributes);
  });

  xit('returns 1 entity when passed 1 entity', () => {
    const sampleAttributes = { app: {}, form: { '0': { children: [] } } };
    expect(deserialize(sampleAttributes)).toEqual(true);
  });

  // xit('returns 2 entities when passed 2 entities', () => {
  //   const sampleAttributes = {
  //     app: {},
  //     form: { '0': { children: ['1'] }, '1': {} },
  //   };
  //   expect(deserialize(sampleAttributes).content.entities.length).toEqual(1);
  // });

  xit('can return an arbitrarily nested object', () => {
    const sampleAttributes = {
      app: {},
      form: {
        '0': {
          foo: 'a',
          children: ['1'],
        },
        '1': {
          foo: 'b',
          children: ['2'],
        },
        '2': {
          foo: 'c',
          children: ['3'],
        },
        '3': { foo: 'd' },
      },
    };

    console.log(deserialize(sampleAttributes));
    let result = deserialize(sampleAttributes).content;
    expect(result.foo).toEqual('a');
    result = result.entities[0];
    expect(result.foo).toEqual('b');
    result = result.entities[0];
    expect(result.foo).toEqual('c');
    result = result.entities[0];
    expect(result.foo).toEqual('d');
  });
});

export function hasChildren(node) {
  return (
    typeof node === 'object' &&
    typeof node.entities !== 'undefined' &&
    node.entities.length > 0
  );
}

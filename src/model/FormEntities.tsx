import { EntityTypes, RenderModes, InputTypes } from './Types';
type Partial<T> = { [P in keyof T]?: T[P] };
const uuidv4 = require('uuid/v4');

interface Form {
  /** The entities which exist directly beneath the form. */
  children: Array<string>;
  /** Whether files should be allowed to be attached to an event associated with this form. */
  allowEventAttachedFiles: boolean;
  /** Controls the behavior of whether/how child inputs have their external IDs automatically generated */
  autoIds: {
    enabled: boolean;
    /** ex. ABC.1 The value used between prefix and externalID */
    separator: null | string;
    /** The value used prior to separator */
    prefix: null | string;
  };
  customTabOrder: boolean;
  /** Whether top-level FormSection entities should be considered as tabs. */
  sectionTabs: boolean;
  formAttachedFiles: { title?: string };
  QbyQ: { alignment?: string };
  uuid: number;
  /** All properties below are legacy properties from current Form Designer */
  metadata: {
    majorVersion: number;
    description: string;
    topLevelSections: string;
  };
  crf: {
    type: string;
    id: number;
    name: string;
    title: string;
    study: {
      type: string;
      id: number;
      title: string;
      shortCode: string;
    };
  };
}

type FormConfig = Partial<Form>;

export function generateForm(config?: FormConfig): Form {
  const FormDefault = {
    type: EntityTypes.Form,
    children: [],
    topLevel: false,
    allowEventAttachedFiles: false,
    autoIds: {
      enabled: true,
      separator: '.',
      prefix: 'FORM',
    },
    uuid: 0,
    customTabOrder: false,
    sectionTabs: true,
    formAttachedFiles: {},
    QbyQ: {},
    metadata: {
      majorVersion: 1,
      description: '',
      topLevelSections: 'tabs',
    },
    crf: {
      type: 'CRF',
      id: 311,
      name: 'BST',
      title: 'BST',
      study: {
        type: 'Study',
        id: 13,
        title: 'JIM',
        shortCode: 'J',
      },
    },
  };

  const enforceDefaults = {
    ...(config && config.autoIds && config.autoIds.enabled === false
      ? { separator: null, prefix: null }
      : {
          separator:
            config && config.autoIds && config.autoIds.separator
              ? config.autoIds.separator
              : '',
          prefix:
            config && config.autoIds && config.autoIds.prefix
              ? config.autoIds.prefix
              : '',
        }),
  };
  return { ...FormDefault, ...config, ...enforceDefaults };
}

interface FormEntity {
  /** The Form Entity's UUID generated client-side. */
  uuid: string;
  /** Width of the entity itself in grid units. */
  width: Number;
  /** Column the entity begins at (1 based unit). */
  column: Number;
  /** Row the entity should be rendered at (1 based unit). */
  row: Number;
  validators: Array<Object>;
  internalId: string;
}
type FormEntityConfig = Partial<FormEntity>;

const FORM_ENTITY_DEFAULT = {
  width: 6,
  column: 1,
  row: 1,
  validators: [],
};

export function generateFormEntity(
  defaultProps = FORM_ENTITY_DEFAULT,
  config?: FormEntityConfig
): FormEntity {
  const uuid = uuidv4();
  return { ...defaultProps, uuid, internalId: uuid, ...config };
}

interface FormSection extends FormEntity {
  /** The child form entities within the form section. */
  children: Array<string>;
  /** The value used as a label for FormSections that are Top Level */
  legend: String;
  topLevel: boolean;
}
type FormSectionConfig = Partial<FormSection>;

export function generateFormSection(config?: FormSectionConfig): FormSection {
  const FORM_SECTION_DEFAULT = {
    type: EntityTypes.FormSection,
    width: 24,
    prepend: 0,
    append: 0,
    children: [],
    topLevel: false,
    legend: '',
  };
  return {
    ...generateFormEntity(),
    ...FORM_SECTION_DEFAULT,
    ...config,
  };
}

type ExternalIdentifier = string;

interface FormInput extends FormEntity {
  /** Width of leading prompts in grid units. */
  prePromptWidth: Number;
  /** Prompt prefix that should appear before rendered representations of this input. */
  prePrompt: String;
  /** Width of trailing prompts in grid units. */
  postPromptWidth: Number;
  /** Prompt suffix that should appear before rendered representations of this input. */
  postPrompt: String;
  /** Resulting value should not be assumed to be globally-unique among all other forms */
  externalIdentifier: ExternalIdentifier;
  /** Order of the input  in the Form's tab order */
  tabOrder: number;
  /** Auto number policy */
  autoNumberRule: string;
  /** Name of the form input */
  name: string;
  /** label used for SAS computing */
  //@TODO: dc
  sasCodeLabel: string;
  //@TODO: dc
  dependencies: Object;
  //@TODO: dc
  validations: Array<Object>;
  //@TODO: dc
  dataType: string;
}
type FormInputConfig = Partial<FormInput>;

// const DateDefaults: any = {
//   precision: '',
//   fixed: false,
//   timeZoneChoice: false,
// };

export function generateFormInput(config?: FormInputConfig): FormInput {
  const FORM_INPUT_DEFAULT = {
    externalIdentifier: '',
    prePromptWidth: 3,
    prePrompt: '',
    postPromptWidth: 0,
    postPrompt: '',
    tabOrder: 0,
    autoNumberRule: 'N+',
    name: '',
    sasCodeLabel: '',
    //@TODO: dc
    dependencies: {
      type: 'DependencyExpression',
      // operator: 'ALL',
      conditions: [],
    },
    //@TODO: dc
    validations: [],
    //@TODO: dc
    dataType: 'string',
  };
  return { ...generateFormEntity(), ...FORM_INPUT_DEFAULT, ...config };
}

interface TextInput extends FormInput {
  /** Max length for input. Default is 60, and if NO_MAX is passed in, no max length will be applied to this field. */
  maxLength: number;
  /** The default contents of representations of this input item. */
  defaultContent: string;
  /** Whether the input is defined as permitting auto tab to the next field during data collection. */
  autoTab: boolean;
}
type TextInputConfig = Partial<TextInput>;

export function generateTextInput(config?: TextInputConfig): TextInput {
  const TEXT_INPUT_DEFAULT = {
    type: EntityTypes.TextInput,
    maxLength: 60,
    defaultContent: 'this is a default content',
    autoTab: false,
    // ...DateDefaults,
  };
  return {
    ...generateFormEntity(),
    ...generateFormInput(),
    ...TEXT_INPUT_DEFAULT,
    ...config,
  };
}

interface TextArea extends FormInput {
  /** Set the number of rows in the text area (HTML element property). */
  sizeRows: number;
  /** Set the number of columns in the text area (HTML element property). */
  numColumns: number;
}
type TextAreaConfig = Partial<TextArea>;

export function generateTextArea(config?: TextAreaConfig): TextArea {
  const TEXT_AREA_DEFAULT = {
    type: EntityTypes.TextArea,
    sizeRows: 3,
    numColumns: 0,
  };
  return {
    ...generateFormEntity(),
    ...generateFormInput(),
    ...TEXT_AREA_DEFAULT,
    ...config,
  };
}

interface Checkbox extends FormInput {
  /** Default state of the Checkbox. */
  defaultState: boolean;
}

type CheckboxConfig = Partial<Checkbox>;

export function generateCheckbox(config?: CheckboxConfig): Checkbox {
  const CHECK_BOX_DEFAULT = {
    type: EntityTypes.Checkbox,
    width: 1,
    defaultState: false,
  };
  return {
    ...generateFormEntity(),
    ...generateFormInput(),
    ...CHECK_BOX_DEFAULT,
    ...config,
  };
}

// type SelectionInputTypes =
//   | InputTypes.string
//   | InputTypes.integer
//   | InputTypes.float;

interface SelectionInput extends FormInput {
  /** Defines if the Selection Input will render as a select or radio button set */
  mode: Object;
  /** Expected type of the selection input */
  // dataType: SelectionInputTypes;
  //@TODO: dc
  /** Options to be rendered */
  options: Array<Object>;
}

type SelectionInputConfig = Partial<SelectionInput>;

export function generateSelectionInput(
  config?: SelectionInputConfig
): SelectionInput {
  const SELECTION_INPUT_DEFAULT = {
    type: EntityTypes.SelectionInput,
    options: [{ value: '', label: '' }],
    mode: RenderModes.menu,
    //@TODO: dc
    // dataType: 0,
  };

  return {
    ...generateFormEntity(),
    ...generateFormInput(),
    ...SELECTION_INPUT_DEFAULT,
    ...config,
  };
}

interface TextBlock extends FormEntity {
  /** Content to be rendered in the text block. */
  text: string;
  /** Background color of the text block. */
  backgroundColor: string;
}

type TextBlockConfig = Partial<TextBlock>;

export function generateTextBlock(config?: TextBlockConfig): TextBlock {
  const TEXT_BLOCK_DEFAULT = {
    type: EntityTypes.TextBlock,
    text: '',
    backgroundColor: 'blue',
  };
  console.log(' config', generateFormEntity());

  return {
    ...generateFormEntity(),
    ...TEXT_BLOCK_DEFAULT,
    ...config,
  };
}

interface ImageBlock extends FormEntity {
  /** Alternate text for an image (HTML property) */
  alt: string;
  /** Title of the image (HTML property) */
  title: string;
  /** UTL of the image (HTML property) */
  url: string;
  /** id of the FormAttachedFile in indexedDb */
}

type ImageBlockConfig = Partial<ImageBlock>;

export function generateImageBlock(config?: ImageBlockConfig): ImageBlock {
  const IMAGE_BLOCK_DEFAULT = {
    type: EntityTypes.ImageBlock,
    alt: '',
    title: '',
    url: '',
  };
  return {
    ...generateFormEntity(),
    // ...generateFormInput(),
    ...IMAGE_BLOCK_DEFAULT,
    ...config,
  };
}

/** Also known as AdverseEventInput */
interface ASTextInput extends FormInput {
  /** The dictionary used to lookup the term. */
  dictionaryName: string;
}

type ASTextInputConfig = Partial<ASTextInput>;

export function generateASTextInput(config?: ASTextInputConfig): ASTextInput {
  const AUTO_SUGGEST_INPUT_DEFAULT = {
    type: EntityTypes.ASTextInput,
    dictionaryName: '',
  };
  return {
    ...generateFormEntity(),
    ...generateFormInput(),
    ...AUTO_SUGGEST_INPUT_DEFAULT,
    ...config,
  };
}

interface EchoInput extends TextInput {
  /** Whether the input implementation may be made editable. */
  editeable: boolean;
  /** External ID of the input who's value will be renderd in this input */
  sourceInput: ExternalIdentifier;
}

type EchoInputConfig = Partial<EchoInput>;

export function generateEchoInput(config?: EchoInputConfig): EchoInput {
  const ECHO_INPUT_DEFAULT = {
    type: EntityTypes.EchoInput,
    editeable: false,
    sourceInput: '',
  };
  return {
    ...generateFormEntity(),
    ...generateFormInput(),
    ...generateTextInput(),
    ...ECHO_INPUT_DEFAULT,
    ...config,
  };
}

enum EvaluationPolicy {
  placeHolder,
}

interface CDSTextInput extends TextInput {
  /** Get the script which should be evaluated to produce the value of the input. */
  script: string;
  /** Whether the input implementation may be made editable. */
  editeable: boolean;
  /** Policy that dictates when the script will operate. */
  evaluationPolicy: EvaluationPolicy;
}

type CDSTextInputConfig = Partial<CDSTextInput>;

export function generateCDSTextInput(
  config?: CDSTextInputConfig
): CDSTextInput {
  const CDSTextInputDefault = {
    type: EntityTypes.CDSTextInput,
    editeable: false,
    evaluationPolicy: EvaluationPolicy.placeHolder,
    script: '',
    // ...DateDefaults,
  };
  return {
    ...generateFormEntity(),
    ...generateFormInput(),
    ...generateTextInput(),
    ...CDSTextInputDefault,
    ...config,
  };
}

interface RandomizationInput extends TextInput {
  /** Name of the randomization script server side */
  randomShim: string;
}

type RandomizationInputConfig = Partial<RandomizationInput>;

export function generateRandomizationInput(
  config?: RandomizationInputConfig
): RandomizationInput {
  const RandomizationInputDefault = {
    type: EntityTypes.RandomizationInput,

    randomShim: 'test',
  };
  return {
    ...generateFormEntity(),
    ...generateFormInput(),
    ...generateTextInput(),
    ...RandomizationInputDefault,
    ...config,
  };
}

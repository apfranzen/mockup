import React from 'react';
import ReactDOM from 'react-dom';
import Enzyme, { mount, shallow } from 'enzyme';
import { wrapInTestContext } from 'react-dnd-test-utils';
import { createStore } from 'redux';
import Adapter from 'enzyme-adapter-react-16';
import DisplayHeading from '../components/common/DisplayHeading';
import { Provider } from 'react-redux';
import reducer from '../redux-modules/reducer';
import LayoutRoot from '../components/LayoutRoot';
import TextInput from '../components/FormEntities/FormEntity/TextInput';
import FormSection from '../components/FormEntities/FormEntity/FormSection';
import FormEntity from '../components/FormEntities/FormEntity';
import ExistingRow from '../components/FormEntities/ExistingRow';
// import { store, counter } from './reduxModule';
// import Main from './Main';
// import Grid from './Grid';

Enzyme.configure({ adapter: new Adapter() });

describe('test', () => {
  it('works', () => {
    expect(1 + 1).toEqual(2);
  });
});

describe('A stateless component', () => {
  it('mounts', () => {
    const root = Enzyme.mount(<DisplayHeading legend="green" />);
    // console.log(root.debug());
    // console.log(root.find(DisplayHeading).instance());
    expect(root.find(DisplayHeading).length).toEqual(1);
  });
});

describe.only('A state-full component', () => {
  it('mounts', () => {
    const store = createStore(reducer);

    function LayoutWithState() {
      return (
        <Provider store={store}>
          <LayoutRoot />
        </Provider>
      );
    }
    const MainWithContext = wrapInTestContext(LayoutWithState);

    const root = Enzyme.mount(<MainWithContext />);
    const manager = root.instance().getManager();

    const monitor = manager.getMonitor();

    monitor['getSourceClientOffset'] = jest.fn(() => ({
      x: 10,
      y: 20,
    }));
    monitor['isOver'] = jest.fn(() => true);

    // get reference to dragSource
    // Obtain a reference to the backend
    const backend = root
      .instance()
      .getManager()
      .getBackend();

    // get reference to dragSource
    const dragSource = root
      .find(FormEntity)
      .filterWhere(n => n.props().model.uuid === '4.1')
      .instance();

    const dropTarget = root
      .find(ExistingRow)
      .filterWhere(n => n.props().sectionUUID === '4.7')
      .instance();

    backend.simulateBeginDrag([dragSource.getHandlerId()]);

    backend.simulateHover([dropTarget.getHandlerId()]);
    backend.simulateHover([dropTarget.getHandlerId()]);

    console.log(monitor.isOver());
    expect(dropTarget.state.canDrop).toEqual(true);
    backend.simulateDrop();

    backend.simulateEndDrag();
    expect(store.getState().form['4.7'].children).toContain('4.1');
    console.log(store.getState().form['4.1'].column);
    expect(store.getState().form['4'].children).not.toContain('4.1');
  });
});

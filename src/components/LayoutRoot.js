/** @jsx jsx */
import FormContainer from '../containers/FormContainer';
import EntityPalette from './EntityPalette/EntityPalette';
import CustomDragLayer from './FormEntities/FormEntity/CustomDragLayer';
import PropertyPanel from './PropertyPanel/PropertyPanel';
import DevStateHandler from './DevStateHandler';
import { ThemeProvider } from 'emotion-theming';
import styled from '@emotion/styled';
import { css, jsx, Global } from '@emotion/core';
import Toolbar from './Toolbar';
import HTML5Backend from 'react-dnd-html5-backend';
import { DragDropContext } from 'react-dnd';
import { createContext, useState, useEffect } from 'react';

const rootStyle = css`
  display: flex;
  justify-content: flex-start;
  height: calc(100vh - 100px);
  padding: 20px 0 20px 0;
  height: 100%;
`;

const Section = styled.section`
  filter: drop-shadow(0 0 0.2rem grey);
  background: white;
  margin: 0 6px 0 6px;
  flex: ${props => (props.flex ? props.flex : null)};
  overflow: hidden; // to prevent the sections from growing in width due to content
`;

const themes = {
  colors: {
    blue: '#3465a4',
  },
  form: {
    minWidth: 950, // fixed width based on data collection
    gridWidth: 40,
    gridTemplateColumnWidth: 30,
    gutter: 10,
  },
};
const getUrlParams = href => {
  const studyId = href.includes('studies')
    ? href.split('studies/')[1].split('/')[0]
    : null;
  const formId = () => {
    if (href.includes('new')) return null;
    if (href.includes('designs')) return href.split('designs/')[1];

    if (href.includes('CDART ') && !href.includes('designs'))
      return href.split('forms/')[1].split('/')[0];
  };
  const formVersion = () => {
    if (!href.includes('designs') && href.includes('versions')) {
      return href.split('/versions/')[1];
    }
    return null;
  };
  return { studyId, formId: formId(), formVersion: formVersion() };
};

export const HrefContext = createContext();
function LayoutRoot(props) {
  const [studyId, setStudyId] = useState('');
  const [formVersion, setFormVersion] = useState('');
  const [formId, setFormId] = useState('');
  const [versionedForm, setVersionedForm] = useState('');
  useEffect(() => {
    const href = window.location.href;
    const { studyId, formId, formVersion } = getUrlParams(href);
    setStudyId(studyId);
    setFormId(formId);
    setFormVersion(formVersion);
  }, []);

  return (
    <HrefContext.Provider value={{ studyId, formId, formVersion }}>
      <ThemeProvider theme={themes}>
        <Global
          styles={css`
            body {
              font-family: Helvetica Neue, Arial;
            }

            textarea {
              font-size: 12px;
              font-family: Helvetica Neue, Arial;
            }

            input {
              font-size: 12px;
              font-family: Helvetica Neue, Arial;
            }

            p {
              font-size: 12px;
              font-family: Helvetica Neue, Arial;
            }
            label {
              font-size: 12px;
              font-family: Helvetica Neue, Arial;
            }
          `}
        />
        <DevStateHandler>
          <Toolbar />
          <div css={rootStyle}>
            <Section flex="1">
              <EntityPalette />
            </Section>
            <CustomDragLayer />
            <Section>
              <FormContainer />
            </Section>

          </div>
        </DevStateHandler>
      </ThemeProvider>
    </HrefContext.Provider>
  );
}

export default DragDropContext(HTML5Backend)(LayoutRoot);

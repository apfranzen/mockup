import React, { useState, useRef, useContext, Component } from 'react';
import {
  createEntity,
  testLoadForm,
  loadFormState,
  addFiles,
  deleteFiles,
  updateFiles,
  serializeForm,
  setHideSaveDialog,
  updateMultiple,
  setMessage,
  serverGetSuccess,
} from '../redux-modules/actions';
import {
  CommandBar,
  Callout,
  initializeIcons,
  Toggle,
  MessageBar,
  MessageBarType,
  ProgressIndicator,
} from 'office-ui-fabric-react/';
import { createRef } from 'office-ui-fabric-react/lib/Utilities'; // office-ui-fabric-react isn't using react 16 yet, so they want you to use their createRef
import _ from 'lodash';
import { connect } from 'react-redux';
import { utility } from '../lib/utility';
import FileList from './PropertyPanel/FormAttachedFiles/FileList';
import { DevContext } from './DevStateHandler';
import RadioButton from './common/formInputs/RadioButton';
import { serialize } from '../model/serializer';
import { SaveDialog } from './SaveDialog';
import { ImportDialog } from './ImportDialog';
import db from '../db/db';

const uuidv4 = require('uuid/v4');
initializeIcons();
// Data for CommandBar

function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute(
    'href',
    'data:text/plain;charset=utf-8,' + encodeURIComponent(text)
  );
  element.setAttribute('download', filename);

  element.style.display = 'none';
  document.body.appendChild(element);

  element.click();

  document.body.removeChild(element);
}

function duplicateHandler(model, createEntity) {
  createEntity(
    1,
    model.type,
    model.row + 1,
    model.column,
    'new',
    _.omit(model, ['dependencies, validations', 'uuid'])
  );
}
class Toolbar extends Component {
  constructor() {
    super();
    this.state = {
      test: false,
      name: '',
      showMessageBar: true,
      hideImportMenu: true,
    };
  }

  handleFileAdd = event => {
    function getBase64(file, callback) {
      const reader = new FileReader();

      reader.addEventListener('load', () => callback(reader.result));

      reader.readAsDataURL(file);
    }

    // see CDART2 JSONInportExportController ln 132
    const { name, size, type } = event.target.files[0];
    var uploadedFile = event.target.files[0];

    var formData = new FormData();

    getBase64(
      uploadedFile,
      function(base64Data) {
        // add to IndexedDB
        // console.log(name, size, type);

        this.props.addFiles(name, type, base64Data.split(',')[1], uuidv4());
      }.bind(this)
    );

    formData.append('selectFile', uploadedFile);
    console.log({ formData });
  };

  componentDidUpdate(prevProps) {
    if (
      this.props.fetchStatus !== prevProps.fetchStatus &&
      this.props.fetchStatus === 'success'
    ) {
      this.setState({ showMessageBar: true });
      setTimeout(() => {
        this.setState({ showMessageBar: false });
      }, 5000);
    }
    return null;
  }

  setName = value => {
    this.setState({
      name: value,
    });
  };

  setShowMessageBar = () => {
    this.setState(state => {
      return {
        showMessageBar: !state.showMessageBar,
        ...(state.showMessageBar === false ? { name: '' } : {}),
      };
    });
  };

  setHideImportMenu = () => {
    this.setState(state => ({ hideImportMenu: !state.hideImportMenu }));
  };

  handleSave = (source, formName) => {
    const href = window.location.href;
    const versionedForm = !href.includes('designs');
    if (source === 'save' && !versionedForm) {
      this.props.serializeForm(href, 'save', formName);
      this.setName('');
    } else {
      this.props.serializeForm(href, 'saveAs', formName);
      this.setName('');
    }
  };

  _button = createRef();
  _devbutton = createRef();
  _onShowMenuClicked = () => {
    this.setIsCalloutVisible(isCalloutVisible => !isCalloutVisible);
  };

  _onRenderMenu = () => {
    return (
      <Callout
        onDismiss={() =>
          this._button.current && this._button.current.dismissMenu()
        }
        gapSpace={0}
        target={'#button'}
        setInitialFocus={true}
        isBeakVisible={false}
      >
        <div
          style={{
            height: 300,
            width: 200,
            // border: '1px solid blue',
            padding: 6,
            display: 'flex',
            // minHeight: 100vh;
            flexDirection: 'column',
          }}
        >
          <button
            onClick={e =>
              this.setState(state => ({ ...state, test: !state.test }))
            }
          >
            Test
          </button>
          <Toggle
            checked={this.props.form[0].eventAttachedFiles}
            label="Event Attached Files"
          />
          <Toggle
            checked={this.props.allowFormAttachedFiles}
            label="Form Attached Files"
          />
          <div
            style={{
              margin: 'auto auto 0 auto',
              // background: 'purple',
              // border: '1px solid purple',
              // color: 'purple',
              width: '100%',
            }}
          >
            <FileList
              files={this.props.formAttachedFiles}
              deleteFiles={this.props.deleteFiles}
            />

            <input
              type="file"
              id="myFile"
              multiple
              size="50"
              onChange={e => {
                e.preventDefault();
                // console.log(e.target.files);
                this.handleFileAdd(e);
              }}
            />
          </div>
        </div>
      </Callout>
    );
  };

  _devonRenderMenu = () => {
    const { _appState } = this.props;

    const { devMode, toggleDevMode } = useContext(DevContext);
    function persistState() {
      const currentStorage = localStorage.getItem('formState');

      localStorage.setItem(currentStorage, JSON.stringify(_appState));
    }

    return (
      <Callout
        onDismiss={() =>
          this._devbutton.current && this._devbutton.current.dismissMenu()
        }
        gapSpace={0}
        target={'#devbutton'}
        setInitialFocus={true}
        isBeakVisible={false}
      >
        <div
          style={{
            height: 300,
            width: 200,
            padding: 6,
            display: 'flex',
            flexDirection: 'column',
          }}
        >
          <div>
            <div style={{ maxWidth: '160px' }}>
              <RadioButton
                value={devMode}
                handleChange={toggleDevMode}
                options={[
                  { value: false, label: 'normal' },
                  { value: true, label: 'x-ray' },
                ]}
              />
            </div>
            <button onClick={persistState}>update local</button>
          </div>
        </div>
      </Callout>
    );
  };

  getItems = () => {
    return [
      {
        key: 'save',
        name: 'Save',
        iconProps: {
          iconName: 'Save',
        },
        onClick: () => {
          if (
            !window.location.href.includes('?action=new') &&
            window.location.href.includes('designs')
          ) {
            this.handleSave('save', this.props.formName);
          } else {
            this.props.setHideSaveDialog(false);
          }
        },
      },
      {
        key: 'saveAs',
        name: 'Save As',
        iconProps: {
          iconName: 'Save',
        },
        onClick: () => {
          this.props.setHideSaveDialog(false);
        },
      },
      {
        key: 'upload',
        name: 'Import',
        iconProps: {
          iconName: 'Upload',
        },
        onClick: () => {
          this.setState(state => ({ hideImportMenu: false }));
        },
      },

      {
        key: 'download',
        name: 'Export',
        iconProps: {
          iconName: 'Download',
        },
        onClick: () => {
          db.table('files')
            .toArray()
            .then(files => {
              const formAttachedFiles = utility.arrayToObject(files, 'uuid');
              const parsedForm = serialize(this.props.form, formAttachedFiles);
              download('export.json', JSON.stringify(parsedForm, 0, 4));
            });
        },
      },
      {
        componentRef: this._button,
        id: 'button',
        key: 'DatePicker',
        name: 'Files',
        iconProps: {
          iconName: 'Attach',
        },
        subMenuProps: {},
        menuAs: this._onRenderMenu,
      },
      {
        componentRef: this._devbutton,
        id: 'devbutton',
        key: 'DevButton',
        name: 'Dev',
        iconProps: {
          iconName: 'DeveloperTools',
        },
        subMenuProps: {},
        menuAs: this._devonRenderMenu.bind(this),
      },
      {
        key: 'settings',
        name: 'Settings',
        iconProps: {
          iconName: 'Settings',
        },
      },
    ];
  };
  getFarItems = () => {
    return [
      {
        key: 'copy',
        name: 'Duplicate',
        iconProps: {
          iconName: 'Copy',
        },
        disabled: !this.props.activeEntityUUID,
        onClick: () =>
          duplicateHandler(this.props.currentSelected, this.props.createEntity),
      },
    ];
  };
  getUploadItem = () => {
    return [
      {
        key: 'upload',
        name: 'Upload',
        iconProps: {
          iconName: 'Upload',
        },
      },
    ];
  };
  render() {
    return (
      <div>
        <CommandBar
          items={this.getItems()}
          // overflowButtonthis.props={{ ariaLabel: 'More commands' }}
          farItems={this.getFarItems()}
          ariaLabel={
            'Use left and right arrow keys to navigate between commands'
          }
          createEntity={this.props.createEntity}
        />
        {this.props.fetchStatus === 'pending' && (
          <ProgressIndicator label="Loading Form" />
        )}
        {(this.props.fetchStatus === 'success' ||
          this.props.fetchStatus === 'error') &&
          this.state.showMessageBar && (
            <MessageBar
              messageBarType={MessageBarType[this.props.fetchStatus]}
              isMultiline={false}
              onDismiss={e => this.setShowMessageBar()}
              dismissButtonAriaLabel="Close"
            >
              {this.props.message || 'No text'}
            </MessageBar>
          )}
        <SaveDialog
          setHideSaveDialog={this.props.setHideSaveDialog}
          handleSave={this.handleSave}
          setName={this.setName}
          hideSaveDialog={this.props.hideSaveDialog}
          name={this.state.name}
        />
        <ImportDialog
          updateMultiple={this.props.updateMultiple}
          setMessage={this.props.setMessage}
          serverGetSuccess={this.props.serverGetSuccess}
          hideImportMenu={this.state.hideImportMenu}
          setHideImportMenu={this.setHideImportMenu}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const {
    app: {
      activeEntityUUID,
      fetchStatus,
      formAttachedFiles,
      cdartStatus: { hideSaveDialog, message, formName },
    },
    form,
  } = state;
  return {
    formName,
    message,
    hideSaveDialog,
    formAttachedFiles,
    activeEntityUUID,
    form,
    _appState: state,
    currentSelected: state.form[activeEntityUUID],
    fetchStatus,
  };
};

const _Toolbar = connect(
  mapStateToProps,
  {
    createEntity,
    serializeForm,
    testLoadForm,
    loadFormState,
    addFiles,
    deleteFiles,
    updateFiles,
    setHideSaveDialog,
    updateMultiple,
    setMessage,
    serverGetSuccess,
    // exportAppState,
  }
)(Toolbar);
export default _Toolbar;

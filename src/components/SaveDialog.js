import React from 'react';
import {
  Dialog,
  DialogFooter,
  PrimaryButton,
  DefaultButton,
} from 'office-ui-fabric-react/';
export function SaveDialog(props) {
  const { hideSaveDialog, name, setName } = props;
  return (
    <Dialog
      hidden={hideSaveDialog}
      dialogContentProps={{
        name: 'Set Form name',
        subText:
          'Enter a unique version name below. Name must begin with a letter, $ or underscore and may not contain a hyphen, backslash, forward slash, semicolon or a colon.',
      }}
      modalProps={{
        isBlocking: true,
      }}
    >
      <input onChange={e => setName(e.target.value)} value={name} />
      <DialogFooter>
        <PrimaryButton
          onClick={e => {
            props.handleSave('save', props.name);
          }}
          text="Save"
        />
        <DefaultButton
          onClick={e => props.setHideSaveDialog(true)}
          text="Cancel"
        />
      </DialogFooter>
    </Dialog>
  );
}

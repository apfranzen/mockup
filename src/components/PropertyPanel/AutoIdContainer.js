import React, { Component, useState } from 'react';
import { connect } from 'react-redux';
import { utility } from '../../lib/utility';
import { helpers } from '../../lib/helpers';
import {
  customTabOrder,
  setAutoId,
  batchUpdate,
  updateProperty,
  selectEntity,
} from '../../redux-modules/actions';
import {
  indent,
  unindent,
  applyExternalId,
  tabNameReconcile,
  parseExternalIdentifier,
  incrementLetter,
} from './autoIdHelpers';
import AutoIdLayout from './AutoIdLayout';
import { isFormInput } from '../../model/Types';

const getIndex = (id, formInputArrByTabOrder) =>
  formInputArrByTabOrder
    .map(x => {
      return x.uuid;
    })
    .indexOf(id);

function AutoIdContainer(props) {
  const autoIds = props.model[0].autoIds;
  function handleTabNameReconcile(form) {
    props.batchUpdate(tabNameReconcile(form));
  }

  const nextBit = () => {
    const { formInputArrByTabOrder } = props;
    const targetIndex = getIndex(activeEntityUUID, formInputArrByTabOrder);

    const prevIndex = targetIndex - 1;
    const targetInput = props.model[activeEntityUUID];
    const prevInput = prevIndex >= 0 ? formInputArrByTabOrder[prevIndex] : null;
    const targetRule = targetInput.autoNumberRule.split(',');
    const targetLastRuleBit = targetInput.autoNumberRule.split(',')[
      targetInput.autoNumberRule.split(',').length - 1
    ];
    const previousLastRuleBit =
      prevIndex >= 0
        ? prevInput.autoNumberRule.split(',')[
            prevInput.autoNumberRule.split(',').length - 1
          ]
        : null;
    const previousExtIdArr =
      prevIndex >= 0
        ? parseExternalIdentifier(
            prevInput.externalIdentifier,
            model[0].autoIds
          )
        : null;
    const targetExtIdArr = parseExternalIdentifier(
      targetInput.externalIdentifier,
      model[0].autoIds
    );

    const prevLastIdBit =
      prevIndex >= 0 ? previousExtIdArr[targetRule.length] : null;
    const checkTwo = targetRule[targetRule.length - 1];

    if (
      targetIndex === 0 ||
      (previousExtIdArr && targetExtIdArr.length >= previousExtIdArr.length)
    ) {
      if (targetLastRuleBit.includes('N')) {
        return { 'L+': 'a' };
      } else {
        return { 'N+': 1 };
      }
    } else if (
      previousExtIdArr &&
      targetExtIdArr.length <= previousExtIdArr.length
    ) {
      if (checkTwo.includes('N+')) {
        return { 'L+': 'a' };
      } else if (targetLastRuleBit.includes('N')) {
        return { 'L+': incrementLetter(prevLastIdBit) };
      } else {
        return { 'N+': Number(prevLastIdBit) + 1 };
      }
    }
    return { 'N+': 1 };
  };

  function handleReorder(sourceId, destinationId) {
    const { formInputArrByTabOrder } = props;
    const autoIds = props.model[0].autoIds;
    const sourceIndex = getIndex(sourceId, formInputArrByTabOrder);
    const destinationIndex = getIndex(destinationId, formInputArrByTabOrder);

    function handleReorder(formInputArrByTabOrder) {
      return helpers
        .reorderArray(sourceIndex, destinationIndex, formInputArrByTabOrder)
        .map((input, index) => ({
          ...input,
          tabOrder: index + 1,
        }));
    }

    const inputsByOrder = handleReorder(
      formInputArrByTabOrder,
      sourceIndex,
      destinationIndex
    );

    props.batchUpdate(
      utility.arrayToObject(applyExternalId(inputsByOrder, autoIds), 'uuid')
    );
  }

  function unindentHandler() {
    const { formInputArrByTabOrder, activeEntityUUID } = props;
    const updatedRule = unindent(
      formInputArrByTabOrder[getIndex(activeEntityUUID, formInputArrByTabOrder)]
        .autoNumberRule
    );
    console.log(updatedRule);

    const updatedFormInputs = formInputArrByTabOrder.map((item, index) =>
      item.uuid === activeEntityUUID
        ? Object.assign(
            {},
            formInputArrByTabOrder[
              getIndex(activeEntityUUID, formInputArrByTabOrder)
            ],
            {
              autoNumberRule: updatedRule.toString(),
            }
          )
        : item
    );

    props.batchUpdate(
      utility.arrayToObject(
        applyExternalId(updatedFormInputs, props.model[0].autoIds),
        'uuid'
      )
    );
  }

  function indentHandler() {
    const { formInputArrByTabOrder, activeEntityUUID } = props;
    console.log('indent', activeEntityUUID, formInputArrByTabOrder);
    const updatedRule = indent(
      formInputArrByTabOrder[getIndex(activeEntityUUID, formInputArrByTabOrder)]
        .autoNumberRule
    );

    const updatedFormInputs = formInputArrByTabOrder.map((item, index) =>
      item.uuid === activeEntityUUID
        ? Object.assign(
            {},
            formInputArrByTabOrder[
              getIndex(activeEntityUUID, formInputArrByTabOrder)
            ],
            {
              autoNumberRule: updatedRule.toString(),
            }
          )
        : item
    );

    props.batchUpdate(
      utility.arrayToObject(
        applyExternalId(updatedFormInputs, props.model[0].autoIds),
        'uuid'
      )
    );
  }

  function moveUpHandler(e, targetIndex, ruleValue) {
    const { formInputArrByTabOrder, activeEntityUUID } = props;

    const ruleArr = formInputArrByTabOrder[
      getIndex(activeEntityUUID, formInputArrByTabOrder)
    ].autoNumberRule.split(',');

    const updatedExtId = ruleArr.map((bit, index) =>
      index === targetIndex ? ruleValue : bit
    );
    console.log({ targetIndex, ruleValue });
    const updatedFormInputs = formInputArrByTabOrder.map((item, index) =>
      item.uuid === activeEntityUUID
        ? Object.assign(
            {},
            formInputArrByTabOrder[
              getIndex(activeEntityUUID, formInputArrByTabOrder)
            ],
            {
              autoNumberRule: updatedExtId.toString(),
            }
          )
        : item
    );

    const result = applyExternalId(updatedFormInputs, props.model[0].autoIds);

    props.batchUpdate(utility.arrayToObject(result, 'uuid'));
  }

  function toggleCustomTabOrder(event) {
    const { customTabOrder } = props;
    const value = props.model[0].customTabOrder;
    if (!value) {
      // if turning customTabOrder off, order inputs according to DOM layout
      const sortedResult = helpers
        .entitiesByDomOrder(props.model)
        .filter(isFormInput)
        .map((input, index) => ({ ...input, tabOrder: index + 1 }));
      props.batchUpdate(utility.arrayToObject(sortedResult, 'uuid'));
    }

    customTabOrder(!value);
  }

  function setAutoIdProp(event) {
    const { formInputArrByTabOrder } = props;
    const target = event.target;
    const value = target.name === 'enabled' ? !target.checked : target.value;
    const name = target.name;
    console.log({ [name]: value });

    props.setAutoId({
      [name]: value,
    });

    // update all of the externalIdentifiers if enabling autoIds
    if (name === 'enabled' && value) {
      props.batchUpdate(
        utility.arrayToObject(
          applyExternalId(
            formInputArrByTabOrder,
            props.model[0],
            name === 'prefix' ? value : props.model[0].autoIds.prefix,
            name === 'separator' ? value : '.'
          ),
          'uuid'
        )
      );
    } else if (name === 'enabled' && !value) {
      props.batchUpdate(
        utility.arrayToObject(
          applyExternalId(formInputArrByTabOrder, props.model[0].autoIds),
          'uuid'
        )
      );
    }
  }

  const [direction, handleDirection] = useState(null);

  const customTabOrder = props.model[0].customTabOrder;
  const { activeEntityUUID } = props;

  const { formInputArrByTabOrder, model } = props;

  return (
    <AutoIdLayout
      autoIds={autoIds}
      model={model}
      formInputArrByTabOrder={formInputArrByTabOrder}
      customTabOrder={customTabOrder}
      activeEntityUUID={activeEntityUUID}
      selectEntity={props.selectEntity}
      toggleCustomTabOrder={toggleCustomTabOrder}
      nextBit={activeEntityUUID ? nextBit() : null}
      setAutoIdProp={setAutoIdProp}
      updateProperty={props.updateProperty}
      handleTabNameReconcile={handleTabNameReconcile}
      handleReorder={handleReorder}
      batchUpdate={batchUpdate}
      handleDirection={handleDirection}
      direction={direction}
      unindentHandler={unindentHandler}
      indentHandler={indentHandler}
      moveUpHandler={moveUpHandler}
    />
  );
}

const mapStateToProps = state => ({
  model: { ...state.form },
  activeEntityUUID: state.app.activeEntityUUID,

  formInputArrByTabOrder: utility.formInputsByTabOrder(state.form),
});
AutoIdContainer = connect(
  mapStateToProps,
  { customTabOrder, setAutoId, batchUpdate, updateProperty, selectEntity }
)(AutoIdContainer);

export default AutoIdContainer;

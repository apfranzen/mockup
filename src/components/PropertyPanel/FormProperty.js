import React, { Component } from 'react';
import AutoIdContainer from './AutoIdContainer';
const entityProperties = {
  marginBottom: '16px',
  marginLeft: '20px',
  padding: '6px',
};

class FormProperty extends Component {
  render() {
    return (
      <div style={{ ...entityProperties }}>
        <AutoIdContainer />
      </div>
    );
  }
}

export default FormProperty;

import React from 'react';
import { Icon } from 'office-ui-fabric-react/';
import props from 'ramda/es/props';
import { Lozenge } from '../../../containers/vals_deps/ConditionDisplay/Lozenge';
function list(files, deleteFiles) {
  const label = file => `${file.name}


  ${file.contentType}`;
  console.log(deleteFiles);
  return files.map(file => (
    <li key={file.name}>
      {`${file.name}`}
      <Lozenge color="#f50">{file.contentType}</Lozenge>
      <Icon onClick={e => deleteFiles(file.id)} iconName="Delete" />
    </li>
  ));
}
const FileList = ({ files, deleteFiles }) => {
  console.log({ files });

  return !files.length ? (
    <div>No Form Attached Files Uploaded</div>
  ) : (
    <div>{list(files, deleteFiles)}</div>
  );
};
export default FileList;

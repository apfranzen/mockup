import React from 'react';
import { DropTarget } from 'react-dnd';
const style = {
  border: '1px solid gray',
  height: '15rem',
  width: '15rem',
  padding: '2rem',
  textAlign: 'center',
};
const TargetBox = ({ canDrop, isOver, connectDropTarget }) => {
  const isActive = canDrop && isOver;
  return connectDropTarget(
    <div style={style}>{isActive ? 'Release to drop' : 'Drag file here'}</div>
  );
};
export default DropTarget(
  props => props.accepts,
  {
    canDrop(props, monitor) {
      console.log(monitor.getItem());
      return true;
    },
    drop(props, monitor) {
      console.log('noDrop');
      if (props.onDrop) {
        console.log('onDrop');

        props.onDrop(props, monitor);
      }
    },
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver(),
    canDrop: monitor.canDrop(),
  })
)(TargetBox);

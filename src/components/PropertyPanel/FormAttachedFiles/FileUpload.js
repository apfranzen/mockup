import React from 'react';
import TargetBox from './TargetBox';
import FileList from './FileList';
import { connect } from 'react-redux';
import { utility } from '../../../lib/utility';
import { setAutoId, batchUpdate } from '../../../redux-modules/actions';
import { Label } from '../../common/formInputs/Label';
// const accepts = useMemo(() => [FILE], []);
function FileUpload(props) {
  function handleFileDrop(item, monitor) {
    function getBase64(file, callback) {
      const reader = new FileReader();

      reader.addEventListener('load', () => callback(reader.result));

      reader.readAsDataURL(file);
    }

    // see CDART2 JSONInportExportController ln 132
    if (monitor) {
      var droppedFile = monitor.getItem().files[0];
      /*
        Strategy 1: handle file transformation client-side, persist in redux store and then serialize and send to server on save


      Strategy 2: send to CDART at http://cdart2-test.cscc.unc.edu:8080/CDART2/formService/addFile
        - This will return:
              file: {
                contentType: "image/png"
                fileContent: "iVBORw0KGgoAAAANSUhEUgAAAUUAAAEGCAYAAADy..." // this is a string of base64

                to show in <img>
                <img src="data:image/png;base64,iVBORw0KGgoAAA.."

              }
       */
      var formData = new FormData();
      console.log({ droppedFile });

      getBase64(droppedFile, function(base64Data) {
        props.setFormAttachedFile({
          contentType: droppedFile.type,
          name: droppedFile.name,
          fileContent: base64Data,
        });
      });

      // HTML file input, chosen by user
      formData.append('selectFile', droppedFile);
      console.log({ formData });

      // re-enable this

      // setDroppedFiles(droppedFile);

      //       axios
      //         .post(`${baseUrl}/CDART2/formService/addFile`, {
      //           data: formData,
      //           headers: {
      //             // Cookie: ''
      //           },
      //         })
      //         .then(function(response) {
      //           /*response:
      // "formAttachedFiles" : {
      // "3c8bfac2-2391-449f-cc78-2f8db6f18d10" : {
      // fileContent: '',
      //   "name" : "Screenshot from 2018-07-19 17-18-56.png",
      //   "contentType" : "image/png",
      //   "visible" : true
      // }
      // }, */
      //           // props.setFormAttachedFile(response.data);
      //           console.log(response);
      //         })
      //         .catch(function(error) {
      //           console.log(error.message);
      //         });
    }
  }

  return (
    <>
      <Label htmlFor="defaultState">Enable Form Attached Files:</Label>
      <input
        // style={cbInputStyle}
        id="defaultState"
        type="checkbox"
        name="enabled"
        checked={props.allowFormAttachedFiles}
      />
      {props.allowFormAttachedFiles
        ? [
            <TargetBox accepts="__NATIVE_FILE__" onDrop={handleFileDrop} />,
            <FileList files={props.formAttachedFiles} />,
          ]
        : null}
    </>
  );
}

const mapStateToProps = state => ({
  model: { ...state.form },

  formInputs: utility.formInputsByTabOrder(state.form),
  formAttachedFiles: Object.keys(state.form[0].formAttachedFiles).map(
    key => state.form[0].formAttachedFiles[key]
  ),
});

export default connect(
  mapStateToProps,
  { setAutoId, batchUpdate }
)(FileUpload);

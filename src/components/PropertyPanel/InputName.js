/** @jsx jsx */
import React, { Component, useState, useEffect } from 'react';
import { DragSource } from 'react-dnd';
import { DropTarget } from 'react-dnd';
import { utility } from '../../lib/utility';
import { Icon } from 'office-ui-fabric-react';
import { Lozenge } from '../../containers/vals_deps/ConditionDisplay/Lozenge';
import { css, jsx } from '@emotion/core';
import { EditStack } from './EditStack';
import {
  incrementLetter,
  decrementLetter,
  parseExternalIdentifier,
} from './autoIdHelpers';
const TestDiv = () => (
  // <div style={{ background: 'red', width: 100, height: 30 }}
  <p>test</p>
);

function defineBitType(ruleBit, nameBit, index) {
  if (index > 0 && ruleBit[index - 1] === 'N+') {
    return [['L+', 'a']];
  }
  if (ruleBit[index] === 'N') {
    return [['N', nameBit], , ['N+', String(Number(nameBit) + 1)]];
  } else if (ruleBit[index] === 'N+') {
    return [['N', String(Number(nameBit) - 1)], ['N+', nameBit]];
  } else if (ruleBit[index] === 'L') {
    return [['L', nameBit], ['L+', incrementLetter(nameBit)]];
  } else if (ruleBit[index] === 'L+') {
    return [['L', decrementLetter(nameBit)], ['L+', nameBit]];
  }
}

function InputName(props) {
  useEffect(() => {
    if (!props.active) {
      selectBit(null);
    }
  }, [props.active]);
  useEffect(() => {
    if (!props.autoIds.enabled) {
      props.selectEntity(null);
    }
  }, [props.autoIds]);
  const [isHovering, setIsHovering] = useState(false);
  const [whichBit, selectBit] = useState(null);
  function handleMouseHover() {
    setIsHovering(!isHovering);
  }

  function allowDrop(event) {
    event.preventDefault();
  }

  function dropHandler(event) {
    event.stopPropagation();
  }

  function mouseDownHandler(event, uuid) {
    if (props.autoIds.enabled) props.selectEntity(uuid);
  }

  const { customTabOrder, connectDragSource, connectDropTarget } = props;
  const externalIdBits = parseExternalIdentifier(
    props.input.externalIdentifier,
    props.autoIds
  );

  // can be used to conditionally add padding
  // const calcPadding = () => (props.input.autoNumberRule().includes('N,L+') ? '30px' : null);
  console.log(externalIdBits);
  const activeAndEnabled = props.active && props.autoIds.enabled;
  return connectDropTarget(
    connectDragSource(
      <span
        onMouseEnter={handleMouseHover}
        onMouseLeave={handleMouseHover}
        style={{
          cursor: props.customTabOrder ? 'grab' : null,
          ...(props.active
            ? {
                borderTop: '1px solid #0078d4',
                borderBottom: '1px solid #0078d4',
              }
            : {
                borderTop: '1px solid transparent',
                borderBottom: '1px solid transparent',
              }),
          background: props.index % 2 ? 'rgb(244,244,244)' : 'transparent',
          opacity: props.isDragging ? 0 : 1,
          display: 'grid',
          alignItems: 'center',
          gridColumnGap: 6,
          gridColumn: '1 /-1',
          gridTemplateColumns: '1fr 3fr',
          // fontWeight: props.active ? 'bold' : null,
          padding: 14,
          ...(props.isOver && props.canDrop && props.direction === 'up'
            ? { borderTop: '3px solid blue' }
            : {}),
          ...(props.isOver && props.canDrop && props.direction === 'down'
            ? { borderBottom: '3px solid green' }
            : {}),
        }}
        onMouseDown={e => mouseDownHandler(e, props.input.uuid)}
        id={props.index}
      >
        <span
          style={{
            // background: 'white',
            gridColumn: '1 / 2',
            diplay: 'grid',
            // alignItems: 'middle',
            // justifyItems: 'center',
            // borderRight: '6px solid rgb(244, 244, 244)',
          }}
        >
          {customTabOrder && (
            <Icon
              iconName="GripperBarHorizontal"
              style={{
                verticalAlign: 'middle',
              }}
            />
          )}
          <p
            style={{
              display: 'inline',
              // marginLeft: '6px',
              marginLeft: customTabOrder ? '6px' : `${16 + 6}px`,
            }}
          >{`${props.input.tabOrder}`}</p>
        </span>

        <span
          style={{
            // background: 'white',
            gridColumn: '2 / 3',
            // paddingLeft: 6,
          }}
        >
          <Lozenge color="#f50" style={{ padding: '8px 3px 8px 3px' }}>
            <span
              style={{
                // background: activeBit ? '#06f' : 'lightgrey',
                // padding: '6px',
                marginRight: 6,
              }}
            >
              <div
                style={{
                  maxWidth: '80px',
                  minWidth: '80px',
                  display: 'inline',
                  // color: 'lightgrey',
                  cursor: 'pointer',
                }}
                title="Click to add to rule"
              >
                {props.autoIds.enabled && `${props.autoIds.prefix}`}
              </div>
            </span>
            {externalIdBits.map((nameBit, index) => {
              const activeBit = whichBit === index;
              return (
                <span
                  kry={index}
                  onClick={e => {
                    e.stopPropagation();
                    return props.autoIds.enabled
                      ? whichBit !== index
                        ? selectBit(whichBit === index ? null : index)
                        : null
                      : null;
                  }}
                  style={{
                    background: activeBit ? '#06f' : 'lightgrey',
                    padding: '6px',
                    marginRight: 6,
                    // display: 'block',
                  }}
                >
                  <div
                    style={{
                      maxWidth: '80px',
                      minWidth: '80px',
                      display: 'inline',
                    }}
                  >
                    {console.log(
                      'here2: ',
                      props.input.autoNumberRule.split(',')[index]
                    )}
                    {activeBit ? (
                      <select
                        style={{ zIndex: 100 }}
                        value={props.input.autoNumberRule.split(',')[index]}
                        onBlur={e => selectBit(null)}
                        // mouseUp and mouseDown are required for firefox per this issue: https://github.com/facebook/react/issues/12584
                        onMouseDown={e => e.stopPropagation()}
                        onMouseUp={e => e.stopPropagation()}
                        onChange={e => {
                          e.stopPropagation();
                          console.log('moveUp1: ', props.input.autoNumberRule);

                          console.log('moveUp2', typeof e.target.value);
                          props.moveUpHandler(e, index, e.target.value);
                        }}
                      >
                        {defineBitType(
                          props.input.autoNumberRule.split(','),
                          nameBit,
                          index
                        ).map((option, index2) => (
                          <option key={index2} value={option[0]}>
                            {option[1]}
                          </option>
                        ))}
                      </select>
                    ) : (
                      `${nameBit}`
                    )}
                  </div>
                </span>
              );
            })}

            {props.active && (
              <>
                <Icon
                  iconName="TriangleLeft12"
                  onClick={
                    props.input.autoNumberRule.split(',').length > 1
                      ? props.unindentHandler
                      : null
                  }
                  style={{
                    color:
                      props.input.autoNumberRule.split(',').length > 1
                        ? 'blue'
                        : 'grey',
                    fontSize: '.8em',
                    paddingRight: '6px',
                    cursor: 'pointer',
                  }}
                  title="Unindent"
                  disable
                />

                <span
                  onClick={e => {
                    // e.stopPropagation();
                    selectBit(externalIdBits.length);
                    props.indentHandler(e, Object.keys(props.nextBit)[0]);
                  }}
                  style={{
                    // background: activeBit ? '#06f' : 'lightgrey',
                    padding: '0 6px 0 6px',
                    marginRight: 6,
                  }}
                >
                  <div
                    style={{
                      maxWidth: '80px',
                      minWidth: '80px',
                      display: 'inline',
                      color: 'lightgrey',
                      cursor: 'pointer',
                    }}
                    title="Click to add to rule"
                  >
                    {props.nextBit && Object.values(props.nextBit)[0]}
                  </div>
                </span>
                {/* <p>{JSON.stringify(props.nextBit)}</p> */}
              </>
            )}
          </Lozenge>

          {/* {props.active && (
            <div>
              <p>{props.input.autoNumberRule}</p>

              <EditStack
                autoIds={props.autoIds}
                activeEntityUUID={props.activeEntityUUID}
                indentHandler={props.indentHandler}
                unindentHandler={props.unindentHandler}
                moveUpHandler={props.moveUpHandler}
              />
            </div>
          )} */}
        </span>
      </span>
    )
  );
}

InputName = DragSource(
  props => 'uniqueInputItem',
  {
    beginDrag: props => ({
      type: 'uniqueInputItem',
      uuid: props.input.uuid,
      index: props.index,
      role: 'autoId',
    }),
    canDrag: props => props.customTabOrder,
  },
  (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
  })
)(InputName);

export default DropTarget(
  props => 'uniqueInputItem',
  {
    hover: (props, monitor, component) => {
      const item = monitor.getItem();
      const itemType = monitor.getItemType();
      const dragIndex = item.index;
      const hoverIndex = props.index;

      // Don't replace items with themselves
      if (dragIndex === hoverIndex) {
        return;
      }

      // only trigger reordering if type of entity is Tab
      if (itemType !== 'uniqueInputItem') {
        return;
      }

      // const monitorThis = clientOffset.y -
      const direction = hoverIndex > dragIndex ? 'down' : 'up';

      props.handleDirection(direction);

      // Time to actually perform the action
      // props.handleReorder(dragId, hoverId);
      // props.reorderTabs(dragIndex, hoverIndex);

      // Note: we're mutating the monitor item here!
      // Generally it's better to avoid mutations,
      // but it's good here for the sake of performance
      // to avoid expensive index searches.
      // item.index = hoverIndex;
    },
    canDrop: (props, monitor) => {
      const item = monitor.getItem();
      const sourceId = item.uuid;
      const destinationId = props.input.uuid;
      return sourceId !== destinationId;
    },
    drop: (props, monitor) => {
      const item = monitor.getItem();
      const sourceId = item.uuid;
      const destinationId = props.input.uuid;
      props.handleReorder(sourceId, destinationId);
    },
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    isOver: monitor.isOver(),
    clientOffset: monitor.getClientOffset(),
    dropResult: monitor.getDropResult(),
    initialClientOffset: monitor.getInitialClientOffset(),
    initialSourceClientOffset: monitor.getInitialSourceClientOffset(),
    getDifferenceFromInitialOffset: monitor.getDifferenceFromInitialOffset(),
  })
)(InputName);

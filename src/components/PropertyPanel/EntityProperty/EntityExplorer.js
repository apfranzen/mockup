import React, { Component } from 'react';
import DependencyWrapper from '../../../containers/vals_deps/dependency/DependencyWrapper';
import ValidationWrapper from '../../../containers/vals_deps/validation/ValidationWrapper';
import { EntityTypes } from '../../../model/Types';
import Accordion from './Accordion';
import { FormEntityProperties } from './FormEntityProperties';
import DisplayHeading from '../../common/DisplayHeading';

const entityProperties = {
  margin: '0px 6px 0px 6px',
  padding: '6px',
};

const TabTypes = {
  Properties: 0,
  Validations: 1,
  Depenendecies: 2,
};

const EntityExplorerRenderDef = {
  Default: [TabTypes.Properties],
  [EntityTypes.CDSTextInput]: [
    TabTypes.Properties,
    TabTypes.Validations,
    TabTypes.Depenendecies,
  ],
  [EntityTypes.TextInput]: [
    TabTypes.Properties,
    TabTypes.Validations,
    TabTypes.Depenendecies,
  ],
  [EntityTypes.EchoInput]: [
    TabTypes.Properties,
    TabTypes.Validations,
    TabTypes.Depenendecies,
  ],
  [EntityTypes.TextArea]: [TabTypes.Properties, TabTypes.Depenendecies],
  [EntityTypes.Checkbox]: [TabTypes.Properties, TabTypes.Depenendecies],
  [EntityTypes.SelectionInput]: [TabTypes.Properties, TabTypes.Depenendecies],
  [EntityTypes.TextBlock]: [TabTypes.Properties],
  [EntityTypes.ImageBlock]: [TabTypes.Properties],
  [EntityTypes.ASTextInput]: [TabTypes.Properties, TabTypes.Depenendecies],
  [EntityTypes.FormSection]: [TabTypes.Properties],
  [EntityTypes.Form]: [TabTypes.Properties],
};

class EntityExplorer extends Component {
  render() {
    if (!this.props.model) {
      return (
        <DisplayHeading legend={'Please select an entity to view property'} />
      );
    }
    const {
      model: { type },
      updateProperty,
      createEntity,
    } = this.props;
    const renderDef =
      EntityExplorerRenderDef[type] || EntityExplorerRenderDef['Default'];
    return (
      <div style={entityProperties}>
        {this.props.model ? (
          <div>
            <Accordion>
              {[
                <div key="properties" label={'Properties'} isOpen show>
                  <FormEntityProperties
                    model={this.props.model}
                    resizeEntity={this.props.resizeEntity}
                    updateProperty={updateProperty}
                    createEntity={createEntity}
                    customTabOrder={this.props.customTabOrder}
                    autoIds={this.props.autoIds}
                  />
                </div>,
                <div
                  key="validations"
                  label="Validators"
                  show={renderDef.includes(TabTypes.Validations)}
                >
                  <ValidationWrapper key={this.props.model.uuid} />
                </div>,
                <div
                  key="dependencies"
                  label="Dependencies"
                  show={renderDef.includes(TabTypes.Depenendecies)}
                >
                  {/* This forces re-render for dependencies to reset the selected condition */}
                  <DependencyWrapper key={this.props.model.uuid} />
                </div>,
              ]}
            </Accordion>
          </div>
        ) : (
          <p>Please select an entity</p>
        )}
      </div>
    );
  }
}

export default EntityExplorer;

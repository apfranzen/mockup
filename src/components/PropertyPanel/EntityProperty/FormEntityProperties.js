import { Form, Formik } from 'formik';
import React from 'react';
import InputPropertyTemplate from './InputPropertyTemplate';
import ASTextInputConfig from './inputSpecificConfig/ASTextInput';
import CDSTextInputConfig from './inputSpecificConfig/CDSTextInput';
import CheckboxConfig from './inputSpecificConfig/CheckBox';
import EchoInputConfig from './inputSpecificConfig/EchoInput';
import RandomizationInputConfig from './inputSpecificConfig/RandomizationInput';
import FormSectionConfig from './inputSpecificConfig/FormSection';
import ImageBlockConfig from './inputSpecificConfig/ImageBlock';
import SelectionInputConfig from './inputSpecificConfig/SelectionInput';
import TextAreaConfig from './inputSpecificConfig/TextArea';
import TextBlockConfig from './inputSpecificConfig/TextBlock';
import TextInputConfig from './inputSpecificConfig/TextInput';
// import _ from 'lodash';
import { Toggle, Separator, Stack } from 'office-ui-fabric-react';
import * as Yup from 'yup';
import { InputTypes } from '../../../model/Types';
/* empty vales to reset form values in the case that the toher initialValues object doesn't have a value for the field */
const defaultValues = {
  name: '',
  externalIdentifier: '',
  tabOrder: '',
  sasCodeLabel: '',
  QbyQ: '',
  dataType: '', // TextInput & others
  defaultContent: '', // TextInput
  maxLength: '', // TextInput
  defaultState: '', //Checkbox
  legend: '', //Tabs
  sizeRow: '', //TextArea
  mode: '', //SelectionInput
  options: '', //SelectionInput
  url: '', //ImageBlock
  altText: '', //ImageBlock
  title: '', //ImageBlock
  precision: '',
  fixed: false, // text input date
  timeZoneChoice: false, // text input date
  startingDate: '', // text input date #confirm
};

const configs = {
  TextInput: TextInputConfig,
  EchoInput: EchoInputConfig,
  CDSTextInput: CDSTextInputConfig,
  TextArea: TextAreaConfig,
  Checkbox: CheckboxConfig,
  SelectionInput: SelectionInputConfig,
  TextBlock: TextBlockConfig,
  ImageBlock: ImageBlockConfig,
  ASTextInput: ASTextInputConfig,
  FormSection: FormSectionConfig,
  RandomizationInput: RandomizationInputConfig,
};

export const FormEntityProperties = props => {
  const { model } = props;
  // Yup.addMethod(Yup.mixed, 'greaterThan', function(yourArgs) {
  //   const message = 'second failure message';
  //   return this.test('greaterThan', message, function(value) {
  //     return false;
  //   });
  // });
  const validateByType = {
    [InputTypes.string]: /^[a-zA-Z0-9_]*$/,
    [InputTypes.integer]: /^[-+]?\d+$/,
    [InputTypes.float]: /[-+]?[0-9]*\.?[0-9]+/,
    [InputTypes.date]: /^((0|1)\d{1})-((0|1|2)\d{1})-(\d{2})/,
  };
  const getInputSpecificValidation = values2 => {
    console.log(Object.keys(validateByType), values2.dataType);

    return {
      options: Yup.array()
        .of(
          Yup.object().shape({
            label: Yup.string()
              .required('Required')
              .matches(/^[a-zA-Z0-9_]*$/, 'Label Error Message'),
            value: Yup.string()
              .matches(
                validateByType[values2.dataType],
                `Value must of Data Type Selected: ${values2.dataType}`
              )
              .required('Required'), // these constraints take precedence
          })
        )
        .required('Must have options') // these constraints are shown if and only if inner constraints are satisfied
        .min(1, 'Minimum of 1 option'),
    };
  };
  const promptToggle = type => {
    const targetValue = props.model[type];

    props.resizeEntity(
      props.model.uuid,
      type,
      targetValue,
      targetValue ? -targetValue : 3,
      props.model.column
    );
  };

  // function duplicateHandler(model) {
  //   props.createEntity(
  //     4,
  //     model.type,
  //     model.row + 1,
  //     model.column,
  //     'new',
  //     _.omit(model, ['dependencies, validations', 'uuid'])
  //   );
  // }

  function promptEnabled(model, prompt) {
    if (
      props.model[prompt] &&
      props.model[prompt] > 0 &&
      prompt in props.model
    ) {
      return true;
    } else {
      return false;
    }
  }
  console.log('reload', promptEnabled(props.model, 'prePromptWidth'));
  const schema = Yup.object().shape({
    options: Yup.array().of(
      Yup.object().shape({
        label: Yup.string().required('Required'), // these constraints take precedence
        value: Yup.string().required('Required'), // these constraints take precedence
      })
    ),
    // .required('Must have friends') // these constraints are shown if and only if inner constraints are satisfied
    // .min(3, 'Minimum of 2 options required'),
  });

  const complicatedSchema = values =>
    Yup.lazy(values => {
      console.log(values);
      return Yup.object().shape({
        // prePromptWidth: Yup.number()
        // .positive('Must be non-negative')
        // .greaterThan()
        // .required(),
        // postPromptWidth: Yup.number()
        // .positive('Must be non-negative')
        // .test('test-name', 'custom failure message', function(value) {
        //   return false;
        // })
        // .required(),
        // name: Yup.string().required('Required'),
        width: Yup.number()
          .min(1, 'Must be at least 1')
          .required(),
        externalIdentifier: Yup.string().matches(
          /^[a-zA-Z0-9_]*$/,
          'External Id must only contain alphanumeric and underscore characters'
        ),
        // ...getInputSpecificValidation(values),
      });
    });

  return (
    <div>
      <Separator>Layout</Separator>
      {/* <div
      // style={{ display: 'flex', justifyContent: 'space-around' }}
      > */}
      <Stack horizontal horizontalAlign="space-around">
        <Toggle
          checked={promptEnabled(props.model, 'prePromptWidth')}
          label="Field Prompt"
          onChange={e => promptToggle('prePromptWidth')}
          disabled={!('prePromptWidth' in props.model)}
        />
        <Toggle
          checked={promptEnabled(props.model, 'postPromptWidth')}
          label="Post Prompt"
          onFocus={() => console.log('onFocus called')}
          onBlur={() => console.log('onBlur called')}
          onChange={e => promptToggle('postPromptWidth')}
          disabled={!('postPromptWidth' in props.model)}
        />
      </Stack>
      {/* </div> */}

      <Separator>Input Config</Separator>

      <Formik
        enableReinitialize={true}
        initialValues={{ ...defaultValues, ...model }}
        onSubmit={(values, { setSubmitting }) => {
          console.log('updateProperty', model.uuid, values);
          props.updateProperty(model.uuid, values);
        }}
      // validateOnBlur
      // validationSchema={schema}
      // validationSchema={complicatedSchema}
      >
        {formikProps => {
          const {
            errors,
            dirty,
            isSubmitting,
            handleSubmit,
            handleReset,
            setFieldValue,
          } = formikProps;

          const InputSpecificConfig = configs[model.type];

          return (
            <Form onSubmit={handleSubmit}>
              {model.topLevel ? null : (
                <InputPropertyTemplate
                  model={model}
                  customTabOrder={props.customTabOrder}
                  autoIds={props.autoIds}
                />
              )}
              <InputSpecificConfig
                model={model}
                values={formikProps.values}
                errors={formikProps.errors}
                setFieldValue={formikProps.setFieldValue}
                updateProperty={props.updateProperty}
              />
              <button
                type="button"
                className="outline"
                onClick={handleReset}
                disabled={!dirty || isSubmitting}
              >
                Reset
              </button>
              <button
                type="submit"
              // disabled={isSubmitting || !dirty || Object.keys(errors).length}
              >
                Submit
              </button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
};

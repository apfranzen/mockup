import React, { Component } from 'react';
import { Icon, Text } from 'office-ui-fabric-react';
// import PropTypes from 'prop-types';

class AccordionSection extends Component {
  // static propTypes = {
  //   children: PropTypes.instanceOf(Object).isRequired,
  //   isOpen: PropTypes.bool.isRequired,
  //   label: PropTypes.string.isRequired,
  //   onClick: PropTypes.func.isRequired,
  // };

  onClick = () => {
    this.props.onClick(this.props.label);
  };

  render() {
    const {
      onClick,
      props: { isOpen, label },
    } = this;

    return (
      <div
        style={{
          // background: isOpen ? '#fae042' : '#6db65b',
          borderTop: '1px solid black',
          padding: '5px 10px',
        }}
      >
        <div
          onClick={onClick}
          style={{
            cursor: 'pointer',
            display: 'flex',
            alignItems: 'center',
            background: 'rgb(244, 244, 244)',
            // justifyContent: 'center',
          }}
        >
          {!isOpen && <Icon iconName="CaretHollow" />}
          {isOpen && <Icon iconName="CaretSolid" />}
          <Text variant="large">{label}</Text>
          {/* <p style={{ fontSize: '16px', fontFamily: 'sans-serif' }}>{label}</p> */}
        </div>
        {isOpen && (
          <div
            style={{
              background: 'white',
              // border: '2px solid #008f68',
              marginTop: 10,
              padding: '10px 20px',
            }}
          >
            {this.props.children}
          </div>
        )}
      </div>
    );
  }
}

export default AccordionSection;

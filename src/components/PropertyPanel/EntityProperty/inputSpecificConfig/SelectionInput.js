/** @jsx jsx */
import React, { Component } from 'react';
import { SelectionInput } from '../../../common/formInputs/SelectionInput';
import { FieldArray, getIn, ErrorMessage } from 'formik';
import { EntityTypes, RenderModes } from '../../../../model/Types';
import { DataTypeByInput } from '../../../../containers/_validations';
import { Field } from 'formik';
import {
  Icon,
  initializeIcons,
  Text,
  Separator,
  Button,
  IconButton,
  CompoundButton,
} from 'office-ui-fabric-react/';
import { getDataTypeFormPartial } from './DataTypePartials';

import { css, jsx } from '@emotion/core';
import { DisplayState } from '../formikHelpers';
initializeIcons();

class SelectionInputConfig extends Component {
  render() {
    const { values, errors } = this.props;
    const DataTypeFormPartial = getDataTypeFormPartial(values.dataType);
    return (
      <div>
        {/* <DisplayState {...values} />; */}
        <div>
          <Field
            name="dataType"
            options={DataTypeByInput[EntityTypes.SelectionInput].map(
              dataType => ({
                label: dataType.charAt(0).toUpperCase() + dataType.slice(1),
                value: dataType,
              })
            )}
            label="Data Type"
            component={SelectionInput}
          />

          <DataTypeFormPartial {...this.props} />
          <Field
            name="mode"
            options={[RenderModes.menu, RenderModes.radio]}
            label="Display"
            component={SelectionInput}
          />
          <Separator>Options Config</Separator>

          <div>
            <FieldArray
              name="options"
              render={arrayHelpers => (
                <div>
                  <div
                    style={{
                      marginLeft: '10px',
                      display: 'grid',
                      gridGap: '6px',
                      gridTemplateColumns: '16px 40% 40% 16px',
                      padding: 6,
                    }}
                  >
                    <Text
                      style={{ gridColumn: 2, justifySelf: 'center' }}
                      variant="medium"
                    >
                      Label*:
                    </Text>
                    <Text
                      style={{ gridColumn: 3, justifySelf: 'center' }}
                      variant="medium"
                    >
                      Value*:
                    </Text>

                    {values.options &&
                      values.options.map((option, index) => (
                        <>
                          <p style={{ gridColumn: 1, alignSelf: 'center' }}>
                            {index + 1}
                          </p>
                          <Field
                            css={props => css`
                              padding: 0.25rem;
                              grid-column: 2;
                              height: 1.5rem;
                              border: 1px solid #ccc;
                              border-color: ${getIn(
                                props.errors,
                                `options.[${index}].label`
                              )
                                ? 'red'
                                : ''};
                              &:focus {
                                border-color: #007eff;
                                box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
                                  0 0 0 3px rgba(0, 126, 255, 0.1);
                                outline: none;
                              }
                            `}
                            name={`options[${index}].label`}
                            label="Label"
                            error={getIn(errors, `options.[${index}].label`)}
                          />

                          <Field
                            css={props => css`
                              padding: 0.25rem;
                              grid-column: 3;
                              height: 1.5rem;
                              border: 1px solid #ccc;
                              border-color: ${getIn(
                                errors,
                                `options.[${index}].value`
                              )
                                ? 'red'
                                : ''};
                              &:focus {
                                border-color: #007eff;
                                box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
                                  0 0 0 3px rgba(0, 126, 255, 0.1);
                                outline: none;
                              }
                            `}
                            name={`options[${index}].value`}
                            label="Value"
                            error={getIn(errors, `options.[${index}].value`)}
                          />
                          <Icon
                            style={{ gridColumn: 4, alignSelf: 'center' }}
                            iconName={'Remove'}
                            onClick={() => {
                              arrayHelpers.remove(index);
                            }}
                            title="Remove"
                          />
                          <ErrorMessage
                            render={msg => (
                              <Text
                                style={{ gridColumn: 2, color: 'red' }}
                                variant="small"
                              >
                                {msg}
                              </Text>
                            )}
                            name={`options[${index}].label`}
                          />
                          <ErrorMessage
                            render={msg => (
                              <Text
                                style={{ gridColumn: 3, color: 'red' }}
                                variant="small"
                              >
                                {msg}
                              </Text>
                            )}
                            name={`options[${index}].value`}
                          />
                        </>
                      ))}

                    <Button
                      style={{
                        gridColumn: '1 / 5',
                        width: '100%',
                      }}
                      text="Add"
                      iconProps={{ iconName: 'Add' }}
                      onClick={() =>
                        arrayHelpers.push({ label: '', value: '' })
                      }
                    />
                  </div>

                  {/* <i
                    style={{ color: 'green' }}
                    className="far fa-plus-circle fa"
                    onClick={() => arrayHelpers.push({ value: '', label: '' })}
                    title="Remove"
                  /> */}
                </div>
              )}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default SelectionInputConfig;

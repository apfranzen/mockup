import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SelectionInput } from '../../../common/formInputs/SelectionInput';
import { TextInput } from '../../../common/formInputs/TextInput';
import { helpers } from '../../../../lib/helpers';
import { Field } from 'formik';
import Checkbox from '../../../common/formInputs/Checkbox';

class EchoInputConfig extends Component {
  render() {
    const { errors, previousExtIds } = this.props;

    return (
      <div>
        <div>
          <Field
            name="defaultContent"
            label="Default Text"
            component={TextInput}
          />
          <Field
            name="source"
            component={SelectionInput}
            options={previousExtIds}
            label="Source Input"
          />
          <Checkbox name="editable" label="Editable Input" />
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { form } = state;
  const targetId = state.app.activeEntityUUID;
  const data = state.form;

  const result = helpers.priorIds(form, targetId);
  return { previousExtIds: result.map(id => data[id].externalIdentifier) };
};
EchoInputConfig = connect(mapStateToProps)(EchoInputConfig);

export default EchoInputConfig;

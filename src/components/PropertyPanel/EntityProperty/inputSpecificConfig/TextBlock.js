import React from 'react';
import { Icon, initializeIcons } from 'office-ui-fabric-react/';
import { Field } from 'formik';
import { TextArea } from '../../../common/formInputs/TextArea';
initializeIcons();

function TextBlockConfig(props) {
  const { errors } = props;

  function handleUpdate(e) {
    const value = e.target.value;
    props.updateProperty(props.model.uuid, {
      text: value,
    });
  }
  return (
    <div>
      <div>
        <div>
          <i className="fas fa-bolt" style={{ color: 'blue' }} /> Content
          Updates as you type
        </div>
        <Field
          name="text"
          label="Text"
          style={{ height: 200, width: 400 }}
          component={TextArea}
          onChange={handleUpdate}
          rows="3"
          cols="50"
        />

        <Icon iconName={'MarkDownLanguage'} />
      </div>
    </div>
  );
}

export default TextBlockConfig;

import React, { useState } from 'react';
import { SelectionInput } from '../../../common/formInputs/SelectionInput';
import { getIn } from 'formik';
import { TextInput } from '../../../common/formInputs/TextInput';
import { EntityTypes, DataType } from '../../../../model/Types';
import { timeZones } from '../../../../containers/vals_deps/timeZones';
import { Field, FieldArray } from 'formik';
import { DatePicker } from 'office-ui-fabric-react';
import { DisplayState } from '../formikHelpers';
import Checkbox from '../../../common/formInputs/Checkbox';

function DateFormPartial(props) {
  const { setFieldValue, values } = props;

  // const _onClick = () => {
  //   this.setState( value: null });
  // };

  const [dateTimeValue, setDateTimeValue] = useState(new Date());

  const _onFormatDate = date => {
    const handleDay =
      date.getDate().toString().length === 2
        ? String(date.getDate())
        : '0'.concat(String(date.getDate()));

    const handleMonth =
      date.getMonth() > 8
        ? String(date.getMonth() + 1)
        : '0'.concat(String(date.getMonth() + 1));

    return handleMonth + '-' + handleDay + '-' + date.getFullYear();
  };

  const _onSelectDate = date => {
    const formatted = _onFormatDate(new Date(date));
    setFieldValue('startingDate', formatted);
    // console.log(typeof someFunction(date));
    if (date) setDateTimeValue(date);
  };

  const _onParseDateFromString = value => {
    if (!value) return null;
    console.log(value);
    const values = (value || '').trim().split('-');
    const month = Math.max(1, Math.min(12, parseInt(values[0], 10))) - 1;
    const day = Math.max(1, Math.min(31, parseInt(values[1], 10)));

    let year = parseInt(values[2], 10);

    return new Date(year, month, day);
  };

  const DayPickerStrings = {
    months: [
      'January',
      'February',
      'March',
      'April',
      'May',
      'June',
      'July',
      'August',
      'September',
      'October',
      'November',
      'December',
    ],

    shortMonths: [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec',
    ],

    days: [
      'Sunday',
      'Monday',
      'Tuesday',
      'Wednesday',
      'Thursday',
      'Friday',
      'Saturday',
    ],

    shortDays: ['S', 'M', 'T', 'W', 'T', 'F', 'S'],

    goToToday: 'Go to today',
    prevMonthAriaLabel: 'Go to previous month',
    nextMonthAriaLabel: 'Go to next month',
    prevYearAriaLabel: 'Go to previous year',
    nextYearAriaLabel: 'Go to next year',
    closeButtonAriaLabel: 'Close date picker',

    isRequiredErrorMessage: 'Start date is required.',

    invalidInputErrorMessage: 'Invalid date format.',
  };

  return (
    <>
      {/* <DisplayState {...values} /> */}
      <Field
        name="precision"
        label="Date Precision"
        style={{ height: '6rem' }}
        options={[
          { label: 'Year', value: 'y' },
          { label: 'Month', value: 'm' },
          { label: 'Day', value: 'd' },
          { label: 'Hour', value: 'h' },
          { label: 'Minute', value: 't' },
          { label: 'Second', value: 's' },
        ]}
        // style={{ border: '3px solid blue' }}
        multiple
        onChange={evt => {
          // console.log({ setFieldValue });
          setFieldValue(
            'precision',
            [].slice
              .call(evt.target.selectedOptions)
              .map(option => option.value)
          );
        }}
        component={SelectionInput}
      />
      <Checkbox name="fixed" label="All date components required" />
      <Checkbox name="timeZoneChoice" label="Display Time Zone Options" />

      <Field
        name="timezone"
        component={SelectionInput}
        label="Time Zone"
        options={Object.keys(timeZones).map(tz => ({
          label: tz,
          value: tz,
        }))}
        onChange={evt => {
          setFieldValue(
            'timezone',
            [].slice
              .call(evt.target.selectedOptions)
              .map(option => option.value)[0]
          );
        }}
      />
      {/* <Field
        component={TextInput}
        name="startingDate"
        value={values['startingDate']}
        label="Calendar Starting Date"
      /> */}
      {/* <p>dateTimeValue: {JSON.stringify(dateTimeValue)}</p> */}
      <p>mm-dd-yyyy</p>
      <DatePicker
        label="Start date: "
        isRequired={false}
        allowTextInput={true}
        initialPickerDate={new Date()}
        name="startingDate"
        strings={DayPickerStrings}
        value={_onParseDateFromString(getIn(values, 'startingDate'))}
        formatDate={_onFormatDate}
        parseDateFromString={_onParseDateFromString}
        onSelectDate={_onSelectDate}
      />
    </>
  );
}
function CommonFormPartial(props) {
  return (
    <>
      <Field name="maxLength" label="Max Length" component={TextInput} />
      <Field name="defaultContent" label="Default Text" component={TextInput} />
    </>
  );
}

export const getDataTypeFormPartial = dataType => {
  const Fragments = {
    [DataType.string]: CommonFormPartial,
    [DataType.date]: DateFormPartial,
    [DataType.integer]: CommonFormPartial,
    [DataType.float]: CommonFormPartial,
  };
  if (!Fragments[dataType]) return () => <p>Select Data Type</p>;

  return Fragments[dataType];
};

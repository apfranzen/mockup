import React, { Component, useContext } from 'react';
import { AsyncSelect } from '../../../../lib/hooks';
import { apiUrl } from '../../../../redux-modules/actions';
import { HrefContext } from '../../../LayoutRoot';

function RandomizationInputConfig(props) {
  const { studyId } = useContext(HrefContext);

  return (
    <div>
      <p>RandomizationInput</p>
      <AsyncSelect
        trigger={true}
        url={`${apiUrl}/studies/${studyId}/randomizationOptions`}
        name="randomShim"
        label="Randomization"
        resolver={function(data) {
          return Object.keys(data).map(randomizationScriptValue => ({
            value: randomizationScriptValue,
            label: data[randomizationScriptValue],
          }));
        }}
      />
    </div>
  );
}

export default RandomizationInputConfig;

import React, { Component } from 'react';
import { DataTypeByInput } from '../../../../containers/_validations';
import { getDataTypeFormPartial } from './DataTypePartials';
import { SelectionInput } from '../../../common/formInputs/SelectionInput';
import { Field } from 'formik';

class TextInputConfig extends Component {
  render() {
    const { errors, model, values } = this.props;
    const DataTypeFormPartial = getDataTypeFormPartial(values.dataType);
    console.log({ errors });
    return (
      <div>
        <div>
          <Field
            name="dataType"
            options={DataTypeByInput[model.type].map(dataType => ({
              label: dataType.charAt(0).toUpperCase() + dataType.slice(1),
              value: dataType,
            }))}
            label="Data Type"
            component={SelectionInput}
          />
          <DataTypeFormPartial {...this.props} />
        </div>
      </div>
    );
  }
}

export default TextInputConfig;

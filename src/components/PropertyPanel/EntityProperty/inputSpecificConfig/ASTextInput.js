import React, { useEffect, useState, useContext } from 'react';
import { SelectionInput } from '../../../common/formInputs/SelectionInput';
import { Field } from 'formik';
import { AsyncSelect } from '../../../../lib/hooks';
import { apiUrl } from '../../../../redux-modules/actions';
import { SampleContext } from '../../../DevStateHandler';
import { HrefContext } from '../../../LayoutRoot';
// const studyId = 13;
function ASTextInputConfig(props) {
  const { studyId, formId, formVersion } = useContext(HrefContext);

  return (
    <div>
      <AsyncSelect
        trigger={true}
        url={`${apiUrl}/studies/${studyId}/dictionaries`}
        name="dictionaryName"
        label="Select Dictionary"
        onChange={e => {}}
        onChange={e => props.setFieldValue('dictionaryName', e.target.value)}
        resolver={function(data) {
          return data.map(item => ({
            value: `${item}`,
            label: `${item}`,
          }));
        }}
      />
    </div>
  );
}

export default ASTextInputConfig;

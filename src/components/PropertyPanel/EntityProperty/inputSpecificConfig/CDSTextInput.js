import React, { Component } from 'react';
import { SelectionInput } from '../../../common/formInputs/SelectionInput';
import { TextInput } from '../../../common/formInputs/TextInput';
import { DataTypeByInput } from '../../../../containers/_validations';
import { getDataTypeFormPartial } from './DataTypePartials';
import { Field } from 'formik';
import Checkbox from '../../../common/formInputs/Checkbox';
class CDSTextInputConfig extends Component {
  render() {
    const { errors, model, values } = this.props;
    const DataTypeFormPartial = getDataTypeFormPartial(values.dataType);
    return (
      <div>
        <div>
          <p>CDSTextInputConfig</p>
          <Field
            name="dataType"
            component={SelectionInput}
            options={DataTypeByInput[model.type].map(dataType => ({
              label: dataType.charAt(0).toUpperCase() + dataType.slice(1),
              value: dataType,
            }))}
            label="Data Type"
          />
          <DataTypeFormPartial {...this.props} />
          <Field
            component={TextInput}
            name="defaultContent"
            label="Default Text"
          />
          <Field
            name="cdsName"
            component={SelectionInput}
            options={['script1', 'script2']}
            label="Script"
          />
          <Field
            name="evaluationPolicy"
            component={SelectionInput}
            options={[
              { label: 'Every time form is loaded', value: 'RENDER_EVERY' },
              { label: 'When form is first loaded', value: 'RENVER_FIRST' },
              {
                label: 'If no collected data exists',
                value: 'IF_COLLECTED_DATA_NULL',
              },
            ]}
            label="Script Runs"
          />
          <Checkbox name="editable" label="Editable Input" />
        </div>
      </div>
    );
  }
}

export default CDSTextInputConfig;

import React, { useEffect, useState } from 'react';
import { SelectionInput } from '../../../common/formInputs/SelectionInput';
import { connect } from 'react-redux';
import { TextInput } from '../../../common/formInputs/TextInput';
import { Field } from 'formik';
import db from '../../../../db/db.js';

function ImageBlockConfig(props) {
  const { errors } = props;
  const [files, setImageFile] = useState([]);
  useEffect(() => {
    db.table('files')
      .toArray()
      .then(files => {
        const imageFiles = files.filter(file =>
          file.contentType.includes('image')
        );
        setImageFile(imageFiles);
      });
  }, [files]);
  return (
    <div>
      <div>
        <Field
          name="url"
          options={files.map(file => ({
            value: file.uuid,
            label: file.name,
          }))}
          label="Select Image"
          component={SelectionInput}
        />

        <Field name="alt" type="text" label="Alt Text" component={TextInput} />
        <Field name="title" type="text" label="Title" component={TextInput} />
      </div>
    </div>
  );
}
const mapStateToProps = (state, ownProps) => {
  const {
    form: {
      0: { formAttachedFiles },
    },
  } = state;
  return {
    formAttachedFiles,
  };
};
const ConnectedImageBlockConfig = connect(mapStateToProps)(ImageBlockConfig);

export default ConnectedImageBlockConfig;

import React, { Component } from 'react';
import { TextInput } from '../../../common/formInputs/TextInput';
import { Field } from 'formik';
import Checkbox from '../../../common/formInputs/Checkbox';
class CheckboxConfig extends Component {
  render() {
    const { errors } = this.props;

    return (
      <div>
        <div>
          <Checkbox name="defaultState" label="Default State" />
        </div>
      </div>
    );
  }
}

export default CheckboxConfig;

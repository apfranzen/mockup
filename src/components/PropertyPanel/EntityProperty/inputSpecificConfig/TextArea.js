import React, { Component } from 'react';
import { SelectionInput } from '../../../common/formInputs/SelectionInput';
import { utility } from '../../../../lib/utility';
import { Field } from 'formik';
class TextAreaConfig extends Component {
  render() {
    return (
      <div>
        <div>
          <Field
            name="sizeRows"
            options={utility.createArrRange(1, 50)}
            label="Height"
            component={SelectionInput}
          />
        </div>
      </div>
    );
  }
}

export default TextAreaConfig;

import React, { Component } from 'react';
import { TextInput } from '../../../common/formInputs/TextInput';
import { Field } from 'formik';
class FormSectionConfig extends Component {
  render() {
    const { errors } = this.props;
    console.log('here: ', this.props);

    return (
      <div>
        <div>
          <p>FormSection Config</p>
          <Field name="legend" label="Legend" component={TextInput} />
        </div>
      </div>
    );
  }
}

export default FormSectionConfig;

import React, { Component } from 'react';
import { TextInput } from '../../common/formInputs/TextInput';
import DisplayHeading from '../../common/DisplayHeading';
import { InputFeedback } from '../../common/formInputs/InputFeedback';
class LayoutControl extends Component {
  render() {
    const { values, errors, touched, availableColumns } = this.props;
    const availableWidthError =
      availableColumns < 0
        ? `Total Width of Input Exceeds Available Columns by ${Math.abs(
            availableColumns
          )}`
        : false;
    return (
      <div>
        <div>
          <div
          // style={{
          //   border: availableColumns < 0 ? `1px solid red` : '1px solid blue',
          // }}
          >
            <DisplayHeading legend="Layout" />
            <InputFeedback error={availableWidthError} />
            <TextInput
              name="prePromptWidth"
              type="number"
              label="Field Prompt Width"
              error={errors.prePromptWidth || availableColumns < 0}
            />
            <TextInput
              name="width"
              type="number"
              label="Width"
              error={errors.width || availableColumns < 0}
            />
            <TextInput
              name="postPromptWidth"
              type="number"
              label="Post Prompt Width"
              error={errors.postPromptWidth || availableColumns < 0}
            />
            {/* <p>
              {`Total entity can be increased ${availableColumns} more columns`}
            </p> */}
          </div>
        </div>
      </div>
    );
  }
}

export default LayoutControl;

import React, { Component } from 'react';
import { TextInput } from '../../common/formInputs/TextInput';
import { TextArea } from '../../common/formInputs/TextArea';
import { Field } from 'formik';

class InputPropertyTemplate extends Component {
  render() {
    const customTabOrder = this.props.customTabOrder;
    const autoIds = this.props.autoIds;
    const { errors } = this.props;
    return (
      <>
        <div>
          <Field name="name" label="Field Name" component={TextInput} />
          {/* <Field
            disabled={autoIds.enabled}
            name="externalIdentifier"
            label="Field Identifier"
            component={TextInput}
            title="Disabled if Auto Naming is enabled"
          /> */}
        </div>
        <div>
          {/* <Field
            disabled={autoIds.enabled}
            name="tabOrder"
            type="number"
            label="Tab Order"
            size="2"
            component={TextInput}
            title="Disabled if Auto Naming is enabled"
          /> */}

          <Field
            name="sasCodeLabel"
            type="text"
            label="Field Label"
            component={TextInput}
          />

          <Field
            name="QbyQ"
            type="text"
            label="QbyQ"
            component={TextArea}
            rows="3"
            cols="50"
          />

          {/* Conditional Functionailty Requested for use-case of wanting a prompt to be associated with a field for QxQ report */}
          {this.props.model.hasOwnProperty('prePromptWidth') &&
            !this.props.model.prePromptWidth && (
              <Field
                name="prePrompt"
                type="text"
                label="Field Prompt"
                component={TextArea}
                rows="3"
                cols="50"
              />
            )}

          {this.props.model.hasOwnProperty('postPromptWidth') &&
            !this.props.model.postPromptWidth && (
              <Field
                name="postPrompt"
                type="text"
                label="Post Prompt"
                component={TextArea}
                rows="3"
                cols="50"
              />
            )}
        </div>
      </>
    );
  }
}

export default InputPropertyTemplate;

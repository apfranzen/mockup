import React from 'react';
import { Text, Toggle, CompoundButton } from 'office-ui-fabric-react';
import { TextInput } from '../common/formInputs/TextInput';
import InputName from './InputName';

function AutoIdLayout(props) {
  const {
    autoIds,
    nextBit,
    customTabOrder,
    activeEntityUUID,
    selectEntity,
    mouseDownHandler,
    handleReorder,
    batchUpdate,
    handleDirection,
    direction,
    unindentHandler,
    indentHandler,
    moveUpHandler,
    formInputArrByTabOrder,
    updateProperty,
  } = props;
  return (
    <div>
      <div
        style={{
          display: 'grid',
          gridTemplateColumns: '1.75fr 3fr',

          marginBottom: 10,
        }}
      >
        <div
          style={{
            gridColumn: 1,
            gridRow: 1,
            background: 'white',
          }}
        >
          <Text style={{ fontWeight: 'bold' }} variant="xLarge">
            Auto Tab Order
          </Text>
          <Toggle
            checked={!customTabOrder}
            // onText="Custom"
            // offText="Auto"
            name="customTabOrder"
            onChange={props.toggleCustomTabOrder}
          />
        </div>

        <div
          style={{
            gridColumn: 2,
            gridRow: 1,
            background: 'white',
            // marginBottom: 12,
          }}
        >
          <Text
            style={{ fontWeight: 'bold', justifySelf: 'center' }}
            variant="xLarge"
          >
            Auto Naming
          </Text>
          <Toggle
            checked={autoIds.enabled}
            // onText="(by Tab Order)"
            // offText="Custom"
            name="enabled"
            onChange={props.setAutoIdProp}
          />

          <div
            style={{
              display: 'grid',
              gridTemplateColumns: '1fr 3fr',
              padding: 10,
              gridGap: 4,
            }}
          >
            <label style={{ gridColumn: 1 }} forhtml="prefix">
              Prefix:
            </label>
            <input
              style={{ gridColumn: 2 }}
              disabled={!autoIds.enabled}
              type="text"
              name="prefix"
              id="prefix"
              onChange={props.setAutoIdProp}
              value={autoIds.prefix ? autoIds.prefix : ''}
            />

            <label style={{ gridColumn: 1 }} forhtml="separator">
              Separator:
            </label>
            <input
              style={{ gridColumn: 2 }}
              disabled={!autoIds.enabled}
              type="text"
              name="separator"
              id="separator"
              onChange={props.setAutoIdProp}
              value={autoIds.separator ? autoIds.separator : ''}
            />

            <label style={{ gridColumn: 1 }} forHtml="externalIdentifier">
              {JSON.stringify(props.updateProperty)}
            </label>

            <input
              style={{ gridColumn: 2 }}
              disabled={autoIds.enabled || !activeEntityUUID}
              name="externalIdentifier"
              label="Field Identifier"
              component={TextInput}
              title="Disabled if Auto Naming is enabled"
              onChange={e =>
                props.updateProperty(activeEntityUUID, {
                  externalIdentifier: e.target.value,
                })
              }
              value={
                activeEntityUUID &&
                props.model[activeEntityUUID].hasOwnProperty(
                  'externalIdentifier'
                )
                  ? props.model[activeEntityUUID].externalIdentifier
                  : ''
              }
            />
          </div>
        </div>

        {/* <CompoundButton
          primary={true}
          secondaryText="Sync Tab Order and Auto Naming with Layout"
          disabled={false}
          checked={true}
          iconProps={{ iconName: 'Sync' }}
          onClick={e => props.handleTabNameReconcile(props.model)}
          style={{
            gridColumn: '1 / -1',
            gridRow: 2,
            minWidth: '100%',
          }}
        >
          Sync
        </CompoundButton> */}
      </div>
      <div style={{ marginLeft: 6 }}>
        {formInputArrByTabOrder
          .sort((a, b) => a.tabOrder - b.tabOrder)
          .map((input, index) => (
            <InputName
              autoIds={autoIds}
              nextBit={nextBit}
              customTabOrder={customTabOrder}
              key={index}
              activeEntityUUID={activeEntityUUID}
              selectEntity={selectEntity}
              index={index}
              input={input}
              mouseDownHandler={mouseDownHandler}
              active={activeEntityUUID === input.uuid}
              handleReorder={handleReorder}
              batchUpdate={batchUpdate}
              handleDirection={handleDirection}
              direction={direction}
              updateProperty={updateProperty}
              unindentHandler={unindentHandler}
              indentHandler={indentHandler}
              moveUpHandler={moveUpHandler}
            />
          ))}
      </div>
    </div>
  );
}
export default AutoIdLayout;

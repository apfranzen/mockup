import React, { Component } from 'react';
// import FormSection from '../../../data/FormSection';
// import Tab from './Tab';
import {
  selectTab,
  updateProperty,
  setPropertyPanelTab,
  setFormAttachedFile,
  resizeEntity,
  createEntity,
} from '../../redux-modules/actions';
import { connect } from 'react-redux';
import Tabs from '../common/Tabs/Tabs';
import Tab from '../common/Tabs/Tab';
// import { helpers } from '../../../lib/helpers';
// import { TabContent } from "./TabContent";
import { TabContent } from './TabContent';
import { translate } from '../../lib/translate';
import { getEntitiesInRow } from '../../redux-modules/formModelHelpers';
import { entityStylesConst } from '../../components/FormEntities/_styles';

const propertyPanelStyle = {
  // maxWidth: '35%',
  position: 'relative',
};

export const TabIds = {
  EntityExplorer: 'EntityExplorer',
  FormProperty: 'FormProperty',
};

class PropertyPanel extends Component {
  constructor(props) {
    super(props);

    this.activeRef = React.createRef();
    // this.state = {
    //   propertyPanelTab: TabIds.EntityExplorer,
    // };
  }

  mouseDownHandler = tabUUID => event => {
    // this.setState({ propertyPanelTab: tabUUID });
    this.props.setPropertyPanelTab(tabUUID);
  };

  dragOverHandler(event) {
    event.preventDefault();
  }

  componentDidMount() {
    if (this.activeRef.current) {
      this.activeRef.current.scrollIntoView();
    }
  }

  render() {
    const { model } = this.props;
    const { propertyPanelTab } = this.props;
    return (
      <div style={propertyPanelStyle}>
        {/* aligns bottom of div with FormTabs */}
        <Tabs style={{ marginBottom: '-6px' }}>
          <Tab
            mouseDownHandler={this.mouseDownHandler(TabIds.EntityExplorer)}
            active={propertyPanelTab === TabIds.EntityExplorer}
            uuid={TabIds.EntityExplorer}
            legend={
              model
                ? `${translate.entityTypeToDescriptor(model.type)}`
                : 'Entity'
            }
            accentColor={
              model && `${entityStylesConst[model.type].backgroundColor}`
            }
          />
          <Tab
            mouseDownHandler={this.mouseDownHandler(TabIds.FormProperty)}
            active={propertyPanelTab === TabIds.FormProperty}
            uuid={TabIds.FormProperty}
            legend="Form"
          />
        </Tabs>
        <TabContent
          activeTab={propertyPanelTab}
          model={model}
          updateProperty={this.props.updateProperty}
          resizeEntity={this.props.resizeEntity}
          createEntity={this.props.createEntity}
          setFormAttachedFile={this.props.setFormAttachedFile}
          allowFormAttachedFiles={this.props.allowFormAttachedFiles}
          customTabOrder={this.props.customTabOrder}
          autoIds={this.props.autoIds}
        />
      </div>
    );
  }
}

const mapStateToProps = state => {
  const { activeEntityUUID } = state.app;
  const { propertyPanelTab } = state.app;
  const model = activeEntityUUID ? state.form[activeEntityUUID] : null;
  const parentId = Object.keys(state.form).filter(
    entityId =>
      state.form[entityId].type === 'FormSection' &&
      state.form[entityId].children.includes(activeEntityUUID)
  );
  const allowFormAttachedFiles = state.form[0].allowFormAttachedFiles;

  return {
    model,

    propertyPanelTab,
    allowFormAttachedFiles,
    customTabOrder: state.form[0].customTabOrder,
    autoIds: state.form[0].autoIds,
  };
};

PropertyPanel = connect(
  mapStateToProps,
  {
    selectTab,
    updateProperty,
    setPropertyPanelTab,
    setFormAttachedFile,
    resizeEntity,
    createEntity,
  }
)(PropertyPanel);
export default PropertyPanel;

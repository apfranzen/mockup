import { helpers } from '../../lib/helpers';
import { utility } from '../../lib/utility';
import { FormInputTypes, isFormInput } from '../../model/Types';

/**
 *
 * @param {Number} evaluateThisIndexOfRule The index of the current ruleArr being evaluated
 * @param {Array} currentIdArr The last detirmined ID that will be used to detirmine the next ID
 * @param {String} currentRule The rule of the last detirmined ID that will be used to detirmine the next ID
 * @param {String} nextRule The rule of the next input that will be evaluated
 * @param {Number} currentIndex Index of current rule in arr of rules
 */
const handleLetter = (
  evaluateThisIndexOfRule,
  currentIdArr,
  currentRule,
  nextRule,
  currentIndex
) => {
  const nextRuleArr = nextRule.split(',');
  if (currentIndex === 0) {
    return 'a';
  }
  const previousLetter = currentIdArr.toString().match(/[a-z]+|[^a-z]+/gi)[
    evaluateThisIndexOfRule
  ];
  if (!previousLetter) {
    return 'a';
  } else if (nextRuleArr[evaluateThisIndexOfRule - 1] === 'N+') {
    // handle starting at a with prev N+
    return 'a';
  } else if (previousLetter && nextRuleArr[evaluateThisIndexOfRule] === 'L+') {
    return incrementLetter(previousLetter);
  } else if (previousLetter && nextRuleArr[evaluateThisIndexOfRule] === 'L') {
    return previousLetter;
  } else if (nextRuleArr[evaluateThisIndexOfRule] === 'N+') {
    return 'a';
  }
};

export function parseExternalIdentifier(extId, autoIds) {
  const { prefix, separator } = autoIds;

  if (extId.toString().includes(prefix)) {
    return extId
      .toString()
      .replace(prefix, '')
      .match(/[a-z]+|[^a-z]+/gi);
  } else {
    return extId.toString().match(/[a-z]+|[^a-z]+/gi);
  }
}
function incrementLetterChar(prev) {
  return prev !== 'z'
    ? String.fromCharCode(prev.charCodeAt(prev.length - 1) + 1)
    : 'a';
}

export const incrementLetter = previousLettersStr => {
  /*
z => aa
aa => ab
ab => ac
az => ba
zz => zza
*/
  if (previousLettersStr[previousLettersStr.length - 1] === 'z') {
    if (previousLettersStr[previousLettersStr.length - 2] === 'z') {
      // case where all z's - time to concat 'a' to end
      return previousLettersStr.concat('a');
    }

    const secondToLast =
      previousLettersStr.length < 2 ||
      previousLettersStr[previousLettersStr.length - 2] === 'z'
        ? 'a'
        : incrementLetterChar(
            previousLettersStr[previousLettersStr.length - 2]
          );

    return previousLettersStr
      .slice(0, previousLettersStr.length - 2)
      .concat(secondToLast)
      .concat('a');
  }

  return previousLettersStr
    .slice(0, previousLettersStr.length - 1)
    .concat(
      incrementLetterChar(previousLettersStr[previousLettersStr.length - 1])
    );
};

export const decrementLetter = previousLetter => {
  if (previousLetter === 'a') return 'a';
  return String.fromCharCode(previousLetter.charCodeAt(0) - 1);
};

/**
 *
 * @param {String} nextRule The rule of the next input that will be evaluated
 * @param {Number} evaluateThisIndexOfRule The index of the current ruleArr being evaluated
 * @param {Array} currentIdArr The last detirmined ID that will be used to detirmine the next ID
 * @param {Number} currentIndex Index of current rule in arr of rules
 */
const handleNumber = (
  nextRule,
  evaluateThisIndexOfRule,
  currentIdArr,
  currentIndex
) => {
  const nextRuleArr = nextRule.split(',');

  // handle case where currentIndex === 0 and rule is N
  if (nextRuleArr[evaluateThisIndexOfRule] === 'N' && currentIndex === 0) {
    return '0';
  } else if (nextRuleArr[0] === 'N+' && currentIndex === 0) {
    // handle case where currentIndex === 0 and rule is N+
    return '1';
  } else if (nextRuleArr[evaluateThisIndexOfRule] === 'N+') {
    // case of N+,L+,N+
    return currentIdArr && evaluateThisIndexOfRule < currentIdArr.length
      ? incrementNumber(
          currentIdArr.toString().match(/[a-z]+|[^a-z]+/gi)[
            evaluateThisIndexOfRule
          ]
        )
      : 1;
  } else if (nextRuleArr[evaluateThisIndexOfRule] === 'N+') {
    // get next number rule: N+ (typical case)
    return currentIdArr.toString().match(/[a-z]+|[^a-z]+/gi)[
      evaluateThisIndexOfRule
    ] === undefined
      ? 1
      : incrementNumber(
          currentIdArr.toString().match(/[a-z]+|[^a-z]+/gi)[
            evaluateThisIndexOfRule
          ]
        );
  } else if (nextRuleArr[evaluateThisIndexOfRule] === 'N') {
    // N (typical case)

    return currentIdArr.toString().match(/[a-z]+|[^a-z]+/gi)[
      evaluateThisIndexOfRule
    ] === undefined
      ? 0
      : currentIdArr.toString().match(/[a-z]+|[^a-z]+/gi)[
          evaluateThisIndexOfRule
        ];
  }
};

const incrementNumber = previousN => Number(previousN) + 1;

/**
 *
 * @param {Array} ruleArr ex. ['N+', 'L+'] Array of the current auto name rule to evaluate
 * @param {Number} currentIndex  Index within ruleArr currently being evaluated
 * @param {String} currentId The last detirmined ID that will be used to detirmine the next ID
 * @param {Array} arrRules Array of all rules being evaluated
 */
const evaluateCurrentRule = (ruleArr, currentIndex, arrRules, currentId) => {
  const result = ruleArr
    .map((rule, index) => {
      return rule.includes('N')
        ? handleNumber(arrRules[currentIndex], index, currentId, currentIndex)
        : handleLetter(
            index,
            currentId,
            arrRules[currentIndex - 1],
            arrRules[currentIndex],
            currentIndex
          );
    })
    .toString()
    .replace(/,/g, '');

  return result;
};

/**
 *
 * @param {Array} arrRules ex. ['1', '1a'...]
 * @param {Number} goalIndex ex. 2
 * @param {Number} [currentId] optional - Current ID being evaluated
 * @param {Number} [currentIndex=0] optional - Current index of arrRules to evaluate
 */
export const getAutoName = (
  arrRules,
  goalIndex,
  currentIndex = 0,
  currentExternalId = ''
) => {
  const ruleArr = arrRules[currentIndex].split(',');

  if (goalIndex === 0) {
    console.log('hereeee1', currentExternalId);

    return evaluateCurrentRule(
      ruleArr,
      currentIndex,
      arrRules
      // currentExternalId
    );
  } else if (currentIndex === goalIndex) {
    const result = evaluateCurrentRule(
      ruleArr,
      currentIndex,
      arrRules,
      currentExternalId
    );
    return result;
  } else {
    const nextExternalId = evaluateCurrentRule(
      ruleArr,
      currentIndex,
      arrRules,
      currentExternalId
    );
    return getAutoName(arrRules, goalIndex, currentIndex + 1, nextExternalId);
  }
};

export function inputsByOrder(form, customTabOrder) {
  if (!customTabOrder) {
    // if turning customTabOrder off, order inputs according to DOM layout

    return helpers
      .entitiesByDomOrder(form)
      .filter(isFormInput)
      .map((input, index) => {
        return {
          ...input,
          tabOrder: index + 1,
        };
      });
  } else return utility.formInputsByTabOrder(form);
}

/**
 *
 * @param {object} form orderd by tabOrder
 * @param {boolean} merge overload - to merge with rest of form or not
 */
export const tabNameReconcile = (form, merge) => {
  const customTabOrder = form[0].customTabOrder;
  const autoIds = form[0].autoIds;
  const setExternalId = applyExternalId(
    inputsByOrder(form, customTabOrder),
    autoIds
  );

  const updatedSubset = utility.arrayToObject(setExternalId, 'uuid');
  if (!merge) {
    return updatedSubset;
  } else {
    return { ...form, ...updatedSubset };
  }
};

export const indent = rule => {
  const ruleArr = rule.split(',');

  if (ruleArr[ruleArr.length - 1] === 'N+') {
    return rule.concat(',L+');
  } else if (ruleArr[ruleArr.length - 1] === 'N') {
    return rule.concat(',L+');
  } else if (ruleArr[ruleArr.length - 1] === 'L') {
    return rule.concat(',N+');
  } else if (ruleArr[ruleArr.length - 1] === 'L+') {
    return rule.concat(',N+');
  }
};

export const unindent = rule => {
  const ruleArr = rule.split(',');
  const lastRuleBit = ruleArr[ruleArr.length - 1];
  if (lastRuleBit === 'L+' || (lastRuleBit === 'N+' && ruleArr.length > 1)) {
    ruleArr.pop();
    return ruleArr.toString();
  } else {
    return rule;
  }
};

/**
 *
 * @param {Array} arrAllInputs instantiated instances of formInputs
 * @param {Form} form
 * @param {Function} mutate
 */
export const applyExternalId = (inputsOrdered, autoIds) => {
  const { prefix, separator, enabled } = autoIds;
  if (!enabled) return inputsOrdered;

  const arrRules = inputsOrdered.map(input => input.autoNumberRule);
  const applyPrefix = prefix ? prefix : '';
  const applySeparator = separator ? separator : separator;

  return inputsOrdered.map((input, index) => {
    console.log(
      'parsed: ',
      parseExternalIdentifier(input.externalIdentifier, autoIds)
    );
    return Object.assign({}, input, {
      externalIdentifier: `${prefix}${getAutoName(
        arrRules,
        index,
        undefined,
        parseExternalIdentifier(input.externalIdentifier, autoIds)
      )}`,
    });
  });
};

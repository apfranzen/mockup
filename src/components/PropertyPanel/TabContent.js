import React from 'react';
import EntityExplorer from './EntityProperty/EntityExplorer';
import FormProperty from './FormProperty';
import DisplayHeading from '../common/DisplayHeading';

const tabContent = {
  EntityExplorer: EntityExplorer,
  FormProperty: FormProperty,
  default: () => (
    <DisplayHeading legend={'Please select an entity to view property'} />
  ),
};

export const TabContent = ({ activeTab, ...props }) => {
  const TabContent = tabContent[activeTab] || tabContent['default'];
  return <TabContent {...props} />;
};

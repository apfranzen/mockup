/** @jsx jsx */
import { jsx } from '@emotion/core';
import * as R from 'ramda';
import { Component } from 'react';
import { DragSource, DropTarget } from 'react-dnd';
import { findDOMNode } from 'react-dom';
import styled from '@emotion/styled';
import { compose } from 'redux';
import { accepts } from '../../../containers/FormEntityContainer';
import { EntityTypes } from '../../../model/Types';
import { Separator } from 'office-ui-fabric-react';

const StyledTab = styled('div')(props => ({
  opacity: props.isDragging ? 0 : 1,
  textAlign: 'center',
  cursor: 'move',
  // width: '140px',
  padding: '0px 6px 0px 6px',
  height: '1.2rem',
  // marginLeft: '4px',
  // border: '1px solid blue',
  display: 'inline-block',
  background: '',
  margin: '4px 0px 4px 0px',
  textShadow: props.active && !props.isDragging ? '0px 0px 1px black' : '',
  borderRight: '1px solid rgb(200, 198, 196)',
  // background: props.active ? 'white' : 'rgb(244, 244, 244)',
  // transition: 'background-color 0.2s ease-in-out',
}));

const Legend = styled('div')(props => ({
  fontSize: '.85rem',
  // margin: '0 6px 0 0',
  justifyContent: 'center',
  borderBottom: '3px solid rgb(244, 244, 244)',
  borderTop:
    props.active && !props.isDragging
      ? props.accentColor
        ? `3px solid ${props.accentColor}`
        : '3px solid#06f'
      : '3px solid rgb(244,244,244)',
}));

class Tab extends Component {
  render(props) {
    const { connectDragSource, connectDropTarget } = this.props;

    return (
      <StyledTab
        title={this.props.legend}
        onMouseDown={this.props.mouseDownHandler}
        draggable={this.props.draggable}
        isDragging={this.props.isDragging}
        isOver={this.props.isOver}
        item={this.props.item}
        itemType={this.props.itemType}
        active={this.props.active}
        accentColor={this.props.accentColor}
        ref={instance =>
          compose(
            connectDragSource(instance),
            connectDropTarget(instance)
          )
        }
      >
        {/* <Separator>{this.props.legend}</Separator>
        <p>{this.props.legend}</p> */}
        <Legend
          active={this.props.active}
          accentColor={this.props.accentColor}
          // style={{ marginTop: '4px' }}
          id={this.props.uuid}
        >
          <div>{this.props.legend}</div>
        </Legend>
      </StyledTab>
    );
  }
}
Tab = DragSource(
  props => 'uniqueTabItem',
  {
    canDrag: props => props.draggable,
    beginDrag: props => ({
      role: 'tab',
      id: props.model.uuid,
      index: props.index,
    }),
  },
  (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
  })
)(Tab);

export default DropTarget(
  props => R.flatten(['uniqueTabItem', accepts[EntityTypes.Form]]),
  {
    canDrop: (props, monitor) => {
      return true;
    },
    hover: (props, monitor, component) => {
      if (!props.isDropTarget) return; // prevents hover methods for Tabs that aren't intended to be drop targets
      const item = monitor.getItem();
      const itemType = monitor.getItemType();
      const dragIndex = item.index;
      const hoverIndex = props.index;
      const tabId = props.model.uuid;
      if (!props.draggable) return;

      if (itemType !== 'uniqueTabItem') {
        if (!props.active) props.selectTab(tabId);
      }

      // Don't replace items with themselves
      if (itemType === 'uniqueTabItem') {
        if (dragIndex === hoverIndex) {
          return;
        }

        // only trigger reordering if type of entity is Tab
        if (itemType !== 'uniqueTabItem') {
          return;
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(
          component
        ).getBoundingClientRect();

        // Get horizontal middle
        const hoverMiddleX =
          (hoverBoundingRect.right - hoverBoundingRect.left) / 2;

        // Determine mouse position
        const clientOffset = monitor.getClientOffset();

        // Get pixels to the top
        const hoverClientX = clientOffset.x - hoverBoundingRect.left;

        // Only perform the move when the mouse has crossed half of the items width
        // When dragging right, only move when the cursor is below 50%
        // When dragging left, only move when the cursor is above 50%

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientX < hoverMiddleX) {
          return;
        }

        // // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientX > hoverMiddleX) {
          return;
        }

        // Time to actually perform the action

        props.reorderTabs(dragIndex, hoverIndex);

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        item.index = hoverIndex;
      }
    },
  },
  (connect, monitor) => ({
    item: monitor.getItem(),
    itemType: monitor.getItemType(),
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    isOver: monitor.isOver(),
    clientOffset: monitor.getClientOffset(),
    dropResult: monitor.getDropResult(),
    initialClientOffset: monitor.getInitialClientOffset(),
    initialSourceClientOffset: monitor.getInitialSourceClientOffset(),
    getDifferenceFromInitialOffset: monitor.getDifferenceFromInitialOffset(),
  })
)(Tab);

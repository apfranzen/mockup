/** @jsx jsx */
import { css, jsx } from '@emotion/core';

function Tabs(props) {
  return (
    <div
      css={theme => css`
        display: flex;
        flex-wrap: wrap;
        max-width: 100%;
        white-space: nowrap;
        align-items: center;
        padding: 12px;
        background: rgb(244, 244, 244);
        /* overflow-x: scroll; */
      `}
      {...props}
    />
  );
}

export default Tabs;

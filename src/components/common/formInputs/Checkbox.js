import React from 'react';
import { Formik, Field, ErrorMessage } from 'formik';
import { Label } from './Label';
import { InputFeedback } from './InputFeedback';

export default function Checkbox(props) {
  return (
    <Field name={props.name}>
      {({ field, form }) =>
        console.log(field) || (
          <div
            style={{
              display: 'grid',
              gridTemplateColumns: `2fr 1fr`,

              // width: '60%',
              // backgroundColor: 'green',
              margin: '.5rem',
              ...props.style
            }}
          >
            <Label htmlFor={field.id} >{props.label}</Label>
            <input
              type="checkbox"
              {...props}
              style={{ justifySelf: 'start' }}
              checked={field.value}
              onChange={() => {
                if (field.value) {
                  form.setFieldValue(props.name, false);
                } else {
                  form.setFieldValue(props.name, true);
                }
              }}
            />
            {/* <ErrorMessage name={`${field.name}`} component={InputFeedback} /> */}
          </div>
        )
      }
    </Field>
  );
}

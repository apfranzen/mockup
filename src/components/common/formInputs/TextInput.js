/** @jsx jsx */
import { Field, ErrorMessage } from 'formik';
import React from 'react';
import { InputFeedback } from './InputFeedback';
import { Label } from './Label';
import { css, jsx } from '@emotion/core';
export const TextInput = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  ...props
}) => {
  const error = errors[field.name];
  const isTouched = touched[field.name];
  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: `1.5fr 3fr`,
        // justifyContent: 'space-between',
        // width: '60%',
        // backgroundColor: 'green',
        margin: '.5rem',
      }}
    >
      <Label htmlFor={field.id} error={error}>
        {props.label}
      </Label>
      <input
        {...field}
        {...props}
        css={props => css`
          padding: 0.25rem;
          grid-column: 2;
          height: 1.5rem;
          border: 1px solid #ccc;
          border-color: ${error ? 'red' : ''};
          &:focus {
            border-color: #007eff;
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075),
              0 0 0 3px rgba(0, 126, 255, 0.1);
            outline: none;
          }
        `}
        autocomplete="off"
      />
      <ErrorMessage name={`${field.name}`} component={InputFeedback} />
    </div>
  );
};

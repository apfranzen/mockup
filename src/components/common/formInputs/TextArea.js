import React from 'react';
import { Field, ErrorMessage } from 'formik';
import { Label } from './Label';
import { InputFeedback } from './InputFeedback';
export const TextArea = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  ...props
}) => {
  const error = errors[field.name];
  const isTouched = touched[field.name];
  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: `1.5fr 3fr`,
        // justifyContent: 'space-between',
        // width: '60%',
        margin: '.5rem',
      }}
    >
      <Label htmlFor={field.id} error={error}>
        {props.label}
      </Label>
      <textarea
        style={{ ...props.style, gridColumn: 2, height: '1.5rem' }}
        {...field}
        {...props}
      />

      <ErrorMessage name={`${field.name}`} component={InputFeedback} />
    </div>
  );
};

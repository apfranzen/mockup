import React from 'react';
import { Label } from './Label';
import { InputFeedback } from './InputFeedback';
import { Field } from 'formik';
import { Icon } from 'office-ui-fabric-react';
export const SelectionInput = ({
  field, // { name, value, onChange, onBlur }
  form: { touched, errors }, // also values, setXXXX, handleXXXX, dirty, isValid, status, etc.
  ...props
}) => {
  const { options } = props;
  const error = errors[field.name];
  const isTouched = touched[field.name];
  return (
    <div
      style={{
        display: 'grid',
        gridTemplateColumns: `1.5fr 3fr`,
        // justifyContent: 'space-between',
        // width: '60%',
        margin: '.5rem',
      }}
    >
      <Label htmlFor={field.id} error={error}>
        {props.label}
      </Label>
      <select
        {...field}
        {...props}
        style={{ gridColumn: 2, height: '1.5rem', ...props.style }}
      >
        <option value="">-- Make Selection --</option>
        {options.map(option =>
          typeof option !== 'object' ? (
            <option value={option}>{option}</option>
          ) : (
            <option value={option.value}>{option.label}</option>
          )
        )}
      </select>
      <InputFeedback error={error} />
    </div>
  );
};

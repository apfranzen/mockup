import React from 'react';
import { Text } from 'office-ui-fabric-react';
const test = { test: 'me' };
export const InputFeedback = error => (
  <div
    className="input-feedback"
    style={{ color: error ? 'red' : '', gridRow: 2, gridColumn: '2' }}
  >
    <Text variant="small">{error.children}</Text>
  </div>
);

import React from 'react';
import styled from '@emotion/styled';

const Wrapper = styled('div')`
  display: block;
  max-width: 100%;
`;

const FieldSet = styled('div')`
  border: 0.1rem black solid;
  border-radius: 0;
  overflow: hidden;
  display: table;
  margin: 0;
  padding: 0 0 0.05rem 0.05rem;
  text-align: center;
  width: 100%;
`;

const Input = styled('input')`
  display: none;
`;

const Label = styled('label')`
  display: table-cell;
  padding: 0.5rem 1rem;
  margin: 0 auto;
  background-color: ${props => (props.active ? '#3465a4' : 'white')};
  color: ${props => (props.active ? 'white' : 'black')};
  transition: background-color 0.2s ease-in-out;
  max-width: 100%;
`;

function RadioButton({ options, handleChange, value }) {
  return (
    <Wrapper class="fancy-radio-wrapper">
      <FieldSet class="fancy-radio-inner">
        {options.map(option => (
          <>
            <Input
              onClick={handleChange ? e => handleChange(option.value) : null}
              type="radio"
              id={option.label}
              name={'option.label'}
              value={option.value}
            />
            <Label
              style={{ width: `${100 / options.length}%` }}
              active={value === option.value}
              for={option.label}
            >
              {option.label}
            </Label>
          </>
        ))}
      </FieldSet>
    </Wrapper>
  );
}

export default RadioButton;

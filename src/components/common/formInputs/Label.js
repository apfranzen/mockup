import React from 'react';
import { Text } from 'office-ui-fabric-react';
/** @jsx jsx */
import { css, jsx } from '@emotion/core';

export const Label = ({ error, className, children, ...props }) => {
  return (
    <label
      css={props => css`
        grid-column: 1;
        color: ${error ? 'red' : ''};
        text-align: left;
        margin-right: 20px;
        /* white-space: nowrap; */
        // align-self: center;
        &:after {
          content: ${props.required ? ' *' : ''};
        };
        ${props.style}
      `}
      {...props}
    >
      <Text variant="medium">{children}:</Text>
    </label>
  );
};

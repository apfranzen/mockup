import React from 'react';

const DisplayHeading = props => (
  <p
    style={{
      fontWeight: '700',
      fontStyle: 'italic',
      textAlign: 'center',
    }}
  >
    {props.legend}
  </p>
);

export default DisplayHeading;

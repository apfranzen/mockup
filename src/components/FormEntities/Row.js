import React, { useContext } from 'react';
import ExistingRow from './ExistingRow';
import InsertNewRow from './InsertNewRow';
import ExistingRowPreview from './ExistingRowPreview';
import InsertNewRowPreview from './InsertNewRowPreview';
import { DevContext } from '../DevStateHandler';

export const AddType = {
  prior: 'prior',
  following: 'following',
};

function Row(props) {
  const { devMode } = useContext(DevContext);
  return (
    <div
      className="row"
      style={{
        gridColumn: `1 / ${props.model.width + 1}`,
        gridRow: props.gridRow,
        alignSelf: 'stretch',
        minHeight: 20,
        height: '100%',
        background: !props.createEntity ? 'red' : ''
      }}
    >
      {devMode && (
        <p
          style={{
            color: 'black',
            position: 'absolute',
            top: 0,
            right: 10,
            zIndex: 100000,
            fontSize: 8,
          }}
        >
          {JSON.stringify(props.numRows)}
        </p>
      )}

      <>
        {props.numRows > 0 && (
          <InsertNewRow {...props} mode={AddType.prior} />
        )}
        <ExistingRow {...props} />
        {props.numRows === props.gridRow && (
          <InsertNewRow {...props} mode={AddType.following} />
        )}
      </>

    </div>
  );
}

export default Row;

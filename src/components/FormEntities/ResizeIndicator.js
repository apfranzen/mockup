/** @jsx jsx */
import { css, jsx } from '@emotion/core';

export function ResizeIndicator({ width }) {
  return (
    <div
      css={theme => css`
        display: flex;
        align-items: center;
        justify-content: center;
        position: absolute;
        text-align: center;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: ${theme.colors.blue};
        z-index: 10000;
      `}
    >
      <p style={{ color: 'white', fontSize: '14px' }}>{width}</p>
    </div>
  );
}

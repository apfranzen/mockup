import React, { useEffect, useState } from 'react';

function InsertNewRowPreview(props) {
  const [validPossibleDrop, setValidPossibleDrop] = useState(false);
  const [timeOut, setTimeOut] = useState(false);
  const expand = validPossibleDrop && timeOut;
  useEffect(() => {
    if (props.isOver && props.canDrop) {
      setValidPossibleDrop(true);
    }

    return function cleanUp() {
      setValidPossibleDrop(false);
    };
  });

  useEffect(() => {
    if (validPossibleDrop) {
      setTimeout(() => {
        setTimeOut(true);
      }, 500);
    }
    return function cleanUp() {
      setTimeOut(false);
    };
  }, [validPossibleDrop]);

  const { i } = props;
  return (
    <div
      InsertNewRowPreview={i + 1}
      className="InsertNewRowPreview"
      style={{
        position: 'relative',
        display: 'grid',
        gridColumn: `1 / ${props.parentWidth + 1}`,
        alignSelf: 'stretch',
        // minHeight: 30,
        minHeight: expand ? 28 : 10,
        backgroundColor:
          props.isOver && props.canDrop ? 'rgba(144,238,144 ,.5 )' : null,
      }}
    />
  );
}

export default InsertNewRowPreview;

import React, { useContext } from 'react';
import { EntityTypes } from '../../model/Types';
import { DropTarget } from 'react-dnd';
import { ActionTypes } from '../../redux-modules/actions';

import FormEntityContainer from '../../containers/FormEntityContainer';
import { formStyle } from './_styles';
import { DevContext } from '../DevStateHandler';
const round = (value, decimals) =>
  Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);

function ExistingRow(props) {
  const { connectDropTarget } = props;
  const validPossibleDrop = props.canDrop;
  const { devMode } = useContext(DevContext);

  return (
    <div
      ref={connectDropTarget}
      // ExistingRow={i + 1}
      className="existingRow"
      style={{
        display: 'grid',
        position: 'relative',
        gridColumn: `1 / ${props.model.width + 1}`,
        gridTemplateColumns: `repeat(${props.model.width}, [col] ${formStyle.gridTemplateColumnWidth}px)`,
        // gridTemplateRows: 'minmax(20px, auto)', // forces entities in the same row to be
        gridColumnGap: '10px',
        // gridRow: `${i + 1}`,
        border: devMode ? '1px solid red' : '',
        alignSelf: 'stretch',
        // border: '1px solid green',
        // marginTop: 2,
        minHeight:
          !props.numRows && props.model.topLevel ? 'calc(100vh - 200px)' : 28,
        ...(!props.childrenEntities.filter(child => child.row === props.gridRow)
          .length
          ? {
            border: '3px solid rgba(0, 120, 212,.5 )',
            // backgroundImage: `url("data:image/svg+xml,%3Csvg width='15' height='15' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23c9c3d2' fill-opacity='0.4' fill-rule='evenodd'%3E%3Cpath d='M0 40L40 0H20L0 20M40 40V20L20 40'/%3E%3C/g%3E%3C/svg%3E")`,
          }
          : {}),
        // ...(!props.children.filter(child => child.row === props.gridRow).length
        //   ? {
        //       backgroundColor: '#3465a4',
        //       backgroundImage: `url("data:image/svg+xml,%3Csvg width='15' height='15' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23c9c3d2' fill-opacity='0.4' fill-rule='evenodd'%3E%3Cpath d='M0 40L40 0H20L0 20M40 40V20L20 40'/%3E%3C/g%3E%3C/svg%3E")`,
        //     }
        //   : {}),
        ...(validPossibleDrop
          ? { backgroundColor: 'rgba(0, 120, 212,.5 )' }
          : {}),

        // border: !props.children.filter(child => child.row === props.gridRow)
        //   .length
        //   ? '1px solid green'
        //   : null,
      }}
    >
      {devMode && (
        <p
          style={{
            color: 'red',
            position: 'absolute',
            top: 0,
            left: 50 * props.gridRow,
            zIndex: 100000,
          }}
        >
          {props.gridRow}
        </p>
      )}
      {props.childrenEntities
        .filter(child => child.row === props.gridRow)
        .map((child, index, array) => (
          <FormEntityContainer
            key={`${child.id}.${props.model.uuid}.fromExistingRow`}
            sectionUUID={props.sectionUUID}
            id={child.id}
            parentWidth={props.model.parentWidth}
          />
        ))}
    </div>
  );
}

export default DropTarget(
  props => props.accepts,
  {
    canDrop: (props, monitor) => {
      const dropTarget = props;
      const droppedItem = monitor.getItem();
      // handle nested drop
      if (droppedItem.model.type === 'FormSection') {
        console.log(dropTarget.model.uuid);

        // console.log(dropTarget.model.uuid, monitor.isOver({ shallow: true }));
        if (droppedItem.model.uuid === dropTarget.model.uuid) {
          console.log('here 2');

          //this allows a FormSection to be moved where the offset is short enough that the drop target on the drug FormSection needs to be dissallowed to allow the move
          return false;
        } else if (dropTarget.model.uuid === droppedItem.sectionUUID) {
          console.log('here: ', dropTarget.model.uuid);

          return true;
        } else {
          return true; // allows form sections to be added to an existing row - such as with a new tab with no content
        }
        // else if (monitor.isOver() && monitor.isOver({ shallow: true })) {
        //   return false;
        // }
      } else if (monitor.isOver() && monitor.isOver({ shallow: true })) {
        console.log('allow 2: ', dropTarget.model.uuid);
        console.log('here 3');
        return true;
      }
    },
    drop: (props, monitor) => {
      const item = monitor.getItem();
      const {
        model: { type, row },
        action,
      } = item;

      // some logic to allow/disallow drop
      const { createEntity, moveEntity } = props;
      const { gridWidth } = formStyle;
      const id = props.sectionUUID;
      const sectionWidthBeg = props.initForm;

      const destinationRow = props.gridRow;

      const whichColumn =
        round(
          (monitor.getSourceClientOffset().x - sectionWidthBeg) / gridWidth,
          0
        ) + 1;
      console.log(
        'ActionTypes.CREATEENTITY',
        action === ActionTypes.CREATEENTITY,
        createEntity,
        id,
        EntityTypes[type],
        destinationRow,
        whichColumn,
        'existing'
      );

      if (action === ActionTypes.CREATEENTITY) {
        createEntity(
          id,
          EntityTypes[type],
          destinationRow,
          whichColumn,
          'existing'
        );
      } else if (action === ActionTypes.MOVEENTITY) {
        const toNewRow = false;
        const from = {
          parentId: item.sectionUUID,
          row,
          column: item.model.column,
        };
        const to = {
          parentId: id,
          row: destinationRow,
          column: whichColumn,
        };

        moveEntity(item.model.uuid, toNewRow, from, to);
      }
    },
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    isOver: monitor.isOver(),
    clientOffset: monitor.getClientOffset(),
    dropResult: monitor.getDropResult(),
    initialClientOffset: monitor.getInitialClientOffset(),
    initialSourceClientOffset: monitor.getInitialSourceClientOffset(),
    getDifferenceFromInitialOffset: monitor.getDifferenceFromInitialOffset(),
  })
)(ExistingRow);

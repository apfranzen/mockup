import { EntityTypes } from '../../model/Types';

export var entityStylesConst = {
  [EntityTypes.FormSection]: {
    backgroundColor: 'rgba(243, 234, 95, 1)',
  },

  [EntityTypes.TextInput]: {
    backgroundColor: 'rgb(80, 165, 215)',
    // backgroundColor: '#6C788F',
  },

  [EntityTypes.SelectionInput]: {
    backgroundColor: 'rgb(55, 62, 152)',

    // backgroundColor: 'red',
  },

  [EntityTypes.TextArea]: {
    backgroundColor: 'rgb(225, 77, 128)',
    // backgroundColor: '#205EE2',
  },
  [EntityTypes.Checkbox]: {
    backgroundColor: 'rgb(109, 110, 113)',
    // backgroundColor: '#00C5EC',
  },
  [EntityTypes.TextBlock]: {
    backgroundColor: 'rgb(241, 85, 35)',
    // backgroundColor: 'purple',
  },
  [EntityTypes.ImageBlock]: {
    backgroundColor: 'rgb(189, 214, 49)',
    // backgroundColor: 'brown',
  },
  [EntityTypes.ASTextInput]: {
    backgroundColor: 'rgb(128, 32, 100)',
    // backgroundColor: 'green',
  },
  [EntityTypes.EchoInput]: {
    backgroundColor: 'rgb(231, 32, 37)', // backgroundColor: 'orange',
  },
  [EntityTypes.CDSTextInput]: {
    backgroundColor: 'rgb(47, 111, 183)', // backgroundColor: 'blue',
  },
  [EntityTypes.RandomizationInput]: {
    backgroundColor: 'rgb(158, 105, 163)', // backgroundColor: 'blue',
  },
};

export const offset = 32;

export const formStyle = {
  minWidth: 950, // fixed width based on data collection
  gridWidth: 40,
  gridTemplateColumnWidth: 30,
  gutter: 10,
};

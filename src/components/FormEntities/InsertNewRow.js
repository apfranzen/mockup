import React, { useEffect, useState } from 'react';
import { EntityTypes } from '../../model/Types';
import { DropTarget } from 'react-dnd';
import { ActionTypes } from '../../redux-modules/actions';
import { AddType } from './Row';
import { formStyle } from './_styles';
import { Icon } from 'office-ui-fabric-react';
const round = (value, decimals) =>
  Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);

function InsertNewRow(props) {
  const [validPossibleDrop, setValidPossibleDrop] = useState(false);
  const [timeOut, setTimeOut] = useState(false);
  const expand = validPossibleDrop;
  // const expand = validPossibleDrop && timeOut;
  useEffect(() => {
    if (props.isOver && props.canDrop) {
      setValidPossibleDrop(true);
    }

    return function cleanUp() {
      setValidPossibleDrop(false);
    };
  }, []);

  useEffect(() => {
    if (validPossibleDrop) {
      setTimeout(() => {
        setTimeOut(true);
      }, 500);
    }
    return function cleanUp() {
      setTimeOut(false);
    };
  }, [validPossibleDrop]);

  const { connectDropTarget, i } = props;
  const fullHeight = props.lastInSection && props.mode === AddType.following;
  return (
    <div
      style={{
        position: 'relative',
        display: 'grid',
        gridColumn: `1 / ${props.parentWidth + 1}`,
        alignSelf: 'stretch',
        // minHeight: 30,
        minHeight: 10,
        background: validPossibleDrop ? 'green' : '',
      }}
    >
      {expand && props.isOver && props.canDrop && (
        <Icon
          iconName="TriangleSolidRight12"
          style={{
            position: 'absolute',
            top: -12,
            zIndex: 1001,
            fontSize: '2em',
          }}
        />
      )}

      <div
        ref={connectDropTarget}
        InsertNewRow={i + 1}
        className="InsertNewRow"
        style={{
          position: 'absolute',
          height:
            expand && props.isOver && props.canDrop && true ? '100vh' : 10,
          backgroundColor:
            props.isOver && props.canDrop ? 'rgba(144,238,144 ,.5 )' : null,
          width: '100%',
          zIndex: '1000',
        }}
      />
    </div>
  );
}

export default DropTarget(
  props => props.accepts,
  {
    canDrop: (props, monitor) => {
      // return true;
      const item = monitor.getItem();
      const dropTarget = props;
      const droppedItem = monitor.getItem();
      return true;
      if (
        monitor.isOver() &&
        monitor.isOver({ shallow: true }) &&
        // logic to allow moving items to end of section
        props.mode !== 'following' &&
        droppedItem.model.uuid !== dropTarget.model.uuid
      )
        if (droppedItem.sectionUUID === dropTarget.model.uuid) {
          return (
            // prevents inserting to new row from being a drop target for immediately prior or following exsiting row
            item.model.row + 1 !== props.gridRow &&
            item.model.row !== props.gridRow
          );
        }

      return true;
    },

    hover: (props, monitor) => { },
    drop: (props, monitor) => {
      const item = monitor.getItem();
      const {
        model: { type, row },
        action,
      } = item;

      // some logic to allow/disallow drop
      const { createEntity, moveEntity } = props;
      const { gridWidth } = formStyle;
      const id = props.sectionUUID;
      const sectionWidthBeg = props.initForm;

      const destinationRow =
        props.mode === AddType.prior ? props.gridRow : props.gridRow + 1;

      const whichColumn =
        round(
          (monitor.getSourceClientOffset().x - sectionWidthBeg) / gridWidth,
          0
        ) + 1;

      if (action === ActionTypes.CREATEENTITY) {
        createEntity(id, EntityTypes[type], destinationRow, whichColumn, 'new');
      } else if (action === ActionTypes.MOVEENTITY) {
        const toNewRow = true;

        const from = {
          parentId: item.sectionUUID,
          row,
          column: item.model.column,
        };
        const to = {
          parentId: id,
          row: destinationRow,
          column: whichColumn,
        };

        moveEntity(item.model.uuid, toNewRow, from, to);
      }
    },
  },
  (connect, monitor) => ({
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    isOver: monitor.isOver(),
    isOverCurrent: monitor.isOver({ shallow: true }),
    clientOffset: monitor.getClientOffset(),
    dropResult: monitor.getDropResult(),
    initialClientOffset: monitor.getInitialClientOffset(),
    initialSourceClientOffset: monitor.getInitialSourceClientOffset(),
    getDifferenceFromInitialOffset: monitor.getDifferenceFromInitialOffset(),
  })
)(InsertNewRow);

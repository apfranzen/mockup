import React, { Component } from 'react';
import { utility } from '../../lib/utility';
import Prompt from './subentities/Prompt';
import { address, FormEntityUI } from '../../lib/address';
import { EntityTypes, PromptModes } from '../../model/Types';
import { ResizeIndicator } from './ResizeIndicator';
import Resizer from './subentities/Resizer';
import { formStyle } from './_styles';

const entityWrapperStyle = entity => ({
  display: 'grid',
  gridColumn: `1 / ${entity.column + utility.total(entity)}`,
  gridTemplateColumns: `repeat(${utility.total(entity)}, [col] 1fr)`,
  gridRow: `${entity.row}`,
  zIndex: '1',
  borderRadius: '2px',
  position: 'relative',
  alignSelf: 'start',
  backgroundColor: entity.type === EntityTypes.FormSection ? null : 'white',
});

class FormEntity extends Component {
  constructor(props) {
    super(props);
    this.sectionWidth = React.createRef();
  }

  render(props) {
    return (
      <div
        id={`hereThis${this.props.id}.${this.props.model.type}.wrapper`}
        style={{
          ...entityWrapperStyle(this.props.model),
        }}
      >
        {'prePromptWidth' in this.props.model &&
        this.props.model.prePromptWidth > 0 ? (
          <Prompt
            id={`${this.props.model.type}.${this.props.model.id}.prePrompt`}
            mode={PromptModes.prePrompt}
            width={this.props.model.prePromptWidth}
            type={this.props.model.type}
            model={this.props.model}
            dragPreview={this.props.dragPreview}
          />
        ) : null}

        <FormEntityUI
          accepts={this.props.accepts}
          parentWidth={this.props.parentWidth}
          root={this.props.root}
          sectionUUID={this.props.sectionUUID}
          offset={this.props.offset}
          id={this.props.model.uuid}
          initForm={this.props.initForm}
          gridWidth={formStyle.gridWidth}
          createEntity={this.props.createEntity}
          moveEntity={this.props.moveEntity}
          model={this.props.model}
          childrenEntities={this.props.childrenEntities}
          numRows={this.props.numRows}
          resizeIndicator={ResizeIndicator}
          resizer={Resizer}
          entityCandidate={this.props.entityCandidate}
        />
        {'postPromptWidth' in this.props.model &&
        this.props.model.postPromptWidth > 0 ? (
          <Prompt
            id="postPrompt"
            mode={PromptModes.postPrompt}
            width={this.props.model.postPromptWidth}
            type={this.props.model.type}
            model={this.props.model}
            dragPreview={this.props.dragPreview}
          />
        ) : null}
      </div>
    );
  }
}

export default FormEntity;

import React, { useContext } from 'react';
import { formStyle } from './_styles';
import { DevContext } from '../DevStateHandler';

const round = (value, decimals) =>
  Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);

function ExistingRowPreview(props) {
  const validPossibleDrop = props.canDrop;
  const { devMode } = useContext(DevContext);
  return (
    <div
      className="existingRow"
      style={{
        display: 'grid',
        position: 'relative',
        gridColumn: `1 / ${props.model.width + 1}`,
        gridTemplateColumns: `repeat(${props.model.width}, [col] ${formStyle.gridTemplateColumnWidth}px)`,
        gridColumnGap: '10px',
        border: devMode ? '1px solid red' : '',
        alignSelf: 'stretch',
        minHeight: 28,

        ...(validPossibleDrop
          ? { backgroundColor: 'rgba(144,238,144 ,.5 )' }
          : {}),
      }}
    >
      {devMode && (
        <p
          style={{
            color: 'red',
            position: 'absolute',
            top: 0,
            left: 50 * props.gridRow,
            zIndex: 100000,
          }}
        >
          {props.gridRow}
        </p>
      )}
    </div>
  );
}

export default ExistingRowPreview;

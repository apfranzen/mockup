/** @jsx jsx */
import React from 'react';
import { css, jsx } from '@emotion/core';
import { entityStylesConst } from '../_styles';

export default function Ribbon({ type }) {
  const entityColor = entityStylesConst[type].backgroundColor;
  return (
    <div
      // class="ribbon-wrapper-green"
      css={props => css`
        width: 50px;
        height: 53px;
        overflow: hidden;
        position: absolute;
        top: -3px;
        right: -3px;
        &:after {
          content: ${props.required ? ' *' : ''};
        }
      `}
    >
      <div
        // class="ribbon-green"
        css={props => css`
          -webkit-transform: rotate(45deg);
          -moz-transform: rotate(45deg);
          -ms-transform: rotate(45deg);
          -o-transform: rotate(45deg);
          position: relative;
          padding: 1.2px 0;
          left: -5px;
          top: 15px;
          width: 120px;
          background-color: ${entityColor};
          color: purple;
          -webkit-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.3);
          -moz-box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.3);
          box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.3);
          &:before {
            left: 0;
          }
          &:after {
            content: '';
            border-top: 3px solid #6e8900;
            border-left: 3px solid transparent;
            border-right: 3px solid transparent;
            position: absolute;
            bottom: -3px;
            right: 0;
          }
        `}
      />
    </div>
  );
}

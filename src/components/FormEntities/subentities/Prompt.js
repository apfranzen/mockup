import React, { useContext } from 'react';
import TextareaAutosize from 'react-autosize-textarea';
import Resizer from './Resizer';
import { ResizeContext } from '../../../containers/FormEntityContainer';

export const entityStyle = width => ({
  cursor: 'move',
  borderRadius: '2px',
  position: 'relative',
  gridColumn: `span ${width}`,
  minHeight: '28px',
});

function Prompt(props) {
  const { isResizing } = useContext(ResizeContext);
  const { resizeIndicator } = props;
  const promptResizing = isResizing === props.mode;

  const promptStyle = {
    position: 'relative',
    gridTemplateColumns: null,
    gridColumn: `${
      props.mode === 'prePrompt'
        ? 1
        : 1 + props.model.prePromptWidth + props.model.width
    } /  ${
      props.mode === 'prePrompt'
        ? 1 + props.model.prePromptWidth
        : 1 +
          props.model.prePromptWidth +
          props.model.width +
          props.model.postPromptWidth
    }`,
    display: 'flex',
    alignItems: 'center',
    ...(props.mode === 'prePrompt' && !promptResizing ? { marginLeft: 4 } : {}),
    ...(props.mode === 'postPrompt' && !promptResizing
      ? { marginRight: 4 }
      : {}),
  };

  const getPrePrompt =
    props.dragPreview || !props.autoIds.enabled
      ? null
      : `${String(props.model.externalIdentifier).replace(
          props.autoIds.prefix,
          ''
        )}${props.autoIds.separator || ''}`.replace('\n', '');
  const handleChange = ev => {
    const value = ev.target.value.replace(/\n/g, ''); //don't allow line break to match existing implementation

    props.updateProperty(props.model.uuid, {
      [props.mode]: value,
    });
  };

  const handleKeyPress = event => {
    if (event.key === 'Enter') {
      return;
    }
  };

  const getValue = `${props.model[props.mode]}`;
  const ResizeIndicator = props.resizeIndicator;
  return (
    <div style={promptStyle} id={`${props.id}`}>
      {promptResizing && <ResizeIndicator width={props.width} />}
      {props.mode === 'prePrompt' && (
        <span style={{ marginRight: 3 }}>{getPrePrompt}</span>
      )}
      <TextareaAutosize
        style={{
          width: '100%',
          resize: 'none',
          overflow: 'hidden',
          background: 'rgba(0, 0, 0, 0)',
          userSelect: 'none',
        }}
        onKeyPress={handleKeyPress}
        onChange={handleChange}
        value={props.mode === 'prePrompt' ? getValue : props.model.postPrompt}
        spellCheck={false}
      />

      <Resizer
        element="FormEntity"
        className="resizer"
        model={props.model}
        resizeType={`${props.mode}Width`}
        perspective={props.mode === 'prePrompt' ? 'left' : 'right'}
      />
    </div>
  );
}

export default Prompt;

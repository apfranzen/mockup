/** @jsx jsx */
import { useContext, useState, useEffect } from 'react';
import { connect } from 'react-redux';
import {
  resizeStart,
  resizeEnd,
  resizeEntity,
} from '../../../redux-modules/actions';
import { utility } from '../../../lib/utility';
import { ResizeContext } from '../../../containers/FormEntityContainer';
import { formStyle } from '../_styles';
import { css, jsx } from '@emotion/core';

function Resizer(props) {
  const { gridWidth } = formStyle;
  const { setIsResizing } = useContext(ResizeContext);

  const { resizeType } = props; // prePrompt, width, postPrompt
  const [initialMousePosition, setInitialMousePosition] = useState(0);
  const [initColumn, setInitColumn] = useState(0);
  const [resizeTargetInitColumn, setResizeTargetInitColumn] = useState(0);
  const [targetColDelta, setColumnsResized] = useState(null);
  const [isDragging, setIsDragging] = useState(false);

  useEffect(() => {
    if (targetColDelta !== null) {
      props.resizeEntity(
        props.model.uuid,
        resizeType,
        resizeTargetInitColumn,
        targetColDelta,
        initColumn,
        props.perspective
      );
    }
    console.log(targetColDelta);
  }, [targetColDelta]);

  useEffect(() => {
    if (isDragging) {
      document.addEventListener('mousemove', mousemovehandler);
      document.addEventListener('mouseup', mouseuphandler);
    }

    return function cleanUp() {
      document.removeEventListener('mousemove', mousemovehandler);
      document.removeEventListener('mouseup', mouseuphandler);
    };
  }, [isDragging]);

  function mousemovehandler(event) {
    if (isDragging) {
      const currentDeltaCalc = utility.round(
        (event.clientX - initialMousePosition) / gridWidth,
        0
      );
      setColumnsResized(currentDeltaCalc);
    }
  }

  function mouseuphandler(event) {
    setIsDragging(false);
    props.resizeEnd(props.model.uuid);
    setIsResizing(false);
  }

  function cleanUpResizeType(type) {
    if (type === 'width') {
      return type;
    }
    const result = type.replace('Width', '');
    return result;
  }

  function mouseDownHandler(event) {
    setInitialMousePosition(event.clientX);
    setResizeTargetInitColumn(props.model[props.resizeType]);
    setInitColumn(props.model.column);
    setIsDragging(true);
    setIsResizing(cleanUpResizeType(props.resizeType)); //todo
    props.resizeStart({
      resizeTarget: {
        type: props.resizeType,
        initWidth: props.model[props.resizeType],
      },
      uuid: props.model.uuid,
    });
  }

  const getTitle = () => {
    if (props.resizeType === 'prePromptWidth') {
      return 'Field Prompt';
    } else if (props.resizeType === 'postPromptWidth') {
      return 'Post Prompt';
    } else if (props.resizeType === 'width') {
      return 'Width';
    }
  };

  return (
    <div
      title={`Resize ${getTitle()}`}
      css={theme => css`
        height: 100%;
        position: absolute;
        bottom: 0;
        width: 5px;
        z-index: 100;
        padding: 0px;
        left: ${props.perspective === 'left' ? 0 : ''};
        right: ${props.perspective === 'right' ? 0 : ''};
        &:hover {
          cursor: col-resize;
          background: ${theme.colors.blue};
        }
      `}
      onMouseDown={mouseDownHandler}
    />
  );
}

const mapStateToProps = (state, ownProps) => ({
  ...ownProps,
});

export default connect(
  mapStateToProps,
  { resizeStart, resizeEnd, resizeEntity }
)(Resizer);

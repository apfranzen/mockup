import React, { useRef, useEffect } from 'react';
import FormEntityContainer from '../../../containers/FormEntityContainer';
import FormTabs from '../../FormTabs/FormTabs';
import styled from '@emotion/styled';
/** @jsx jsx */
import { css, jsx } from '@emotion/core';
function StyledFormRoot(props) {
  return (
    <div
      id="formRoot"
      css={theme => ({
        minWidth: `${theme.form.minWidth}px`,
        maxWidth: `${theme.form.minWidth}px`,
      })}
      {...props}
    />
  );
}

const FormGrid = styled.div`
  display: grid;
  grid-template-columns: ${props =>
    `repeat(24, ${props.theme.form.gridTemplateColumns})`};
  grid-gap: ${props => `${props.theme.form.gutter}px`};
  min-height: 80vh;
  max-height: 80vh;
  overflow: hidden;
  background: 'red';
  background-image: url("data:image/svg+xml,%3Csvg width='15' height='15' viewBox='0 0 40 40' xmlns='http://www.w3.org/2000/svg'%3E%3Cg fill='%23c9c3d2' fill-opacity='0.4' fill-rule='evenodd'%3E%3Cpath d='M0 40L40 0H20L0 20M40 40V20L20 40'/%3E%3C/g%3E%3C/svg%3E");
`;

function FormRoot(props) {
  const formDimensions = useRef(null);

  useEffect(() => {
    if (formDimensions.current.getBoundingClientRect().left !== props.initForm2) {
      console.log('use_effect')
      props.initForm(formDimensions.current.getBoundingClientRect().left)
    };
  }, []);

  return (
    <StyledFormRoot>
      <FormTabs
        tabs={props.tabs}
        activeTabUUID={props.activeTab.uuid}
        selectTab={props.selectTab}
        reorderTabs={props.reorderTabs}
      />
      <FormGrid ref={formDimensions}>
        <FormEntityContainer
          key={`root.fromRoot.${props.activeTab.uuid}`}
          id={props.activeTab.uuid}
          sectionUUID={0}
          parentWidth={24}
        />
      </FormGrid>
    </StyledFormRoot>
  );
}

export default FormRoot;

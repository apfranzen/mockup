import React, { useContext } from 'react';
// import { inputStyle } from '../feStyles';
import { formEntityStyle } from './formEntityStyle';
import { ResizeContext } from '../../../containers/FormEntityContainer';
import { DisplayState } from '../../PropertyPanel/EntityProperty/formikHelpers';

function TextArea(props) {
  const { resizeIndicator } = props;
  const { isResizing } = useContext(ResizeContext);
  const entityResizing = isResizing === 'width';
  return (
    <div style={{ ...formEntityStyle(props.model) }}>
      {/* <div
        style={{
          margin: '4px',
          backgroundColor: '#E6E6E6',
          width: '100%',
          height: '20px',
        }}
      /> */}
      <textarea
        style={{
          resize: 'none',
          margin: '4px 0 0 4px',
          width: 'calc(100% - 14px)',
        }}
        className="form-control"
        type={props.model.type}
        rows={props.model.sizeRows}
        disabled
      >
        {props.content}
      </textarea>
      {props.children}
      {entityResizing && resizeIndicator}
      {props.resizerLeft}
      {props.resizer}
    </div>
  );
}

export default TextArea;

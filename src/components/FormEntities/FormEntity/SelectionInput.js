/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import React, { useContext } from 'react';
import { RenderModes } from '../../../model/Types';
import { formEntityStyle } from './formEntityStyle';
import { ResizeContext } from '../../../containers/FormEntityContainer';
import RadioButton from '../../common/formInputs/RadioButton';
import { entityStylesConst } from '../_styles';

function SI_Fragment(props) {
  const { isResizing } = useContext(ResizeContext);
  const { resizeIndicator } = props;
  const entityResizing = isResizing === 'width';
  const entityColor = entityStylesConst[props.model.type].backgroundColor;
  return (
    <div
      style={{
        ...formEntityStyle(props.model),
      }}
    >
      {props.model.mode === RenderModes.menu ? (
        <div
          style={{
            ...formEntityStyle(props.model),
            width: '100%',
            padding: '4px 0 4px 0',
          }}
        >
          {entityResizing && resizeIndicator}
          <select
            style={{
              width: 'calc(100% - 4px)',
              height: '19px',
            }}
            className="form-control"
            type={props.model.type}
          >
            {/* blank option inserted to match data collection behavior */}
            <option />
            {props.model.options.map((option, index) => (
              <option key={index} value={option.value}>
                {option.label}
              </option>
            ))}
          </select>
        </div>
      ) : (
        <div
          style={{
            ...formEntityStyle(props.model),
            padding: '4px',
          }}
        >
          {entityResizing && resizeIndicator}
          <RadioButton options={props.model.options} />
        </div>
      )}
      {props.resizerLeft}
      {props.resizer}
      {props.children}
    </div>
  );
}

export default SI_Fragment;

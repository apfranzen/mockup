import React from 'react';
import { formEntityStyle } from './formEntityStyle';

function Checkbox(props) {
  return (
    <div
      style={{
        ...formEntityStyle(props.model),
      }}
    >
      {props.children}
      <input
        type="checkbox"
        readOnly //this only shows the state - no onChange handler
        checked={props.model.defaultState}
      />
    </div>
  );
}

export default Checkbox;

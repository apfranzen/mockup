import React, { Component } from 'react';
import { inputStyle } from '../feStyles';
import { formEntityStyle } from './formEntityStyle';

class ASTextInput extends Component {
  render() {
    return (
      <div style={formEntityStyle(this.props.model)}>
        <input
          type={this.props.model.type}
          style={inputStyle(this.props.model)}
          disabled
        />
        {this.props.children}
        {this.props.resizerLeft}
        {this.props.resizer}
      </div>
    );
  }
}

export default ASTextInput;

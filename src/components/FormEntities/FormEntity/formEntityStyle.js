import { isFormInput } from '../../../model/Types';

export const formEntityStyle = model =>
  console.log({ model }) || {
    position: 'relative',
    cursor: 'move',
    // borderRadius: '2px',
    alignSelf: 'start',

    gridColumn: `${model.prePromptWidth + 1} / ${model.prePromptWidth +
      model.width +
      1}`,
    ...(isFormInput(model.type) ? { backgroundColor: '#FFFF99' } : {}),
  };

export const formEntityInputStyle = {
  margin: '4px',
};

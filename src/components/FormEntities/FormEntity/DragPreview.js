import React, { Component } from 'react';
import FormEntity from '../FormEntityDragPreview';
import { formStyle } from '../_styles';
export default class DragPreview extends Component {
  render() {
    const { model } = this.props;
    const getWidth = () =>
      this.props.width
        ? this.props.width
        : this.props.itemType === 'FormSection'
          ? 24
          : 6;
    return (
      <div
        style={{
          width: `${getWidth() * formStyle.gridWidth - 5}px`, // subtract 5 pixels because that's half the gutter
          borderRadius: '6px solid green',
          display: 'grid',
          gridTemplateColumns: `repeat(${this.props.model.width}, [col] 1fr)`,
        }}
      >
        <FormEntity
          {...this.props.item}
          model={model}
          id={model.uuid}
          entityCandidate={this.props.entityCandidate}
          dragPreview
        />
      </div>
    );
  }
}

import React, { useContext } from 'react';
import { ResizeContext } from '../../../containers/FormEntityContainer';
const ReactMarkdown = require('react-markdown');

function TextBlock(props) {
  const { isResizing } = useContext(ResizeContext);
  const { resizeIndicator } = props;
  const entityResizing = isResizing === 'width';
  return (
    <div
      style={{
        // background: 'green',
        minHeight: '18px',
        gridColumn: `1 / ${props.model.width + 1}`,
        fontSize: '12px',
        margin: '4px',
      }}
    >
      {props.children}
      {entityResizing && resizeIndicator}
      <ReactMarkdown source={props.model.text} />
      {props.resizerLeft}
      {props.resizer}
    </div>
  );
}

export default TextBlock;

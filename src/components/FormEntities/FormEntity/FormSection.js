import React, { useContext, useRef } from 'react';
import { ResizeContext } from '../../../containers/FormEntityContainer';
import Row from '../Row';
import { formEntityStyle } from './formEntityStyle';
import { formStyle } from '../_styles';
import { ResizeTypes } from '../../../model/Types';

function FormSection(props) {
  const { isResizing } = useContext(ResizeContext);
  const { resizeIndicator } = props;
  const entityResizing = isResizing === 'width';
  const rootTab = props.sectionUUID === 0;
  const fsStyle = {
    ...formEntityStyle(props.model),
    display: 'grid',
    gridTemplateColumns: `repeat(${props.model.width}, [col] ${formStyle.gridTemplateColumnWidth}px)`,
    gridColumnGap: '10px',
    gridColumn: `1 / ${props.model.width + 1}`,
    // minWidth: '80vh',
    margin: '0px',
    cursor: 'move',
    // border: '1px solid black',
    minHeight: '20px',
    background: rootTab ? null : 'rgb(255, 255, 153)',
    ...(rootTab
      ? null
      : { borderTop: '1px solid grey', borderBottom: '1px solid grey' }),
  };
  const Resizer = props.resizer;
  return (
    <div
      style={fsStyle}
      id="FormSection FormEntity"
      type={props.model.type}
      value={props.model.id}
      readOnly={true}
    >
      {entityResizing && resizeIndicator}
      {props.entityCandidate
        ? null
        : [
            [
              ...Array.from({
                length: props.numRows >= 1 ? props.numRows : 1,
              }).map((_, i) => [
                [
                  <Row
                    dragPreview={props.dragPreview}
                    key={`${i}.${props.model.id}`}
                    accepts={props.accepts}
                    parentWidth={props.parentWidth}
                    sectionUUID={props.model.uuid}
                    offset={props.offset}
                    model={props.model}
                    numRows={props.numRows}
                    initForm={props.initForm}
                    gridWidth={props.gridWidth}
                    createEntity={props.createEntity}
                    moveEntity={props.moveEntity}
                    childrenEntities={props.childrenEntities}
                    gridRow={i + 1}
                    lastInSection={props.numRows === i + 1}
                  />,
                ],
              ]),
            ],
            !rootTab ? (
              <Resizer
                id={`${props.model.uuid}.resizer`}
                element="FormEntity"
                uuid={props.model.uuid}
                className="resizer"
                model={props.model}
                resizeType={ResizeTypes.width}
                perspective="left"
              />
            ) : null,

            !rootTab ? (
              <Resizer
                id={`${props.model.uuid}.resizer`}
                element="FormEntity"
                uuid={props.model.uuid}
                className="resizer"
                model={props.model}
                resizeType={ResizeTypes.width}
                perspective="right"
              />
            ) : null,
          ]}
    </div>
  );
}

export default FormSection;

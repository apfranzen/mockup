import React, { useContext, useEffect, useState } from 'react';
import { ResizeContext } from '../../../containers/FormEntityContainer';
import { connect } from 'react-redux';
import { loadFiles } from '../../../redux-modules/actions';
import { formEntityStyle } from './formEntityStyle';
import db from '../../../db/db.js';
function ImageBlock(props) {
  const { isResizing } = useContext(ResizeContext);
  const { resizeIndicator } = props;
  const entityResizing = isResizing === 'width';
  const [imageFile, setImageFile] = useState(null);

  useEffect(() => {
    db.table('files')
      .toArray()
      .then(files => {
        const file = files.filter(
          file => String(file.uuid) === props.model.url
        )[0];

        setImageFile(file);
      });
  }, [props.model]);

  return (
    <div
      style={{
        gridColumn: `${1} / ${props.model.width + 1}`,
      }}
    >
      {entityResizing && resizeIndicator}

      {!imageFile || imageFile.fileContent === '' ? (
        <span>
          <span role="img" aria-label="Thumbnail">
            ️️🖼️
          </span>
          Please select an image from Image Block Property Panel
        </span>
      ) : (
        <img
          style={{
            height: 'auto',
            width: '100%',
          }}
          src={`data:${imageFile.contentType};base64,${imageFile.fileContent}`}
          alt={imageFile.name}
        />
      )}
      {props.children}
      {props.resizerLeft}
      {props.resizer}
    </div>
  );
}

const mapStateToProps = state => ({
  id: 1,
  activeEntityUUID: state.app.activeEntityUUID,
  activeTab: state.form[state.app.activeTab],
  tabs: state.form['0'].children.map(topLevelId => {
    return {
      uuid: state.form[topLevelId].uuid,
      legend: state.form[topLevelId].legend,
    };
  }),
});

const _ImageBlock = connect(
  mapStateToProps,
  {
    loadFiles,
  }
)(ImageBlock);

export default _ImageBlock;

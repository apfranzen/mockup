import React from 'react';
import { DragLayer } from 'react-dnd';
import { utility } from '../../../lib/utility';
import { EntityTypes } from '../../../model/Types';
import { generateEntity } from '../../../redux-modules/formModelHelpers';
import DragPreview from './DragPreview';
const layerStyles = {
  position: 'fixed',
  pointerEvents: 'none',
  zIndex: 1000,
  left: 0,
  top: 0,
  width: '50%',
  height: '100%',
};

function getItemStyles(props) {
  const { initialOffset, currentOffset } = props;

  if (!initialOffset || !currentOffset) {
    return {
      display: 'none',
    };
  }

  let { x, y } = currentOffset;

  if (props.snapToGrid) {
    x -= initialOffset.x;
    y -= initialOffset.y;
    x += initialOffset.x;
    y += initialOffset.y;
  }

  const transform = `translate(${x}px, ${y}px)`;
  return {
    transform,
    WebkitTransform: transform,
  };
}

function CustomDragLayer(props) {
  const { isDragging, item, itemType } = props;

  function renderItem() {
    if (itemType === 'uniqueInputItem' || itemType === 'uniqueTabItem') {
      // handle case for reordeing items in auto naming menu
      return;
      // <DragPreview itemType={EntityTypes.TextInput} gridWidth={4} />;
    } else if (item.role === 'entityCandidate') {
      // preview for adding new item by dragging
      const newEntity = generateEntity(EntityTypes[item.model.type]);
      return (
        <DragPreview
          item={{
            // sectionUUID: '1',
            // itemType: EntityTypes[item.model.type],
            // accepts: ['FormSection'],
            model: newEntity,
            // numRows: 1,
            children: [],
            // numRows: 0,
          }}
          model={newEntity}
          gridWidth={props.gridWidth}
          width={utility.total(newEntity)}
          // numRows={0}
          entityCandidate
        />
      );
    } else if (EntityTypes[itemType] && item.role === 'existingEntity') {
      return (
        <DragPreview
          item={item}
          model={item.model}
          itemType={item.model.type}
          width={utility.total(item.model)}
          gridWidth={props.gridWidth}
        />
      );
    }
  }

  if (!isDragging) {
    return null;
  }
  return (
    <div style={layerStyles}>
      <div style={getItemStyles(props)}>{renderItem()}</div>
    </div>
  );
}

function collect(monitor) {
  return {
    item: monitor.getItem(),
    itemType: monitor.getItemType(),
    initialOffset: monitor.getInitialSourceClientOffset(),
    currentOffset: monitor.getSourceClientOffset(),
    isDragging: monitor.isDragging(),
    clientOffset: monitor.getClientOffset(),
  };
}

export default DragLayer(collect)(CustomDragLayer);

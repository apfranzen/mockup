/** @jsx jsx */
import { css, jsx } from '@emotion/core';
import React, { useContext } from 'react';
import { ResizeContext } from '../../../containers/FormEntityContainer';
import { formEntityStyle, formEntityInputStyle } from './formEntityStyle';
import { entityStylesConst } from '../_styles';
import { useTraceUpdate } from '../../../lib/useTraceUpdate';
import Ribbon from '../subentities/Ribbon';
import { ResizeTypes } from '../../../model/Types';

function TextInput2(props) {
  const { isResizing } = useContext(ResizeContext);
  const { resizeIndicator } = props;
  const entityResizing = isResizing === 'width';
  const entityColor = entityStylesConst[props.model.type].backgroundColor;
  useTraceUpdate(props);
  console.log('props.model.uuid', props.model.uuid);
  const ResizeIndicator = props.resizeIndicator;
  const Resizer = props.resizer;

  return (
    <div
      style={{
        ...formEntityStyle(props.model),
        display: 'flex',
        alignItems: 'center',
        position: 'relative',
      }}
    >
      {entityResizing && <ResizeIndicator width={props.model.width} />}
      <div
        style={{
          ...formEntityInputStyle,
          backgroundColor: 'green',
          width: '100%',
          height: '19px',
        }}
      />
      <Ribbon type={props.model.type} />
      {props.children}

      <Resizer
        id={`${props.model.uuid}.resizer`}
        element="FormEntity"
        uuid={props.model.uuid}
        className="resizer"
        model={props.model}
        resizeType={ResizeTypes.width}
        perspective="left"
      />

      <Resizer
        id={`${props.model.uuid}.resizer`}
        element="FormEntity"
        uuid={props.model.uuid}
        className="resizer"
        model={props.model}
        resizeType={ResizeTypes.width}
        perspective="right"
      />
    </div>
  );
}

export default TextInput2;

import React, { useEffect, useContext } from 'react';
import { DragSource } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';
import { FormEntityUI } from '../../lib/address';
import { utility } from '../../lib/utility';
import { EntityTypes, PromptModes, ResizeTypes } from '../../model/Types';
import { ActionTypes } from '../../redux-modules/actions';
import Prompt from './subentities/Prompt';
import Resizer from './subentities/Resizer';
import { entityStylesConst, formStyle } from './_styles';
import { ResizeIndicator } from '../FormEntities/ResizeIndicator';
import { DevContext } from '../DevStateHandler';
import Ribbon from './subentities/Ribbon';
const entityWrapperStyle = entity => ({
  display: 'grid',
  gridColumn: `${entity.column} / ${entity.column + utility.total(entity)}`,
  gridTemplateColumns: `repeat(${utility.total(entity)}, [col] ${
    formStyle.gridTemplateColumnWidth
    }px)`,
  gridRow: 1, // forces entities in the same row to be
  gridColumnGap: '10px',
  zIndex: '1',
  borderRadius: '2px',
  position: 'relative',
  alignSelf: 'start',
  cursor: 'move',
  backgroundColor:
    entity.type === EntityTypes.FormSection ? null : 'rgb(255, 255, 153)',
});

function FormEntity(props) {
  const { connectDragPreview } = props;
  const { connectDragSource } = props;

  // this side-steps browser native drag image, which has vastly varying implementations in different browsers and also different OS's. This was speicifically problematic with Chrome on Windows, as the drag image was <1 opacity, and the opacity decreased left to right, whcih made the drag and drop operation unclear to the user as exact size is important when adjusting layout in form.
  useEffect(() => {
    if (connectDragPreview) {
      connectDragPreview(getEmptyImage(), {
        captureDraggingState: true,
      });
    }
  });

  function mouseDownHandler(event) {
    event.stopPropagation();
    const {
      model: { uuid },
    } = props;

    props.selectEntity(uuid);
  }

  const { devMode } = useContext(DevContext);
  console.log('reloaded', props.model.type)
  return (
    <div
      ref={connectDragSource}
      id={`hereThis${props.id}.${props.model.type}.wrapper`}
      style={{
        ...entityWrapperStyle(props.model),
        opacity: props.isDragging ? 0.5 : 1,
        ...(props.active && !props.model.topLevel
          ? {
            boxShadow: `0 0 2px 3px ${entityStylesConst[`${props.model.type}`].backgroundColor}`,
          }
          : {}),
      }}
      onMouseDown={mouseDownHandler}
    >
      {devMode && (
        <p
          style={{
            color: 'black',
            position: 'absolute',
            top: 0,
            right: 10,
            zIndex: 100000,
            fontSize: 8,
          }}
        >
          {props.model.uuid.substring(props.model.uuid.length - 3)} row{' '}
          {props.model.row} column {props.model.column} width{' '}
          {props.model.width}
        </p>
      )}
      {'prePromptWidth' in props.model && props.model.prePromptWidth > 0 ? (
        <Prompt
          id={`${props.model.type}.${props.model.id}.prePrompt`}
          mode={PromptModes.prePrompt}
          width={props.model.prePromptWidth}
          prompt={props.model.prePrompt}
          type={props.model.type}
          autoIds={props.autoIds}
          externalIdentifier={props.model.externalIdentifier}
          model={props.model}
          updateProperty={props.updateProperty}
          resizeIndicator={ResizeIndicator}
        />
      ) : null}
      <FormEntityUI
        accepts={props.accepts}
        parentWidth={props.parentWidth}
        root={props.root}
        sectionUUID={props.sectionUUID}
        offset={props.offset}
        id={props.model.uuid}
        initForm={props.initForm}
        gridWidth={formStyle.gridWidth}
        createEntity={props.createEntity}
        moveEntity={props.moveEntity}
        model={props.model}
        childrenEntities={props.childrenEntities}
        numRows={props.numRows}
        resizeIndicator={ResizeIndicator}
        resizer={Resizer}
      />

      {'postPromptWidth' in props.model && props.model.postPromptWidth > 0 ? (
        <Prompt
          id={PromptModes.postPrompt}
          mode={PromptModes.postPrompt}
          width={props.model.postPromptWidth}
          prompt={props.model.prePrompt}
          type={props.model.type}
          autoIds={props.autoIds}
          externalIdentifier={props.model.externalIdentifier}
          model={props.model}
          updateProperty={props.updateProperty}
          resizeIndicator={ResizeIndicator}
        />
      ) : null}
    </div>
  );
}
/**
 * When using higher-order components, sometimes we still need to get to the
 * original component for calling instance methods.
 *
 * @param {React.Component} component - Possibly decorated component.
 * @returns {React.Component} The original component before decoration.
 */
function undecorate(component) {
  let curr = component;
  for (; ;) {
    // react-dnd
    if (curr.getDecoratedComponentInstance) {
      console.log('react-dnd');
      curr = curr.getDecoratedComponentInstance();
      continue;
    }

    // react-redux when { withRef: true } is passed as 4th param to connect.
    if (curr.getWrappedInstance) {
      curr = curr.getWrappedInstance();
      continue;
    }

    break;
  }

  return curr;
}

const NODE = Symbol('Node');
function targetCollector(connect, monitor) {
  const connectDropTarget = connect.dropTarget();
  return {
    // Consumer does not have to know what we're doing ;)
    connectDropTarget: node => {
      monitor[NODE] = node;
      connectDropTarget(node);
    },
  };
}
export default DragSource(
  props => EntityTypes[props.model.type],
  {
    beginDrag: (props, monitor, component) =>
      console.log(component) || {
        ...props,
        role: 'existingEntity',
        action: ActionTypes.MOVEENTITY,
        // formEntityRef: props.formEntityRef,
      },
    // prevents unexpected behavior of dragging entity while resizing in specifc cases
    canDrag: props => (props.isResizing ? false : true),
  },
  (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
  })
)(FormEntity);

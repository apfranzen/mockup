import React from 'react';
import { Dialog } from 'office-ui-fabric-react/';
import { transformEntities } from '../model/deserializer';
const getFileText = file => {
  const reader = new FileReader();
  return new Promise((resolve, reject) => {
    reader.onerror = () => {
      reader.abort();
      reject(new DOMException('Problem parsing input file.'));
    };

    reader.onload = () => {
      resolve(reader.result);
    };
    reader.readAsText(file);
  });
};
const handleFileUpload = async (
  event,
  updateMultiple,
  setMessage,
  setHideImportMenu,
  serverGetSuccess
) => {
  var uploadedFile = event.target.files[0];

  try {
    const resultString = await getFileText(uploadedFile);
    const input = JSON.parse(resultString);
    const output = transformEntities(input);
    const getFirstTab = Object.values(output).filter(
      entity => entity.type === 'FormSection'
    )[0].uuid;
    const formName = 'Sample Name';
    //  ? output.data.content.crf.name
    //  : output.data.name;

    updateMultiple([
      { form: output },
      { 'app.activeTab': getFirstTab },
      { 'app.cdartStatus.formName': formName },
    ]);

    serverGetSuccess();
    setMessage(`Successfully Loaded JSON into Form Design`);
    setHideImportMenu();
  } catch (e) {
    console.warn(e.message);
  }
};
export function ImportDialog(props) {
  const { hideImportMenu } = props;
  return (
    <Dialog
      hidden={hideImportMenu}
      dialogContentProps={{
        name: 'Import Form Designer JSON',
        subText:
          'Import Form Designer JSON previously exported from Form Designer.',
      }}
    >
      <input
        type="file"
        id="importJson"
        // multiple
        size="50"
        onChange={e => {
          e.preventDefault();
          console.log(e.target.files);
          handleFileUpload(
            e,
            props.updateMultiple,
            props.setMessage,
            props.setHideImportMenu,
            props.serverGetSuccess
          );
        }}
      />
    </Dialog>
  );
}

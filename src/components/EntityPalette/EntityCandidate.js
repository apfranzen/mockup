import React, { Component } from 'react';
import { EntityTypes } from '../../model/Types';
import { translate } from '../../lib/translate';
import { DragSource } from 'react-dnd';
import { getEmptyImage } from 'react-dnd-html5-backend';
import { ActionTypes } from '../../redux-modules/actions';
class EntityCandidate extends Component {
  componentDidMount() {
    const { connectDragPreview } = this.props;
    if (connectDragPreview) {
      connectDragPreview(getEmptyImage(), {
        captureDraggingState: true,
      });
    }
  }
  render() {
    const { connectDragSource } = this.props;

    return connectDragSource(
      <div key={this.props.index} style={this.props.style}>
        <span>
          {translate.entityTypeToDescriptor(EntityTypes[this.props.entityType])}
        </span>
      </div>
    );
  }
}

export default (EntityCandidate = DragSource(
  props => props.entityType,
  {
    beginDrag: props => ({
      action: ActionTypes.CREATEENTITY,
      model: { type: props.entityType },
      role: 'entityCandidate',
    }),
  },
  (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    connectDragPreview: connect.dragPreview(),
    isDragging: monitor.isDragging(),
  })
)(EntityCandidate));

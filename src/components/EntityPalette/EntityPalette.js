import React, { Component } from 'react';
import { entityStylesConst } from '../FormEntities/_styles';
import DeleteTarget from './DeleteTarget';
import { connect } from 'react-redux';
import {
  addStart,
  addEnd,
  deleteTab,
  deleteEntity,
} from '../../redux-modules/actions';
import { EntityTypes } from '../../model/Types';
import DisplayHeading from '../common/DisplayHeading';
import EntityCandidate from './EntityCandidate';

/*

  Drag and Drop Approach:
  Firefox, Chrome and Edge all implement drag and drop differently. The following approach was selected in an effort to support these browsers. It is important to use a differnet solution than e.dataTransfer.setDragImage because windows shows a transluscent image, which is not not ideal when the user is trying to align FormEntity to a grid on the Form.
  Gotcha: Firefox shows a paper/document icon when draggin in linux. No issue on Windows (primary target OS for users)

  < -- methods -- >

  - onMouseDown: sets the intialX & initialY coordinates for the intial absolute positioning when the mouse has been pressed, but not dragged. onDragOver event listener is added to document. This is a workaround to expose the coordinated of the mouse during the drag event. Chrome doesn't allow onMouseMove to fire along with onDragStart
  - onDragStart: feeds a blank pixel as the dragImage to prevent the default dragImage so that the actual width can be represented during the drag to the user.
  - onDragOver(on document): Exposes the current mouse x and y coordinates to translate DragPreview by
  - onDragEnd: Reset react state and clean up event handlers manually added.
 */
export const subPaletteStyle = {
  backgroundColor: 'white',
  padding: '8px 6px 8px 6px',
  marginBottom: '20px',
};

const entityStyle = type => ({
  // paddingBottom: '6px',
  margin: '10px 0px 10px 0px',
  padding: '10px 0 10px 0',
  textAlign: 'center',
  border: `3.5px solid ${
    type ? entityStylesConst[type].backgroundColor : 'red'
  }`,
  borderRadius: '6px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
});

class EntityPalette extends Component {
  render() {
    const entitiesToRender = Object.keys(EntityTypes).filter(
      entity => entity !== 'Form'
    );
    return (
      <>
        <div style={subPaletteStyle}>
          <DisplayHeading legend="Drag Here to Remove" />
          <DeleteTarget
            style={entityStyle()}
            deleteTab={this.props.deleteTab}
            deleteEntity={this.props.deleteEntity}
          />
        </div>
        <div style={subPaletteStyle}>
          <DisplayHeading legend="Drag to Form to Add" />
          {entitiesToRender.map((entityType, index) => (
            <EntityCandidate
              key={`${EntityCandidate}.${index}`}
              entityType={entityType}
              index={index}
              style={entityStyle(EntityTypes[entityType])}
            />
          ))}
        </div>
      </>
    );
  }
}

const mapStateToProps = state => ({});

EntityPalette = connect(
  mapStateToProps,
  {
    addStart,
    addEnd,
    deleteTab,
    deleteEntity,
  }
)(EntityPalette);

export default EntityPalette;

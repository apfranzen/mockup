import React, { Component } from 'react';
import { DropTarget } from 'react-dnd';
import * as R from 'ramda';
import { accepts } from '../../containers/FormEntityContainer';
import { EntityTypes } from '../../model/Types';
import { Icon } from 'office-ui-fabric-react';
class DeleteTarget extends Component {
  constructor(props) {
    super();
  }

  render() {
    const { connectDropTarget } = this.props;
    return connectDropTarget(
      <div
        style={{ ...this.props.style, border: '' }}
        // onDrop={this.dropHandler}
        // onDragOver={this.dragOverHandler}
      >
        <Icon
          iconName={'Delete'}
          style={{ color: 'rgb(231, 32, 37)', fontSize: '4rem' }}
          //   css={theme => css`
          //   color: ${theme.colors.blue};
          //   font-size: 0.85rem;
          // `}
        />
        {/* <span>
          <i className="far fa-minus-square fa-7x" style={{ color: 'red' }} />
        </span> */}
      </div>
    );
  }
}

export default DropTarget(
  props => R.flatten(['uniqueTabItem', accepts[EntityTypes.Form]]),
  {
    drop: (props, monitor) => {
      const item = monitor.getItem();
      const itemType = monitor.getItemType();
      const { deleteTab, deleteEntity } = props;
      // if criteria met, do the action
      if (itemType === 'uniqueTabItem') {
        deleteTab(item.id);
      } else if (item.role === 'existingEntity') {
        deleteEntity(item.model.uuid, item.sectionUUID);
      }
    },
  },
  (connect, monitor) => ({
    item: monitor.getItem(),
    connectDropTarget: connect.dropTarget(),
    canDrop: monitor.canDrop(),
    isOver: monitor.isOver(),
    clientOffset: monitor.getClientOffset(),
    dropResult: monitor.getDropResult(),
    initialClientOffset: monitor.getInitialClientOffset(),
    initialSourceClientOffset: monitor.getInitialSourceClientOffset(),
    getDifferenceFromInitialOffset: monitor.getDifferenceFromInitialOffset(),
  })
)(DeleteTarget);

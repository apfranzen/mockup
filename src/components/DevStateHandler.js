import React, { useState, createContext, useEffect } from 'react';
import { connect } from 'react-redux';
import { setDevState } from '../redux-modules/actions';
import axios from 'axios';
import _ from 'lodash';
import RadioButton from '../components/common/formInputs/RadioButton';
export const DevContext = createContext(null);
export const SampleContext = createContext();

let DevStateHandler = props => {
  const listStateKeys = Object.keys(localStorage).filter(item =>
    item.includes('_state_')
  );
  const [recipe, setRecipe] = useState(null);

  // useEffect(() => {
  //   const formState = localStorage.getItem('formState');
  //   console.log('formState: ', formState)
  //   if (formState) setRecipe(formState);
  // }, []);

  // useEffect(() => {
  //   const result = getLocalStorage(recipe);
  //   console.log('result3', result);


  //   // if (result) props.setDevState(result);
  // }, [recipe]);

  function getLocalStorage(key) {
    const result = JSON.parse(localStorage.getItem(`${key}`));
    return result;
  }

  function handleSubmission(form) {
    const url =
      'null';




    const serializedState = JSON.stringify(
      JSON.parse(localStorage.getItem('_state_BST')).form
    );
    console.log(serializedState);
    var reqData = {

    };
    axios
      .post(url, {
        data: Object.keys(reqData)
          .map(function (key) {
            return (
              encodeURIComponent(key) + '=' + encodeURIComponent(reqData[key])
            );
          })
          .join('&'),
        headers: {
          'Content-Type': 'multipart/form-data',
        },
        crossdomain: true,
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  const [devMode, setDevMode] = useState(false);

  function toggleDevMode(value) {
    setDevMode(value);
  }

  function handleRecipeChange(value) {
    localStorage.setItem('formState', value);
    setRecipe(value);
  }

  return (
    <>
      <DevContext.Provider
        value={{ devMode, toggleDevMode }}
        toggleDevMode={toggleDevMode}
      >
        <SampleContext.Provider count={42}>
          {props.children}
        </SampleContext.Provider>
      </DevContext.Provider>
    </>
  );
};

const mapStateToProps = state => ({
  state,
});

DevStateHandler = connect(
  mapStateToProps,
  { setDevState }
)(DevStateHandler);

export default DevStateHandler;

/** @jsx jsx */
import React, { Component } from 'react';
// import FormSection from '../../../data/FormSection';
import {
  selectTab,
  selectEntity,
  reorderTabs,
  createTab,
  moveEntityToTab,
} from '../../redux-modules/actions';
import { connect } from 'react-redux';
import Tab from '../common/Tabs/Tab';
import Tabs from '../common/Tabs/Tabs';
import { css, jsx } from '@emotion/core';
import { Icon, initializeIcons } from 'office-ui-fabric-react/';
initializeIcons();

const AddTab = props => {
  const style_Add_Tab = css`
    border: none;
    align-self: center;
    justify-self: center;
    background: transparent;
  `;
  const clickHandler = () => {
    props.createTab();
  };
  return (
    <button title="Add Tab" css={style_Add_Tab} onClick={clickHandler}>
      <Icon
        iconName={'Add'}
        css={theme => css`
          color: ${theme.colors.blue};
          font-size: 0.85rem;
        `}
      />
    </button>
  );
};

const formTabsStyle = {
  width: '100%',
  display: 'grid',
  gridTemplateColumns: 'auto 50px',
  position: 'relative',
  background: 'rgb(244, 244, 244)',
};

class FormTabs extends Component {
  constructor(props) {
    super(props);
  }

  dropHandler = dropTargetId => event => {
    const dragTargetId = this.props.activeTabUUID;
    if (dragTargetId !== dropTargetId)
      this.props.reorderTabs(dragTargetId, dropTargetId);
    this.setState({ isOver: null });
  };

  dragEndHandler = event => {
    this.setState({ isOver: null });
  };

  mouseLeaveHandler = event => {
    const tabContainer = document.getElementById('tabcontainer');
    tabContainer.removeChild(
      tabContainer.children[tabContainer.children.length - 1]
    );
    document.getElementById('tabcontainer').style.backgroundColor =
      this.props.form.children().length > 1 ? 'darkgrey' : 'white';
  };

  mouseDownHandler = tabUUID => event => {
    event.stopPropagation();

    if (this.props.activeTab !== tabUUID) this.props.selectTab(tabUUID);
    this.props.selectEntity(tabUUID);
  };

  render() {
    return (
      <div style={formTabsStyle}>
        <Tabs>
          {this.props.tabs.map((tab, index) => (
            <Tab
              {...{
                key: `${tab.uuid}.formTab`, // note: this key must be persistent so that reordering the tabs works correctly
                index,
                legend: tab.legend,
                active: tab.uuid === this.props.activeTabUUID,
                selectTab: this.props.selectTab,
                model: tab,
                mouseDownHandler: this.mouseDownHandler(tab.uuid),
                reorderTabs: this.props.reorderTabs,
                moveEntityToTab: this.props.moveEntityToTab,
                draggable: true,
                isDropTarget: true,
              }}
            />
          ))}
        </Tabs>
        <AddTab createTab={this.props.createTab} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  tabs: state.form['0'].children.map(topLevelId => {
    return {
      uuid: state.form[topLevelId].uuid,
      legend: state.form[topLevelId].legend,
    };
  }),
  activeTab: state.app.activeTab,
});

FormTabs = connect(
  mapStateToProps,
  { selectTab, selectEntity, reorderTabs, createTab, moveEntityToTab }
)(FormTabs);
export default FormTabs;

1. existing non-versioned form: GET
    http://cdart2-test:8080/CDART2/api/studies/44/forms/designs/2424

1. Post a new nonversioned form: POST
    http://cdart2-test:8080/CDART2/api/studies/13/forms/designs/?action=new

1. Update a non-versioned form: POST
    http://cdart2-test:8080/CDART2/api/studies/44/forms/designs/2424


1. versioned (deployed) form:  GET
    http://cdart2-test:8080/CDART2/api/studies/13/forms/311/versions/694


1. Returns a list of SubjectValidator scripts for a study:  GET
    http://cdart2-test:8080/CDART2/api/studies/13/scripts/


1. get dictionaries:  GET
    http://cdart2-test:8080/CDART2/api/studies/44/dictionaries



1. List of eventDefs: GET
    http://cdart2-test:8080/CDART2/api/studies/13/eventDefs


1. List Randomization Selection Options: GET
    http://cdart2-test:8080/CDART2/api/studies/44/randomizationOptions

1. List of all forms for study: GET
    /api/studies/{studyID}/forms
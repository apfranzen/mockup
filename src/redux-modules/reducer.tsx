import * as model from '../model/FormEntities';
import { reformat } from './reformat';
import { move } from './move';
import { reorderTabs } from './reorderTabs';
import { RenderModes, EntityTypes } from '../model/Types';
import {
  generateEntity,
  isExceptionEntityInsertation,
  exceedParentWidth,
  getEntitiesInRow,
} from './formModelHelpers';
import { ActionTypes, increment } from './actions';
import * as R from 'ramda';
// import { entityPaletteStyle } from '../components/EntityPalette/EntityPalette';
import { helpers } from '../lib/helpers';
import { accepts } from '../containers/FormEntityContainer';
import { utility } from '../lib/utility';
import { compose } from 'redux';
import { generateCondition } from '../containers/vals_deps/dependencyHelpers';
import NoOpValidator from '../model/validation/model.NoOpValidator';
import { operators } from '../containers/_validations';
import { TabIds } from '../components/PropertyPanel/PropertyPanel';

import { useDataApi } from '../lib/hooks';
import axios from 'axios';
import qs from 'qs';
import {
  deserialize,
  transformEntities,
  cdartSampleInput,
  cdartSample3,
  cdartSampleInput2,
} from '../model/deserializer';
import db from '../db/db.js';
import { tabNameReconcile } from '../components/PropertyPanel/autoIdHelpers';

const uuidv1 = require('uuid/v1');
const validatorTemplate = [];

const dependencyTemplate = {
  type: 'DependencyExpression',
  operator: operators.AND,
  conditions: [],
};
const dependencyTemplate2 = {
  type: 'DependencyExpression',
  operator: operators.AND,
  conditions: [
    // {
    //   type: 'RangeValidator',
    //   customFailureMessage: 'This is a failure message',
    //   validState: true,
    //   strong: false,
    //  nullIsValid: true,
    //   inputIndex: '1',
    //   id: '4.4',
    //   min: 1,
    //   max: 100,
    //   minInclusive: true,
    //   maxInclusive: true,
    // },
    {
      type: 'RangeValidator',
      customFailureMessage: 'This is a failure message',
      validState: true,
      strong: false,
      nullIsValid: false,
      inputIndex: '1',
      id: '4.1',
      min: 0,
      max: 10,
      minInclusive: true,
      maxInclusive: true,
    },
    {
      type: 'PatternValidator',
      customFailureMessage: 'This is a failure message',
      validState: true,
      strong: false,
      nullIsValid: true,
      inputIndex: '1',
      id: '4.2',
      value: 'Some Pattern',
      // properties: {

      // },
    },
    {
      type: 'NoOpValidator',
      customFailureMessage: 'This is a failure message',
      validState: true,
      strong: false,
      nullIsValid: false,
      inputIndex: '1',
      id: '4.3',
    },
    {
      type: 'SubjectInputValidator',
      customFailureMessage: 'This is a failure message',
      validState: true,
      strong: false,
      nullIsValid: true,
      inputIndex: '1',
      id: '4.4',
      value: 'some script',
    },
    {
      type: 'CompositeCondition',
      operator: operators.ANY,
      conditions: [
        {
          type: 'RangeValidator',
          customFailureMessage: 'This is a failure message',
          validState: true,
          strong: false,
          nullIsValid: true,
          inputIndex: '1',
          id: '4.5',
          min: 1,
          max: 100,
          minInclusive: true,
          maxInclusive: true,
        },
      ],
    },
    {
      type: 'RangeValidator',
      customFailureMessage: 'This is a failure message',
      validState: true,
      strong: false,
      nullIsValid: true,
      inputIndex: '1',
      id: '4.1',
      min: 1,
      max: 100,
      minInclusive: true,
      maxInclusive: true,
    },
    {
      type: 'CompositeCondition',
      operator: operators.ONE,
      conditions: [
        {
          type: 'RangeValidator',
          customFailureMessage: 'This is a failure message',
          validState: true,
          strong: false,
          nullIsValid: true,
          inputIndex: '1',
          id: '4.2',
          min: 1,
          max: 100,
          minInclusive: true,
          maxInclusive: true,
        },
        {
          type: 'CompositeCondition',
          operator: operators.ANY,
          conditions: [
            {
              type: 'EnumerationValidator',
              customFailureMessage: 'This is a failure message',
              validState: true,
              strong: false,
              nullIsValid: true,
              inputIndex: '1',
              id: '4.3',
              value: 22,
            },
            {
              type: 'RangeValidator',
              customFailureMessage: 'This is a failure message',
              validState: true,
              strong: false,
              nullIsValid: true,
              inputIndex: '1',
              id: '4.4',
              min: 1,
              max: 100,
              minInclusive: true,
              maxInclusive: true,
            },
            {
              type: 'RangeValidator',
              customFailureMessage: 'This is a failure message',
              validState: true,
              strong: false,
              nullIsValid: true,
              inputIndex: '1',
              id: '4.5',
              min: 1,
              max: 100,
              minInclusive: true,
              maxInclusive: true,
            },
            {
              type: 'RangeValidator',
              customFailureMessage: 'This is a failure message',
              validState: true,
              strong: false,
              nullIsValid: true,
              inputIndex: '1',
              id: '4.1',
              min: 1,
              max: 100,
              minInclusive: true,
              maxInclusive: true,
            },
            {
              type: 'RangeValidator',
              customFailureMessage: 'This is a failure message',
              validState: true,
              strong: false,
              nullIsValid: true,
              inputIndex: '1',
              id: '4.2',
              min: 1,
              max: 100,
              minInclusive: true,
              maxInclusive: true,
            },
            {
              type: 'RangeValidator',
              customFailureMessage: 'This is a failure message',
              validState: true,
              strong: false,
              nullIsValid: true,
              inputIndex: '1',
              id: '4.3',
              min: 1,
              max: 100,
              minInclusive: true,
              maxInclusive: true,
            },
          ],
        },
      ],
    },
  ],
};

const appSeed = {
  initForm: null,
  // initForm: {
  //   x: 268.3125,
  //   y: 102,
  //   width: 804.5625,
  //   height: 791.59375,
  //   top: 102,
  //   right: 1072.875,
  //   bottom: 893.59375,
  //   left: 300,
  // },
  cdartStatus: {
    hideSaveDialog: true,
    formName: '',
  },
  fetchStatus: null,
  activeTab: '1',
  propertyPanelTab: TabIds.EntityExplorer,
  activeEntityUUID: null,
  isResizing: false,
  isDragging: false,
  isAddingInput: false,
  dropTarget: {},
  formEntityRenderPolicy: {
    [EntityTypes.FormSection]: {
      minWidth: 3,
    },

    [EntityTypes.TextInput]: {
      minWidth: 3,
    },

    [EntityTypes.SelectionInput]: {
      minWidth: 3,
    },
    [EntityTypes.TextArea]: {
      minWidth: 3,
    },
    [EntityTypes.Checkbox]: {
      minWidth: 1,
    },
    [EntityTypes.TextBlock]: {
      minWidth: 1,
    },
    [EntityTypes.ImageBlock]: {
      minWidth: 1,
    },
    [EntityTypes.ASTextInput]: {
      minWidth: 1,
    },
    [EntityTypes.EchoInput]: {
      minWidth: 1,
    },
    [EntityTypes.CDSTextInput]: {
      minWidth: 3,
    },
  },
};
console.log(
  282,
  model.generateFormSection({
    uuid: '1',
    children: [],
    legend: 'First Tab',
    topLevel: true,
  })
);

export const formSeed = {
  '0': {
    type: 'Form',
    uuid: '0',
    internalId: '0',
    children: ['1', '2', '3'],
    width: 24,
    allowFormAttachedFiles: true,
    autoIds: {
      enabled: false,
      separator: '.',
      prefix: 'ABC',
    },
    formAttachedFiles: {},
    QbyQ: {},
    metadata: {
      majorVersion: 1,
      description: '',
      topLevelSections: 'tabs',
    },
    crf: {
      type: 'CRF',
      id: 0,
      name: '',
      title: '',
      study: {
        type: 'Study',
        id: 0,
        title: '',
        shortCode: '',
      },
    },
  },
  1: model.generateFormSection({
    uuid: '1',
    internalId: '1',
    children: [],
    legend: 'First Tab',
    topLevel: true,
  }),
  2: model.generateFormSection({
    uuid: '2',
    internalId: '2',
    children: [],
    legend: 'Second Tab',
    topLevel: true,
  }),
  3: model.generateFormSection({
    uuid: '3',
    internalId: '3',
    children: [],
    legend: 'Third Tab',
    topLevel: true,
  }),

};
export const formSeed2 = {
  '0': {
    type: 'Form',
    children: ['1', '2', '3'],
    width: 24,
    allowFormAttachedFiles: true,
    autoIds: {
      enabled: false,
      separator: '.',
      prefix: 'ABC',
    },
    formAttachedFiles: {},
    QbyQ: {},
    metadata: {
      majorVersion: 1,
      description: '',
      topLevelSections: 'tabs',
    },
    crf: {
      type: 'CRF',
      id: 0,
      name: '',
      title: '',
      study: {
        type: 'Study',
        id: 0,
        title: '',
        shortCode: '',
      },
    },
  },
  1: model.generateFormSection({
    uuid: '1',
    children: ['4', '8'],
    legend: 'First Tab',
    topLevel: true,
  }),
  2: model.generateFormSection({
    uuid: '2',
    children: [],
    legend: 'Second Tab',
    topLevel: true,
  }),
  3: model.generateFormSection({
    uuid: '3',
    children: [],
    legend: 'Third Tab',
    topLevel: true,
  }),
  4: model.generateFormSection({
    uuid: '4',
    children: ['4.1', '4.2', '4.3', '4.4', '4.5', '4.6', '4.7'],
    width: 24,
    row: 1,
    topLevel: false,
  }),
  '4.1': model.generateTextInput({
    uuid: '4.1',
    width: 1,
    prePromptWidth: 1,
    postPromptWidth: 1,
    column: 5,
    row: 1,
    externalIdentifier: 'ABC4.1',
    tabOrder: 1,
    validators: validatorTemplate,
    dependencies: dependencyTemplate,
  }),
  '4.2': model.generateTextInput({
    uuid: '4.2',
    width: 2,
    prePromptWidth: 3,
    postPromptWidth: 2,
    column: 7,
    row: 2,
    tabOrder: 2,
    externalIdentifier: 'ABC4.2',
    validators: validatorTemplate,
    dependencies: dependencyTemplate,
  }),
  '4.3': model.generateTextInput({
    uuid: '4.3',
    width: 2,
    prePromptWidth: 3,
    postPromptWidth: 2,
    column: 2,
    row: 3,
    externalIdentifier: 'ABC4.3',
    validators: validatorTemplate,
    dependencies: dependencyTemplate,

    tabOrder: 3,
  }),
  '4.4': model.generateTextInput({
    uuid: '4.4',
    width: 2,
    prePromptWidth: 3,
    postPromptWidth: 2,
    column: 1,
    row: 4,
    externalIdentifier: 'ABC4.4',
    validators: validatorTemplate,
    dependencies: dependencyTemplate,

    tabOrder: 4,
  }),
  '4.5': model.generateTextInput({
    uuid: '4.5',
    width: 2,
    prePromptWidth: 3,
    postPromptWidth: 2,
    column: 9,
    row: 5,
    externalIdentifier: 'ABC4.5',
    validators: validatorTemplate,
    dependencies: dependencyTemplate,

    tabOrder: 5,
  }),
  '4.6': model.generateTextInput({
    uuid: '4.6',
    width: 2,
    prePromptWidth: 3,
    postPromptWidth: 2,
    column: 10,
    row: 6,
    externalIdentifier: 'ABC4.6',
    tabOrder: 6,
    validators: validatorTemplate,
    dependencies: dependencyTemplate,
  }),
  '4.7': model.generateFormSection({
    uuid: '4.7',
    column: 3,
    row: 7,
    width: 10,
    //externalIdentifier : 'ext4.7',
    // tabOrder: 2,
  }),
  8: model.generateFormSection({
    uuid: '8',
    children: ['8.1', '8.2', '8.3'],
    width: 14,
    column: 10,
    row: 2,
  }),
  8.1: model.generateTextInput({
    uuid: '8.1',
    width: 2,
    prePromptWidth: 3,
    postPromptWidth: 2,
    column: 7,
    row: 1,
    externalIdentifier: 'ABC8.1',
    validators: validatorTemplate,
    dependencies: dependencyTemplate,

    tabOrder: 7,
  }),
  8.2: model.generateTextInput({
    uuid: '8.2',
    width: 2,
    prePromptWidth: 3,
    postPromptWidth: 2,
    column: 7,
    row: 2,
    externalIdentifier: 'ABC8.2',
    validators: validatorTemplate,
    dependencies: dependencyTemplate,

    tabOrder: 8,
  }),
  8.3: model.generateTextInput({
    uuid: '8.3',
    width: 2,
    prePromptWidth: 3,
    postPromptWidth: 2,
    column: 7,
    row: 3,
    validators: validatorTemplate,
    dependencies: dependencyTemplate,

    externalIdentifier: 'ABC8.3',
    tabOrder: 9,
  }),
};

export const reducers: any = {
  [ActionTypes.INITFORM]: (state: any, action: any) => {
    console.log('init: ', action.payload.width);
    return R.assocPath(['app', 'initForm'], action.payload, state);
  },

  [ActionTypes.SELECTTAB]: (state: any, action: any) =>
    R.assocPath(['app', 'activeTab'], action.payload.tabId, state),

  /*
export const setMessage = () => ({
  type: ActionTypes.SETMESSAGE,
});
    */
  [ActionTypes.SETMESSAGE]: (state: any, action: any) => {
    const { message } = action;
    const { app } = state;
    const cdartStatus = app.cdartStatus;
    console.log('setmessage');

    return {
      ...state,
      app: {
        ...app,
        cdartStatus: { ...cdartStatus, message },
      },
    };
  },
  [ActionTypes.SETHIDESAVEDIALOG]: (state: any, action: any) => {
    // const {
    //   app: {
    //     cdartStatus: { hideSaveDialog },
    //   },
    // } = action;
    const { app } = state;
    const cdartStatus = app.cdartStatus;
    const hideSaveDialog = app.cdartStatus.hideSaveDialog;
    const { status } = action;
    console.log(action);

    return {
      ...state,
      app: {
        ...app,
        cdartStatus: { ...cdartStatus, hideSaveDialog: status },
      },
    };
  },

  [ActionTypes.DELETETAB]: (state: any, action: any) => {
    const { form } = state;
    const calcActiveTab = () => {
      const indexTabDeleted = state.form['0'].children.indexOf(
        action.payload.tabId
      );
      if (indexTabDeleted === 0) {
        return 0;
      } else {
        return indexTabDeleted - 1;
      }
    };
    const newRootChildren = state.form['0'].children.filter((childId: any) => {
      return childId !== action.payload.tabId;
    });

    const getAllDescendantIds = (form: any, nodeId: any) =>
      form[nodeId].hasOwnProperty('children')
        ? form[nodeId].children.reduce(
          (acc: any, childId: any) => [
            ...acc,
            childId,
            ...getAllDescendantIds(form, childId),
          ],
          []
        )
        : [nodeId];

    const descendantIds = getAllDescendantIds(form, action.payload.tabId);

    const result = R.omit([action.payload.tabId, ...descendantIds], form);

    return {
      app: {
        ...state.app,
        activeTab: newRootChildren[calcActiveTab()],
      },
      form: {
        ...result,

        '0': { ...state.form['0'], children: newRootChildren },
      },
    };
  },

  [ActionTypes.DELETEENTITY]: (state: any, action: any) => {
    const { entityId, parentId } = action;
    const entityToDelete = state.form[entityId];
    const isFormSection = entityToDelete.type === EntityTypes.FormSection;

    const focusedChildren = state.form[parentId].children.filter(
      (item: any) =>
        item !== entityId && state.form[item].row >= state.form[entityId].row
    );

    const sourceChildrenIdToDecrement = (form: any) => {
      const result = focusedChildren.map((entityId: any) => ({
        ...form[entityId],
        row: form[entityId].row - 1,
      }));
      return {
        ...utility.arrayToObject(result, 'uuid'),
      };
    };

    if (!isFormSection) {
      console.log('here');

      return {
        ...state,
        app: { ...state.app, activeEntityUUID: null },
        form: {
          ...R.omit([entityId], state.form),
          [parentId]: {
            ...state.form[parentId],
            children: state.form[parentId].children.filter(
              child => child !== state.form[entityId].uuid
            ),
          },
          ...sourceChildrenIdToDecrement(state.form),
        },
      };
    } else if (isFormSection) {
      const { form } = state;

      const startingChildren = state.form[entityId].children;

      const getAllDescendantIds = (form: any, nodeId: any) =>
        form[nodeId].hasOwnProperty('children')
          ? form[nodeId].children.reduce(
            (acc: any, childId: any) => [
              ...acc,
              childId,
              ...getAllDescendantIds(form, childId),
            ],
            []
          )
          : [nodeId];

      const descendantIds = getAllDescendantIds(form, entityId);

      const result = R.omit([entityId, ...descendantIds], form);
      return {
        ...state,
        form: {
          ...result,
          [parentId]: {
            ...state.form[parentId],
            children: state.form[parentId].children.filter(
              (item: any) => item !== entityId
            ),
          },
        },
      };
    }

    return state;
  },

  [ActionTypes.MOVEENTITYTOTAB]: (state: any, action: any) => {
    const { entityId, tabId, sourceSetionId } = action;
    const { form, app } = state;
    const removedChildren = form[sourceSetionId].children.filter(
      (item: any) => item !== entityId
    );

    const newEntity: any = form[tabId].children
      .map((childId: any) => form[childId].row)
      .reduce(function (previousLargestNumber: any, currentLargestNumber: any) {
        return currentLargestNumber > previousLargestNumber
          ? currentLargestNumber
          : previousLargestNumber;
      }, 0);

    const result = {
      ...state,
      // 1. remove item from children of original section
      form: {
        ...form,
        [sourceSetionId]: {
          ...form[sourceSetionId],
          children: removedChildren,
        },
        // 2. add to children of new section
        [tabId]: {
          ...form[tabId],
          children: [...form[tabId].children].concat(entityId),
        },
        // 3. reset the column/row ?
        [entityId]: {
          ...form[entityId],
          row: newEntity + 1,
          column: 1,
        },
      },
      app: {
        ...app,
        activeEntityUUID: entityId,
        activeTab: tabId,
      },
    };

    return result;
  },

  [ActionTypes.CREATETAB]: (state: any, action: any) => {
    const { form, app } = state;
    const proposedEntity = generateEntity(EntityTypes.FormSection, {
      legend: 'New Tab',
      topLevel: true,
    });
    const parentId = '0';

    const result = {
      ...state,
      form: {
        ...form,
        [proposedEntity.uuid]: proposedEntity,
        [parentId]: {
          ...form[parentId],
          // 1. add new entity
          children: form[parentId].children.concat(proposedEntity.uuid),
        },
      },
      app: {
        ...app,
        ['activeEntityUUID']: proposedEntity.uuid,
        activeTab: proposedEntity.uuid,
        propertyPanelTab: TabIds.EntityExplorer,
      },
    };
    return result;
  },

  [ActionTypes.CREATEENTITY]: (state: any, action: any) => {
    const { form, app } = state;
    const {
      payload: { parentId, entityType, row, column, mode, config },
    } = action;
    const columnDestinationSection = form[parentId].column - 1;

    /**
     * increments total number of FormInputs to assign to the new entity
     * @param form form Object
     */
    function assignSequential(form) {
      return (
        Object.keys(form).filter(
          id =>
            !['Form', 'FormSection', 'TextBlock', 'ImageBlock'].includes(
              form[id].type
            )
        ).length + 1
      );
    }

    const proposedEntity = generateEntity(action.payload.entityType, {
      ...config,
      row,
      column: column - columnDestinationSection,
      tabOrder: assignSequential(state.form),
      externalIdentifier: assignSequential(state.form),
    });
    function findParent(form) {
      return Object.keys(form).filter(
        entityId => form[entityId].type === 'FormSection'
      );
    }

    console.log(findParent(state.form));

    /*
      1. if insertion is invalid, don't
    */

    if (mode === 'new') {
      console.log(parentId, proposedEntity);
      return {
        ...state,
        form: {
          ...form,
          [proposedEntity.uuid]: proposedEntity,
          [parentId]: {
            ...form[parentId],
            // 1. add new entity
            children: form[parentId].children.concat(proposedEntity.uuid),
          },
          // 2. map over existing entities in section and increment row
          ...form[parentId].children
            .filter((child: any) => form[child].row >= proposedEntity.row)
            .forEach((child: any) => ({
              [`${child}`]: {
                ...form[child],
                row: form[child].row++,
              },
            })),
        },
        app: {
          ...app,
          ['activeEntityUUID']: proposedEntity.uuid,
        },
      };
    } else if (!isExceptionEntityInsertation(form, parentId, proposedEntity)) {
      /*
        2. add entity to state.form
      */
      return {
        ...state,
        form: {
          ...form,
          [proposedEntity.uuid]: proposedEntity,
          [parentId]: {
            ...form[parentId],
            children: form[parentId].children.concat(proposedEntity.uuid),
          },
        },
        app: {
          ...app,
          ['activeEntityUUID']: proposedEntity.uuid,
        },
      };
    } else {
      console.log('EXCEPTION');

      return state;
    }
  },

  [ActionTypes.SELECTENTITY]: (state: any, action: any) =>
    R.assocPath(['app', 'activeEntityUUID'], action.id, state),

  [ActionTypes.CLEARFLASH]: (state: any, action: any) => state,

  /**
  {
  type: 'RESIZEENTITY',
  targetType,
  entityId,
  width,
}
   */

  [ActionTypes.RESIZEENTITY]: (state: any, action: any) => {
    const {
      entityId,
      targetType,
      perspective,
      initTargetCol,
      initColumn,
      targetColDelta,
    } = action;
    // return state;

    console.log(state.form[entityId][targetType]);
    // const proposedWidth =
    //   increaseDecrease === 'increase'
    //     ? state.form[entityId][targetType] + 1
    //     : state.form[entityId][targetType] - 1;
    console.log(initTargetCol - targetColDelta);
    // return state;
    const proposedTargetWidth =
      perspective === 'right'
        ? targetColDelta + initTargetCol
        : initTargetCol - targetColDelta;
    const proposedColumn2 = initColumn + targetColDelta;
    console.log(proposedColumn2);
    function getColumn() {
      return proposedColumn2;
      if (
        perspective === 'left' &&
        targetColDelta - state.form[entityId].column > 0
      ) {
        return state.form[entityId].column + 1;
      } else if (
        perspective === 'left' &&
        targetColDelta - state.form[entityId].column < 0
      ) {
        return state.form[entityId].column - 1;
      } else return state.form[entityId].column;
    }

    const proposedEntity = {
      ...state.form[entityId],
      [targetType]: proposedTargetWidth,
      ...(perspective === 'left' ? { column: getColumn() } : {}),
    };
    const { form } = state;
    const parentId = Object.keys(form)
      .filter(item => form[item].type === EntityTypes.FormSection)
      .filter(item => form[item].children.includes(entityId))[0];
    const result = {
      ...state,
      form: {
        ...state.form,
        [entityId]: proposedEntity,
      },
    };
    if (proposedEntity.type === EntityTypes.FormSection) {
      // 1. get the small bound formSection can resize to
      const smallBound = R.reduce(
        R.maxBy((n: any) => n),
        0,
        form[entityId].childrens.map(
          (item: any) => form[item].column + utility.total(form[item]) - 1
        )
      );

      // 2. get the large bound of formSections parent || 24, whichever is smaller
      const largeBound =
        form[parentId].column + utility.total(form[parentId]) - 1;
      // 3. reduce the template number of columns apppropriately

      if (
        proposedEntity.column + utility.total(proposedEntity) - 1 <=
        largeBound &&
        utility.total(proposedEntity) >= smallBound
      ) {
        // valid resize
        return result;
      } else {
        // invalid resize
        return state;
      }
    }
    /*
        1. if insertion is invalid, don't
      */

    const getBounds = () => {
      const model = state.form[entityId];
      // if (!model) {
      //   return null;
      // } else
      if ('topLevel' in model ? model.topLevel : false) {
        return { right: 25, left: 1 };
      } else {
        const entitiesInRow = getEntitiesInRow(state.form, parentId, model.row);
        const greaterThanTarget = entitiesInRow.filter(
          (entity: any) => entity.column > model.column
        );
        const lessThanTarget = entitiesInRow.filter(
          (entity: any) => entity.column < model.column
        );
        console.log(lessThanTarget);

        const bounds = {
          left: lessThanTarget.length
            ? lessThanTarget
              .filter(entity => entity.column < model.column)
              .sort((a, b) => a.column - b.column)
              .slice(-1)[0].column +
            utility.total(
              lessThanTarget
                .filter(entity => entity.column < model.column)
                .sort((a, b) => a.column - b.column)
                .slice(-1)[0]
            )
            : 1,
          right: greaterThanTarget.length
            ? greaterThanTarget.sort((a: any, b: any) => a.column - b.column)[0]
              .column
            : state.form[parentId].width + 1,
        };
        return bounds;
      }
    };
    console.log(getBounds(), proposedEntity.column);

    if (
      utility.total(proposedEntity) + proposedEntity.column >
      getBounds().right ||
      proposedEntity.width < 1 ||
      proposedEntity.prePromptWidth < 0 ||
      proposedEntity.postPromptWidth < 0 ||
      proposedEntity.column < getBounds().left
    ) {
      return state;
    } else if (exceedParentWidth(proposedEntity, parentId, form)) {
      console.log('dont insert, exception!');
      return state;
    } else {
      console.log('no exceptions');

      return result;
    }
  },
  [ActionTypes.LOADFILES]: (state: any, action: any) => {
    const { app, form } = state;
    const { payload } = action;
    console.log(payload);
    return {
      ...state,
      app: {
        ...state.app,
        formAttachedFiles: payload.map(file => R.omit(['fileContent'], file)),
      },
    };
  },

  [ActionTypes.MOVEENTITY]: (state: any, action: any) => {
    const { form, app } = state;
    const {
      payload: { to, from, entityType, entityId, toNewRow },
    } = action;
    const lastInRow =
      getEntitiesInRow(form, from.parentId, from.row).length === 1;
    const sameSection = to.parentId === from.parentId;
    const id = entityId;
    const upperRowBound = to.row > from.row ? to.row : from.row;
    const lowerRowBound = to.row < from.row ? to.row : from.row;
    const directionDown = to.row > from.row;
    const columnDestinationSection = form[to.parentId].column - 1;
    const proposedEntity = generateEntity(state.form[id].type, {
      ...state.form[id],
      row: to.row,
      column: to.column,
    });
    const doInsertion = () => {
      if (sameSection) {
        if (toNewRow && lastInRow) {
          console.log('here');

          const increment = (formStateInput: any) => {
            const childrenIdToIncrement = formStateInput[to.parentId].children
              .filter(
                (entityId: any) =>
                  (directionDown
                    ? form[entityId].row > lowerRowBound &&
                    form[entityId].row < upperRowBound
                    : form[entityId].row >= lowerRowBound &&
                    form[entityId].row < upperRowBound) && entityId !== id
              )
              .map((entityId: any) => ({
                ...formStateInput[entityId],
                row: directionDown
                  ? formStateInput[entityId].row - 1
                  : formStateInput[entityId].row + 1,
              }));
            return childrenIdToIncrement;
          };

          const result = {
            ...state,
            form: {
              ...state.form,
              ...utility.arrayToObject(increment(form), 'uuid'),
              [id]: {
                ...state.form[id],
                row: directionDown ? to.row - 1 : to.row,
                column: to.column - columnDestinationSection,
              },
            },
          };
          console.log({ result });

          return result;
        } else if (!toNewRow && !lastInRow) {
          const targetEntity = form[id];
          const result = {
            ...state,
            form: {
              ...form,
              [id]: {
                ...targetEntity,
                row: to.row,
                column: to.column - columnDestinationSection,
              },
            },
          };
          return result;
        } else if (!toNewRow && lastInRow) {
          console.log('here: ', sameSection, { to, from });

          const childrenIdToIncrement = form[to.parentId].children
            .filter(
              (entityId: any) =>
                (directionDown
                  ? form[entityId].row > lowerRowBound
                  : form[entityId].row > upperRowBound) && entityId !== id
            )
            .map((entityId: any) => ({
              ...form[entityId],
              row: form[entityId].row - 1,
            }));

          const targetEntity = form[id];
          const result = {
            ...state,
            form: {
              ...form,
              [id]: {
                ...targetEntity,
                row: directionDown ? to.row - 1 : to.row,
                column: to.column - columnDestinationSection,
              },
              ...utility.arrayToObject(childrenIdToIncrement, 'uuid'),
            },
          };
          if (to.row === from.row) {
            return {
              ...state,
              form: {
                ...form,
                [id]: {
                  ...targetEntity,
                  row: directionDown ? to.row - 1 : to.row,
                  column: to.column - columnDestinationSection,
                },
                // ...utility.arrayToObject(childrenIdToIncrement, 'uuid'),
              },
            };
          } else if (to.row !== from.row) {
            return result;
          }
        } else if (toNewRow && !lastInRow) {
          console.log('here');

          const childrenIdToIncrement = form[to.parentId].children
            .filter(
              (entityId: any) =>
                (directionDown
                  ? form[entityId].row >= upperRowBound
                  : form[entityId].row >= lowerRowBound) && entityId !== id
            )
            .map((entityId: any) => ({
              ...form[entityId],
              row: form[entityId].row + 1,
            }));

          const targetEntity = form[id];
          const result = {
            ...state,
            form: {
              ...form,
              [id]: {
                ...targetEntity,
                row: directionDown ? to.row : to.row,
                column: to.column - columnDestinationSection,
              },
              ...utility.arrayToObject(childrenIdToIncrement, 'uuid'),
            },
          };
          return result;
        }
      } else if (!sameSection) {
        console.log('here: ', sameSection, to.row, from.row);

        if (!toNewRow && !lastInRow) {
          console.log('here: ', { action });

          // const childrenIdToIncrement = form[to.parentId].children
          //   .filter(
          //     (entityId: any) =>
          //       (directionDown
          //         ? form[entityId].row >= upperRowBound
          //         : form[entityId].row >= lowerRowBound) && entityId !== id
          //   )
          //   .map((entityId: any) => ({
          //     ...form[entityId],
          //     row: form[entityId].row + 1,
          //   }));
          const targetEntity = form[id];
          const result = {
            ...state,
            form: {
              ...form,
              [from.parentId]: {
                ...form[from.parentId],
                children: form[from.parentId].children.filter(
                  (entityId: any) => entityId !== id
                ),
              },
              [to.parentId]: {
                ...form[to.parentId],
                children: form[to.parentId].children.concat(id),
              },
              [id]: {
                ...targetEntity,
                row: to.row,
                column: to.column,
              },
              // ...utility.arrayToObject(childrenIdToIncrement, 'uuid'),
            },
          };
          console.log({ result });

          return result;
        } else if (toNewRow && !lastInRow) {
          console.log('here: ', { action });

          const childrenIdToIncrement = form[to.parentId].children
            .filter(
              (entityId: any) => form[entityId].row >= to.row && entityId !== id
            )
            .map((entityId: any) => ({
              ...form[entityId],
              row: form[entityId].row + 1,
            }));
          const targetEntity = form[id];
          const result = {
            ...state,
            form: {
              ...form,
              [from.parentId]: {
                ...form[from.parentId],
                children: form[from.parentId].children.filter(
                  (entityId: any) => entityId !== id
                ),
              },
              [to.parentId]: {
                ...form[to.parentId],
                children: form[to.parentId].children.concat(id),
              },
              [id]: {
                ...targetEntity,
                row: to.row,
                column: to.column,
              },
              ...utility.arrayToObject(childrenIdToIncrement, 'uuid'),
            },
          };

          return result;
        } else if (!toNewRow && lastInRow) {
          const sourceChildrenIdToDecrement = (form: any) => {
            const result = form[from.parentId].children
              .filter(
                (entityId: any) =>
                  form[entityId].row > from.row && entityId !== id
              )
              .map((entityId: any) => ({
                ...form[entityId],
                row: form[entityId].row - 1,
              }));
            const result2 = {
              ...form,
              ...utility.arrayToObject(result, 'uuid'),
            };
            return result2;
          };

          const targetEntity = form[id];
          const moveEntityReference = (form: any) => {
            const result = {
              ...form,
              [from.parentId]: {
                ...form[from.parentId],
                children: form[from.parentId].children.filter(
                  (entityId: any) => entityId !== id
                ),
              },
              [to.parentId]: {
                ...form[to.parentId],
                children: form[to.parentId].children.concat(id),
              },
            };
            console.log({ result });
            return result;
          };
          const insertEntity = (form: any) => ({
            ...form,
            [id]: {
              ...targetEntity,
              row: to.row,
              column: to.column - columnDestinationSection,
            },
          });

          const result2 = compose(
            sourceChildrenIdToDecrement,
            moveEntityReference,
            insertEntity
          )(form);
          // const result = {
          //   ...state,
          //   form: {
          //     ...form,

          //     [id]: {
          //       ...targetEntity,
          //       row: to.row,
          //       column: to.column,
          //     },
          //     ...utility.arrayToObject(sourceChildrenIdToDecrement, 'uuid'),
          //   },
          // };
          console.log('here: ', result2);

          return { ...state, form: { ...result2 } };
        } else if (toNewRow && lastInRow) {
          console.log('here: ', { action });
          const targetEntity = form[id];
          const destinationChildrenIdToIncrement = (form: any) => {
            const result = form[to.parentId].children
              .filter(
                (entityId: any) =>
                  form[entityId].row >= to.row && entityId !== id
              )
              .map((entityId: any) => ({
                ...form[entityId],
                row: form[entityId].row + 1,
              }));

            const result2 = {
              ...form,
              ...utility.arrayToObject(result, 'uuid'),
            };
            return result2;
          };
          const sourceChildrenIdToDecrement = (form: any) => {
            const result = form[from.parentId].children
              .filter(
                (entityId: any) =>
                  form[entityId].row > from.row && entityId !== id
              )
              .map((entityId: any) => ({
                ...form[entityId],
                row: form[entityId].row - 1,
              }));
            return {
              ...form,
              ...utility.arrayToObject(result, 'uuid'),
            };
          };

          const moveEntityReference = (form: any) => {
            const result = {
              ...form,
              [from.parentId]: {
                ...form[from.parentId],
                children: form[from.parentId].children.filter(
                  (entityId: any) => entityId !== id
                ),
              },
              [to.parentId]: {
                ...form[to.parentId],
                children: form[to.parentId].children.concat(id),
              },
            };
            console.log({ result });
            return result;
          };
          const insertEntity = (form: any) => ({
            ...form,
            [id]: {
              ...targetEntity,
              row: to.row,
              column: to.column - columnDestinationSection,
            },
          });

          const result2 = compose(
            sourceChildrenIdToDecrement,
            destinationChildrenIdToIncrement,
            moveEntityReference,
            insertEntity
          )(form);

          return { ...state, form: { ...result2 } };
        }
      }
    };
    const proposedState = doInsertion();

    if (
      isExceptionEntityInsertation(
        proposedState.form,
        to.parentId,
        proposedState.form[id]
      )
    ) {
      console.log('dont insert, exception!');
      return state;
    } else {
      /*
          2. add entity to state.form
        */
      console.log('noExceptions: ', action.payload);

      return proposedState;
    }

    return state;
  },

  //  unfinalized actions start here

  [ActionTypes.LOADFORMFROMSERVER]: (state: any, action: any) => {
    /*
      success:
        load form, possibly show spinner in mid term
      fail: tell user why
      how to diferentiate between versioned and non-versioned endpoints?
      need to pass study id, form id parameter from the url
    */
    // const { data, isError, isLoading } = useDataApi(
    //   true,
    //   'useDataApi(true, /44/forms/designs/?action=new'
    // );
    // console.log(data, isError, isLoading);

    return { state };
  },
  [ActionTypes.HANDLEORDER]: (state: any, action: any) => {
    const { enabled, separator, prefix } = state.form[0].autoIds;
    const { customTabOrder } = state.form[0];
    console.log(customTabOrder);
    // return state;
    if (!customTabOrder) {
      // do tab order
      const sortedResult = helpers
        .entitiesByDomOrder(state.form)
        .map((input, index) => ({ ...input, tabOrder: index }));
      const result = {
        ...state,
        form: { ...state.form, ...sortedResult },
      };
      return result;
    } else {
      // do DOM order
    }

    //     if (enabled) {
    //       // const ordered = utility.formInputsByTabOrder(state.form);
    //       // console.log(ordered);

    //       const reconciled = tabNameReconcile(state.form, false);
    //       const result = {
    //         ...state,
    //         form: { ...state.form, ...reconciled },
    //       };
    //       console.log(result);
    //       return result;
    //       /*
    //       1. order according to taborder or DOM
    // // helpers
    //         .entitiesByDomOrder(props.model)
    //       //  formInputArrByTabOrder: utility.formInputsByTabOrder(state.form),
    //       2. call  tabNameReconcile(form)
    //       */
    //     }

    return state;
  },
  [ActionTypes.HANDLEAUTONAME]: (state: any, action: any) => {
    // return state;
    const { enabled, separator, prefix } = state.form[0].autoIds;

    if (enabled) {
      // const ordered = utility.formInputsByTabOrder(state.form);
      // console.log(ordered);

      const reconciled = tabNameReconcile(state.form, false);
      console.log(
        Object.values(reconciled).map(
          (entity: any) => entity.externalIdentifier
        )
      );

      const result = {
        ...state,
        form: { ...state.form, ...reconciled },
      };
      console.log({ result });
      return result;
      /*
      1. order according to taborder or DOM
// helpers
        .entitiesByDomOrder(props.model)
      //  formInputArrByTabOrder: utility.formInputsByTabOrder(state.form),
      2. call  tabNameReconcile(form)
      */
    }

    return state;
  },

  [ActionTypes.serializeForm]: (state: any, action: any) => {
    console.log(action.type);

    return { state };
  },

  [ActionTypes.SETDEVSTATE]: (state: any, action: any) => {
    const { serializedState } = action;
    return { ...serializedState };
  },

  [ActionTypes.EXPORTAPPSTATE]: (state: any, action: any) => {
    const serializedState = JSON.stringify(state.form);
    console.log(serializedState);

    return state;
  },

  [ActionTypes.TESTLOADFORM]: (state: any, action: any) => {
    const result = transformEntities(cdartSampleInput2);
    // const tabUuid = result[Object.keys(result)[1]].uuid;
    const getFirstTab: any = Object.values(result).filter(
      (entity: any) => entity.type === 'FormSection'
    )[0];
    console.log(getFirstTab);

    return {
      ...state,
      app: { ...state.app, activeTab: getFirstTab.uuid },
      form: result,
    };
  },

  [ActionTypes.SETFORMATTACHEDFILE]: (state: any, action: any) => {
    const {
      payload: { fileObj },
    } = action;
    const { form } = state;

    console.log(fileObj);

    const result = {
      ...state,
      form: {
        ...form,
        [0]: {
          ...form[0],
          formAttachedFiles: {
            ...form[0].formAttachedFiles,
            [uuidv1()]: fileObj,
          },
        },
      },
    };

    return result;
  },

  [ActionTypes.SETPROPERTYPANELTAB]: (state: any, action: any) => {
    const {
      payload: { tab },
    } = action;
    const { app } = state;

    console.log({ action });

    const result = {
      ...state,
      app: {
        ...app,
        propertyPanelTab: tab,
      },
    };

    return result;
  },

  [ActionTypes.SERVERGETPENDING]: (state: any, action: any) => {
    const { text } = action;
    const { app } = state;
    console.log('SERVERGETPENDING');

    return {
      ...state,
      app: { ...state.app, fetchStatus: 'pending' },
    };
  },

  [ActionTypes.SERVERGETSUCCESS]: (state: any, action: any) => {
    const { formObj, name } = action;
    const { app } = state;
    console.log('SERVERGETSUCCESS');

    // const getFirstTab: any = Object.values(formObj).filter(
    //   (entity: any) => entity.type === 'FormSection'
    // )[0];
    const result = {
      ...state,
      app: {
        ...state.app,
        fetchStatus: 'success',
        // activeTab: getFirstTab.uuid,
      },
      // form: { ...formObj, 0: { ...formObj[0], name } },
    };
    // const result = {
    //   ...state,
    //   app: {
    //     ...state.app,
    //     fetchStatus: 'success',
    //     activeTab: getFirstTab.uuid,
    //   },
    //   form: { ...formObj, 0: { ...formObj[0], name } },
    // };

    return result;
  },

  [ActionTypes.SERVERGETFAILURE]: (state: any, action: any) => {
    const { text } = action;
    const { app } = state;
    console.log('SERVERGETFAILURE');

    const result = {
      ...state,
      app: {
        ...state.app,
        fetchStatus: 'error',
        // activeTab: getFirstTab.uuid,
      },
      // form: { ...formObj, 0: { ...formObj[0], name } },
    };

    return result;
  },
  [ActionTypes.UPDATEMULTIPLE]: (state: any, action: any) => {
    const { payload } = action;

    const inProgress = payload.reduce(
      (acc: any, curr: any, currIdx: any, srcArr) => {
        const path: any = Object.keys(curr)[0].split('.');
        const value: any = Object.values(curr)[0];

        return R.assocPath(path, value, acc);
      },
      state
    );

    return inProgress;
  },
  [ActionTypes.ADDCONDITION]: (state: any, action: any) => {
    const {
      payload: { id, pathToParentCondition, properties, mode },
    } = action;
    const { form } = state;

    const newEntity = generateCondition(properties);
    console.log(properties, newEntity);
    // return state;
    if (mode === 'validation') {
      const newValidator = form[id].validators.concat(newEntity);

      const result = {
        ...state,
        form: {
          ...form,
          [id]: {
            ...state.form[id],
            validators: newValidator,
          },
        },
      };
      return result;
    }
    console.log({ action, newEntity });
    const pathArr = pathToParentCondition
      .split('.')
      .slice(1, pathToParentCondition.split('.').length);
    const pathToArr = pathArr.reduce(
      (acc: any, curr: any) => acc.concat(['conditions', parseInt(curr, 10)]),
      []
    );

    const lensPathToCondition = R.lensPath(pathToArr);
    const lensPathToViewCondition = R.lensPath(pathToArr);
    const existingConditions: any = R.view(
      lensPathToViewCondition,
      form[id].dependencies
    );

    const updatedConditions = R.set(
      lensPathToCondition,
      {
        ...existingConditions,
        conditions: existingConditions.conditions.concat(newEntity),
      },
      form[id].dependencies
    );

    const result = {
      ...state,
      form: {
        ...form,
        [id]: {
          ...state.form[id],
          dependencies: {
            ...state.form[id].dependencies,
            ...updatedConditions,
          },
        },
      },
    };

    return result;
  },

  // export const deleteCondition = (id, pathToParentCondition) => ({
  //   type: ActionTypes.DELETECONDITION,
  //   payload: {
  //     id,
  //     pathToParentCondition,
  //   },
  // });

  [ActionTypes.DELETECONDITION]: (state: any, action: any) => {
    const {
      payload: { id, pathToEntity, mode },
    } = action;
    const { form } = state;
    console.log({ action });

    if (mode === 'validation') {
      // const newValidator = form[id].validators.filter((condition: any) => condition.id !== );
      const targetIndex = pathToEntity.split('.')[1];
      // console.log(form[id].validators[id.split('.')[1]]);
      const updatedValidators = form[id].validators.filter(
        (o: any, index: any) => {
          console.log({ index, targetIndex });

          return String(index) !== targetIndex;
        }
      );
      const result = {
        ...state,
        form: {
          ...form,
          [id]: {
            ...state.form[id],
            validators: updatedValidators,
          },
        },
      };
      return result;
    }

    const pathArr = pathToEntity
      .split('.')
      .filter(
        (element: any, index: any, array: any) => index !== array.length - 1
      )
      .slice(1, pathToEntity.split('.').length);
    // const pathArr = [6, 1];
    const indexOfCondition = Number(
      pathToEntity.split('.')[pathToEntity.split('.').length - 1]
    );

    const pathToArr = pathArr.reduce(
      (acc: any, curr: any) => acc.concat(['conditions', parseInt(curr, 10)]),
      []
    );

    const lensPathToCondition = R.lensPath(pathToArr);
    const lensPathToViewCondition = R.lensPath(pathToArr);
    const existingConditions: any = R.view(
      lensPathToViewCondition,
      form[id].dependencies
    );

    const updatedConditions = R.set(
      lensPathToCondition,
      {
        ...existingConditions,
        conditions: existingConditions.conditions.filter(
          (item: any, index: any) => {
            console.log(index, indexOfCondition);
            return index !== indexOfCondition;
          }
        ),
      },
      form[id].dependencies
    );

    const result = {
      ...state,
      form: {
        ...form,
        [id]: {
          ...state.form[id],
          dependencies: {
            ...state.form[id].dependencies,
            ...updatedConditions,
          },
        },
      },
    };

    return result;
  },

  [ActionTypes.UPDATEDEPENDENCYCONDITION]: (state: any, action: any) => {
    const { id, pathToCondition, updatedProperties, mode } = action.payload;
    const { app, form } = state;
    console.log({ action });

    if (mode === 'validation') {
      // const newValidator = form[id].validators.filter((condition: any) => condition.id !== );

      const targetIndex = pathToCondition.split('.')[1];

      const newEntity = generateCondition(
        // form[id].validators[targetIndex].type,
        updatedProperties
      );

      // console.log(form[id].validators[id.split('.')[1]]);
      const updatedValidators = form[id].validators.filter(
        (o: any, index: any) => String(index) !== targetIndex
      );
      updatedValidators.splice(targetIndex, 0, newEntity);
      const result = {
        ...state,
        form: {
          ...form,
          [id]: {
            ...state.form[id],
            validators: updatedValidators,
          },
        },
      };
      return result;
    }
    const pathArr = pathToCondition
      .split('.')
      .slice(1, pathToCondition.split('.').length);
    console.log({ pathArr });

    const pathToArr = pathArr.reduce(
      (acc: any, curr: any) => acc.concat(['conditions', parseInt(curr, 10)]),
      []
    );
    console.log({ pathToArr });

    const lensPathToCondition = R.lensPath(pathToArr);
    const updatedConditions = R.set(
      lensPathToCondition,
      updatedProperties,
      form[id].dependencies
    );

    const result = {
      ...state,
      form: {
        ...form,
        [id]: {
          ...state.form[id],
          dependencies: {
            ...state.form[id].dependencies,
            ...updatedConditions,
          },
        },
      },
    };

    return result;
  },
  [ActionTypes.UPDATEPROPERTY]: (state: any, action: any) => {
    const { updatedProperty, id } = action.payload;
    const { app, form } = state;
    console.log({ updatedProperty, id });
    const result = {
      ...state,
      form: {
        ...form,
        [id]: { ...state.form[id], ...updatedProperty },
      },
    };
    return result;
  },

  [ActionTypes.SETAUTOID]: (state: any, action: any) => {
    const { updatedValue } = action;
    const { app, form } = state;
    const handleEnabled = updatedValue.hasOwnProperty('enabled')
      ? updatedValue.enabled
      : state.form[0].autoIds.enabled;

    const handleSeparator = () => {
      return handleEnabled
        ? {
          separator: updatedValue.separator || '.',
        }
        : { separator: null };
    };

    const handlePrefix = () => {
      return handleEnabled
        ? { prefix: updatedValue.prefix || 'FORM' }
        : { prefix: null };
    };

    const result = {
      ...state,
      form: {
        ...state.form,
        [0]: {
          ...state.form[0],
          autoIds: {
            ...state.form[0].autoIds,
            ...updatedValue,
            ...handleSeparator(),
            ...handlePrefix(),
          },
        },
      },
    };
    console.log(handleEnabled, handleSeparator());
    console.log({
      autoIds: {
        ...state.form[0].autoIds,
        ...updatedValue,
        ...handleSeparator(),
        ...handlePrefix(),
      },
    });

    return result;
  },
  [ActionTypes.CUSTOMTABORDER]: (state: any, action: any) => {
    const { customTabOrder } = action;
    const { app, form } = state;
    console.log({ customTabOrder });
    const result = {
      ...state,
      form: {
        ...state.form,
        [0]: {
          ...state.form[0],
          customTabOrder,
        },
      },
    };

    return result;
  },

  [ActionTypes.BATCHUPDATE]: (state: any, action: any) => {
    const { inputs } = action;
    const { app, form } = state;

    const result = {
      ...state,
      form: {
        ...state.form,
        ...inputs,
      },
    };

    return result;
  },

  [ActionTypes.REORDERTABS]: (state: any, action: any) => {
    const { dragTargetId, dropTargetId } = action;
    const originalArrayOrder = state.form[0].children;
    const result = helpers.reorderArray(
      dragTargetId,
      dropTargetId,
      originalArrayOrder
    );

    const resul2 = {
      ...state,
      form: {
        ...state.form,
        0: { ...state.form[0], children: result },
      },
    };
    return resul2;
  },

  [ActionTypes.RESIZESTART]: (state: any, action: any) => ({
    ...state,
    app: { ...state.app, isResizing: true },
  }),
  [ActionTypes.RESIZEEND]: (state: any, action: any) => ({
    ...state,
    app: { ...state.app, isResizing: false },
  }),
  [ActionTypes.ADDSTART]: (state: any, action: any) => ({
    ...state,
    isAddingInput: action.entity,
  }),
  [ActionTypes.DRAGSTART]: (state: any, action: any) => ({
    ...state,
    isDragging: {
      targetUUID: action.targetUUID,
      sectionUUID: action.sectionUUID,
      metaData: action.metaData,
    },
  }),
  [ActionTypes.ADDEND]: (state: any, action: any) => ({
    ...state,
    isAddingInput: false,
  }),
  [ActionTypes.DRAGEND]: (state: any, action: any) => ({
    ...state,
    isDragging: false,
  }),
  [ActionTypes.SETDROPTARGET]: (state: any, action: any) => ({
    ...state,
  }),
  [ActionTypes.INCREMENT]: (state: any, action: any) => {
    console.log('increment');
    return { ...state, app: { ...state.app, count: state.app.count + 1 } };
  },
  // [ActionTypes.LOADFORMSTATE]: (state: any, action: any) => {
  //   /**
  //    * if location.origin contains formId, get that form and load into state
  //    * if not, load a blank form
  //    */

  //   return state;
  // },

  ENTITYRESIZED: (state: any, action: any) => {
    return {
      ...state,
      [action.entityUUID]: {
        ...state[action.entityUUID],
        ...action.resizeUpdatedProps,
      },
    };
  },
  REFORMAT: (state: any, action: any) => ({
    ...state,
    ...reformat(state, action),
  }),
  MOVE: (state: any, action: any) => ({
    ...state,
    ...move(state, action),
  }),
};
const arrEntitiesToCreate = [];
export default function form(
  state = { app: appSeed, form: formSeed },
  action: any = ''
) {
  const nextReducer = reducers[action.type];
  console.log({ nextReducer });

  return nextReducer ? nextReducer(state, action) : state;
}

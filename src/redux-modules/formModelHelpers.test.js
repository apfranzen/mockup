import { doEntitiesOverlap } from './formModelHelpers';

const makeEntity = (column, width) => ({
  column,
  width,
});

describe('doEntitiesOverlap', () => {
  // L
  it('returns true when the 1st start column is within the 2nd entity', () => {
    expect(doEntitiesOverlap(makeEntity(10, 10))(makeEntity(5, 10))).toBe(true);
  });
  // F
  it('returns true when the 1st end column is within the 2nd entity', () => {
    expect(doEntitiesOverlap(makeEntity(5, 10))(makeEntity(10, 10))).toBe(true);
  });
  // I
  it('returns true when the 1st entity is within the 2nd entity', () => {
    expect(doEntitiesOverlap(makeEntity(10, 5))(makeEntity(5, 15))).toBe(true);
  });
  // O
  it('returns true when the 2nd entity is within the 1st entity', () => {
    expect(doEntitiesOverlap(makeEntity(5, 15))(makeEntity(10, 5))).toBe(true);
  });
  it('returns false when the 1st entity is entirely after the 2nd entity', () => {
    expect(doEntitiesOverlap(makeEntity(15, 5))(makeEntity(5, 5))).toBe(false);
  });
  it('returns false when the 1st entity is entirely before the 2nd entity', () => {
    expect(doEntitiesOverlap(makeEntity(5, 5))(makeEntity(15, 5))).toBe(false);
  });
});

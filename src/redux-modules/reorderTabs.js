import { helpers } from '../lib/helpers';
export const reorderTabs = (formState, action) => {
  const { dragTargetId, dropTargetId } = action;

  const dragIndex = formState['0'].children.indexOf(dragTargetId);
  const dropIndex = formState['0'].children.indexOf(dropTargetId);

  return helpers.reorderArray(dragIndex, dropIndex, formState['0'].children);
};

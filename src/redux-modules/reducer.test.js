import { EntityTypes } from '../model/Types';
import { reducers } from './reducer';
import { ActionTypes } from './actions';

// !ASK - should the new instance of a tab be passed in?
describe('The createTab reducer', () => {
  it('returns updated with with a new Tab', () => {
    const oldState = {
      app: {
        activeTab: 2,
      },
      form: { 0: { children: [] } },
    };
    const action = {
      type: ActionTypes.CREATETAB,
    };

    const newState = reducers[ActionTypes.CREATETAB](oldState, action).form;

    expect(newState[0].children.length).toBe(
      oldState.form[0].children.length + 1
    );

    expect(
      Object.keys(newState).filter(
        entity => newState[entity].type === EntityTypes.FormSection
      )
    );
  });
});

describe('The selectTab reducer', () => {
  it('returns updated state with the given tab selected', () => {
    const newActiveTab = 3;
    const oldState = {
      app: {
        activeTab: 2,
      },
    };
    const newState = { app: { activeTab: newActiveTab } };
    const action = {
      type: ActionTypes.SELECTTAB,
      payload: {
        tabId: newActiveTab,
      },
    };
    expect(
      reducers[ActionTypes.SELECTTAB](oldState, action).app.activeTab
    ).toEqual(newActiveTab);
  });
});

describe('The deleteTab reducer', () => {
  const deletedTab = 'someTabUUID';
  const oldState = {
    app: {
      activeTab: deletedTab,
    },
    form: {
      '0': { type: EntityTypes.Form, uuid: 0, children: ['someTabUUID'] },
      someTabUUID: { type: EntityTypes.FormSection, uuid: 'someUUID' },
    },
  };

  const action = {
    type: ActionTypes.DELETETAB,
    payload: {
      tabId: deletedTab,
    },
  };
  it('returns updated state without the deleted tab', () => {
    expect(
      Object.keys(reducers[ActionTypes.DELETETAB](oldState, action).form)
    ).not.toContain(deletedTab);
    expect(
      reducers[ActionTypes.DELETETAB](oldState, action).form[0].children
    ).not.toContain(deletedTab);
  });
});
/**
 * deleteEntity(entityId: id, parentId: id)
 */
//!ASK context doesn't seem to be a part of jest API
//@CONSIDER should there be a min of 1 tab
describe('The deleteEntity reducer', () => {
  const oldState = {
    form: {
      '0': { type: EntityTypes.Form, uuid: '0', children: ['1'] },
      '1': { type: EntityTypes.FormSection, uuid: '1', children: ['2', '3'] },
      '2': { type: EntityTypes.TextInput, uuid: '2' },
      '3': { type: EntityTypes.FormSection, uuid: '3' },
    },
  };
  // !ASK are multiple reducers feasable to test
  describe('when passed a non-FormSection entity', () => {
    const action = {
      type: ActionTypes.DELETEENTITY,
      entityId: '2',
      parentId: '1',
    };
    const newState = {
      form: {
        '0': { type: EntityTypes.Form, uuid: '0', children: ['1'] },
        '1': { type: EntityTypes.FormSection, uuid: '1', children: ['3'] },
        '3': { type: EntityTypes.FormSection, uuid: '3' },
      },
    };

    it('returns the updated state without the entity', () => {
      expect(
        Object.keys(reducers[ActionTypes.DELETEENTITY](oldState, action).form)
      ).not.toContain(action.entityId);
      expect(
        reducers[ActionTypes.DELETEENTITY](oldState, action).form[
          action.parentId
        ].children
      ).not.toContain(action.entityId);
    });
  });
  describe('when passed a FormSection entity returns the updated state without the FormSection', () => {
    const oldState = {
      form: {
        '0': { type: EntityTypes.Form, uuid: '0', children: ['1'] },
        '1': { type: EntityTypes.FormSection, uuid: '1', children: ['2', '3'] },
        '2': { type: EntityTypes.TextInput, uuid: '2' },
        '3': { type: EntityTypes.FormSection, uuid: '3' },
      },
    };
    const action = {
      type: 'DELETEENTITY',
      entityId: '1',
      parentId: '0',
    };
    const newState = {
      form: {
        '0': { type: EntityTypes.Form, uuid: '0', children: [] },
      },
    };

    expect(
      Object.keys(reducers[ActionTypes.DELETEENTITY](oldState, action).form)
    ).not.toContain(action.entityId);
    expect(
      reducers[ActionTypes.DELETEENTITY](oldState, action).form[0].children
    ).not.toContain(action.entityId);
    expect(
      Object.keys(reducers[ActionTypes.DELETEENTITY](oldState, action).form)
    ).not.toContain(oldState.form[action.entityId].children);
  });
});

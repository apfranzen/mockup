import { applyMiddleware, compose, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import LogRocket from 'logrocket';

import thunk from 'redux-thunk';

import rootReducer from './reducer';

export default function configureStore(preloadedState) {
  const middlewares = [thunk, LogRocket.reduxMiddleware()];
  const middlewareEnhancer = applyMiddleware(...middlewares);

  const enhancers = [middlewareEnhancer];
  const composedEnhancers = compose(...enhancers);

  const store = createStore(
    rootReducer,
    preloadedState,
    composeWithDevTools(composedEnhancers)
  );


  if (process.env.NODE_ENV !== 'production' && module.hot) {
    module.hot.accept('./reducer', () => store.replaceReducer(rootReducer))
  }
  return store;
}

export const move = (state, action) => {
  const { dragTargetUUID, dropTargetUUID, dragSectionUUID } = action;

  // const direction = dragDistance > 0 ? 1 : -1;

  const dragSection = state[dragSectionUUID];
  const dragTarget = state[dragTargetUUID];
  const dropTarget = state[dropTargetUUID];

  // const getNewPadding = new model.generatePadding({ width: dragTarget.width });
  // const replaceDragTargetWithPadding = () => {
  //   return Object.assign([], [...dragSection.children], {
  //     [dragTargetIndex]: getNewPadding.uuid,
  //     [dropTargetIndex]: dragTarget.uuid,
  //     [dropTargetIndex + 1]: dropTarget.uuid,
  //   });
  // };

  const result = {
    [dragSectionUUID]: {
      ...dragSection,
      // children: replaceDragTargetWithPadding(),
    },
    // [getNewPadding.uuid]: getNewPadding,
    [dropTargetUUID]: {
      ...dropTarget,
      width: dropTarget.width - dragTarget.width,
    },
  };

  return result;
};

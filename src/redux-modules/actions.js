import { utility } from '../lib/utility';
import { dispatch } from 'react-redux';
import axios from 'axios';
import { transformEntities } from '../model/deserializer';
import db from '../db/db.js';
import { serialize } from '../model/serializer';
import qs from 'qs';
import { finalPostHttp } from './httpHelpers';
export const ActionTypes = {
  INITFORM: 'INITFORM',
  SELECTTAB: 'SELECTTAB',
  DELETETAB: 'DELETETAB',
  MOVEENTITYTOTAB: 'MOVEENTITYTOTAB',
  DELETEENTITY: 'DELETEENTITY',
  RESIZEENTITY: 'RESIZEENTITY',
  RESIZEENTITYPROMPT: 'RESIZEENTITYPROMPT',
  MOVEENTITY: 'MOVEENTITY',
  CREATEENTITY: 'CREATEENTITY',
  SELECTENTITY: 'SELECTENTITY',
  LOADFORMFROMSERVER: 'LOADFORMFROMSERVER',
  CLEARFLASH: 'CLEARFLASH',
  CREATETAB: 'CREATETAB',
  REORDERTABS: 'REORDERTABS',
  UPDATEDEPENDENCYCONDITION: 'UPDATEDEPENDENCYCONDITION',
  // unfinalized actions
  INCREMENT: 'INCREMENT',
  TESTLOADFORM: 'TESTLOADFORM',
  LOADFORMSTATE: 'LOADFORMSTATE',
  SETDEVSTATE: 'SETDEVSTATE',
  EXPORTAPPSTATE: 'EXPORTAPPSTATE',
  SETFORMATTACHEDFILE: 'SETFORMATTACHEDFILE',
  SETPROPERTYPANELTAB: 'SETPROPERTYPANELTAB',
  DELETECONDITION: 'DELETECONDITION',
  ADDCONDITION: 'ADDCONDITION',
  SETAUTOID: 'SETAUTOID',
  CUSTOMTABORDER: 'CUSTOMTABORDER',
  BATCHUPDATE: 'BATCHUPDATE',
  RESIZESTART: 'RESIZESTART',
  RESIZEEND: 'RESIZEEND',
  ADDSTART: 'ADDSTART',
  DRAGSTART: 'DRAGSTART',
  ADDEND: 'ADDEND',
  DRAGEND: 'DRAGEND',
  SETDROPTARGET: 'SETDROPTARGET',
  UPDATEPROPERTY: 'UPDATEPROPERTY',
  // flow control
  SERIALIZEFORM: 'SERIALIZEFORM',
  SERVERGETPENDING: 'SERVERGETPENDING',
  SERVERGETSUCCESS: 'SERVERGETSUCCESS',
  SERVERGETFAILURE: 'SERVERGETFAILURE',
  // db actions
  LOADFILES: 'LOADFILES',
  ADDFILES: 'ADDFILES',
  DELETEFILES: 'DELETEFILES',
  UPDATEFILES: 'UPDATEFILES',
  // ui action
  SETHIDESAVEDIALOG: 'SETHIDESAVEDIALOG',
  SETMESSAGE: 'SETMESSAGE',
  // set nested state
  UPDATEMULTIPLE: 'UPDATEMULTIPLE',
  HANDLEAUTONAME: 'HANDLEAUTONAME',
  HANDLEORDER: 'HANDLEORDER',
};

export const apiUrl =
  process.env.NODE_ENV === 'production'
    ? process.env.REACT_APP_PROD_API_URL
    : '';

export const updateMultiple = payload => ({
  type: ActionTypes.UPDATEMULTIPLE,
  payload,
});

export const initForm = payload => ({ type: ActionTypes.INITFORM, payload });

export const selectTab = tabId => ({
  type: ActionTypes.SELECTTAB,
  payload: {
    tabId,
  },
});
export const setHideSaveDialog = status =>
  console.log(status) || {
    type: ActionTypes.SETHIDESAVEDIALOG,
    status,
  };

export const setMessage = message => ({
  type: ActionTypes.SETMESSAGE,
  message,
});

export const createTab = newTab => ({
  type: ActionTypes.CREATETAB,
  payload: {
    newTab,
  },
});

export const deleteTab = tabId => ({
  type: ActionTypes.DELETETAB,
  payload: {
    tabId,
  },
});

export const moveEntityToTab = (entityId, tabId, sourceSetionId) => ({
  type: ActionTypes.MOVEENTITYTOTAB,
  entityId,
  tabId,
  sourceSetionId,
});

export const deleteEntity = (entityId, parentId) => dispatch => {
  dispatch({
    type: ActionTypes.DELETEENTITY,
    entityId,
    parentId,
  });
  dispatch({ type: ActionTypes.HANDLEAUTONAME });
};

export const resizeEntity = (
  entityId,
  targetType,
  initTargetCol,
  targetColDelta,
  initColumn,
  perspective = 'right'
) => ({
  type: 'RESIZEENTITY',
  targetType,
  entityId,
  initTargetCol,
  targetColDelta,
  initColumn,
  perspective,
});

export const moveEntity = (entityId, toNewRow, from, to) => dispatch => {
  dispatch({
    type: ActionTypes.MOVEENTITY,
    payload: {
      entityId,
      toNewRow,
      from,
      to,
    },
  });

  dispatch({ type: ActionTypes.HANDLEAUTONAME });
};

export const createEntity = (
  parentId,
  entityType,
  row,
  column,
  mode,
  config
) => {
  return dispatch => {
    dispatch({
      type: ActionTypes.CREATEENTITY,
      payload: {
        parentId,
        entityType,
        row,
        column,
        mode,
        config,
      },
    });

    dispatch({ type: ActionTypes.HANDLEAUTONAME });
  };
};

export const selectEntity = id => ({
  type: 'SELECTENTITY',
  id,
});

export const serializeForm = (href, saveMode, newName) => {
  // const { href, saveMode, newName } = action;
  const getStudyId = href => {
    return href.split(`/studies/`)[1].split('/')[0];
  };
  const getFormId = href => {
    return href.split('/designs/')[1].split('/')[0];
  };
  const hideDialog = () => {
    if (href.includes('?action=new')) {
      return false;
    } else if (href.includes('forms/designs/')) {
      if (saveMode === 'saveAs') {
        return false;
      } else {
        return true;
      }
    }
  };
  const getUrlParams = parsedForm => {
    console.log(parsedForm);
    // new form, or forking a versioned form
    if (href.includes('?action=new') || !href.includes('designs')) {
      console.log('here');

      return {
        url: `${apiUrl}/studies/${getStudyId(href)}/forms/designs/?action=new`,
        params: {
          formId: '',
          name: newName,
          json: parsedForm,
        },
      };
    } else if (href.includes('forms/designs/')) {
      // updating a form design (replacing)
      console.log('here');
      if (saveMode === 'saveAs') {
        return {
          url: `${apiUrl}/studies/${getStudyId(href)}/forms/designs/${getFormId(
            href
          )}`,
          params: {
            formId: '',
            name: newName,
            json: parsedForm,
          },
        };
      } else {
        console.log('here');
        return {
          url: `${apiUrl}/studies/${getStudyId(href)}/forms/designs/${getFormId(
            href
          )}`,
          params: {
            formId: '',
            // name: 'A2',

            name: newName,
            json: parsedForm,
          },
        };
      }
    }
  };
  const getFiles = async () => {
    return await db.table('files').toArray();
  };
  // getFiles()
  //   .then(files => {
  //     console.log(files);
  //   })
  //   .catch(error => console.log(error));

  return (dispatch, getState) => {
    const {
      form,
      app: {
        cdartStatus: { formName },
      },
    } = getState();
    console.log(hideDialog());
    // dispatch(setHideSaveDialog(hideDialog()));
    db.table('files')
      .toArray()
      .then(files => {
        const formAttachedFiles = utility.arrayToObject(files, 'uuid');
        const parsedForm = serialize(form, formAttachedFiles);
        console.log(parsedForm);
        // dispatch(serializeForm);
        dispatch(serverGetPending());
        console.log(getUrlParams(parsedForm));
        finalPostHttp(getUrlParams(parsedForm))
          .then(function (response) {
            console.log(response.data);
            dispatch(serverGetSuccess());
            dispatch(setMessage(`Form ${formName} Saved Successfully`));
            // pass it a path key and value payload
            /*
            'app.formId': 2647
            */
            console.log('reached');
            dispatch(setHideSaveDialog(true));
            dispatch(updateMultiple([{ 'app.formId': response.data }]));
            return response.data;
          })
          .catch(error => {
            console.log(error);
            dispatch(setMessage('Form Saving Error'));
            dispatch(serverGetFailure());
            return error;
          });
      });
  };
};

export const loadFormFromServer = () => ({
  type: ActionTypes.LOADFORMFROMSERVER,
});

export const clearFlash = () => ({
  type: ActionTypes.CLEARFLASH,
});

export const reorderTabs = (dragTargetId, dropTargetId) => ({
  type: ActionTypes.REORDERTABS,
  dragTargetId,
  dropTargetId,
});

/*
  flow control actions here
*/

export const serverGetPending = text =>
  console.log('serverGetPending') || {
    type: ActionTypes.SERVERGETPENDING,
    text,
  };

export const serverGetSuccess = (formObj, name) => ({
  type: ActionTypes.SERVERGETSUCCESS,
  formObj,
  name,
});

export const serverGetFailure = text => ({
  type: ActionTypes.SERVERGETFAILURE,
  text,
});

/*
  db actions
*/
export const loadFiles = () => {
  return dispatch => {
    console.log('loadTodoes');
    db.table('files')
      .toArray()
      .then(files => {
        dispatch({
          type: ActionTypes.LOADFILES,
          payload: files,
        });
      });
  };
};

export const addFiles = (name, contentType, fileContent, uuid) => {
  return dispatch => {
    const fileToAdd = { name, contentType, fileContent, uuid, done: false };
    db.table('files')
      .add(fileToAdd)
      .then(id => {
        dispatch(loadFiles());
      });
  };
};

export const deleteFiles = id => {
  console.log('deleteFiles');
  return dispatch => {
    db.table('files')
      .delete(id)
      .then(() => {
        console.log('deleteFiles2');
        dispatch(loadFiles());
      });
  };
};

export const updateFiles = (id, updatePropsObj) => {
  return dispatch => {
    db.table('files')
      .update(id, { ...updatePropsObj })
      .then(() => {
        // dispatch({
        //   type: UPDATE_TODO,
        //   payload: { id, done }
        // });
      });
  };
};

/*
  unfinalized actions starting here
 */

export const increment = () =>
  console.log('increment action creator') || {
    type: ActionTypes.INCREMENT,
  };

export const testLoadForm = formState =>
  console.log('test load form') || {
    type: ActionTypes.TESTLOADFORM,
    formState,
  };

export const loadFormState = location => {
  // if (!location) {
  //   return state;
  // } else if (location.href.includes('?action=new')) {
  //   console.log('new');
  //   return state;
  // } else
  db.table('files').clear();
  console.log(location);
  const href = location.href;
  const handleURL = () => {
    if (href.includes('designs')) {
      const studyId = href.split(`/studies/`)[1].split('/')[0];
      console.log(studyId);
      // const versionId = href.split('/versions/')[1].split('/')[0];
      const formId = href.split('/designs/')[1].split('/')[0];
      console.log(formId);

      return `${apiUrl}/studies/${studyId}/forms/designs/${formId}`;
    } else {
      // versioned form
      console.log('here');

      // console.log(href.split('CDART2/')[1]);
      // studies/13/api/forms/311/versions/694
      return `${apiUrl}/${href.split('CDART2/')[1]}`;
    }
  };
  const getStudyId = url => {
    return url.split('/studies/')[1].split('/')[0];
  };

  const getVersion = url => {
    if (href.includes('designs')) {
      return url.split('/versions/')[1].split('/')[0];
    } else {
      return;
    }
  };

  if (
    !location.href.includes('/?action=new') &&
    // need to include case of including forms in case there is no path after domain name
    location.href.includes('forms')
  ) {
    // return state;
    return dispatch => {
      console.log('here');

      dispatch(serverGetPending());
      dispatch(setMessage('Loading form...'));
      axios({
        method: 'get',

        url: handleURL(),
        // headers: { 'Content-Type': 'multipart/form-data' },

        auth: {

        },
        withCredentials: true,
      })
        .then(function (response) {
          // console.log(
          //   JSON.parse(
          //     '{"json-version":"1.0","content":{"type":"Form","allowEventAttachedFiles":false,"autoIds":{"prefix":"FORM","enabled":false,"separator":null},"existingName":"dd","existingId":null,"entities":[{"type":"FormSection","legend":"Tab","layout":{"width":24},"entities":[{"doubleEntry":false,"maxLength":8000,"defaultText":"","autoNumberRule":"N ","inputWidth":6,"type":"TextInput","index":"FORM35076510","tabOrder":1,"sasCodeLabel":"","autoTab":false,"name":"","cloneId":0,"inputType":{"dataType":"<st></st>ring"},"prompts":{"pre":{"prompt":"name","promptPreWidth":3},"post":{"prompt":"","promptPostWidth":2}},"externalIdentifier":"FORM71931810","layout":{"width":11,"prepend":2,"append":2}}]}],"metadata":{"majorVersion":1,"minorVersion":1,"topLevelSections":null},"formAttachedFiles":{},"QbyQ":{}}}'
          //   )
          // );

          const versionedForm = !href.includes('designs');
          const formName = versionedForm
            ? response.data.content.crf.name
            : response.data.name;

          var formDataFromCdart = href.includes('designs')
            ? JSON.parse(response.data.structure.replace(/'/g, ''))
            : response.data;
          // console.log(JSON.parse(formDataFromCdart));
          const parsedForm = transformEntities(formDataFromCdart);
          console.log(parsedForm);

          const getFirstTab = Object.values(parsedForm).filter(
            entity => entity.type === 'FormSection'
          )[0].uuid;
          dispatch(serverGetSuccess(parsedForm));
          // dispatch(updateMultiple([{ 'app.cdartStatus.formName': 'success' }]));
          dispatch(
            updateMultiple([
              { form: parsedForm },
              { 'app.activeTab': getFirstTab },
              { 'app.cdartStatus.formName': formName },
            ])
          );
          dispatch(setMessage(`Successfully Loaded Form Design: ${formName}`));
        })
        .catch(function (error) {
          console.log('serverGetFailure');
          dispatch(serverGetFailure());
          dispatch(setMessage(error.message));
        });
    };
  } else if (location.href.includes('/versions/')) {
    const getFormId = url => {
      return url.split('/forms/')[1].split('/')[0];
    };
    // accesing a versioned form
    return dispatch =>
      axios({
        method: 'get',
        url: `${apiUrl}/studies/${getStudyId(href)}/forms/${getFormId(
          href
        )}/versions/${getVersion(href)}`,
        // headers: { 'Content-Type': 'multipart/form-data' },

        auth: {

        },
        withCredentials: true,
      })
        .then(response => {
          dispatch(increment());
          const parsedForm = transformEntities(response.data);

          const getFirstTab = Object.values(parsedForm).filter(
            entity => entity.type === 'FormSection'
          )[0];

          dispatch(serverGetSuccess(parsedForm));
        })
        .catch(error => {
          console.log(error);
          console.log('serverGetFailure');

          dispatch(serverGetFailure());
        });
  } else {
    console.log('none');

    return { type: ActionTypes.LOADFORMSTATE };
  }
};

export const postHttp = (
  { url, params },
  successMessage,
  errorMessage,
  successActions,
  failureActions
) =>
// ({
//   type: ActionTypes.POSTHTTP,
//   url,
//   payload,
// });
{
  console.log(
    url,
    params,
    successMessage,
    errorMessage,
    successActions,
    failureActions
  );
  const setFormData = params => {
    const { json, name, formId } = params;
    var formData = new FormData();
    formData.set('name', name);
    formData.set('formId', '');
    formData.set('json', JSON.stringify(json));
    return formData;
  };
  console.log(setFormData(params));
  return dispatch =>
    axios({
      url,
      method: 'post',
      data: setFormData(params),
      headers: {
        'content-type': `multipart/form-data;`,
        // 'content-type': `multipart/form-data; boundary=${formData._boundary}`,
      },
      // paramsSerializer: params => {
      //   console.log('params');
      //   return qs.stringify(
      //     'params'
      //     // { arrayFormat: 'brackets' }
      //   );
      // },
      auth: {

      },
      withCredentials: true,
    })
      .then(function (response) {
        // clear saveDialog in redux
        console.log('success');
        console.log(successActions);
        dispatch(setMessage('successMessage'));
        successActions.forEach(action => dispatch(action()));
        // dispatch(successActions());
        console.log(response.data);
      })
      .catch(function (error) {
        console.log(error);
        // dispatch(setMessage(errorMessage));
        console.log('serverGetFailure');
        failureActions.forEach(action => dispatch(action()));
        // dispatch(serverGetFailure());
      });
};

export const setDevState = serializedState => ({
  type: ActionTypes.SETDEVSTATE,
  serializedState,
});

export const exportAppState = () => ({
  type: ActionTypes.EXPORTAPPSTATE,
});

export const setFormAttachedFile = fileObj => ({
  type: ActionTypes.SETFORMATTACHEDFILE,
  payload: {
    fileObj,
  },
});

export const setPropertyPanelTab = tab => ({
  type: ActionTypes.SETPROPERTYPANELTAB,
  payload: {
    tab,
  },
});

export const deleteCondition = (id, pathToEntity, mode) => ({
  type: ActionTypes.DELETECONDITION,
  payload: {
    id,
    pathToEntity,
    mode,
  },
});

export const addCondition = (id, pathToParentCondition, properties, mode) => ({
  type: ActionTypes.ADDCONDITION,
  payload: {
    id,
    pathToParentCondition,
    properties,
    mode,
  },
});

export const updateDependencyCondition = (
  id,
  pathToCondition,
  updatedProperties,
  mode
) => ({
  type: ActionTypes.UPDATEDEPENDENCYCONDITION,
  payload: {
    id,
    pathToCondition,
    updatedProperties,
    mode,
  },
});

export const setAutoId = updatedValue => ({
  type: 'SETAUTOID',
  updatedValue,
});
export const customTabOrder = customTabOrder =>
  console.log({ customTabOrder }) || {
    type: 'CUSTOMTABORDER',
    customTabOrder,
  };

export const batchUpdate = inputs => ({
  type: 'BATCHUPDATE',
  inputs,
});
export const resizeStart = (targetMetaData, uuid) => ({
  type: 'RESIZESTART',
  targetMetaData,
  uuid,
});

export const resizeEnd = uuid => ({ type: 'RESIZEEND', uuid });

export const addStart = entity => ({
  type: 'ADDSTART',
  entity: entity,
});

export const addEnd = entity => ({ type: 'ADDEND', entity });

export const dragStart = (targetUUID, sectionUUID, metaData) => ({
  type: 'DRAGSTART',
  targetUUID,
  sectionUUID,
  metaData,
});

export const dragEnd = targetUUID => ({
  type: 'DRAGEND',
  targetUUID,
});

export const setDropTarget = (targetUUID, sectionUUID, metaData) => ({
  type: 'SETDROPTARGET',
  targetUUID,
  sectionUUID,
  metaData,
});

export const drop = (targetUUID, sectionUUID, metaData) => (
  dispatch,
  getState
) => {
  const {
    app: appState,
    form: {
      byId: {
        [sectionUUID]: { children: siblings },
        [targetUUID]: { type: targetType },
      },
    },
  } = getState();

  const dragDistance = utility.round(
    (metaData.screenX - appState.isDragging.metaData.screenX) /
    appState.gridWidth,
    0
  );

  dispatch(setDropTarget(targetUUID, sectionUUID, metaData));

  if (
    ((siblings.indexOf(appState.isDragging.targetUUID) ===
      siblings.indexOf(targetUUID) - 1 ||
      siblings.indexOf(appState.isDragging.targetUUID) ===
      siblings.indexOf(targetUUID) + 1) && // are the drag target and drop target next to one another?
      targetType === 'Padding') || // is this a padding?
    targetUUID === appState.isDragging.targetUUID
  ) {
    dispatch(
      reformat(
        targetUUID,
        appState.isDragging.targetUUID,
        sectionUUID,
        dragDistance
      )
    );
  } else if (targetType === 'Padding') {
    dispatch(
      move(
        appState.isDragging.targetUUID,
        targetUUID,
        sectionUUID,
        appState.isDragging.sectionUUID
      )
    );
  }
};

export const reformat = (
  dropTargetUUID,
  dragTargetUUID,
  sectionUUID,
  dragDistance
) => ({
  type: 'REFORMAT',
  dropTargetUUID,
  dragTargetUUID,
  sectionUUID,
  dragDistance,
});

export const move = (
  dragTargetUUID,
  dropTargetUUID,
  dropSectionUUID,
  dragSectionUUID
) => ({
  type: 'MOVE',
  dragTargetUUID,
  dropTargetUUID,
  dropSectionUUID,
  dragSectionUUID,
});

export const updateProperty = (id, updatedProperty) => ({
  type: ActionTypes.UPDATEPROPERTY,
  payload: { id, updatedProperty },
});

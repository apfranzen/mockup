react-dnd

- items: plain js object that describes what is being dragged. { fromCell: 'C5', piece: 'queen' }
  The types let you specify which drag sources and drop targets are compatible.
- monitors: tiny wrappers over components to expose state

```
function collect(monitor) {
  return {
    highlighted: monitor.canDrop(),
    hovered: monitor.isOver()
  };
}
```

- connectors: backend handles DOM events, components use react state. Connectors let you assign one of the predefined roles (drag source, drag preview, drop target) to the DOM nodes

canDrop - assuming the Type of that drop is allowed, add additional logic if this drop should be allowed here

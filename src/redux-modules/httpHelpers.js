import axios from 'axios';

export const finalPostHttp = async ({ url, params }) => {
  const setFormData = params => {
    const { json, name, formId } = params;
    var formData = new FormData();
    formData.set('name', name);
    formData.set('formId', '');
    formData.set('json', JSON.stringify(json));
    return formData;
  };
  return await axios({
    url,
    method: 'post',
    data: setFormData(params),
    headers: {
      'content-type': `multipart/form-data;`,
      // 'content-type': `multipart/form-data; boundary=${formData._boundary}`,
    },
    // paramsSerializer: params => {
    //   console.log('params');
    //   return qs.stringify(
    //     'params'
    //     // { arrayFormat: 'brackets' }
    //   );
    // },
    auth: {

    },
    withCredentials: true,
  });
};

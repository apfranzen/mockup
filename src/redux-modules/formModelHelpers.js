import { EntityTypes } from '../model/Types';
import * as model from '../model/FormEntities';
import { utility } from '../lib/utility';
export const generateEntity = (entityType, config) => {
  const types = {
    [EntityTypes.FormSection]: model.generateFormSection(config),
    [EntityTypes.TextArea]: model.generateTextArea(config),
    [EntityTypes.Checkbox]: model.generateCheckbox(config),
    [EntityTypes.TextInput]: model.generateTextInput(config),
    [EntityTypes.SelectionInput]: model.generateSelectionInput(config),
    [EntityTypes.TextBlock]: model.generateTextBlock(config),
    [EntityTypes.ImageBlock]: model.generateImageBlock(config),
    [EntityTypes.ASTextInput]: model.generateASTextInput(config),
    [EntityTypes.EchoInput]: model.generateEchoInput(config),
    [EntityTypes.CDSTextInput]: model.generateCDSTextInput(config),
    [EntityTypes.RandomizationInput]: model.generateRandomizationInput(config),
  };

  return types[entityType];
};

/**
 * @param {Object} formState
 * @param {string} parentId
 * @param {number} row
 * @returns Array<ids>
 */
export const getEntitiesInRow = (formState, parentId, row) =>
  formState[parentId].children
    .map(childId => (formState[childId].row === row ? formState[childId] : ''))
    .filter(item => item !== '');

/**
 *
 * @param {Object} entityA - Proposed entity to insert
 * @param {Object} entityB - entity to compare against entityA
 */
export const doEntitiesOverlap = entityA => entityB => {
  const entityEndColumn = entity => entity.column + utility.total(entity);

  return (
    // L
    (entityA.column >= entityB.column &&
      entityA.column < entityEndColumn(entityB) &&
      entityA.uuid !== entityB.uuid) || // F
    (entityEndColumn(entityA) > entityB.column &&
      entityEndColumn(entityA) <= entityEndColumn(entityB) &&
      entityA.uuid !== entityB.uuid) || // O
    (entityA.column < entityB.column &&
      entityEndColumn(entityA) > entityEndColumn(entityB))
  );
};

export const exceedParentWidth = (proposedEntity, parentId, formState) => {
  return (
    utility.total(proposedEntity) - 1 + proposedEntity.column >
      formState[parentId].width || proposedEntity.column < 1
  );
};

// FOIL
export const isExceptionEntityInsertation = (
  formState,
  parentId,
  proposedEntity
) => {
  const entitiesInRow = getEntitiesInRow(
    formState,
    parentId,
    proposedEntity.row
  );

  const isOverlapping = entitiesInRow.some(value =>
    doEntitiesOverlap(proposedEntity)(value)
  );

  const isOutsideParent = exceedParentWidth(
    proposedEntity,
    parentId,
    formState
  );

  return [isOverlapping, isOutsideParent].includes(true);
};
